#include "Generator.h"
#include <math.h>
#include <fstream>
#include <time.h>
#include <iostream>


//void Generator::NonHierarchicalLevelGenerationWithConstraints(string filename){
    
//}


int Generator::GenerateTopLevelTile(vector<char> PossibleTiles, int depth, int h, int w, int offset, vector< vector< int> > Method, int WhichDepMat){
    int id = -1;
    double selector;
    
    vector<char> PT(PossibleTiles.size());	//create a copy of the possible tiles vector to use for changes
    vector<float> Probs;
    for (int i = 0; i<PossibleTiles.size(); i++)
        PT[i] = PossibleTiles[i];
    
    while (!PT.empty()){		//while there is still a possible tile
        int ID = 0;
        int P = 0;
        int type=0;
        //check if this state has been observed before
        for (int i = 0; i<Config.DependencyMatrices[WhichDepMat].size(); i++){
            for (int j =0; j <Config.DependencyMatrices[WhichDepMat][i].size(); j++){
                if (this->Config.DependencyMatrices[WhichDepMat][i][j] == 1){
                    
                    if (((h + i - 2) < 0) || ((h + i - 2) >= Map.size()) || ((w + j - 2) < 0)){
                        type=START;
                    } else{
                        for (int b = 0; b < TileTypes.size(); b++){
                            if (TileTypes.at(b) == Map[h + i - 2][w + j - 2]){
                                type = b;
                                break;
                            }
                        }
                    }
                    ID += type*pow(TileTypes.size(), P);
                    P++;
                }
            }
        }
        
        bool Obs = false;
        //check if the state has been observed before
        for (int a = 0; a < PT.size(); a++){
            for (int x = 0; x < this->TileTypes.size(); x++){
                if (PT.at(a) == this->TileTypes.at(x)){
                    if (this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x] != 0){
                        Obs = true;
                        x = (int)this->TileTypes.size();
                        a = (int)PT.size();
                    }
                }
            }
        }
        
        if (!Obs && depth != this->Config.LookAhead){		//if this state hasnt been observed and this is NOT the top level in the lookahead
            this->BackTracks++;                             //then return an error
            return 99;
        }
        else if (!Obs && depth == this->Config.LookAhead){	//if we havent observed this state, and it IS the first step, then break out of
            break;                                          //of the loop, and we will fallback to a simpler network structure
        }
        else if (Obs){                                      //if we have observed this state, then generate the next tile
            float Norm = 0;
            Probs.resize(PT.size());
            for (int i = 0; i< PT.size(); i++)	//reset the value of Probs
                Probs[i] = 0;
            
            for (int a = 0; a < PT.size(); a++){
                for (int x = 0; x < this->TileTypes.size(); x++){
                    if (PT.at(a) == this->TileTypes.at(x)){
                        Probs[a] = this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x];
                        Norm += this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x];
                    }
                }
            }
            for (int i = 0; i<PT.size(); i++){						//renormalize the probabilities
                Probs[i] = Probs[i] / Norm;
            }
            
            //PICK THE TILE
            double PR = 0;
            int tracker = 0;
            selector = ((double)rand()/RAND_MAX);
            //cout<<selector<<endl;
            for (int a = 0; a<PT.size(); a++){
                PR = PR + Probs[a];
                if (Probs[a]>0)
                    tracker = a;
                if (selector <= PR){
                    id = a;
                    break;
                }
            }
            
            if(id==-1){
                cout<<"Broken"<<endl;
            }
            
            if (h < Map.size() && w < Map.at(0).size())
                Map[h][w] = PT[id];
            
            //call the GenTile function to generate the next tile
            //set to a test variable, so we can check the validity
            //of the next state
            int test = 1;
            
            if (depth > 0){
                if (w < this->LengthOfLevel-1)
                    test = GenerateTopLevelTile(TileTypes, depth - 1, h, w+1, offset, Method, WhichDepMat);
            }
            //if the next state isnt valid, remove the tile chosen in this state and try a different one
            if (test == 99){
                int j = 0;
                if (id < PT.size()){
                    for (vector<char>::iterator i = PT.begin(); i != PT.end(); i++){
                        if (PT.at(j) == PT.at(id)){
                            PT.erase(i);
                            break;
                        }
                        j++;
                    }
                } else
                    PT.clear();
            } else{
                for (int i = 0; i < this->TileTypes.size(); i++){
                    if (PT[id] == this->TileTypes[i])
                        return i;
                }
            }
        }
    }									//end of loop
    
    if (depth == this->Config.LookAhead){				//if no valid options on top level, return error code 100
        return 100;
    }
    else{
        this->BackTracks++;
        return 99;				//if no valid options on sub level, return error.
    }							//forces a different choice on upper levels.
}


void Generator::GenerateTopLevelColumns(string filename){
    START = 0;
    
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(TileTypes.size()-1);
    for (int i = 0; i < this->TileTypes.size()-1; i++){
        PossibleTiles[i]=TileTypes[i+1];
        //cout<<PossibleTiles[i]<<" ";
    }
    //    cout<<endl;
    
    //initialize the map to be generated
    this->Map.resize(this->HeightOfLevel);
    for (int i = 0; i < this->HeightOfLevel; i++){
        this->Map[i].resize(this->LengthOfLevel);
        for (int j = 0; j < this->LengthOfLevel; j++){
            this->Map[i][j] = 'X';
        }
    }
    
    int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
    if(rowsPerSplit == 0)
        rowsPerSplit =1;
    for (int j = 0; j <Map[0].size(); j++){
        for (int h=(int)Map.size()-1; h>=0; h--){
            int currRow = h/rowsPerSplit;
            if(currRow >= Config.Rowsplits)
                currRow = Config.Rowsplits-1;
            //try to generate a tile with the first config matrix
            int id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[0], 0);
            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100){
                this->NumberTilesWithEachMatrix[0]++;
            }
            
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[WhichDepMat], WhichDepMat);
                //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                    WhichDepMat++;
            }
            
            //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                while (id == 100 || (this->TileTypes[id] == START)){
                    id = (rand() % (this->TileTypes.size()-1)) + 1;
                }
            }
            Map[h][j] = this->TileTypes.at(id);
        }
    }
    //open the file to print the map into
    ofstream GMap;
    GMap.open(filename.c_str());
    for (int i = 0; i < this->Map.size(); i++){
        for (int j = 0; j < this->Map.at(0).size(); j++)
            GMap << this->Map[i][j];
        GMap << endl;
    }
}

void Generator::GenerateTopLevel(string filename){
    
    START = 0;
    
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(TileTypes.size()-1);
    for (int i = 0; i < this->TileTypes.size()-1; i++){
        PossibleTiles[i]=TileTypes[i+1];
        //cout<<PossibleTiles[i]<<" ";
    }
//    cout<<endl;
    
    //initialize the map to be generated
    this->Map.resize(this->HeightOfLevel);
    for (int i = 0; i < this->HeightOfLevel; i++){
        this->Map[i].resize(this->LengthOfLevel);
        for (int j = 0; j < this->LengthOfLevel; j++){
            this->Map[i][j] = 'X';
        }
    }
    
    int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
    if(rowsPerSplit == 0)
        rowsPerSplit =1;
    for (int h=(int)Map.size()-1; h>=0; h--){
        int currRow = h/rowsPerSplit;
        if(currRow >= Config.Rowsplits)
            currRow = Config.Rowsplits-1;
        for (int j = 0; j <Map[h].size(); j++){
            
            //try to generate a tile with the first config matrix
            int id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[0], 0);
            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100){
                this->NumberTilesWithEachMatrix[0]++;
            }
            
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[WhichDepMat], WhichDepMat);
                //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                WhichDepMat++;
            }
            
            //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                while (id == 100 || (this->TileTypes[id] == START)){
                    id = (rand() % (this->TileTypes.size()-1)) + 1;
                }
            }
            Map[h][j] = this->TileTypes.at(id);
        }
    }
    //open the file to print the map into
    ofstream GMap;
    GMap.open(filename.c_str());
    for (int i = 0; i < this->Map.size(); i++){
        for (int j = 0; j < this->Map.at(0).size(); j++)
        GMap << this->Map[i][j];
        GMap << endl;
    }
}

int Generator::GenerateBottomLevelTile(vector<char> PossibleTiles, int depth, int h, int w, vector< vector< int> > Method, vector< vector< char> > HierMap, int WhichDepMat, bool manualConversion){
    int id = -1;
    double selector;
    int type =-1;
    vector<char> PT;	//create a copy of the possible tiles vector to use for changes
    vector<float> Probs;
    Probs.resize(PT.size());
    for (int i = 0; i<PossibleTiles.size(); i++){
        PT.push_back(PossibleTiles.at(i));
    }
    
    
    while (!PT.empty()){		//while there is still a possible tile
        int ID = 0;
        int P = 0;
        //check if this state has been observed before
        for (int k = 0; k<5; k++){
            for (int m = 2; m >=0; m--){
                if (Method[k][m] == 1){
                    if ((h + (k - 2) < 0) || (h + (k - 2) >= this->HeightOfLevel) || (w + (m - 2) < 0))
                        type = START;
                    else{
                        for (int b = 0; b < this->TileTypes.size(); b++){
                            if (Map[h + (k - 2)][w + (m - 2)] == this->TileTypes.at(b))
                            type = b;
                        }
                    }
                    ID += type*pow(this->TileTypes.size(), P);
                    P++;
                }
            }
        }
        
        bool Obs = false;
        
        int HierTile = 0;
        for (int i = 0; i < this->HierTileTypes.size(); i++){
            if (this->HierTileTypes[i] == HierMap[h / this->Config.height][w / this->Config.width]){
                HierTile = i;
                break;
            }
        }
        //deals with the fact that the last row of high level tiles only accounts for 1 row of low-level tiles
        if(manualConversion){
            if((this->HierTileTypes[HierTile] == 'G' || this->HierTileTypes[HierTile] == 'A') && h<(this->HeightOfLevel-1)){
                HierTile = HierMap[ceil((h/this->Config.height))][w/this->Config.width]-1;
                
                for (int i = 0; i < this->HierTileTypes.size(); i++){
                    if (this->HierTileTypes[i] == HierMap[(h / this->Config.height)][w / this->Config.width]){
                        HierTile = i;
                        break;
                    }
                }
            }
        }
        
        //check if the state has been observed before
        for (int a = 0; a < PT.size(); a++){
            for (int x = 0; x < this->TileTypes.size(); x++){
                if (PT.at(a) == this->TileTypes.at(x)){
                    try{
                        this->HierProbabilities[WhichDepMat][HierTile][ID][x];
                    } catch (const std::out_of_range& oor){
                        cout<<WhichDepMat<<" "<<HierTile<<" "<<ID<<" "<<x<<endl;
                    }
                    if (this->HierProbabilities[WhichDepMat][HierTile][ID][x] != 0){
                        Obs = true;
                        x = (int)this->TileTypes.size();
                        a = (int)PT.size();
                    }
                }
            }
        }
        
        if (!Obs && depth != this->Config.LookAhead){		//if this state hasnt been observed and this is NOT the top level in the lookahead
            this->BackTracks++;				//then return an error
            return 99;
        }
        else if (!Obs && depth == this->Config.LookAhead){	//if we havent observed this state, and it IS the first step, then break out of
            break;								//of the loop, where we will return a random tile
        }
        else if (Obs){					//if we have observed this state, then generate the next tile
            float Norm = 0;
            Probs.resize(PT.size());
            for (int i = 0; i< PT.size(); i++)	//reset the value of Probs
            Probs[i] = 0;
            
            for (int a = 0; a < PT.size(); a++){
                //cout<<PT[a]<<" ";
                
                for (int x = 0; x < this->TileTypes.size(); x++){
                    if (PT.at(a) == this->TileTypes.at(x)){
                        Probs[a] = this->HierProbabilities[WhichDepMat][HierTile][ID][x];
                        Norm += this->HierProbabilities[WhichDepMat][HierTile][ID][x];
                    }
                }
            }
//            cout<<endl;
            for (int i = 0; i<PT.size(); i++){						//renormalize the probabilities
                Probs[i] = Probs[i] / Norm;
            }
            
            //PICK THE TILE
            double PR = 0;
            int tracker = 1;
            selector = ((double)rand()/RAND_MAX);
            //cout<<selector<<": "<<PT.size()<<" ";
            for (int a = 0; a<PT.size(); a++){
                PR = PR + Probs[a];
                //cout<<PR<<" ";
                if (Probs[a]>0)
                    tracker = a;
                if (selector <= PR){
                    id = a;
                    //cout<<a<<" "<<PT[a]<<endl;
                    break;
                }
            }
//            if (/*a >= PT.size() - 1) &&*/ id == -1){
//                id = tracker;
//                cout<<"Broken"<<endl;
//            }
            if (h < Map.size() && w < Map.at(0).size()){
                Map[h][w] = PT[id];
            }
            
            //call the GenTile function to generate the next tile
            //set to a test variable, so we can check the validity
            //of the next state
            int test = 1;
            
            if (depth > 0){
                if (w < this->LengthOfLevel - 1)
                test = GenerateBottomLevelTile(TileTypes, depth - 1, h, w + 1, Method, HierMap, WhichDepMat, manualConversion);
            }
            //if the next state isnt valid, remove the tile chosen in this state and try a different one
            if (test == 99){
                int j = 0;
                if (id < PT.size()){
                    for (vector<char>::iterator i = PT.begin(); i != PT.end(); i++){
                        if (PT.at(j) == PT.at(id)){
                            PT.erase(i);
                            break;
                        }
                        j++;
                    }
                }
                else
                PT.clear();
            }
            else{
                for (int i = 0; i < this->TileTypes.size(); i++){
                    if (PT[id] == this->TileTypes[i])
                    return i;
                }
            }
        }
    }									//end of loop
    
    if (depth == this->Config.LookAhead){				//if no valid options on top level, return error code 100
        return 100;
    }
    else{
        this->BackTracks++;
        return 99;				//if no valid options on sub level, return error.
    }							//forces a different choice on upper levels.
    
}

void Generator::GenerateBottomLevel(string filename, string HierMapFile, bool  manualConversion){
    
    START =0;
    
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(TileTypes.size()-1);
    for (int i = 0; i < this->TileTypes.size()-1; i++){
        PossibleTiles[i]=TileTypes[i+1];
    //    cout<<PossibleTiles[i]<<" ";
    }
    //cout<<endl;
    
    //initialize the map to be generated
    this->Map.resize(this->HeightOfLevel);
    for (int i = 0; i < this->HeightOfLevel; i++){
        this->Map[i].resize(this->LengthOfLevel);
        for (int j = 0; j < this->LengthOfLevel; j++){
            this->Map[i][j] = 'X';
        }
    }
    
    //read in the current HighLevel map
    vector< vector<char > > HierMap;
    vector<char> tmp;
    ifstream Hier;
    Hier.open(HierMapFile.c_str());
    string line = "";
    while (getline(Hier, line)){
        for (int i = 0; i < line.size(); i++)
            tmp.push_back(line.at(i));
        HierMap.push_back(tmp);
        tmp.clear();
    }
    
    for (int i = this->HeightOfLevel-1; i >= 0; i--){
        for (int j = 0; j < this->LengthOfLevel; j++){
            //try to generate a tile with the first config matrix
            int id = GenerateBottomLevelTile(PossibleTiles, this->Config.LookAhead, i, j, this->Config.DependencyMatrices[0], HierMap, 0,  manualConversion);
            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100){
                this->NumberTilesWithEachMatrix[0]++;
            }
            
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                id = GenerateBottomLevelTile(PossibleTiles, this->Config.LookAhead, i, j, this->Config.DependencyMatrices[WhichDepMat], HierMap, WhichDepMat,  manualConversion);
                //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                WhichDepMat++;
            }
            
            //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                id = rand() % this->TileTypes.size() - 1;
                //ensure that the Start tile is not selected
                id++;
            }
            Map[i][j] = this->TileTypes.at(id);
            //cout<<id;
        }
        //cout<<endl;
    }
    
    //open the file to print the map into
    ofstream GMap;
    GMap.open(filename.c_str());
    for (int i = 0; i < this->Map.size(); i++){
        for (int j = 0; j < this->Map.at(0).size(); j++){
            GMap << this->Map[i][j];
        }
        GMap << endl;
    }
    
}

void Generator::CopyProbs(string ProbsFile, bool Hier, int WhichDepMat){
    
    ifstream Probs, FBProbs;
    Probs.open(ProbsFile.c_str());
    
    if (!Hier){
        float P;
        int index = 0;
        vector< float > Tmp;
        
        while (Probs >> P){
            Tmp.push_back(P);
            index++;
            if (index == this->TileTypes.size()){
                index = 0;
                this->Probabilities[WhichDepMat].push_back(Tmp);
                Tmp.clear();
            }
        }
        Probs.close();
        
        Tmp.clear();
    }
    else{
        float P;
        int index = 0;
        int innerIndex = 0;
        vector< float > Tmp;
        vector< vector<float> > TMP;
        
        while (Probs >> P){
            Tmp.push_back(P);
            index++;
            if (index == this->TileTypes.size()){
                index = 0;
                TMP.push_back(Tmp);
                Tmp.clear();
                innerIndex++;
                if (innerIndex == this->Config.Rows[WhichDepMat]){
                    this->HierProbabilities[WhichDepMat].push_back(TMP);
                    innerIndex = 0;
                    TMP.clear();
                }
            }
        }
        Probs.close();
        Tmp.clear();
    }
}

void Generator::CopyConfig(Configuration ToCopy){
    
    Config.DependencyMatrices.resize(ToCopy.DependencyMatrices.size());
    Config.DepComplexities.resize(ToCopy.DepComplexities.size());
    Config.Rows.resize(ToCopy.Rows.size());
    
    for (int i = 0; i < ToCopy.NumberMatrices; i++){
        
        Config.DependencyMatrices[i]=ToCopy.DependencyMatrices[i];
        Config.DepComplexities[i]=ToCopy.DepComplexities[i];
        Config.Rows[i]=ToCopy.Rows[i];
        Config.NumberMatrices++;
    }
    
    this->Config.height = ToCopy.height;
    this->Config.Hierarchical = ToCopy.Hierarchical;
    this->Config.LookAhead = ToCopy.LookAhead;
    this->Config.NumberTilesTypes = ToCopy.NumberTilesTypes;
    this->Config.Rowsplits = ToCopy.Rowsplits;
    this->Config.SizeOfArray = ToCopy.SizeOfArray;
    this->Config.TopDown = ToCopy.TopDown;
    this->Config.width = ToCopy.width;
    
}

Generator::Generator(int DesiredLength, int DesiredHeight){
    LengthOfLevel = DesiredLength;
    HeightOfLevel = DesiredHeight;
    BackTracks = 0;
    Config.NumberMatrices =0;
    srand((unsigned)time(NULL));
}

void Generator::SetTileTypes(vector<char> Types, bool Hier){
    
    if (!Hier){
        for (int i = 0; i < Types.size(); i++)
        this->TileTypes.push_back(Types.at(i));
    }
    else{
        for (int i = 0; i < Types.size(); i++)
        this->HierTileTypes.push_back(Types.at(i));
        
        this->START = 0;
    }
}

void Generator::SetLevelHeight(string SampleMap){
    
    ifstream Sample;
    Sample.open(SampleMap.c_str());
    string line = "";
    int H = 0;
    while (getline(Sample, line))
    H++;
    
    this->HeightOfLevel = H;
}

void Generator::ResizeVectors(bool Hier){
    
    this->Probabilities.resize(this->Config.NumberMatrices);
    if (Hier){
        this->HierProbabilities.resize(this->Config.NumberMatrices);
    }
    
    for (int i = 0; i < this->Config.NumberMatrices; i++){
        this->NumberTilesWithEachMatrix.push_back(0);
    }
}


void Generator::RegenerateTopLevelSection(string output, int xStart, int yStart, int xEnd, int yEnd){
    
    START = 0;
    
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(TileTypes.size()-1);
    for (int i = 0; i < this->TileTypes.size()-1; i++)
        PossibleTiles[i]=TileTypes[i+1];
    
    //initialize the section of the map to be generated
    
    //0 is the top of the map, therefore the start height
    //of the section to be sampled will be a larger number
    //than the end height
    
    for(int h=yStart; h<=yEnd; h++){
        for (int w=xStart; w<xEnd; w++) {
            Map[h][w] = 'X';
        }
    }
    
    //make a copy fo the map so that the lookahead does not destroy the
    //sections fo the map that has already been sampled
    vector<vector< char> > tmpMap;
    for(int i=0; i<Map.size(); i++){
        vector<char> tmpRow;
        for (int j=0; j<Map[i].size(); j++) {
            tmpRow.push_back(Map[i][j]);
        }
        tmpMap.push_back(tmpRow);
        tmpRow.clear();
    }
    
    int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
    if(rowsPerSplit == 0)
        rowsPerSplit =1;
    for (int h=yEnd; h>=yStart; h--){
        int currRow = h/rowsPerSplit;
        if(currRow >= Config.Rowsplits)
            currRow = Config.Rowsplits-1;
        for (int j = xStart; j <xEnd; j++){
            
            //try to generate a tile with the first config matrix
            int id = RegenerateTopLevelTile(tmpMap, PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[0], 0);
            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100){
                this->NumberTilesWithEachMatrix[0]++;
            }
            
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                id = RegenerateTopLevelTile(tmpMap, PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[WhichDepMat], WhichDepMat);
                //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                WhichDepMat++;
            }
            
            //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                id = (rand() % (this->TileTypes.size()-1)) + 1;
                Map[h][j] = TileTypes[id];
                tmpMap[h][j] = TileTypes[id];
            } else if(id == 1 || id == 2)  //copy over the sampled tile from the temporary map
                Map[h][j] = tmpMap[h][j];
        }
    }
    //open the file to print the map into
    ofstream GMap;
    GMap.open(output.c_str());
    for (int i = 0; i < this->Map.size(); i++){
        for (int j = 0; j < this->Map.at(0).size(); j++)
        GMap << this->Map[i][j];
        GMap << endl;
    }
}

int Generator::RegenerateTopLevelSectionWithSmoothing(string output, int xStart, int yStart, int xEnd, int yEnd){
    
    START = 0;
    //cout<<"In resampling with smoothing"<<endl;
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(TileTypes.size()-1);
    for (int i = 0; i < this->TileTypes.size()-1; i++)
        PossibleTiles[i]=TileTypes[i+1];
    
    //initialize the section of the map to be generated
    
    //0 is the top of the map, therefore the start height
    //of the section to be sampled will be a larger number
    //than the end height
    
    //cout<<xStart<<" "<<yStart<<" "<<xEnd<<" "<<yEnd<<endl;
    
    for(int h=yStart; h<=yEnd; h++){
        for (int w=xStart; w<xEnd; w++) {
            Map[h][w] = 'X';
        }
    }
    //cout<<"After map rewrote"<<endl;
    //make a copy fo the map so that the lookahead does not destroy the
    //sections fo the map that has already been sampled
    vector<vector< char> > tmpMap;
    for(int i=0; i<Map.size(); i++){
        vector<char> tmpRow;
        for (int j=0; j<Map[i].size(); j++) {
            tmpRow.push_back(Map[i][j]);
        }
        tmpMap.push_back(tmpRow);
        tmpRow.clear();
    }
    
    //cout<<"after map copied"<<endl;
    
    int tilesSampled=0;
    int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
    if(rowsPerSplit == 0)
        rowsPerSplit =1;
    for (int h=yEnd; h>=yStart; h--){
        int currRow = h/rowsPerSplit;
        if(currRow >= Config.Rowsplits)
            currRow = Config.Rowsplits-1;
        //could potentially resample the entire map
        for (int j = xStart; j <tmpMap[h].size(); j++){
            tilesSampled++;
            //try to generate a tile with the first config matrix
            //cout<<"Before sampling a tile at: "<<j<<" "<<h<<endl;
            int id = RegenerateTopLevelTile(tmpMap, PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[0], 0);
            //cout<<"After sampling a tile at: "<<j<<" "<<h<<endl;

            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100){
                this->NumberTilesWithEachMatrix[0]++;
            }
            
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                id = RegenerateTopLevelTile(tmpMap, PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[WhichDepMat], WhichDepMat);
                //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                    WhichDepMat++;
            }
            
            //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                id = (rand() % (this->TileTypes.size()-1)) + 1;
                Map[h][j] = TileTypes[id];
                tmpMap[h][j] = TileTypes[id];
            } else if(id ==1){  //if the sampled tile matches the tile already in the map, move onto the next row
                Map[h][j] = tmpMap[h][j];
                break;
            } else if(id ==2)   //if they're not the same, keep sampling in that row
                Map[h][j] = tmpMap[h][j];
            //cout<<"Tile placed"<<endl;
        }
    }
    //open the file to print the map into
    ofstream GMap;
    GMap.open(output.c_str());
    for (int i = 0; i < this->Map.size(); i++){
        for (int j = 0; j < this->Map.at(0).size(); j++)
            GMap << this->Map[i][j];
        GMap << endl;
    }
    //cout<<tilesSampled<<endl;
    return tilesSampled;
}

int Generator::RegenerateTopLevelTile(vector<vector<char> > &tmpMap, vector<char> PossibleTiles, int depth, int h, int w, int offset, vector< vector< int> > Method, int WhichDepMat){
    int id = 0;
    double selector;
    
    //cout<<"In top level tile resampling"<<endl;
    vector<char> PT(PossibleTiles.size());	//create a copy of the possible tiles vector to use for changes
    vector<float> Probs;
    for (int i = 0; i<PossibleTiles.size(); i++)
        PT[i] = PossibleTiles[i];
    
    while (!PT.empty()){		//while there is still a possible tile
        //cout<<"in while"<<endl;
        int ID = 0;
        int P = 0;
        int type=0;
        //check if this state has been observed before
        for (int i = 0; i<Config.DependencyMatrices[WhichDepMat].size(); i++){
            for (int j =0; j <Config.DependencyMatrices[WhichDepMat][i].size(); j++){
                if (this->Config.DependencyMatrices[WhichDepMat][i][j] == 1){
                    
                    if (((h + i - 2) < 0) || ((h + i - 2) >= tmpMap.size()) || ((w + j - 2) < 0)){
                        type=START;
                    } else{
                        for (int b = 0; b < TileTypes.size(); b++){
                            if (TileTypes.at(b) == tmpMap[h + i - 2][w + j - 2]){
                                type = b;
                                break;
                            }
                        }
                    }
                    ID += type*pow(TileTypes.size(), P);
                    P++;
                }
            }
        }
        
        bool Obs = false;
        //check if the state has been observed before
        for (int a = 0; a < PT.size(); a++){
            for (int x = 0; x < this->TileTypes.size(); x++){
                if (PT.at(a) == this->TileTypes.at(x)){
                    if (this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x] != 0){
                        Obs = true;
                        x = (int)this->TileTypes.size();
                        a = (int)PT.size();
                    }
                }
            }
        }
        //cout<<"Config found"<<endl;
        if (!Obs && depth != this->Config.LookAhead){		//if this state hasnt been observed and this is NOT the top level in the lookahead
            this->BackTracks++;                             //then return an error
            return 99;
        }
        else if (!Obs && depth == this->Config.LookAhead){	//if we havent observed this state, and it IS the first step, then break out of
            break;                                          //of the loop, and we will fallback to a simpler network structure
        }
        else if (Obs){                                      //if we have observed this state, then generate the next tile
            float Norm = 0;
            Probs.resize(PT.size());
            for (int i = 0; i< PT.size(); i++)	//reset the value of Probs
            Probs[i] = 0;
            
            for (int a = 0; a < PT.size(); a++){
                for (int x = 0; x < this->TileTypes.size(); x++){
                    if (PT.at(a) == this->TileTypes.at(x)){
                        Probs[a] = this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x];
                        Norm += this->Probabilities[WhichDepMat][ID+Config.Rows[WhichDepMat]*offset][x];
                    }
                }
            }
            for (int i = 0; i<PT.size(); i++){						//renormalize the probabilities
                Probs[i] = Probs[i] / Norm;
            }
            
            //PICK THE TILE
            double PR = 0;
            int tracker = 0;
            selector = ((double)rand()/RAND_MAX);
            for (int a = 0; a<PT.size(); a++){
                PR = PR + Probs[a];
                if (Probs[a]>0)
                tracker = a;
                if (selector <= PR){
                    id = a;
                    break;
                }
            }
            //cout<<"TileChosen"<<endl;
            if (h < tmpMap.size() && w < tmpMap.at(0).size())
                tmpMap[h][w] = PT[id];
            
            //call the GenTile function to generate the next tile
            //set to a test variable, so we can check the validity
            //of the next state
            int test = 1;
            
            if (depth > 0){
                //cout<<"In an if statement that shouldnt be reached"<<endl;
                if (w < this->LengthOfLevel-1)
                    test = RegenerateTopLevelTile(tmpMap, TileTypes, depth - 1, h, w+1, offset, Method, WhichDepMat);
            }
            //if the next state isnt valid, remove the tile chosen in this state and try a different one
            if (test == 99){
                int j = 0;
                if (id < PT.size()){
                    for (vector<char>::iterator i = PT.begin(); i != PT.end(); i++){
                        if (PT.at(j) == PT.at(id)){
                            PT.erase(i);
                            break;
                        }
                        j++;
                    }
                } else
                PT.clear();
            } else{
                //cout<<"returning"<<endl;
                if(tmpMap[h][w] == Map[h][w])
                    return 1;
                else
                    return 2;
            }
        }
    }									//end of loop
    
    if (depth == this->Config.LookAhead){				//if no valid options on top level, return error code 100
        return 100;
    }
    else{
        this->BackTracks++;
        return 99;				//if no valid options on sub level, return error.
    }							//forces a different choice on upper levels.
}

int Generator::GenerateTopLevelSectionWithConstraints(string output, int xStart, int yStart, int xEnd, int yEnd){
    
    START = 0;
    vector<vector<char> > tmpMap;
    int attempts=0;
    do{
        attempts++;
        //if(attempts == 6)
        //    return attempts;
        //initialize the set of possible tiles for this level
        vector<char> PossibleTiles(TileTypes.size()-1);
        for (int i = 0; i < this->TileTypes.size()-1; i++)
            PossibleTiles[i]=TileTypes[i+1];
    
        //initialize the section of the map to be generated
    
        //0 is the top of the map, therefore the start height
        //of the section to be sampled will be a larger number
        //than the end height
    
        for(int h=yStart; h<yEnd; h++){
            for (int w=xStart; w<xEnd; w++) {
                Map[h][w] = 'X';
            }
        }

        int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
        if(rowsPerSplit == 0)
            rowsPerSplit =1;
        for (int h=yEnd; h>=yStart; h--){
            int currRow = h/rowsPerSplit;
            if(currRow >= Config.Rowsplits)
                currRow = Config.Rowsplits-1;
            for (int j = xStart; j <xEnd; j++){
                
                //try to generate a tile with the first config matrix
                int id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[0], 0);
                //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
                //if a good tile is generated with first method, track it
                if (id != 100){
                    this->NumberTilesWithEachMatrix[0]++;
                }
            
                int WhichDepMat = 1;
                while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                
                    id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, h, j, currRow, this->Config.DependencyMatrices[WhichDepMat],   WhichDepMat);
                    //track how many tiles generated with fallback method
                    if (id != 100){
                        this->NumberTilesWithEachMatrix[WhichDepMat]++;
                        break;
                    }
                    else
                        WhichDepMat++;
                }
            
                //if it also fails, or we are not using a fallback strategy, generate a tile at random
                if (id == 100){
                    //ignore one of the tiles
                    id = (rand() % (this->TileTypes.size()-1)) + 1;
                    Map[h][j] = TileTypes[id];
                }
            }
        }
        //open the file to print the map into
        ofstream GMap;
        GMap.open(output.c_str());
        for (int i = 0; i < this->Map.size(); i++){
            for (int j = 0; j < this->Map.at(0).size(); j++)
                GMap << this->Map[i][j];
            GMap << endl;
        }
        
        tmpMap.clear();
        for (int k=0; k<Map.size(); k++) {
            vector<char> tmp;
            for (int l=0; l<16; l++) {
                if(Map[k][l] !='X')
                    tmp.push_back(Map[k][l]);
            }
            if(tmp.size()>0)
                tmpMap.push_back(tmp);
            tmp.clear();
        }
        
        
    } while(!MarioPlayabilityConstraint(tmpMap, 0, 0) ||
            !MarioNoIllFormedPipesConstraint(tmpMap, 0, 0));
            //KidIcarusPlayabilitySection(tmpMap)>=2); //Enter any number of constraints here
    //cout<<!MarioNoIllFormedPipesConstraint(tmpMap, 0, 0)<<" "<<!MarioPlayabilityConstraint(tmpMap, 0, 0)<<endl;
    return attempts;
}

int Generator::GenerateTopLevelRowWithConstraints(string output, int row, char domain){
    START= 0;
    
    //we basically assume that the rest of the map is playable, when testing the current portion
    
    //if Mario, assume the rest of the map is empty (except for the bottom row, which is solid)
    if(domain == 'M'){
        for(int h=0; h<row; h++){
            for (int w=0; w<Map[h].size(); w++) {
                if(h==Map.size()-1)
                    Map[h][w]= '#';
                else
                    Map[h][w] = '-';
            }
        }
    } else if(domain == 'K'){   //if Kid Icarus, alternate between platforms and empty space
        for(int h=0; h<row; h++){
            for (int w=0; w<Map[h].size(); w++) {
                if(h%2 == 0)
                    Map[h][w] = 'T';
                else
                    Map[h][w] = '-';
            }
        }
    } else if(domain == 'L'){   //if Loderunner, set the rest of the map as ladders (most traversible tile)
        for(int h=0; h<row; h++){
            for (int w=0; w<Map[h].size(); w++) {
                Map[h][w] = '#';
            }
        }
    }
    
    int attempts =0;
    do{
        attempts++;
        if(attempts == 6)
            return attempts;
        //initialize the set of possible tiles for this level
        vector<char> PossibleTiles(TileTypes.size()-1);
        for (int i = 0; i < this->TileTypes.size()-1; i++)
            PossibleTiles[i]=TileTypes[i+1];
        
        //initialize the section of the map to be generated
        
        //0 is the top of the map, therefore the start height
        //of the section to be sampled will be a larger number
        //than the end height
        
        for (int w=0; w<Map[row].size(); w++)
            Map[row][w] = 'X';
        
        int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
        if(rowsPerSplit == 0)
            rowsPerSplit =1;
        
        int currRow = row/rowsPerSplit;
        if(currRow >= Config.Rowsplits)
            currRow = Config.Rowsplits-1;
        for (int j = 0; j<(int)Map[row].size(); j++){
                
            //try to generate a tile with the first config matrix
            int id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, row, j, currRow, this->Config.DependencyMatrices[0], 0);
            //if it fails, and we are using a fallback strategy, try to generate with the second config matrix
            //if a good tile is generated with first method, track it
            if (id != 100)
                this->NumberTilesWithEachMatrix[0]++;
                
            int WhichDepMat = 1;
            while (id == 100 && WhichDepMat < this->Config.NumberMatrices){
                    
                id = GenerateTopLevelTile(PossibleTiles, this->Config.LookAhead, row, j, currRow, this->Config.DependencyMatrices[WhichDepMat],   WhichDepMat);
                    //track how many tiles generated with fallback method
                if (id != 100){
                    this->NumberTilesWithEachMatrix[WhichDepMat]++;
                    break;
                }
                else
                    WhichDepMat++;
            }
                
                //if it also fails, or we are not using a fallback strategy, generate a tile at random
            if (id == 100){
                //ignore one of the tiles
                id = (rand() % (this->TileTypes.size()-1)) + 1;
                Map[row][j] = TileTypes[id];
            }
        }
        
        //open the file to print the map into
        ofstream GMap;
        GMap.open(output.c_str());
        for (int i = 0; i < this->Map.size(); i++){
            for (int j = 0; j < this->Map.at(0).size(); j++)
                GMap << this->Map[i][j];
            GMap << endl;
        }
    } while(/*!MarioNoIllFormedPipesConstraint(Map, 0, 0) ||
            !MarioPlayabilityConstraint(Map, 0, 0)*/
            row<168 && KidIcarusPlayabilitySection(Map)>=(row+2)); //Enter any number of constraints here

    return attempts;
}


void Generator::GenerateTopLevelWithConstraints(string filename, bool horizontal, int sectionSize, bool sectionBySection){
    //initialize the map to be generated
    Map.resize(this->HeightOfLevel);
    for (int i = 0; i < HeightOfLevel; i++){
        Map[i].resize(LengthOfLevel);
        for (int j = 0; j < LengthOfLevel; j++){
            Map[i][j] = 'X';
        }
    }
    
    if(sectionBySection){
        if(horizontal){
            for (int i=0; i<ceil((float)LengthOfLevel+sectionSize-1); i+=sectionSize)
                GenerateTopLevelSectionWithConstraints(filename, i, 0, min(i+sectionSize, LengthOfLevel), HeightOfLevel-1);
        } else{
            for (int i=HeightOfLevel-1; i>0; i-=sectionSize)
                GenerateTopLevelSectionWithConstraints(filename, 0, max(i-sectionSize+1, 0), LengthOfLevel, i);
        }
    } else{ //line by line
        for (int i=(int)Map.size()-1; i>=0; i--)
            GenerateTopLevelRowWithConstraints(filename, i, 'M');
    }
}

