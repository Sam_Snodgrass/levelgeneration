#pragma once
#include<string>
#include<vector>

using namespace std;

//block of pixels
/*struct Block{
	string type;
 };*/

//learning and generating configuration
struct Configuration{
    int Rowsplits;
    int LookAhead;
    bool TopDown;
    bool Hierarchical;
    int SizeOfArray;
    int SizeOfFBArray;
    int NumberTilesTypes;
    int width;
    int height;
    
    //multiple fallbacks
    int NumberMatrices;
    vector< vector< vector< int> > > DependencyMatrices;
    vector<int> DepComplexities;
    vector<int> Rows;
};