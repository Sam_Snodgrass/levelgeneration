#include "Learner.h"
#include<fstream>
#include<vector>
#include<iostream>
#include <math.h>

Learner::Learner(int Rowsplits, int LookAhead, bool TopDown, bool Hierarchical){
    this->Config.Rowsplits = Rowsplits;
    this->Config.LookAhead = LookAhead;
    this->Config.TopDown = TopDown;
    this->Config.Hierarchical =Hierarchical;
    this->Config.NumberMatrices =0;
    this->Config.NumberTilesTypes=0;
}

//used to set up the Dependency Matrix and the Fallback Dependency Matrix
void Learner::SetDepMatrix(int DepMatrix){
    
    vector<vector< int> > TempDepMat;
    TempDepMat.resize(5);
    for (int i = 0; i < 5; i++)
    TempDepMat[i].resize(3);
    
    for (int i = 0; i < 5; i++){			//initialize array to all 0's
        for (int j = 0; j < 3; j++)
        TempDepMat[i][j] = 0;
    }
    TempDepMat[2][2] = 2;				//block we are finding the probability for
    
    if (this->Config.TopDown){				//if we are generating from the top down...
        if (DepMatrix == 1)				//use the previous block to determine probability
            TempDepMat[2][1] = 1;
        else if (DepMatrix == 2){		//use the previous block and the block above to determine probability
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[1][2] = 1;	//above
        } else if (DepMatrix == 3){		//use previous block, and previous above block, and above block to determine probability
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[1][1] = 1;	//previous above
            TempDepMat[1][2] = 1;	//above
        } else if (DepMatrix == 4){     //use 2 previous blocks and above block
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[2][0] = 1;	//second previous
            TempDepMat[1][2] = 1;	//above
        } else if (DepMatrix == 5){     //use previous block and 2 above blocks
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[1][2] = 1;	//above
            TempDepMat[0][2] = 1;	//second above
        } else{}						//prevent warnings
    }
    else{					//if we are generating from the bottom up
        
        if (DepMatrix == 1)			//only use the previous block to determine probability
        TempDepMat[2][1] = 1;
        else if (DepMatrix == 2){		//use the previous block and the block below to determine probability
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[3][2] = 1;	//below
        } else if (DepMatrix == 3){		//use previous block, and previous below block, and below block to determine probability
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[3][1] = 1;	//previous below
            TempDepMat[3][2] = 1;	//below
        } else if (DepMatrix == 4){     //use 2 previous blocks and below block
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[2][0] = 1;	//second previous
            TempDepMat[3][2] = 1;	//below
        } else if (DepMatrix == 5){     //use previous block and 2 below blocks
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[3][2] = 1;	//below
            TempDepMat[4][2] = 1;	//second below
        } else if (DepMatrix == 6){
            TempDepMat[2][0] = 1;	//second previous
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[3][1] = 1;	//previous below
            TempDepMat[3][2] = 1;	//below
        } else if (DepMatrix == 7){
            TempDepMat[2][1] = 1;	//previous
            TempDepMat[3][1] = 1;	//previous below
            TempDepMat[3][2] = 1;	//below
            TempDepMat[4][2] = 1;	//second below
        } else{}       //prevent warnings
    }
    
    //increment the number of matrices this config uses
    this->Config.NumberMatrices++;
    this->Config.DependencyMatrices.push_back(TempDepMat);
    
    if(DepMatrix == 0)
        Config.DepComplexities.push_back(0);
    else if(DepMatrix == 1)
        Config.DepComplexities.push_back(1);
    else if(DepMatrix == 2)
        Config.DepComplexities.push_back(2);
    else if(DepMatrix == 3 || DepMatrix == 4 || DepMatrix == 5)
        Config.DepComplexities.push_back(3);
    else if(DepMatrix == 6 || DepMatrix == 7)
        Config.DepComplexities.push_back(4);
}

void Learner::SetRows(bool Hier){
    
    int types = 0;
    if (Hier)
        types = (int)this->HierTileTypes.size();
    else
        types = (int)this->TileTypes.size();
    
    for (int i = 0; i < this->Config.NumberMatrices; i++){
        this->Config.Rows.push_back(1);
        for (int j = 0; j < this->Config.DepComplexities[i]; j++)
            this->Config.Rows[i] *= types;
    }
}

void Learner::ResizeVectors(bool Hier){
    
    this->Totals.resize(this->Config.NumberMatrices);
    this->Probabilities.resize(this->Config.NumberMatrices);
    if (Hier){
        this->HierTotals.resize(this->Config.NumberMatrices);
        this->HierProbabilities.resize(this->Config.NumberMatrices);
    }
}

//initailize the totals and probability files to the proper number of rows and columns, and all to 0's
void Learner::InitFiles(string TotName, string HierTotName, string ProbName, string HierProbName, bool Hier, int WhichDepMat){
    
    ofstream Tot, Prob, HierTot, HierProb;
    Tot.open(TotName.c_str());
    Prob.open(ProbName.c_str());
    HierTot.open(HierTotName.c_str());
    HierProb.open(HierProbName.c_str());
    
    int Types = 0;
    if (Hier)
    Types = (int)this->HierTileTypes.size();
    else
    Types = (int)this->TileTypes.size();
    
    for(int k=0; k<Config.Rowsplits; k++){
        for (int i = 0; i<this->Config.Rows[WhichDepMat]; i++){
            for (int j = 0; j <= Types; j++){
                if (j<Types){
                    Tot << 0 << " ";
                    Prob << 0 << " ";
                }
                else{
                    Tot << 0 << " " << endl;
                    Prob << endl;
                }
            }
        }
    }
    
    for (int k = 0; k < this->HierTileTypes.size(); k++){
        for (int i = 0; i < this->Config.Rows[WhichDepMat]; i++){
            for (int j = 0; j <= this->TileTypes.size(); j++){
                if (j < this->TileTypes.size()){
                    HierTot << 0 << " ";
                    HierProb << 0 << " ";
                }
                else{
                    HierTot << 0 << " " << endl;
                    HierProb << endl;
                }
            }
        }
    }
    Tot.close();
    Prob.close();
    HierTot.close();
    HierProb.close();
}

//given a file, this function extracts all the different tile types from the map
void Learner::FindTileTypes(string MapName, bool Hier){
    
    fstream InputMap;
    InputMap.open(MapName.c_str());
    string line = "";
    char c;
    
    if (!Hier){
        while (getline(InputMap, line)){
            for (int i = 0; i < line.size(); i++){
                c = line.at(i);
                bool seen = false;
                for (int j = 0; j < this->TileTypes.size(); j++){
                    if (c == this->TileTypes.at(j) || c =='\n' || c=='\t' || c== '\r'){
                        seen = true;
                        break;
                    }
                }
                if (!seen)
                this->TileTypes.push_back(line.at(i));
            }
        }

    } else{
        while (getline(InputMap, line)){
            for (int i = 0; i < line.size(); i++){
                c = line.at(i);
                bool seen = false;
                for (int j = 0; j < this->HierTileTypes.size(); j++){
                    if (c == this->HierTileTypes.at(j)){
                        seen = true;
                        break;
                    }
                }
                if (!seen)
                this->HierTileTypes.push_back(line.at(i));
            }
        }
    }
}

//checks all files of a certain base name for the different tile types
void Learner::FindAllTileTypes(int NumMaps, string MapNameBase, bool Hier){
    
    if (!Hier){
        this->TileTypes.push_back('S');
    }
    else{
        this->HierTileTypes.push_back('S');
    }
    for (int i = 1; i < NumMaps + 1; i++)
        this->FindTileTypes(MapNameBase+ to_string(i) + ".txt", Hier);
    
    this->Config.NumberTilesTypes = (int)this->TileTypes.size();
}

void Learner::SetTotalsTopLevelColumn(string MapName, int trainingMaps, string TotName, bool Hier, int WhichDepMat){
    
    int NumTypes=-1;
    int START = 0;
    vector<char> Tiles;
    if (Hier){
        NumTypes = (int)this->HierTileTypes.size();
        for (int i = 0; i < NumTypes; i++)
            Tiles.push_back(this->HierTileTypes.at(i));
    }
    else{
        NumTypes = (int)this->TileTypes.size();
        Tiles = TileTypes;
        for (int i = 0; i < NumTypes; i++)
            Tiles.push_back(this->TileTypes[i]);
    }
    
    ifstream TotalsFile;
    TotalsFile.open(TotName.c_str());
    
    if (this->Totals[WhichDepMat].empty()){
        int T = 0;
        int index = 0;
        vector< int > Tmp;
        while (TotalsFile >> T){
            Tmp.push_back(T);
            index++;
            if (index == NumTypes + 1){
                index = 0;
                this->Totals[WhichDepMat].push_back(Tmp);
                Tmp.clear();
            }
        }
    }
    TotalsFile.close();
    
    
    
    vector< vector <char> > Map;
    vector< char > Temp;
    
    for(int m=1; m<=trainingMaps; m++){
        
        ifstream InputMap;
        InputMap.open((MapName+to_string(m)+".txt").c_str());
        string line ="";
        while (getline(InputMap,line)){
            
            if(line.back() == '\n' || line.back() == '\t' || line.back() == '\r' || line.back() == '\0')
                line.pop_back();
            
            Temp.resize(line.length());
            
            for (int i = 0; i < line.size(); i++)
                Temp[i]=line[i];
            
            Map.push_back(Temp);
            Temp.clear();
        }
        int ID;
        int P;
        int id;
        int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
        if(rowsPerSplit == 0)
            rowsPerSplit =1;
        
        for (int w = 0; w < Map[0].size(); w++){
            for (int h=(int)Map.size()-1; h>=0; h--){
                int currRow = h/rowsPerSplit;
                if(currRow >= Config.Rowsplits)
                    currRow = Config.Rowsplits-1;
                ID = 0;
                id = 0;
                P = 0;
                for (int i = 0; i<Config.DependencyMatrices[WhichDepMat].size(); i++){
                    for (int j =0; j <Config.DependencyMatrices[WhichDepMat][i].size(); j++){
                        if (this->Config.DependencyMatrices[WhichDepMat][i][j] == 1){
                            
                            if (((h + i - 2) < 0) || ((h + i - 2) >= Map.size()) || ((w + j - 2) < 0)){
                                id=START;
                            }
                            else{
                                for (int m = 0; m < NumTypes; m++){
                                    if (Tiles.at(m) == Map[h + i - 2][w + j - 2]){
                                        id = m;
                                        break;
                                    }
                                }
                            }
                            ID += id*pow(NumTypes, P);
                            P++;
                        }
                    }
                }
                
                for (int m = 0; m < NumTypes; m++){
                    if (Tiles.at(m) == Map[h][w]){
                        id = m;
                        break;
                    }
                }
                //cout<<WhichDepMat<<" "<<ID<<" "<<Config.Rows[WhichDepMat]<<" "<<currRow<<" "<<ID+Config.Rows[WhichDepMat]*currRow<<" "<<id<<endl;
                this->Totals[WhichDepMat][ID+Config.Rows[WhichDepMat]*currRow][id]++;
                this->Totals[WhichDepMat][ID+Config.Rows[WhichDepMat]*currRow][NumTypes]++;
            }
        }
        Map.clear();
    }
    
    ofstream Tots;
    Tots.open(TotName.c_str());		//save totals to a file
    
    for (int i = 0; i<this->Config.Rows[WhichDepMat] * this->Config.Rowsplits; i++){
        for (int j = 0; j <= NumTypes; j++){
            Tots << this->Totals[WhichDepMat][i][j] << " ";
        }
        Tots << endl;
    }
    
    Tots.close();
}



void Learner::SetTotalsTopLevel(string MapName, int trainingMaps, string TotName, bool Hier, int WhichDepMat){
    
    int NumTypes=-1;
    int START = 0;
    vector<char> Tiles;
    if (Hier){
        NumTypes = (int)this->HierTileTypes.size();
        for (int i = 0; i < NumTypes; i++)
            Tiles.push_back(this->HierTileTypes.at(i));
    }
    else{
        NumTypes = (int)this->TileTypes.size();
        Tiles = TileTypes;
        for (int i = 0; i < NumTypes; i++)
            Tiles.push_back(this->TileTypes[i]);
    }
    
    ifstream TotalsFile;
    TotalsFile.open(TotName.c_str());
    
    if (this->Totals[WhichDepMat].empty()){
        int T = 0;
        int index = 0;
        vector< int > Tmp;
        while (TotalsFile >> T){
            Tmp.push_back(T);
            index++;
            if (index == NumTypes + 1){
                index = 0;
                this->Totals[WhichDepMat].push_back(Tmp);
                Tmp.clear();
            }
        }
    }
    TotalsFile.close();
    
    
    
    vector< vector <char> > Map;
    vector< char > Temp;
    
    for(int m=0; m<=trainingMaps; m++){
        
        ifstream InputMap((MapName+to_string(m)+".txt").c_str());
        if(!InputMap)
            continue;
        string line ="";
        while (getline(InputMap,line)){
            
            if(line.back() == '\n' || line.back() == '\t' || line.back() == '\r' || line.back() == '\0')
                line.pop_back();
            
            Temp.resize(line.length());
            
            for (int i = 0; i < line.size(); i++)
                Temp[i]=line[i];
            
            Map.push_back(Temp);
            Temp.clear();
        }
        int ID;
        int P;
        int id;
        int rowsPerSplit = (int)Map.size()/Config.Rowsplits;
        if(rowsPerSplit == 0)
            rowsPerSplit =1;
        
        for (int h=(int)Map.size()-1; h>=0; h--){
            int currRow = h/rowsPerSplit;
            if(currRow >= Config.Rowsplits)
                currRow = Config.Rowsplits-1;
            
            for (int w = 0; w < Map[h].size(); w++){
                ID = 0;
                id = 0;
                P = 0;
                for (int i = 0; i<Config.DependencyMatrices[WhichDepMat].size(); i++){
                    for (int j =0; j <Config.DependencyMatrices[WhichDepMat][i].size(); j++){
                        if (this->Config.DependencyMatrices[WhichDepMat][i][j] == 1){
                            
                            if (((h + i - 2) < 0) || ((h + i - 2) >= Map.size()) || ((w + j - 2) < 0)){
                                id=START;
                            }
                            else{
                                for (int m = 0; m < NumTypes; m++){
                                    if (Tiles.at(m) == Map[h + i - 2][w + j - 2]){
                                        id = m;
                                        break;
                                    }
                                }
                            }
                            ID += id*pow(NumTypes, P);
                            P++;
                        }
                    }
                }
                
                for (int m = 0; m < NumTypes; m++){
                    if (Tiles.at(m) == Map[h][w]){
                        id = m;
                        break;
                    }
                }
                //cout<<WhichDepMat<<" "<<ID<<" "<<Config.Rows[WhichDepMat]<<" "<<currRow<<" "<<ID+Config.Rows[WhichDepMat]*currRow<<" "<<id<<endl;
                this->Totals[WhichDepMat][ID+Config.Rows[WhichDepMat]*currRow][id]++;
                this->Totals[WhichDepMat][ID+Config.Rows[WhichDepMat]*currRow][NumTypes]++;
            }
        }
        Map.clear();
    }
    
    ofstream Tots;
    Tots.open(TotName.c_str());		//save totals to a file
    
    for (int i = 0; i<this->Config.Rows[WhichDepMat] * this->Config.Rowsplits; i++){
        for (int j = 0; j <= NumTypes; j++){
            Tots << this->Totals[WhichDepMat][i][j] << " ";
        }
        Tots << endl;
    }
    
    Tots.close();
    
//    for(int i=0;i<this->TileTypes.size(); i++)
//        cout<<this->TileTypes[i]<<" ";
//    cout<<endl;
}





void Learner::SetTotalsBottomLevel(string HierMapName, string MapName, string TotName, int WhichDepMat, bool manualConversion){
    
    ifstream HierMap, Map, TotalsFile;
    HierMap.open(HierMapName);
    Map.open(MapName);
    TotalsFile.open(TotName.c_str());
    string line = "";
    
    vector< vector< char> > HierBlocks;
    vector< vector< char> > Blocks;
    vector<char> Temp;
    
    
    if(!HierMap){
        cout<<HierMapName<<endl;
        cout<<"Map not Opened"<<endl;
    }
    //read in the Hier map
    while (getline(HierMap, line)){
        
        for (int i = 0; i < line.size(); i++)
        Temp.push_back(line.at(i));
        
        HierBlocks.push_back(Temp);
        Temp.clear();
    }
    
    //read in the regular map
    while (getline(Map, line)){
        
        for(int z=0; z<line.length(); z++){
            if(line.at(z) == '\n' || line.at(z) == '\r' || line.at(z) == '\t')
            line.erase(line.begin()+z);
        }
        for (int i = 0; i < line.size() - (line.size()%Config.width); i++)
        Temp.push_back(line.at(i));
        
        Blocks.push_back(Temp);
        Temp.clear();
    }
    
    if(!manualConversion){
        while(Blocks.size() > (Blocks.size()-Blocks.size()%Config.height)){
            Blocks.erase(Blocks.begin());
        }
    }
    //read in the current totals count
    if (this->HierTotals[WhichDepMat].empty()){
        int T = 0;
        int index = 0;
        int Hindex = 0;
        vector< int > Tmp;
        vector< vector<int> > Htmp;
        while (TotalsFile >> T){
            Tmp.push_back(T);
            index++;
            if (index == this->TileTypes.size() + 1){
                index = 0;
                Htmp.push_back(Tmp);
                Tmp.clear();
                Hindex++;
                if (Hindex == this->Config.Rows[WhichDepMat]){
                    this->HierTotals[WhichDepMat].push_back(Htmp);
                    Htmp.clear();
                    Hindex = 0;
                }
            }
        }
    }
    
    int START = 0;
    
    for(int i=0; i<this->TileTypes.size(); i++){
        if(this->TileTypes[i] == '\r' || this->TileTypes[i] == '\t' || this->TileTypes[i] == '\n'){
            this->TileTypes.erase(this->TileTypes.begin() + i);
            i--;
        }
    }
    
    int ID;
    int P;
    int id;
    
    for (int h = (int)Blocks.size()-1; h >= 0; h--){
        for (int w = 0; w < HierBlocks.at(0).size()*this->Config.width; w++){
            ID = 0;
            id = 0;
            P = 0;
            for (int i = 0; i<5; i++){
                for (int j = 2; j >= 0; j--){
                    if (this->Config.DependencyMatrices[WhichDepMat][i][j] == 1){
                        if (((h + i - 2) < 0) || ((h + i - 2) >= Blocks.size()) || ((w + j - 2) < 0)){
                            id = START;
                        }
                        else{
                            for (int m = 0; m < this->TileTypes.size(); m++){
                                if (this->TileTypes.at(m) == Blocks[h + i - 2][w + j - 2]){
                                    id = m;
                                    break;
                                }
                            }
                        }
                        ID += id*pow(this->TileTypes.size(), P);
                        P++;
                    }
                }
            }
            for (int m = 0; m < this->TileTypes.size(); m++){
                if (this->TileTypes.at(m) == Blocks[h][w]){
                    id = m;
                    break;
                }
            }
            int HierID = 0;
            for (int d = 0; d < this->HierTileTypes.size(); d++){
                if (this->HierTileTypes.at(d) == HierBlocks[floor((long double)h / this->Config.height)][floor((long double)w / this->Config.width)]){
                    HierID = d;
                    break;
                }
            }
            
            if(manualConversion){
                if((this->HierTileTypes[HierID] == 'G' || this->HierTileTypes[HierID] == 'A') && h<(this->Config.height-1)){
                    for (int i = 0; i < this->HierTileTypes.size(); i++){
                        //cout<<HierMap.size()<<" "<<HierMap[i].size()<<endl;
                        if (this->HierTileTypes[i] == HierBlocks[(h / this->Config.height)][w / this->Config.width]){
                            HierID = i;
                            //if equal to ground or gap (bottom row)
                            break;
                        }
                    }
                }
            }
            
            
            this->HierTotals[WhichDepMat][HierID][ID][id]++;
            this->HierTotals[WhichDepMat][HierID][ID][this->TileTypes.size()]++;
        }
    }
    
    ofstream Tots;
    Tots.open(TotName.c_str());		//save totals to a file
    
    for (int k = 0; k < this->HierTileTypes.size(); k++){
        for (int i = 0; i < this->Config.Rows[WhichDepMat]; i++){
            for (int j = 0; j <= this->TileTypes.size(); j++){
                Tots << this->HierTotals[WhichDepMat][k][i][j] << " ";
            }
            Tots << endl;
        }
    }
    Tots.close();
}

void Learner::SetProbsForOnlyLowLevel(string ProbName, bool Hier, int WhichDepMat){
    
    ifstream Prob;
    Prob.open(ProbName.c_str());
    float P;
    int index = 0;
    int Types = 0;
    if (Hier)
    Types = (int)this->HierTileTypes.size();
    else
    Types = (int)this->TileTypes.size();
    
    if (!Hier){
        vector< float > Tmp(Types);
        while (Prob >> P){
            Tmp[index]=P;
            index++;
            if (index == Types){
                index = 0;
                this->Probabilities[WhichDepMat].push_back(Tmp);
            }
        }
    }
    else{
        vector<float> Tmp;
        vector<vector <float> > TMP;
        int innerIndex = 0;
        while (Prob >> P){
            Tmp.push_back(P);
            index++;
            if (index == Types){
                index = 0;
                TMP.push_back(Tmp);
                Tmp.clear();
                innerIndex++;
                if (innerIndex == this->Config.Rows[WhichDepMat]){
                    innerIndex = 0;
                    this->HierProbabilities[WhichDepMat].push_back(TMP);
                    TMP.clear();
                }
            }
        }
    }
    Prob.close();
    
    ofstream Probs;
    Probs.open(ProbName.c_str());
    
    if (Hier){
        for (int k = 0; k < this->HierTileTypes.size(); k++){
            for (int i = 0; i < this->Config.Rows[WhichDepMat]; i++){
                if (this->HierTotals[WhichDepMat][k][i][Types] == 0)
                this->HierTotals[WhichDepMat][k][i][Types]++;	//if total=0, add 1 to prevent division by 0
                for (int j = 0; j < Types; j++){
                    this->HierProbabilities[WhichDepMat][k][i][j] = (float)this->HierTotals[WhichDepMat][k][i][j] / (float)this->HierTotals[WhichDepMat][k][i][Types];
                    Probs << this->HierProbabilities[WhichDepMat][k][i][j] << " ";
                }
                Probs << endl;
            }
            Probs << endl;
        }
        Probs.close();
    }
    else{
        for (int i = 0; i < this->Totals[WhichDepMat].size(); i++){
            for (int j = 0; j < this->Totals[WhichDepMat].at(0).size() - 1; j++){
                //if there is a 0 in the totals column, replace it with a one to avoid div. by 0
                if (this->Totals[WhichDepMat][i][Types] == 0){
                    this->Probabilities[WhichDepMat][i][j]=0;
                    Probs << this->Probabilities[WhichDepMat][i][j] << " ";
                }
                else{
                    double denom =this->Totals[WhichDepMat][i][Types];//+this->TileTypes.size();
                    this->Probabilities[WhichDepMat][i][j] = (float)this->Totals[WhichDepMat][i][j]/(denom);
                    Probs << this->Probabilities[WhichDepMat][i][j] << " ";
                }
            }
            Probs << endl;
            
        }
        Probs.close();
    }
}

void Learner::SetProbs(string ProbName, bool Hier, int WhichDepMat){
    
    ifstream Prob;
    Prob.open(ProbName.c_str());
    float P;
    int index = 0;
    int Types = 0;
    if (Hier)
    Types = (int)this->HierTileTypes.size();
    else
    Types = (int)this->TileTypes.size();
    
    if (Hier){
        vector< float > Tmp;
        while (Prob >> P){
            Tmp.push_back(P);
            index++;
            if (index == Types){
                index = 0;
                this->Probabilities[WhichDepMat].push_back(Tmp);
                Tmp.clear();
            }
        }
    }
    else{
        vector<float> Tmp;
        vector<vector <float> > TMP;
        int innerIndex = 0;
        while (Prob >> P){
            Tmp.push_back(P);
            index++;
            if (index == Types){
                index = 0;
                TMP.push_back(Tmp);
                Tmp.clear();
                innerIndex++;
                if (innerIndex == this->Config.Rows[WhichDepMat]){
                    innerIndex = 0;
                    this->HierProbabilities[WhichDepMat].push_back(TMP);
                    TMP.clear();
                }
            }
        }
    }
    Prob.close();
    
    ofstream Probs;
    Probs.open(ProbName.c_str());
    
    if (!Hier){
        for (int k = 0; k < this->HierTileTypes.size(); k++){
            for (int i = 0; i < this->Config.Rows[WhichDepMat]; i++){
                if (this->HierTotals[WhichDepMat][k][i][Types] == 0)
                this->HierTotals[WhichDepMat][k][i][Types]++;	//if total=0, add 1 to prevent division by 0
                for (int j = 0; j < Types; j++){
                    this->HierProbabilities[WhichDepMat][k][i][j] = (float)this->HierTotals[WhichDepMat][k][i][j] / (float)this->HierTotals[WhichDepMat][k][i][Types];
                    Probs << this->HierProbabilities[WhichDepMat][k][i][j] << " ";
                }
                Probs << endl;
            }
            Probs << endl;
        }
        Probs.close();
    }
    else{
        for (int i = 0; i < this->Totals[WhichDepMat].size(); i++){
            for (int j = 0; j < this->Totals[WhichDepMat].at(0).size() - 1; j++){
                //if there is a 0 in the totals column, replace it with a one to avoid div. by 0
                if (this->Totals[WhichDepMat][i][Types] == 0)
                this->Totals[WhichDepMat][i][Types]++;
                
                this->Probabilities[WhichDepMat][i][j] = (float)this->Totals[WhichDepMat][i][j] / (float)this->Totals[WhichDepMat][i][Types];
                Probs << this->Probabilities[WhichDepMat][i][j] << " ";
            }
            Probs << endl;
            
        }
        Probs.close();
    }
}

string Learner::GetFallbackStringFromFullString(string full_config, vector< vector<int> > Full_Dependency, vector< vector<int> >Fallback_Dependency){
    
    string fallback_config="";
    int string_index=0;
    for(int i=0; i<Full_Dependency.size(); i++){
        for(int j=0; j<Full_Dependency[i].size(); j++){
            
            if(Full_Dependency[i][j] == 1){
                if(Fallback_Dependency[i][j] == 1)
                fallback_config+=full_config.at(string_index);
                string_index++;
            }
        }
    }
    return fallback_config;
}




