#include "MRF.h"
#include<math.h>
#include<fstream>
#include<iostream>
#include<time.h>

MRF::MRF(int Dependency_Matrix){
	SetDependencyMatrix(Dependency_Matrix);
}

MRF::MRF(){
    srand((int)time(NULL));
}

void MRF::SetDependencyMatrix(int Dependency_Matrix){

    srand((int)time(NULL));
	//1's indicate a previous state that is taken into account
	//2's indicate the current state
	//0's indicate ignored states
	//Dependency_Matrices.clear();
	vector< vector< int> > DepMat;
	DepMat.resize(5);
	Dependency_Complexity.push_back(0);
	for(int i=0; i<DepMat.size(); i++)
		DepMat[i].resize(5);

	for(int i=0; i<DepMat.size(); i++)
		for(int j=0; j<DepMat[i].size(); j++)
			DepMat[i][j] = 0;

	//for each higher order, we need to add a few new dependencies
	//add dependencies for each lower order as well
	
	//set of complexity 2 fallback
	if(Dependency_Matrix == 6){
		//0 0 0 0 0
		//0 0 0 0 0
		//0 1 2 1 0
		//0 0 0 0 0 
		//0 0 0 0 0
		DepMat[2][1] = 1;
		DepMat[2][3] = 1;

		Dependency_Complexity.back()+=2;
		
		//add the current state signifier to the center
		DepMat[2][2] = 2;

		Dependency_Matrices.push_back(DepMat);
		return;
	}
    
    if(Dependency_Matrix == 7){
        //0 0 0 0 0
        //0 0 1 0 0
        //0 0 2 0 0
        //0 0 1 0 0
        //0 0 0 0 0
        DepMat[1][2] = 1;
        DepMat[3][2] = 1;
        
        Dependency_Complexity.back()+=2;
        
        //add the current state signifier to the center
        DepMat[2][2] = 2;
        
        Dependency_Matrices.push_back(DepMat);
        return;
    }
    //--------------------------------------------------
    
    //set of complexity 1 fallback matrices
    if(Dependency_Matrix == 8){
        //0 0 0 0 0
        //0 0 1 0 0
        //0 0 2 0 0
        //0 0 0 0 0
        //0 0 0 0 0
        DepMat[1][2] = 1;
        
        Dependency_Complexity.back()+=1;
        
        //add the current state signifier to the center
        DepMat[2][2] = 2;
        
        Dependency_Matrices.push_back(DepMat);
        return;
    }
    
    if(Dependency_Matrix == 9){
        //0 0 0 0 0
        //0 0 0 0 0
        //0 1 2 0 0
        //0 0 0 0 0
        //0 0 0 0 0
        DepMat[2][1] = 1;
        
        Dependency_Complexity.back()+=1;
        
        //add the current state signifier to the center
        DepMat[2][2] = 2;
        
        Dependency_Matrices.push_back(DepMat);
        return;
    }
    
    if(Dependency_Matrix == 10){
        //0 0 0 0 0
        //0 0 0 0 0
        //0 0 2 1 0
        //0 0 0 0 0
        //0 0 0 0 0
        DepMat[2][3] = 1;
        
        Dependency_Complexity.back()+=1;
        
        //add the current state signifier to the center
        DepMat[2][2] = 2;
        
        Dependency_Matrices.push_back(DepMat);
        return;
    }
    
    if(Dependency_Matrix == 11){
        //0 0 0 0 0
        //0 0 0 0 0
        //0 0 2 0 0
        //0 0 1 0 0
        //0 0 0 0 0
        DepMat[3][2] = 1;
        
        Dependency_Complexity.back()+=1;
        
        //add the current state signifier to the center
        DepMat[2][2] = 2;
        
        Dependency_Matrices.push_back(DepMat);
        return;
    }
    //--------------------------------------------------

	if(Dependency_Matrix == 5){
		
		//add corner dependencies, 5th order
		//1 1 1 1 1
		//1 1 1 1 1
		//1 1 2 1 1
		//1 1 1 1 1
		//1 1 1 1 1
		DepMat[0][0] = 1;
		DepMat[0][4] = 1;
		DepMat[4][0] = 1;
		DepMat[4][4] = 1;

		Dependency_Complexity.back()+=4;

	} if(Dependency_Matrix >= 4){

		//add longer range diagonal dependencies, 4th order
		//0 1 1 1 0
		//1 1 1 1 1
		//1 1 2 1 1
		//1 1 1 1 1
		//0 1 1 1 0
		DepMat[0][1] = 1;
		DepMat[0][3] = 1;
		DepMat[4][1] = 1;
		DepMat[4][3] = 1;
		DepMat[1][0] = 1;
		DepMat[3][0] = 1;
		DepMat[1][4] = 1;
		DepMat[3][4] = 1;

		Dependency_Complexity.back()+=8;

	} if(Dependency_Matrix >= 3){

		//add longer range horizontal and vertical dependencies, 3rd order
		//0 0 1 0 0
		//0 1 1 1 0
		//1 1 2 1 1
		//0 1 1 1 0
		//0 0 1 0 0
		DepMat[0][2] = 1;
		DepMat[4][2] = 1;
		DepMat[2][0] = 1;
		DepMat[2][4] = 1;

		Dependency_Complexity.back()+=4;

	} if(Dependency_Matrix >= 2){

		//add diagonal dependencies, 2nd order
		//0 0 0 0 0
		//0 1 1 1 0
		//0 1 2 1 0
		//0 1 1 1 0
		//0 0 0 0 0
		DepMat[1][1] = 1;
		DepMat[1][3] = 1;
		DepMat[3][1] = 1;
		DepMat[3][3] = 1;

		Dependency_Complexity.back()+=4;

	} if(Dependency_Matrix >= 1){
		
		//add horizontal and vertical dependencies, 1st order
		//0 0 0 0 0
		//0 0 1 0 0
		//0 1 2 1 0
		//0 0 1 0 0
		//0 0 0 0 0
		DepMat[1][2] = 1;
		DepMat[3][2] = 1;
		DepMat[2][1] = 1;
		DepMat[2][3] = 1;

		Dependency_Complexity.back()+=4;

	} if(Dependency_Matrix >= 0){
		//no dependencies, 0th order
		//0 0 0 0 0
		//0 0 0 0 0
		//0 0 2 0 0
		//0 0 0 0 0
		//0 0 0 0 0
	}

	//add the current state signifier to the center
	DepMat[2][2] = 2;

	Dependency_Matrices.push_back(DepMat);
}

void MRF::FindTileTypes(string MapName){

	ifstream Input(MapName.c_str());
	string line;
	while(Input.fail()){
		cout<<"Failed to load the map called: "<<MapName<<endl;
		return;
	}
	while(!getline(Input, line).eof()){

		char tile;
		for(int i=0; i<line.length(); i++){
			bool new_tile = true;
			tile = line.at(i);
			for(int j=0; j<TileTypes.size(); j++){
				if(tile == TileTypes[j] || tile== '\r' || tile == '\n' || tile == '\t'){
					new_tile = false;
					break;
				}
			}
			if(new_tile)
				TileTypes.push_back(tile);
		}
	}
	Input.close();
}

void MRF::FindAllTileTypes(string MapNameBase, int NumberOfMaps){
	if(TileTypes.empty())
		TileTypes.push_back('S');
	
	for(int i=1; i<=NumberOfMaps; i++)
		FindTileTypes(MapNameBase+to_string(i)+".txt");
    
    /*for(int i=0; i<TileTypes.size(); i++){
        if(TileTypes[i] == '\t' || TileTypes[i] == '\n' || TileTypes[i] == '\r'){
            TileTypes.erase(TileTypes.begin()+i);
            i--;
        }
    }*/
}

void MRF::InitializeDistributions(){

	Totals.resize(Dependency_Complexity.size());
	Probabilities.resize(Dependency_Complexity.size());

	for(int i=0; i<Dependency_Complexity.size(); i++){
		int NumberOfConfigurations = 1;

		//find the total number of different configurations possible, given the number of tiles
		//and the number of surrounding tiles taken into account
		for(int j=0; j<Dependency_Complexity[i]; j++)
			NumberOfConfigurations*=TileTypes.size();

		//resize the matrices to the proper sizes
		Totals[i].resize(NumberOfConfigurations);
		Probabilities[i].resize(NumberOfConfigurations);
        
        //totals matrix gets one more column to hold the total number of times a config has been seen
		for(int j=0; j<NumberOfConfigurations; j++){
			Totals_Pair tmp;
			tmp.times_encountered=0;
			tmp.type='~';
			Totals[i][j].resize(1, tmp);

			for(int l=0; l<TileTypes.size(); l++){
				Probabilities_Pair tmp2;
				if(l==0)
					tmp2.type = '~';
				else
					tmp2.type = TileTypes[l];
				tmp2.probability = 0;
				Probabilities[i][j].push_back(tmp2);
			}
		}
	}
}


//deprecated
//This function is now completely useless (yay!)
int MRF::GetID(long long config_id, float NumberOfTypes){
	long long id=config_id;
	long long ID = 0;
	int i = 0;
	while (id != 0){
		ID += id % 100 * pow(NumberOfTypes, i);
		i++;
		id = floor(id / 100.0);
	}
	return (int)ID;
}


string MRF::GetConfigFromID(long long config_id, float NumberOfTypes, int Dependency_Complexity){
    
    long long id = config_id;
    string reverse_config="";
    string config="";
    
    for(int i=0; i<Dependency_Complexity; i++){
        reverse_config+=TileTypes[(int)id/(int)pow(NumberOfTypes,Dependency_Complexity-(i+1))];
        id = id%(int)pow(NumberOfTypes,Dependency_Complexity-(i+1));
    }
    
    for(int i=(int)reverse_config.length()-1; i>=0; i--){
        config+=reverse_config[i];
    }
    
    return config;
}


string MRF::GetFallbackStringFromFullString(string full_config, vector< vector<int> > Full_Dependency, vector< vector<int> >Fallback_Dependency){
    
    string fallback_config="";
    int string_index=0;
    for(int i=0; i<Full_Dependency.size(); i++){
        for(int j=0; j<Full_Dependency[i].size(); j++){
            
            if(Full_Dependency[i][j] == 1){
                if(Fallback_Dependency[i][j] == 1)
                    fallback_config+=full_config.at(string_index);
                string_index++;
            }
        }
    }
    return fallback_config;
}

void MRF::SetTotals(string MapName, int Dependency_Matrix){
	
	//open the map file
	ifstream Input(MapName.c_str());
	if(Input.fail()){
		cout<<"Failed to open map called: "<<MapName<<endl;
		return;
	}
	
	//read the map into a vector
	vector<string> Map;
	vector<string> tmp;
	string line;
	//top row of map will be the first index of the tmp vector
	while(!getline(Input, line).eof()){
		//remove any special end of line characters
		if(line.back() == '\n' || line.back() == '\t' || line.back() == '\r')
			line.pop_back();
		Map.push_back(line);
	}
    
    //grab the last line
    //getline(Input, line);
    //Map.push_back(line);
    
	//pop the tmp vector into the Map so that the order is more intuitive
	//bottom of map in first index, top of map in last index
	/*while(!tmp.empty()){
		Map.push_back(tmp.back());
		tmp.pop_back();
    }*/

    /*for(int h=0; h<Map.size();h++){
        for(int w=0; w<Map[h].size(); w++){
            cout<<Map[h][w];
        }
        cout<<endl;
    }
    cout<<endl;
    */
	//cout<<Map.size()<<" "<<Map[0].size()<<endl;
	//tmp.~vector();
	//loop through the map starting at the bottom row of the map
	for(int row=0; row<Map.size(); row++){
        //cout<<endl;
		for(int column=0; column<Map[row].length(); column++){

			int tile_id=-1;
			long long config_id=0;
			int current_tile_id=0;
			int power=0;

			//check the configuration of the surrounding tiles, given the dependency matrix
			for(int i=0; i<Dependency_Matrices[Dependency_Matrix].size(); i++){
				for(int j=0; j<Dependency_Matrices[Dependency_Matrix][i].size(); j++){

					//if the current tile depends on this neighboring tile
					if(Dependency_Matrices[Dependency_Matrix][i][j] == 1){
						
						//row+(2-i) gives the height of the neighboring tile in the map
						//column+(j-2) gives the column of the neighboring tile in the map
						//if checking for a tile that is out of bounds in any way, use the S(entinel) tile
                        if((row+i-2) >= Map.size() || (row+i-2) < 0 || (column+j-2) >= Map[row].length() || (column+j-2) < 0){
							tile_id = 0;
                        }

						//otherwise, find which tile type it corresponds to
						else{
							for(int t=0; t<TileTypes.size(); t++){
								if(Map[row+i-2][column+j-2] == TileTypes[t]){
									tile_id = t;
									break;
								}
							}
						}

						//add to the Configuration ID, in order to create a unique id for each configuration
                        if(tile_id>0)
                            config_id += tile_id*pow(TileTypes.size(), power);
						power++;
                        //cout<<TileTypes[tile_id];
					}
				}
			}
            //cout<<":";
            //cout<<GetConfigFromID(config_id, TileTypes.size(), 4)<<" ";

			//once the unique configuration id is identified,
			//find the type of the current tile
			for(int t=0; t<TileTypes.size(); t++){
				if(Map[row][column] == TileTypes[t]){
					current_tile_id = t;
					break;
				}
			}

			//determine if there exists a spot in the totals matrix for this configuration+tile type
			//returns the unique index into the matrix given the configuration id
            int index = (int)config_id;
            //int index = GetID(config_id, TileTypes.size());

			//cout<<index<<endl;
			bool encountered =false;
			for(int t=0; t<Totals[Dependency_Matrix][index].size(); t++){
				if(Totals[Dependency_Matrix][index][t].type == TileTypes[current_tile_id]){
					Totals[Dependency_Matrix][index][t].times_encountered++;
					encountered =true;
					break;
				}
			}

			//if the configuration+tile has not been encountered before, add it to the matrix
			if(!encountered){
				Totals_Pair tmp;
				tmp.times_encountered=1;
				tmp.type = TileTypes[current_tile_id];
				Totals[Dependency_Matrix][index].push_back(tmp);
			}

			//increment the total number of times this configuration has been seen
			Totals[Dependency_Matrix][index][0].times_encountered++;
		}
	}
}

void MRF::SetAllTotals(string MapNameBase, int Dependency_Matrix, int NumberOfMaps){

    srand((int)time(NULL));
    
    /*vector<int> Levels;
    for(int i=0; i<NumberOfMaps; i++){
        
        bool Choosing = true;
        int level;
        while(Choosing){
            level = (rand()%150)+1;
            Choosing = false;
            for(int i=0; i<Levels.size(); i++){
                if(level == Levels[i]){
                    Choosing=true;
                    break;
                }
            }
        }
        Levels.push_back(level);
        cout<<Levels.back()<<" ";
    }
    cout<<endl;
    */
    for(int i=1; i<=NumberOfMaps; i++){
        SetTotals(MapNameBase+to_string(i)+".txt", Dependency_Matrix);
    }
    
}

vector<vector<Probabilities_Pair>> MRF::CombineFallBack(int ComplexityOfInitial, vector<vector< int >> InitialDependencyMatrix, vector<vector<vector< int >>> FallbackMatrices, vector<vector<vector< Probabilities_Pair >>> FallbackProbs){
    
    int NumberOfConfigurations = 1;
    for(int j=0; j<ComplexityOfInitial; j++)
        NumberOfConfigurations*=TileTypes.size();
    
    vector<vector< Probabilities_Pair >> CombinedProbabilities(NumberOfConfigurations);
    
    for(int j=0; j<NumberOfConfigurations; j++){
        for(int i=0; i<TileTypes.size(); i++){
            Probabilities_Pair tmp;
            tmp.probability=0;
            tmp.type=TileTypes[i];
            CombinedProbabilities[j].push_back(tmp);
        }
    }
    
    ofstream Probs1("Probs1.txt");
    ofstream Probs2("Probs2.txt");
    ofstream ProbsTotals("ProbsTotals.txt");
    
    for(int i=0; i<NumberOfConfigurations; i++){

        //get the full configuration corresponding to the index into the full array
        string config = GetConfigFromID(i, TileTypes.size(), ComplexityOfInitial);
        
        for(int y=0; y<TileTypes.size(); y++){
            float combined_probs_union =1;
            float combined_probs_intersection=0;
            for(int j=0;j<FallbackMatrices.size(); j++){
                
                //get fallback config from the full config
                string fb_config = GetFallbackStringFromFullString(config, InitialDependencyMatrix, FallbackMatrices[j]);
            
                //get the fallback id from the fallback config
                int fb_index=0;
                int power=0;
                for(int k=0;k<fb_config.length(); k++){
                    for(int l=0;l<TileTypes.size(); l++){
                        if(fb_config.at(k) == TileTypes[l]){
                            fb_index+=l*pow(TileTypes.size(), power);
                            power++;
                            break;
                        }
                    }
                }
            
                /*if(y==0){
                    if(j==0)
                        Probs1<<FallbackProbs[j][fb_index][0].type<<": "<<FallbackProbs[j][fb_index][0].probability<<" ";
                    else
                        Probs2<<FallbackProbs[j][fb_index][0].type<<": "<<FallbackProbs[j][fb_index][0].probability<<" ";
                }*/
                
                char type = CombinedProbabilities[i][y].type;
                bool found=false;
                for(int t=0; t<FallbackProbs[j][fb_index].size(); t++){
                    if(FallbackProbs[j][fb_index][t].type == type){
                        combined_probs_union*=FallbackProbs[j][fb_index][t].probability;
                        //combined_probs_union*=((FallbackTotals[j][fb_index][t].times_encountered+1.0)/(float)(FallbackTotals[j][fb_index][0].times_encountered+TileTypes.size()));
                        //if(!Union)
                        //    combined_probs_intersection+=((FallbackTotals[j][fb_index][t].times_encountered+1.0)/(float)(FallbackTotals[j][fb_index][0].times_encountered+TileTypes.size()));
                        
                        if(j==0)
                            Probs1<<FallbackProbs[j][fb_index][t].type<<": "<<FallbackProbs[j][fb_index][t].probability<<" ";
                        else
                            Probs2<<FallbackProbs[j][fb_index][t].type<<": "<<FallbackProbs[j][fb_index][t].probability<<" ";
 
                        found = true;
                        break;
                    }
                }
                if(!found){
                    combined_probs_union*=(1.0/TileTypes.size());
                    //if(!Union)
                    //    combined_probs_intersection+= (1.0/TileTypes.size());
                    
                    if(j==0)
                        Probs1<<type<<": "<<(1.0/TileTypes.size())<<" ";
                    else
                        Probs2<<type<<": "<<(1.0/TileTypes.size())<<" ";
                }
            }
            //if(Union)
            //    CombinedProbabilities[i][y].probability = combined_probs_union;
            //else
            CombinedProbabilities[i][y].probability = combined_probs_intersection - combined_probs_union;
        }
        
        Probs1<<endl;
        Probs2<<endl;
    }
    Probs1.close();
    Probs2.close();
    
    for(int i=0;i<CombinedProbabilities.size(); i++){
        float total=0;
        for(int j=1; j<CombinedProbabilities[i].size(); j++){
            total+=CombinedProbabilities[i][j].probability;
            //ProbsTotals<<CombinedProbabilities[i][j].type<<": "<<CombinedProbabilities[i][j].probability<<" ";
            //CombinedProbabilities[i][j].probability/=CombinedProbabilities[i][0].probability;
        }
        for(int j=1; j<CombinedProbabilities[i].size(); j++){
            CombinedProbabilities[i][j].probability/=total;
            ProbsTotals<<CombinedProbabilities[i][j].type<<": "<<CombinedProbabilities[i][j].probability<<" ";
        }
        ProbsTotals<<endl;
    }
    ProbsTotals.close();
    return CombinedProbabilities;
}


void MRF::SetProbabilities(int Dependency_Matrix){
    
    if(Dependency_Complexity[Dependency_Matrix] == 0){
        for(int i=1; i<Totals[Dependency_Matrix][0].size(); i++){
            Probabilities_Pair tmp;
            tmp.type = Totals[Dependency_Matrix][0][i].type;
            tmp.probability = ((float)(Totals[Dependency_Matrix][0][i].times_encountered)/(float)(Totals[Dependency_Matrix][0][0].times_encountered));
            for(int j=0; j<Probabilities[Dependency_Matrix][0].size(); j++){
                if(tmp.type == Probabilities[Dependency_Matrix][0][j].type)
                    Probabilities[Dependency_Matrix][0][j].probability = tmp.probability;
            }
        }
        return;
    }

	//for each configuration and tile, divide the total number of times that configuration
	//is seen by the number of times that tile seen following it
	for(int i=0; i<Totals[Dependency_Matrix].size(); i++){

		//if nothing has been seen in that configuration, leave that entry empty, otherwise find the probability
		for(int j=1; j<Totals[Dependency_Matrix][i].size(); j++){
            Probabilities_Pair tmp;
            tmp.type = Totals[Dependency_Matrix][i][j].type;
            tmp.probability = 0;
            
            for(int t=0; t<Totals[Dependency_Matrix][i].size();t++){
                if(Totals[Dependency_Matrix][i][t].type == tmp.type){
                    tmp.probability = (((float)Totals[Dependency_Matrix][i][t].times_encountered)/((float)Totals[Dependency_Matrix][i][0].times_encountered));
                    break;
                }
            }
            for(int k=0; k<Probabilities[Dependency_Matrix][i].size(); k++){
                if(Probabilities[Dependency_Matrix][i][k].type == tmp.type)
                    Probabilities[Dependency_Matrix][i][k].probability = tmp.probability;
                    //.push_back(tmp);
            }
        }
    }
    
    //ADD A SMALL CONSTANT TO AVOID 0S IN THE DISTRIBUTION
    for(int i=0; i<Probabilities[0].size(); i++){
        for(int j=0; j<Probabilities[0][i].size(); j++){
            if(Probabilities[0][i][j].probability == 0)
                Probabilities[0][i][j].probability = .0001;
        }
    }
    
    ofstream Output("MRFprobs.txt");
    for(int i=0; i<Probabilities[Dependency_Matrix].size(); i++){
        Output<<GetConfigFromID(i, TileTypes.size(), 4)<<": ";
        for(int j=0; j<Probabilities[Dependency_Matrix][i].size(); j++){
            Output<<Probabilities[Dependency_Matrix][i][j].type<<": "<<Probabilities[Dependency_Matrix][i][j].probability<<", ";
        }
        Output<<endl;
    }
    Output.close();
}

void MRF::GenerateLevel(string MapName, int width, int height, int Threshold, int Dependency_Matrix){

	vector< vector<char> > Map;
	//ofstream Likelihoods("TCIAIG_Experiments/MRF/KidIcarus/NoSmoothing/likelihoods_1.dat");
    
	Map.resize(height);
	for(int i=0; i<height; i++)
		Map[i].resize(width);

	//start with a randomly generated map, using the distibution of tiles in the training maps
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			//do not place any sentinel tiles in the map
			int selector = (rand()%10000)+1;
			int total=0;
			int prob_index=(int)Probabilities.back()[0].size()-1;
			//assume the last set of probabilities corresponds to the raw distribution of tiles
			for(int k=0; k<Probabilities.back()[0].size(); k++){
				total+=Probabilities.back()[0][k].probability*10000;
				if(total >= selector){
					prob_index = k;
					break;
				}
			}
			Map[i][j] = Probabilities.back()[0][prob_index].type;
		}
	}
    
    double total_likelihood = MapLikelihood(Map);
    double total_likelihood_2 = total_likelihood;
    //Likelihoods<<"0 "<<total_likelihood<<endl;
    
    int generated=0;
    int iter=0;
    vector<vector<int> > Chosen;
    
    while(true/*generated<Threshold*/){
        
        Chosen.clear();
        for(int v=0; v<width; v++){
            for(int v2=0; v2<height; v2++){
                vector<int> point(2);
                point[0] = v;
                point[1] = v2;
                Chosen.push_back(point);
            }
        }
        
        while(Chosen.size()>1){
        	//choose 2 random positions
            int selected = rand()%Chosen.size();
            int h1 = Chosen[selected][1];
            int w1 = Chosen[selected][0];
            Chosen.erase(Chosen.begin()+selected);
            
            selected = rand()%Chosen.size();
            int h2 = Chosen[selected][1];
            int w2 = Chosen[selected][0];
            Chosen.erase(Chosen.begin()+selected);
            
            double pre_swap_likelihood=total_likelihood;
            
            //perform a temporary swap, and compute
            char tmp = Map[h1][w1];
            Map[h1][w1] = Map[h2][w2];
            Map[h2][w2] = tmp;
            
            double post_swap_likelihood=MapLikelihood(Map);

            double a = ((double) rand())/(RAND_MAX);
            //if swapped likelihood is higher, keep swapped
                //if likelihoods is the same, keep swapped with %50 chance
                //if swapped likelihood is less, then keep swapped with %5 chance (constant for now)
            //otherwise, swap them back
            bool accepted=false;
            double ps = exp(post_swap_likelihood-pre_swap_likelihood);
            
            if(a<=ps){// && !isnan(post_swap_likelihood)){
                accepted = true;
                total_likelihood = post_swap_likelihood;
            } else{ //swap them back
                char tmp = Map[h1][w1];
                Map[h1][w1] = Map[h2][w2];
                Map[h2][w2] = tmp;
            }
            iter++;
            //Likelihoods<<iter<<" "<<post_swap_likelihood<<endl;
            
            //cout<<iter<<endl;
            //accounts for the burn in period and the sampling distance
            if(iter>=Threshold){// && (iter%200000)==0 ){
                //ofstream Output("TCIAIG_Experiments/MRF/Loderunner/NoSmoothing/Sample "+to_string(Threshold)+".txt");
                ofstream Output(MapName);
                for(int i=0; i<height; i++){
                    for(int j=0; j<width; j++){
                        Output<<Map[i][j];
                    }
                    Output<<endl;
                }
                Output.close();
                generated++;
                return;
            }
        }
    }
}

vector<vector<Probabilities_Pair>> MRF::SmoothMatrix(vector<vector<Totals_Pair>> ToSmooth, vector<vector<Probabilities_Pair>> Smoother){
    
    ofstream P1("ToSmooth.txt");
    ofstream P2("Smoother.txt");
    ofstream P3("Smoothed.txt");
    
    vector<vector<Probabilities_Pair>> ret;
    ret.resize(Smoother.size());
    for(int i=0; i<Smoother.size(); i++){
        for(int j=1; j<Smoother[i].size(); j++){
            
            char type = Smoother[i][j].type;
            bool found=false;
            
            P2<<type<<": "<<Smoother[i][j].probability<<" ";
            
            for(int t=1; t<ToSmooth[i].size(); t++){
                if(type == ToSmooth[i][t].type){
                    P1<<type<<": "<<ToSmooth[i][t].times_encountered<<" ";
                    float smoothed_prob = ((ToSmooth[i][t].times_encountered + Smoother[i][j].probability)/(ToSmooth[i][0].times_encountered+1.0));
                    Probabilities_Pair tmp;
                    tmp.probability = smoothed_prob;
                    tmp.type = type;
                    ret[i].push_back(tmp);
                    P3<<type<<": "<<tmp.probability<<" ";
                    found=true;
                    break;
                }
            }
            if(!found){
                P1<<type<<": "<<0<<" ";
                Probabilities_Pair tmp;
                tmp.probability = Smoother[i][j].probability;
                tmp.type = type;
                ret[i].push_back(tmp);
                P3<<type<<": "<<tmp.probability<<" ";
            }
        }
        P1<<endl;
        P2<<endl;
        P3<<endl;
    }
    
    P1.close();
    P2.close();
    P3.close();
    
    return ret;
}

vector<vector<Probabilities_Pair>> MRF::SmoothAll(){
    
    vector<vector<vector<int>>> Complexity2Fallbacks;
    vector<vector<vector<int>>> Complexity1Fallbacks;
    vector<vector<vector<int>>> Complexity0Fallbacks;
    vector<vector<vector<Probabilities_Pair>>> Complexity2Probs;
    vector<vector<vector<Probabilities_Pair>>> Complexity1Probs;
    vector<vector<vector<Probabilities_Pair>>> Complexity0Probs;
    
    
    //Complexity 0
    //Complexity0Fallbacks.push_back(Dependency_Matrices.back());
    //Complexity0Probs.push_back(ConvertRawToProperSize(Totals[4].size()));
    
    //Complexity 1
    //for(int i=3; i<7; i++){
    //    Complexity1Probs.push_back(SmoothMatrix(Totals[i], ConvertRawToProperSize((int)Totals[4].size())));
    //    Complexity1Fallbacks.push_back(Dependency_Matrices[i]);
    //}
    
    
    
    //Complexity 2
    for(int i=1; i<3; i++){
        Complexity2Probs.push_back(SmoothMatrix(Totals[i], CombineFallBack(2, Dependency_Matrices[i], Complexity1Fallbacks, Complexity1Probs)));
        Complexity2Fallbacks.push_back(Dependency_Matrices[i]);
    }
    
    return SmoothMatrix(Totals[0], CombineFallBack(4, Dependency_Matrices[0], Complexity2Fallbacks, Complexity2Probs));
}

vector<vector<Probabilities_Pair>> MRF::ConvertRawToProperSize(int NumberOfConfigurations){
    
    vector<vector<Probabilities_Pair>> ret;
    for(int i=0 ;i<NumberOfConfigurations; i++)
        ret.push_back(Probabilities.back()[0]);
    
    return ret;
}

void MRF::LoadMaps(string fileName, int NumberOfMaps){
    
    for(int i=1; i<NumberOfMaps+1; i++){
        ifstream InputMap(fileName+to_string(i)+".txt");
        if(!InputMap.is_open()){
            cout<<fileName+to_string(i)+".txt could not be opened"<<endl;
            continue;
        }
        
        vector<vector<char> > Map;
        string line;
        while(getline(InputMap, line)){
            vector<char> row;
            for(int j=0; j<line.length(); j++){
                if(line[j]!= '\r' && line[j] != '\n' && line[j] != '\t')
                    row.push_back(line[j]);
            }
            Map.push_back(row);
        }
        Maps.push_back(Map);
    }
}

//want to implement a distance function so that it is able to force matches when applicable


vector<float> MRF::FindInstances(vector<vector<char> > Prototype, vector<vector<float> > DistanceMatrix){
    
    //never generate a sentinel tile
    vector<float> histogram(TileTypes.size(), 0);
    float normalizingFactor = (Prototype.size()*Prototype[0].size()) - 1;
    int windowWidth = (int)Prototype.size()/2;
    //for every map
    for(int i=0; i<Maps.size(); i++){
        //for each tile in that map
        for(int mapRow=0; mapRow<Maps[i].size(); mapRow++){
            for(int mapCol=0; mapCol<Maps[i][mapRow].size(); mapCol++){
                
                //find the current tile, and its index into the tiletypes array for future use
                char currentTile = Maps[i][mapRow][mapCol];
                int currTileIndex=-1;
                for(int t=0; t<TileTypes.size();t++){
                    if(currentTile == TileTypes[t]){
                        currTileIndex = t;
                        break;
                    }
                }
                //check the prototype against the section of the map
                float matchingFactor=0;
                float unfilledSection=0;
                for(int rowOffset =-1*windowWidth; rowOffset<=windowWidth; rowOffset++){
                    for(int colOffset=-1*windowWidth; colOffset<=windowWidth; colOffset++){
                        if(rowOffset == 0 && colOffset == 0)
                            continue;
                        char mapTile;
                        char prototypeTile = Prototype[rowOffset+windowWidth][colOffset+windowWidth];
                        if((mapRow+rowOffset) < 0 || (mapCol+colOffset) < 0 || (mapRow+rowOffset) >= Maps[i].size() || (mapCol+colOffset) >= Maps[i][0].size())
                            mapTile = 'S';
                        else
                            mapTile = Maps[i][mapRow+rowOffset][mapCol+colOffset];
                       
                        int pIndex=-1;
                        int mIndex=-1;
                        for(int t=0; t<TileTypes.size(); t++){
                            if(mapTile == TileTypes[t]){
                                mIndex = t;
                            }
                            if(prototypeTile == TileTypes[t]){
                                pIndex = t;
                            }
                        }
                        
                        //if the tile was not found, then it must be 'X' representing unfilled space
                        if(pIndex == -1){
                            unfilledSection++;
                            matchingFactor+=1;
                        } else
                            matchingFactor+=DistanceMatrix[mIndex][pIndex];
                        
                    }
                }
                if(unfilledSection < normalizingFactor){
                    //cout<<matchingFactor<< " ";
                    //scale by the size of the window
                    matchingFactor/=normalizingFactor;
                    if(matchingFactor > .9){
                        //then scale by how much of that window is actually filled in
                        //matchingFactor*=((normalizingFactor-unfilledSection)/unfilledSection);
                        //cout<<matchingFactor<<endl;
                        histogram[currTileIndex]+=matchingFactor;
                    }
                }
            }
        }
    }
    
    float total=0;
    for(int i=0; i<histogram.size(); i++){
        total+=histogram[i];
    }
    for(int i=0; i<histogram.size(); i++){
        //cout<<histogram[i]<<" ";
        histogram[i]/=total;
        //cout<<histogram[i]<<" ";
    }
    //cout<<endl;
    
    return histogram;
}

vector<vector<char> > MRF::InitializeMap(int SizeOfSeed, int mapWidth, int mapHeight){
    
    vector<vector<char> > ToGenerate;
    ToGenerate.resize(mapHeight);
    for(int i=0; i<mapHeight; i++)
        ToGenerate[i].resize(mapWidth, 'X');

    //set the seed area, and set the rest of the map to X, representing empty
    int mapNumber = rand()%(Maps.size());
    for(int h=(int)(ToGenerate.size()/2)-(SizeOfSeed/2); h<((ToGenerate.size()/2)+(SizeOfSeed/2)); h++){
        for(int w=(int)(ToGenerate[h].size()/2)-(SizeOfSeed/2); w<((ToGenerate[h].size()/2)+(SizeOfSeed/2)); w++)
            ToGenerate[h][w] = Maps[mapNumber][h][w];
    }
    
    return ToGenerate;
}

vector<vector<char> > MRF::GetPrototype(int windowSize, vector<vector<char> > ToGenerate, int rowIndex, int colIndex){
    vector<vector<char> > Prototype(windowSize*2+1);
    for(int i=0; i<windowSize*2+1; i++)
        Prototype[i].resize(windowSize*2+1);
    
    for(int row = rowIndex-windowSize; row<=rowIndex+windowSize; row++){
        for(int col = colIndex-windowSize; col<=colIndex+windowSize; col++){
            if(row<0 || row>=ToGenerate.size() || col < 0 || col >= ToGenerate[row].size()){
                Prototype[row - (rowIndex-windowSize)][col - (colIndex - windowSize)] = 'S';
            }
            else
                Prototype[row - (rowIndex-windowSize)][col - (colIndex - windowSize)] = ToGenerate[row][col];
        }
    }
    /*for(int i=0; i<Prototype.size(); i++){
        for(int j=0; j<Prototype[i].size(); j++)
            cout<<Prototype[i][j];
        cout<<endl;
    }*/
    
    return Prototype;
}

void MRF::SampleMap(int windowWidth, int mapWidth, int mapHeight, int SizeOfSeed, int RandomOffset, vector<vector<float> > DistanceMatrix){
    
    srand((int)time(NULL)+RandomOffset);
    vector<vector<char> > ToGenerate;
    ToGenerate = InitializeMap(SizeOfSeed, mapWidth, mapHeight);
    //int SizeFilled = SizeOfSeed;
    
    int RowFilled =0;
    int ColFilled=0;
    while(true){
        
        int TooBig=0;
        //as long as we are generating within the bounds of the new map
        if(((int)(ToGenerate[0].size()/2)-(SizeOfSeed/2)-ColFilled-1) >= 0 && ((int)(ToGenerate[0].size()/2)+(SizeOfSeed/2)+ColFilled) <ToGenerate[0].size()) {
            for(int h=(int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled; h<(int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled; h++){
                //column left of the filled area
                vector<float> Probs = FindInstances(GetPrototype(windowWidth, ToGenerate, h, (int)(ToGenerate[h].size()/2)-(SizeOfSeed/2)-ColFilled-1), DistanceMatrix);
                int choice = rand()%10000;
                float total=0;
                for(int i=0; i<Probs.size(); i++){
                    total+=Probs[i]*10000;
                    if(choice <= total){
                        ToGenerate[h][(int)(ToGenerate[h].size()/2)-(SizeOfSeed/2)-ColFilled-1] = TileTypes[i];
                        break;
                    } else if(i == (Probs.size()-1) && total>0)
                        ToGenerate[h][(int)(ToGenerate[h].size()/2)-(SizeOfSeed/2)-ColFilled-1] = TileTypes[i];
                }
                //if a tile could not be chosen, choose at random
                if(ToGenerate[h][(int)(ToGenerate[h].size()/2)-(SizeOfSeed/2)-ColFilled-1] == 'X'){
                    choice = rand()%(TileTypes.size()-1);
                    ToGenerate[h][(int)(ToGenerate[h].size()/2)-(SizeOfSeed/2)-ColFilled-1] = TileTypes[choice+1];
                }
            
                //column right of the filled area
                Probs = FindInstances(GetPrototype(windowWidth, ToGenerate, h, (int)(ToGenerate[h].size()/2)+(SizeOfSeed/2)+ColFilled), DistanceMatrix);
                choice = rand()%10000;
                total=0;
                for(int i=0; i<Probs.size(); i++){
                    total+=Probs[i]*10000;
                    if(choice <=total){
                        ToGenerate[h][(int)(ToGenerate[h].size()/2)+(SizeOfSeed/2)+ColFilled] = TileTypes[i];
                        break;
                    } else if(i == (Probs.size()-1) && total>0)
                        ToGenerate[h][(int)(ToGenerate[h].size()/2)+(SizeOfSeed/2)+ColFilled] = TileTypes[i];
                }
                //if a tile could not be chosen, choose at random
                if(ToGenerate[h][(int)(ToGenerate[h].size()/2)+(SizeOfSeed/2)+ColFilled] == 'X'){
                    choice = rand()%(TileTypes.size()-1);
                    ToGenerate[h][(int)(ToGenerate[h].size()/2)+(SizeOfSeed/2)+ColFilled] = TileTypes[choice+1];
                }
            }
            ColFilled++;
        } else
            TooBig++;
        
        if(((int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1) >= 0 && ((int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled) < ToGenerate.size()){
            for(int h=(int)(ToGenerate[0].size()/2)-(SizeOfSeed/2)-ColFilled; h<(ToGenerate[0].size()/2)+(SizeOfSeed/2)+ColFilled; h++){
                //row above the filled area
                vector<float> Probs = FindInstances((GetPrototype(windowWidth, ToGenerate, (int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1, h)), DistanceMatrix);
                int choice = rand()%10000;
                float total=0;
                for(int i=0; i<Probs.size(); i++){
                    total+=Probs[i]*10000;
                    if(choice <= total){
                        ToGenerate[(int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1][h] = TileTypes[i];
                        break;
                    } else if(i == (Probs.size()-1) && total>0)
                        ToGenerate[(int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1][h] = TileTypes[i];
                }
                //if a tile could not be chosen, choose at random
                if(ToGenerate[(int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1][h] == 'X'){
                    choice = rand()%(TileTypes.size()-1);
                    ToGenerate[(int)(ToGenerate.size()/2)-(SizeOfSeed/2)-RowFilled-1][h] = TileTypes[choice+1];
                }
            
                //row below the filled area
                Probs = FindInstances((GetPrototype(windowWidth, ToGenerate, (int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled, h)), DistanceMatrix);
                choice = rand()%10000000;
                total=0;
                for(int i=0; i<Probs.size(); i++){
                    total+=Probs[i]*10000000;
                    if(choice <=total){
                        ToGenerate[(int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled][h] = TileTypes[i];
                        break;
                    } else if(i == (Probs.size()-1) && total>0)
                        ToGenerate[(int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled][h] = TileTypes[i];
                }
                //if a tile could not be chosen, choose at random
                if(ToGenerate[(int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled][h] == 'X'){
                    choice = rand()%(TileTypes.size()-1);
                    ToGenerate[(int)(ToGenerate.size()/2)+(SizeOfSeed/2)+RowFilled][h] = TileTypes[choice+1];
                }
            }
            RowFilled++;
        } else
            TooBig++;

        for(int q=0; q<ToGenerate.size(); q++){
            for(int e=0; e<ToGenerate[q].size(); e++)
                cout<<ToGenerate[q][e];
            cout<<endl;
        }
        cout<<endl;
        cout<<"====================================="<<endl;
        cout<<endl;
        
        if(TooBig == 2)
            break;
    }
    
    ofstream Output("New_Sampling_TestMap"+to_string(RandomOffset)+".txt");
    for(int i=0; i<ToGenerate.size(); i++){
        for(int j=0; j<ToGenerate[i].size(); j++)
            Output<<ToGenerate[i][j];
        Output<<endl;
    }
    Output.close();
}

long double MRF::NewComputeNeighborhoodLikelihood(int x, int y, vector< vector<char> > Map){
    
    //assumes a network structure of:
    //      010
    //      121
    //      010
    //And checks all the 5X5 surrounding tiles, i.e.:
    
    long double likelihood =1;
    
    for(int y_pos = y-2; y_pos<=y+2; y_pos++){
        for(int x_pos = x-2; x_pos<=x+2; x_pos++){
            
            
            int tile_id =-1;
            int config_id =0;
            int power =0;
            //check the configuration of the surrounding tiles, given the dependency matrix
            for(int i=0; i<Dependency_Matrices[0].size(); i++){
                for(int j=0; j<Dependency_Matrices[0][i].size(); j++){
                    
                    //if the current tile depends on this neighboring tile
                    if(Dependency_Matrices[0][i][j] == 1){
                        
                        //row+(2-i) gives the height of the neighboring tile in the map
                        //column+(j-2) gives the column of the neighboring tile in the map
                        //if checking for a tile that is out of bounds in any way, use the S(entinel) tile
                        if((y_pos+i-2) >= Map.size() || (y_pos+i-2) < 0 || (x_pos+j-2) >= Map[y_pos].size() || (x_pos+j-2) < 0){
                            tile_id = 0;
                        }
                        
                        //otherwise, find which tile type it corresponds to
                        else{
                            for(int t=0; t<TileTypes.size(); t++){
                                if(Map[y_pos+i-2][x_pos+j-2] == TileTypes[t]){
                                    tile_id = t;
                                    break;
                                }
                            }
                        }
                        
                        //add to the Configuration ID, in order to create a unique id for each configuration
                        if(tile_id>0)
                            config_id += tile_id*pow(TileTypes.size(), power);
                        power++;
                    }
                }
            }
            
            int t_id=-1;
            if(y_pos >= Map.size() || y_pos < 0 || x_pos >=Map[0].size() || x_pos < 0)
                likelihood *= .001;
            else{
                char tile = Map[y_pos][x_pos];
                for(int t=0; t<TileTypes.size(); t++){
                    if(tile == TileTypes[t]){
                        t_id = t;
                        break;
                    }
                }
                likelihood *= Probabilities[0][config_id][t_id].probability;
            }
        }
    }
    
    return likelihood;
}



float MRF::ComputeNeighborhoodLikelihood(int x, int y, vector<vector< char> > Map){
    
    long double likelihood=1;

    //use the outer loops to determine the "current position" in the neighborhood
    for(int k=0; k<Dependency_Matrices[0].size(); k++){
        for(int l=0; l<Dependency_Matrices[0][k].size(); l++){
            
            int tmpX=-1;
            int tmpY=-1;
            //check any positions and surrounding positions
            if(Dependency_Matrices[0][k][l] != 0 || ( (k-1 >=0) && Dependency_Matrices[0][k-1][l] != 0) || ( (l-1 >=0) && Dependency_Matrices[0][k][l-1] != 0) || ( (k+1 <Dependency_Matrices[0].size()) && Dependency_Matrices[0][k+1][l] != 0) || ( (l+1 < Dependency_Matrices[0][k].size()) && Dependency_Matrices[0][k][l+1] != 0)){
                tmpX = x+l-2;
                tmpY = y+k-2;
            } else
                continue;
            
            int tile_id=-1;
            int config_id=0;
            int power=0;
    
            for(int i=0; i<Dependency_Matrices[0].size(); i++){
                for(int j=0; j<Dependency_Matrices[0][i].size(); j++){
            
                    //if it is a used position, check the probability of that tile
                    if(this->Dependency_Matrices[0][i][j] == 1){
                
                        //get the configuration of that tile's position
                        //row+(2-i) gives the height of the neighboring tile in the map
                        //column+(j-2) gives the column of the neighboring tile in the map
                        //if checking for a tile that is out of bounds in any way, use the S(entinel) tile
                        if((tmpY+i-2) >= Map.size() || (tmpY+i-2) < 0 || (tmpX+j-2) >= Map[tmpY].size() || (tmpX+j-2) < 0)
                            tile_id = 0;
                
                        //otherwise, find which tile type it corresponds to
                        else{
                            for(int t=0; t<TileTypes.size(); t++){
                                if(Map[tmpY+i-2][tmpX+j-2] == TileTypes[t]){
                                    tile_id = t;
                                    break;
                                }
                            }
                        }
                
                        //add to the Configuration ID, in order to create a unique id for each configuration
                        config_id += tile_id*pow(TileTypes.size(), power);
                        power++;
                
                    }
                }
            }

            int t_id=-1;
            
            //if the tile is off the map, we have no data for it, so skip
            if(tmpY >= Map.size() || tmpY < 0 || tmpX >=Map[0].size() || tmpX < 0)
                likelihood += log(.001);
            else{
                char tile = Map[tmpY][tmpX];
                for(int t=0; t<TileTypes.size(); t++){
                    if(tile == TileTypes[t]){
                        t_id = t;
                        break;
                    }
                }
                likelihood += log(Probabilities[0][config_id][t_id].probability);
            }
        }
    }
    return likelihood;
}

float MRF::MapLikelihood(vector<vector<char> > Map){
    long double likelihood=0;
    for(int tmpY=0; tmpY<Map.size(); tmpY++){
        for(int tmpX=0; tmpX<Map[tmpY].size(); tmpX++){
            
            int tile_id=-1;
            int config_id=0;
            int power=0;
            
            for(int i=0; i<Dependency_Matrices[0].size(); i++){
                for(int j=0; j<Dependency_Matrices[0][i].size(); j++){
                    
                    //if it is a used position, check the probability of that tile
                    if(this->Dependency_Matrices[0][i][j] == 1){
                        
                        //get the configuration of that tile's position
                        //row+(2-i) gives the height of the neighboring tile in the map
                        //column+(j-2) gives the column of the neighboring tile in the map
                        //if checking for a tile that is out of bounds in any way, use the S(entinel) tile
                        if((tmpY+i-2) >= Map.size() || (tmpY+i-2) < 0 || (tmpX+j-2) >= Map[tmpY].size() || (tmpX+j-2) < 0)
                            tile_id = 0;
                        
                        //otherwise, find which tile type it corresponds to
                        else{
                            for(int t=0; t<TileTypes.size(); t++){
                                if(Map[tmpY+i-2][tmpX+j-2] == TileTypes[t]){
                                    tile_id = t;
                                    break;
                                }
                            }
                        }
                        //add to the Configuration ID, in order to create a unique id for each configuration
                        config_id += tile_id*pow(TileTypes.size(), power);
                        power++;
                    }
                }
            }
            
            int t_id=-1;
            char tile = Map[tmpY][tmpX];
            for(int t=0; t<TileTypes.size(); t++){
                if(tile == TileTypes[t]){
                    t_id = t;
                    break;
                }
            }

            if(Probabilities[0][config_id][t_id].probability == 0)
                likelihood += log(.001);
            else
                likelihood += log(Probabilities[0][config_id][t_id].probability);

            /*if(isnan(likelihood)){
                cout<<likelihood<<" "<<config_id<<" "<<t_id<<" "<<Probabilities[0][config_id][t_id].probability<<" "<<log(Probabilities[0][config_id][t_id].probability)<<endl;
                int tmpl = likelihood*config_id;
                cout<<tmpl<<endl;
            }*/
            //cout<<likelihood<<" "<<log(Probabilities[0][config_id][t_id].probability)<<" "<<Probabilities[0][config_id][t_id].probability<<" "<<config_id<<endl;
        }
    }
    //cout<<likelihood<<endl;
    return likelihood;
}

