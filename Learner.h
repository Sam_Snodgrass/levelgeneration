#pragma once
#include "CommonStructs.h"
#include<vector>

class Learner{
    
public:
    
    vector< vector< vector <float> > > Probabilities;
    
    Configuration Config;
    
    Learner(int RowSplits, int LookAhead, bool TopDown, bool Hierarchical);
    
    void SetDepMatrix(int DepMatrix);
    void InitFiles(string TotName, string HierTotName, string ProbName, string HierProbName, bool Hier, int WhichDepMat);
    void FindTileTypes(string MapName, bool Hier);
    void FindAllTileTypes(int NumMaps, string MapNameBase, bool Hier);
    void SetTotalsNonHierarchical(string MapName, string TotName, int WhichRowSplit, int WhichDepMat);
    void SetTotalsTopLevel(string MapName, int trainingMaps, string TotName, bool Hier, int WhichDepMat);
    void SetTotalsTopLevelColumn(string MapName, int trainingMaps, string TotName, bool Hier, int WhichDepMat);
    void SetTotalsBottomLevel(string HierMapName, string MapName, string TotName, int WhichDepMat, bool manualConversion);
    void SetProbs(string ProbName, bool Hier, int WhichDepMat);
    void SetProbsForOnlyLowLevel(string ProbName, bool Hier, int WhichDepMat);
    void SetSmoothedProbabilities(string file, int Dependency_Matrix);
    void SetRows(bool Hier);
    void ResizeVectors(bool Hier);
    string GetConfigFromID(long long config_id, float NumberOfTypes, int Dependency_Complexity);
    string GetFallbackStringFromFullString(string full_config, vector< vector<int> > Full_Dependency, vector< vector<int> >Fallback_Dependency);
    vector< char > TileTypes;
    vector< char > HierTileTypes;
    vector< vector< vector <int> > > Totals;
    
    
private:
    
    //additional level to account for multiple matrices
    vector< vector< vector< vector <int> > > > HierTotals;
    vector< vector< vector< vector <float> > > > HierProbabilities;
    
    //returns the placement of a certain config within the matrix
    int GetID(int type, float NumTypes);
    
    //returns the type of tile
    char GetType(int id, bool Hier);
};