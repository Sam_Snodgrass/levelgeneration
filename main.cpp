#include<fstream>
#include<ctime>
#include<chrono>
#include<vector>
#include<iostream>
#include<sstream>
#include<sys/stat.h>
#include<cmath>
#include<gsl/gsl_fit.h>
#include<map>
#include<algorithm>
#include "CImg.h"
#include <random>

#include "MapReader.h"
#include "Learner.h"
#include "Generator.h"
#include "Clustering.h"
#include "MRF.h"
#include "ConstraintChecker.h"

using namespace std;

//Domain Adaptation Functions

//finds and returns the top n mappings between the source and target domain based on the relative tile frequency
vector<pair<int, float>> CompareRelativeTileFrequency(string sourceDomainFileName, string targetDomainFileName, int numSourceMaps, int numTargetMaps, string outputFileName, int mappingsToReturn);

void ConvertSourceToTarget(vector<char> mapping, string sourceDomainFileName, int numSourceMaps, string convertedFileName);
pair<int, int> FindCommonTileMappings(vector<char> mappingAtoB, vector<char> mappingBtoA, vector<char> tilesA, vector<char> tilesB);

//various evaluation metrics
int FindBadPipes(string filename);
bool IsGroundMisplaced(string MapName);
double ComputeLinearity(string MapFile, string PointsFile);
double ComputeLeniency(string MapFile);
void ComputeLinVSLen(string folder, string CSVfileName, string model, string mapNameBase, int maps);
pair<long double, double> MapLikelihood(vector<vector<char> > Map, Learner& model);
pair<long double, int> MapLikelihoodConfig(vector<vector<char> > Map, vector<vector<int> > depMatrix, vector<char> tileTypes, vector<float> configProbs);
vector<float> ComputeProbabilitiesConfigurations(vector<vector<int> > depMat, vector<char> tileTypes, int complexity, int numTileTypes, int maps, string mapFolder, string mapName);
int MeasurePlagiarism(string generatedMapName, string trainingMapName);

//bool LoderunnerPlayable(string FileName);
int CountPlayers(string FileName);
//bool KidIcarusPlayability(string Folder, string MapName);

//trains an MdMC on the provided training data, and then samples new maps using the trained model
pair<double, float> MdMCExperiments(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, Learner model, int startingMap);

//trains an MdMC on the provided training data, and then samples new maps using the trained model, while enforcing constraints
void MdMCExperimentsWithConstraints(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, int sectionSize);

//used to sample levels using iterative sampling and only the playability (and ill-formed pipes) constraints
void PlayabilityContraintSampling(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, int sectionSize);

//uses the manually defined functions (only developed for mario) to convert low-level maps into high-level representations
void ManualConversion(string inputMapFolder, string outputMapFolder, string mapName, int numberOfMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles);

//used to cluster chunks of maps together in order to find high-level tiles, then converts low-level maps into their high-level representation
void ClusterAndConvert(string lowLevelMapFolder, string lowLevelMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfClusters, float(*comparisonFunction) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), string comparisonFunctionName, bool removeDuplicates);

//Train with Partial Map
Learner TrainWithPartialMap(string folder, string mapName, string allMapsStructure, int amountOfMapToUse, int dependencyMatrix);

//used for both hierarchical models (manually converted maps and clustering-based)
void TrainAndSample(string trainingMapFolder, string highLevelMapFolder, string mapName, string outputMapFolder, string highLevelOutputMapName, string lowLevelOutputMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfOutputMaps, int widthOfHighLevelOutputMap, int heightOfHighLevelOutputMap, int highLevelDependencyMatrix, int lowLevelDependencyMatrix, int topLevelRowSplits, int highLevelLookAhead, int lowLevelLookAhead, bool manualConversion);

//used for multilevel hierarchies
void TrainAndSampleHighLevelProvided(string trainingMapFolder, string highLevelMapFolder, string mapName, string outputMapFolder, string highLevelOutputMapName, string lowLevelOutputMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfOutputMaps, int widthOfHighLevelOutputMap, int heightOfHighLevelOutputMap, int highLevelDependencyMatrix, int lowLevelDependencyMatrix, int topLevelRowSplits, int highLevelLookAhead, int lowLevelLookAhead, bool manualConversion);

//Used for training and sampling maps with an MRF
void NewMRFexperiments(int trainingMaps, int height, int width, int iterations, int toGenerate, string inputFileBase, string outputFolder);

cimg_library::CImg<float> ConvertMarioMapToImage(int width, int height, string mapName, string imageName, string archetype);
cimg_library::CImg<float> ConvertMarioMapToImagePath(int width, int height, string mapName1, string mapName2, string imageName);
cimg_library::CImg<float> ConvertKIMapToImage(int width, int height, string mapName, string imageName);

void convertImagetoMarioMap(int width, int height, string imageName, string mapName, string archetype);

vector< vector< char> > simplifyMapRepresentation(string mapName, bool simplifyEnemies, bool simplifyPipes, bool simplifyBlocks, bool removePath);
int computeDistanceBetweenSegments(vector<vector< char > > segment_1, vector<vector< char > > segment_2, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), vector<char> tileTypes);

void extractWindowFromFrame(string frameName, string outputWindowName, int windowWidth, int windowHeight);

//annotates the input maps with a path generated by Summerville's agent
bool MarioPlayabilityPath(string mapName);
bool MarioPlayabilityPath(vector< vector< char> > Map);

int findOverlap(string mapSection1, string mapSection2);
vector< vector< double> > trainProbabilisticFSM(vector<int> trace);
vector< vector< double> > trainProbabilisticFSM(vector<int> trace, vector<vector<vector<char> > > medoids, int numSections, string sectionName);
vector< vector< double> > trainProbabilisticFSMOnlyStates(vector<int> trace, vector<vector<vector<char> > > medoids, int numSections, string sectionName, int start);

//for a* agent
long double getPathLikelihood(vector<vector< double> > transitionTable, string pathFile);
long double getPathLikelihood(vector<vector< double> > transitionTable, string pathFile, vector<vector<vector< char> > > medoids, string mapName);
long double getPathLikelihoodOnlyStates(vector<vector< double> > transitionTable, string pathFile, vector<vector<vector< char> > > medoids, string mapName);

//for player paths
long double getPathLikelihood(vector<vector< double> > transitionTable, vector<int> movements);
long double getPathLikelihood(vector<vector< double> > transitionTable, vector<int> movements, vector<vector<vector< char> > > medoids, string sectionName);
long double getPathLikelihoodOnlyStates(vector<vector< double> > transitionTable, vector<int> movements, vector<vector<vector< char> > > medoids, string sectionName);

long double getDistanceFromDistribution(vector<vector< double> > transitionTable, vector<double> actionDistribution, string pathFile);
long double getDistanceFromDistribution(vector<vector< double> > transitionTabel, vector<double> actionDistribution, vector<double> agentActionDistribution);
vector<int> getMoveDiffsFromDistribution(vector<vector< double> > transitionTable, vector<double> actionDistribution, string pathFile);
void ViolationLocationResamplingWithLikelihood(int numToGenerate, double minLikelihood, double maxLikelihood, vector<vector<vector< char> > > medoids, vector<vector< double> > transitionTable);

vector<int> countMovements(vector<double> actionDistribution, string pathFile);

void ViolationLocationResamplingWithLikelihood(int numToGenerate, double minLikelihood, double maxLikelihood, vector<vector<vector< char> > > medoids, vector<vector< double> > transitionTable, vector<double> actionDistribution);

void ViolationLocationResampling(int numToGenerate);

void Demo(int rowSplits, int lookAhead, int trainingMaps, string mapName, string trainingFolderName, int networkStructure, int width, int height, string outputFolderName, int toGenerate);

void LayersDemo(int lookAhead, int trainingMaps, string mapName, string trainingFolderName, string outputFolderName, int toGenerate);

vector<vector<double> > TrainMultiLayerModel(vector<vector<vector<vector< char > > > >& layers, vector<vector<pair<int, int> > >& networkStructures,
                                             vector<vector<char > >& layerTileTypes);
vector<vector<char> > SampleMultiLayerModelWithPath(vector<vector<vector<double> > >& probabilities, vector<vector<vector<pair<int, int> > > >& networkStructures, vector<vector<char> >& playerPathLayer, vector<vector<char> >& layerTileTypes, int lookAhead, string outputMapName);

vector<vector<char> > SampleMultiLayerModel(vector<vector<vector<double> > >& probabilities, vector<vector<vector<pair<int, int> > > >& networkStructures, vector<vector<vector<char> > >& otherLayers, vector<vector<char> >& layerTileTypes, int lookAhead, string outputMapName);

void RegenerateTopLevelSectionWithSmoothingAndLayers(vector<vector<char> >& map, vector<vector<vector<double> > >& probabilities,
                                                     vector<vector<vector<pair<int, int> > > >& networkStructures,
                                                     vector<vector<vector< char> > >& otherLayers, vector<vector<char> >& layerTileTypes,
                                                     int lookAhead, int xStart, int yStart, int xEnd, int yEnd);

vector<vector< char> > readInPlayerPathLayer(string trainingDataName);
vector<vector< char> > readInStructuralLayer(string trainingDataName);

vector<pair<int, int> > setNetworkStructure(int ns, bool mainLayer);
vector<char> FindTileTypes(vector<vector<vector<char> > >& maps);

vector<int> ComputeNumberOfUnreachableBlocks(string levelNameBase, int numberOfLevels);

vector<int> DistanceBetweenPaths(string providedPathName, string levelNameBase, int numberOfLevels);

pair<bool, bool> isSpringNecessary(string levelName);


//Frechet Distance
double ComputeFrechetDistance(vector<pair<float, float> > path_1, vector<pair<float, float> > path_2);
double DynamicFrechetUpdate(vector<pair<float,float> > path_1,vector<pair<float,float> > path_2,int index_1,int index_2,vector<vector<double> > &Dmatrix);






int main(int argc, char* argv[]){
    
    //Demo(5, 3, 29, "deep_standard_", "Mario_Maps/Deep/Standard/structural/", 3, 210, 14, "Demo/", 25);
    //return 0;
    
    //AIIDE 2017 Experiments
    
    //Compute the ordering of the maps using the likelihood of the maps
    /*Learner TopLevelLearner = Learner(1, 3, false, false);
    TopLevelLearner.FindAllTileTypes(29, "Mario_Maps/Deep/Standard/structural/deep_standard_", false);
    
    TopLevelLearner.SetDepMatrix(3);
    
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel("Mario_Maps/Deep/Standard/structural/deep_standard_", 29, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    for(int i=1; i<=29; i++){
        
        vector<vector<char> > map = readInStructuralLayer("Mario_Maps/Deep/Standard/structural/deep_standard_"+to_string(i)+".txt");
        cout<<i<<" "<<MapLikelihood(map, TopLevelLearner).first<<" "<<MapLikelihood(map, TopLevelLearner).second<<endl;
    }
    return 0;
    */
    
    
    string folder = argv[1];
    string oFolder = argv[2];
    int numMaps = stoi(argv[3]);

    cout<<folder<<"\n"<<oFolder<<"\n"<<numMaps<<endl;
    vector<string> mapOrder;
    //Most entropic to least
    /*mapOrder.push_back(folder+"deep_standard_24_16.txt");
    mapOrder.push_back(folder+"deep_standard_24_32.txt");
    mapOrder.push_back(folder+"deep_standard_24_64.txt");
    mapOrder.push_back(folder+"deep_standard_24_128.txt");
    */mapOrder.push_back(folder+"deep_standard_24.txt");
    mapOrder.push_back(folder+"deep_standard_9.txt");
    mapOrder.push_back(folder+"deep_standard_27.txt");
    mapOrder.push_back(folder+"deep_standard_10.txt");
    mapOrder.push_back(folder+"deep_standard_28.txt");
    mapOrder.push_back(folder+"deep_standard_16.txt");
    mapOrder.push_back(folder+"deep_standard_25.txt");
    mapOrder.push_back(folder+"deep_standard_3.txt");
    mapOrder.push_back(folder+"deep_standard_22.txt");
    mapOrder.push_back(folder+"deep_standard_21.txt");
    mapOrder.push_back(folder+"deep_standard_2.txt");
    mapOrder.push_back(folder+"deep_standard_7.txt");
    mapOrder.push_back(folder+"deep_standard_14.txt");
    mapOrder.push_back(folder+"deep_standard_26.txt");
    mapOrder.push_back(folder+"deep_standard_20.txt");
    mapOrder.push_back(folder+"deep_standard_12.txt");
    mapOrder.push_back(folder+"deep_standard_29.txt");
    mapOrder.push_back(folder+"deep_standard_8.txt");
    mapOrder.push_back(folder+"deep_standard_1.txt");
    mapOrder.push_back(folder+"deep_standard_19.txt");
    mapOrder.push_back(folder+"deep_standard_17.txt");
    mapOrder.push_back(folder+"deep_standard_15.txt");
    mapOrder.push_back(folder+"deep_standard_18.txt");
    mapOrder.push_back(folder+"deep_standard_6.txt");
    mapOrder.push_back(folder+"deep_standard_13.txt");
    mapOrder.push_back(folder+"deep_standard_5.txt");
    mapOrder.push_back(folder+"deep_standard_23.txt");
    mapOrder.push_back(folder+"deep_standard_4.txt");
    mapOrder.push_back(folder+"deep_standard_11.txt");
    /*mapOrder.push_back(folder+"deep_standard_11_100.txt");
    mapOrder.push_back(folder+"deep_standard_11_50.txt");
    mapOrder.push_back(folder+"deep_standard_11_25.txt");
    mapOrder.push_back(folder+"deep_standard_11_10.txt");
    //*/
    
    string outputFolder = "";
    if(numMaps == 1)
        outputFolder = to_string(numMaps)+"-map";
    else
        outputFolder = to_string(numMaps)+"-maps";
    
    vector<vector<vector<vector< char >>>> layers;
    
    vector<vector<vector< char >>> structuralLayers;
    vector<vector<vector< char >>> rowSplitLayers;
    
    
    
    /*for(int i=0; i<mapOrder.size(); i++){
        structuralLayers.push_back(readInStructuralLayer(mapOrder[i]));
        rowSplitLayers.push_back(readInStructuralLayer("RowSplitsLayer_2.txt"));
    }*/
    
    for(int i=mapOrder.size()-numMaps; i<mapOrder.size(); i++){
        
        cout<<mapOrder[i]<<endl;
        structuralLayers.push_back(readInStructuralLayer(mapOrder[i]));
        rowSplitLayers.push_back(readInStructuralLayer("RowSplitsLayer_2.txt"));
    }
    
    layers.push_back(structuralLayers);
    layers.push_back(rowSplitLayers);
    
    
    
    //Each of these networkStructure_x vectors is the set of network structures used for the main model and all fallback models
    vector<vector<pair<int, int>>> networkStructures_3;
    networkStructures_3.push_back(setNetworkStructure(3, true));
    networkStructures_3.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_2;
    networkStructures_2.push_back(setNetworkStructure(2, true));
    networkStructures_2.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_1;
    networkStructures_1.push_back(setNetworkStructure(1, true));
    networkStructures_1.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_0;
    networkStructures_0.push_back(setNetworkStructure(0, true));
    networkStructures_0.push_back(setNetworkStructure(0, false));
    
    vector<vector< char> > layerTileTypes;
    layerTileTypes.push_back(FindTileTypes(structuralLayers));
    layerTileTypes.push_back(FindTileTypes(rowSplitLayers));
    
    layerTileTypes[0].insert(layerTileTypes[0].begin(), 'S');
    
    for(int i=0; i<layerTileTypes.size(); i++){
        for(int j=0; j<layerTileTypes[i].size(); j++){
            cout<<layerTileTypes[i][j]<<" ";
        }
        cout<<endl;
    }
    
    vector<vector<vector<double> > > probabilities;
    
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_3, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_2, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_1, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_0, layerTileTypes));

    
    
    //The ns_x defined here are the network structures for each of the layers for each fallback.
    vector<vector<vector<pair<int, int> > > > networkStructures;
    vector<vector<pair<int, int> > > ns_1;
    ns_1.push_back(networkStructures_3[0]);
    ns_1.push_back(networkStructures_2[0]);
    ns_1.push_back(networkStructures_1[0]);
    ns_1.push_back(networkStructures_0[0]);
    
    vector<vector<pair<int, int> > > ns_2;
    ns_2.push_back(networkStructures_3[1]);
    ns_2.push_back(networkStructures_2[1]);
    ns_2.push_back(networkStructures_1[1]);
    ns_2.push_back(networkStructures_0[1]);
    
    networkStructures.push_back(ns_1);
    networkStructures.push_back(ns_2);
    
    
    vector<vector<vector<char> > > otherLayers;
    otherLayers.push_back(rowSplitLayers[0]);
    
    
    
    
    for(int i=1; i<=1000; i++){
        
        //violation detection resampling
        vector<vector< char> > map = SampleMultiLayerModel(probabilities, networkStructures, otherLayers, layerTileTypes, 3, oFolder+outputFolder+"/sampled_"+to_string(i)+".txt");
        bool playable=false;
        int pos=-1;
        pos = MarioPlayabilityDistanceConstraint(map, 0, 0);
        if(pos >= map[0].size()-3){
            playable = true;
            pos = MarioNoIllFormedPipesConstraintPosition(map, 0, 0);
        }
        
        vector<int> ret1;
        if(playable && pos >= 0){
            ret1.push_back(max(pos-1, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+2, (int)map[0].size()-1));
            ret1.push_back(13);
        }
        
        if(!playable){
            ret1.push_back(max(pos-4, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+5, (int)map[0].size()-1));
            ret1.push_back(13);
        }
        
        //fix the level while there are sections to be fixed
        
        while(ret1.size()>0){
            for (int r=0; r<ret1.size(); r+=4){
                RegenerateTopLevelSectionWithSmoothingAndLayers(map, probabilities, networkStructures, otherLayers, layerTileTypes, 3, ret1[r], ret1[r+1], ret1[r+2], ret1[r+3]);
            }
            pos=-1;
            ret1.clear();
            playable = false;
            pos = MarioPlayabilityDistanceConstraint(map, 0, 0);
            if(pos >= map[0].size()-3){
                playable=true;
                pos = MarioNoIllFormedPipesConstraintPosition(map, 0, 0);
            }
            
            if(playable && pos >= 0){
                ret1.push_back(max(pos-1, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+2, (int)map[0].size()-1));
                ret1.push_back(13);
            }
            
            if(!playable){
                ret1.push_back(max(pos-4, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+5, (int)map[0].size()-1));
                ret1.push_back(13);
            }
        }
        
        cout<<"Printing Map "<<i<<endl;
        
        //print the map
        ofstream out(oFolder+outputFolder+"/sampled_"+to_string(i)+".txt");
        for(int y=0; y< map.size(); y++){
            for(int x=0; x<map[y].size(); x++){
                out<<map[y][x];
            }
            out<<endl;
        }
        out.close();
        
        /*if(i%50==1){
            cout<<"Converting Map "<<i<<endl;

            ConvertMarioMapToImage(210, 14, oFolder+outputFolder+"/sampled_"+to_string(i)+".txt", oFolder+outputFolder+"/sampled_"+to_string(i)+".bmp", "standard");
        }*/
    }
    
    return 0;
    
    //Computing the entropy and level ordering
    /*ClusterAndConvert("Mario_Maps/Deep/Standard/structural/", "deep_standard_", 29, 4, 4, 30, &Shape, "Shape", true);
    
    Learner l(1,1, false, false);
    l.FindAllTileTypes(29, "Mario_Maps/Deep/Standard/structural/30-4X4-Shape/deep_standard_", false);

    for(int i=1; i<=29; i++){
        vector<double> histogram(l.TileTypes.size()+1, 1);
        histogram[histogram.size()-1]=histogram.size()-1;
        ifstream in("Mario_Maps/Deep/Standard/structural/30-4X4-Shape/deep_standard_"+to_string(i)+".txt");
        string qq;
        while(getline(in, qq)){
            for(int j=0; j<qq.length(); j++){
                char c = qq[j];
                for(int k=0; k<l.TileTypes.size(); k++){
                    if(c == l.TileTypes[k]){
                        histogram[k]++;
                        histogram[l.TileTypes.size()]++;
                    }
                }
            }
        }
        int numberOfUniqueTiles=0;
        for(int j=0; j<histogram.size()-1; j++){
            if(histogram[j] > 1)
                numberOfUniqueTiles++;
            histogram[j]/=histogram[histogram.size()-1];
        }
        
        
        double entropy=0;
        for(int j=0; j<histogram.size()-1; j++){
            entropy-=histogram[j]*log(histogram[j]);
        }
        cout<<i<<"\t"<<entropy<<"\t"<<numberOfUniqueTiles<<endl;
    }
    
    vector<double> h(l.TileTypes.size(), 1.0/l.TileTypes.size());
    double entropy=0;
    for(int j=0; j<h.size()-1; j++){
        entropy-=h[j]*log(h[j]);
    }
    vector<double> h2(l.TileTypes.size());
    double rest=0;
    for(int i=0; i<l.TileTypes.size()-1; i++){
        h2[i] = 0.000001;
        rest+=0.000001;
    }
    h2[h2.size()-1] = 1.0-rest;
    
    entropy=0;
    for(int j=0; j<h2.size()-1; j++){
        entropy-=h2[j]*log(h2[j]);
    }
    
    cout<<"Worst entropy: "<<entropy<<endl;
    
    
    
    return 0;
    */
    
    
    //CIG2017=====================================================
    
    //ConvertMarioMapToImagePath(197, 14, "CIG17_Experiments/ProvidedPath/Path_2/deep_standard_75_Provided_Path.txt",
                                        //"CIG17_Experiments/ProvidedPath/Path_2/deep_standard_75_Annotated_Path.txt", "PathOverlap.bmp");
    //return 0;
    //Learner l(1,1, false, false);
    //l.FindAllTileTypes(25, "Mario_Maps/Deep/Standard/deep_standard_", false);
    //cout<<l.Config.NumberTilesTypes<<endl;
    //for(int i=0; i<l.TileTypes.size(); i++)
    //    cout<<l.TileTypes[i]<<"\n";
    
    
    //return 0;
    /*ofstream out("MarioMapDifferenceClasses.tsv");
    for(int i=1; i<=25; i++){
        out<<"T_"+to_string(i)+"\tTraining\n";
    }
    
    for(int i=1; i<=1000; i++)
        out<<"S_"+to_string(i)+"\tSingle-layer\n";
    
    for(int i=1; i<=250; i++)
        out<<"1_"+to_string(i)+"\tPath-1\n";
    
    for(int i=1; i<=250; i++)
        out<<"2_"+to_string(i)+"\tPath-2\n";
    
    for(int i=1; i<=250; i++)
        out<<"3_"+to_string(i)+"\tPath-3\n";
    
    for(int i=1; i<=250; i++)
        out<<"4_"+to_string(i)+"\tPath-4\n";
    
    out.close();
    return 0;*/
    
    
    //===============================================================================================================
    
    
    //CIG Experiments
    //Demo(14, 3, 30, "deep_standard_", "Mario_Maps/Deep/Standard/", 3, 210, 14, "CIG17_Experiments/Standard/", 1000);
    
    /*vector<vector<vector<vector< char >>>> layers;
    
    vector<vector<vector< char >>> playerPathLayers;
    vector<vector<vector< char >>> structuralLayers;
    vector<vector<vector< char >>> rowSplitLayers;
    
    for(int i=1; i<=25; i++){
        playerPathLayers.push_back(readInPlayerPathLayer("Mario_Maps/Deep/Standard/deep_standard_"+to_string(i)+"_Annotated_Path.txt"));
    
        structuralLayers.push_back(readInStructuralLayer("Mario_Maps/Deep/Standard/deep_standard_"+to_string(i)+".txt"));

        rowSplitLayers.push_back(readInStructuralLayer("RowSplitsLayer_2.txt"));
    }
    
    layers.push_back(structuralLayers);
    layers.push_back(playerPathLayers);
    layers.push_back(rowSplitLayers);
    
    
    vector<vector<pair<int, int>>> networkStructures_3;
    networkStructures_3.push_back(setNetworkStructure(3, true));
    networkStructures_3.push_back(setNetworkStructure(0, false));
    networkStructures_3.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_2;
    networkStructures_2.push_back(setNetworkStructure(2, true));
    networkStructures_2.push_back(setNetworkStructure(0, false));
    networkStructures_2.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_1;
    networkStructures_1.push_back(setNetworkStructure(1, true));
    networkStructures_1.push_back(setNetworkStructure(0, false));
    networkStructures_1.push_back(setNetworkStructure(0, false));
    
    
    vector<vector< char> > layerTileTypes;
    layerTileTypes.push_back(FindTileTypes(structuralLayers));
    layerTileTypes.push_back(FindTileTypes(playerPathLayers));
    layerTileTypes.push_back(FindTileTypes(rowSplitLayers));
    
    layerTileTypes[0].insert(layerTileTypes[0].begin(), 'S');
    
    for(int i=0; i<layerTileTypes.size(); i++){
        for(int j=0; j<layerTileTypes[i].size(); j++){
            cout<<layerTileTypes[i][j]<<" ";
        }
        cout<<endl;
    }
    
    vector<vector<vector<double> > > probabilities;
    
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_3, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_2, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_1, layerTileTypes));
    
    
    vector<vector<vector<pair<int, int> > > > networkStructures;
    vector<vector<pair<int, int> > > ns_1;
    ns_1.push_back(networkStructures_3[0]);
    ns_1.push_back(networkStructures_2[0]);
    ns_1.push_back(networkStructures_1[0]);
    
    vector<vector<pair<int, int> > > ns_2;
    ns_2.push_back(networkStructures_3[1]);
    ns_2.push_back(networkStructures_2[1]);
    ns_2.push_back(networkStructures_1[1]);
    
    vector<vector<pair<int, int> > > ns_3;
    ns_3.push_back(networkStructures_3[1]);
    ns_3.push_back(networkStructures_2[1]);
    ns_3.push_back(networkStructures_1[1]);
    
    networkStructures.push_back(ns_1);
    networkStructures.push_back(ns_2);
    networkStructures.push_back(ns_3);
    

    vector<vector<vector<char> > > otherLayers;
    otherLayers.push_back(playerPathLayers[0]);
    otherLayers.push_back(rowSplitLayers[0]);
    
    //SampleMultiLayerModelWithPath(probabilities, networkStructures, playerPathLayers[0], layerTileTypes, 3, "CIG17_Experiments/ProvidedPath/Path_1/sampled_1.txt");
    
    
    SampleMultiLayerModel(probabilities, networkStructures, otherLayers, layerTileTypes, 3, "CIG17_Experiments/ProvidedPath/Path_1/sampled_0.txt");
    */
//    SampleMultiLayerModel(probabilities, networkStructures, otherLayers, layers[0], layerTileTypes, 3, "CIG17_Experiments/ProvidedPath/Path_1/sampled_0.txt");
    
    //LayersDemo(3, 25, "deep_standard_", "Mario_Maps/Deep/Standard/", "CIG17_Experiments/ProvidedPath/Path_4/", 250);
    //vector<int> counts = ComputeNumberOfUnreachableBlocks("CIG17_Experiments/ProvidedPath/Path_4/deep_standard_", 250);
    //vector<int> distances = DistanceBetweenPaths("Annotated_Path_2.txt", "CIG17_Experiments/ProvidedPath/Path_4/deep_standard_", 250);
    
    //double avg=0;
    //for(int i=0; i<distances.size(); i++){
    //    avg += distances[i];
    //}
    
    //cout<<"Normalized over length of level: "<<((float)avg/distances.size())<<endl;
    
    //MarioPlayabilityPath("tmpMap");
    //return 0;
    
    //158 path 1
    //Standard 1
    
    /*vector<pair<bool,bool> > neededSprings;
    for(int i=0; i<1000; i++){
        pair<bool, bool> tmp = isSpringNecessary("CIG17_Experiments/Standard/deep_standard_"+to_string(i)+".txt");
        if(tmp.first && !tmp.second){
            cout<<"\n\n";
            cout<<i<<endl;
        }
        
    }
    return 0;
    //*/
    /*
    cout<<endl;
    cout<<endl;
    
    int numberSpringsInMaps=0;
    int numberNeededSprings=0;
    for(int i=0; i<neededSprings.size(); i++){
        cout<<i+1<<"\t"<<neededSprings[i].first<<"\t"<<neededSprings[i].second<<endl;
        if(neededSprings[i].first)
            numberSpringsInMaps++;
        if(neededSprings[i].second)
            numberNeededSprings++;
    }
    
    
    cout<<numberSpringsInMaps<<"\t"<<numberNeededSprings<<endl;
    */
    
    /*vector<double> lins;
    vector<double> lens;
    for(int i=0; i<250; i++){
        ifstream in("CIG17_Experiments/ProvidedPath/Path_4/deep_standard_"+to_string(i)+".txt");
        string line;
        vector<vector<char> > level;
        while(getline(in, line)){
            vector<char> row;
            for(int j=0; j<line.length(); j++){
                if(line[j]!= '\n' && line[j]!= '\t' && line[j]!= '\r' && line[j]!= '\0')
                    row.push_back(line[j]);
            }
            level.push_back((row));
        }
        
        
        lins.push_back(ComputeLinearity(level));
        lens.push_back(ComputeLeniency(level));
    
    }
    
    double linsAvg=0;
    double lensAvg=0;
    for(int i=0; i<lins.size(); i++){
        linsAvg+=lins[i];
        lensAvg+=lens[i];
    }
    
    cout<<linsAvg/lins.size()<<" "<<lensAvg/lens.size()<<endl;
    */
    
    ifstream in("Mario_Maps/Deep/Standard/deep_standard_2_Path.txt");
    string line;
    vector<pair<float, float> > path_1;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<float, float> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        path_1.push_back(pos);
    }
    
    //for(int i=0; i<1000; i++){
    //    MarioPlayabilityPath("CIG17_Experiments/Standard/deep_standard_"+to_string(i));
    //}
    
    //  8.88262
    //  13.1738
    //  13.1738
    //
    
    int size=0;
    double avg=0;
    for(int i=0; i<250; i++){
        ifstream in("CIG17_Experiments/ProvidedPath/Path_2/deep_standard_"+to_string(i)+"_Path.txt");
        string line;
        vector<pair<float, float> > path_2;
        while(getline(in, line)){
            stringstream s;
            s.str(line);
            string word;
            pair<float, float> pos;
            
            getline(s, word, '\t');
            pos.first = stoi(word);
            getline(s, word, '\t');
            pos.second = stoi(word);
            
            path_2.push_back(pos);
        }
        if(path_2.size() == 0)
            continue;
        
        cout<<i<<" "<<ComputeFrechetDistance(path_1, path_2)<<endl;
    }

    return 0;
    //*/
    
    
    
    
    //Demo(14, 3, 30, "deep_standard_", "Mario_Maps/Deep/Standard/", 3, 210, 14, "Testing/", 5);
    //MarioPlayabilityPath("Mario_Maps/Deep/Standard/deep_standard_2");
    //return 0;
    
    //ConvertMarioMapToImage(210, 14, "IJCAI17_Experiments/VLR_Likelihood_2/mario_0_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/mario_VLR_Likelihood_2.bmp", "standard");
    
    //ConvertMarioMapToImage(210, 14, "IJCAI17_Experiments/VLR_Likelihood_2/mario_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/mario_VLR_Likelihood_3.bmp", "standard");
    //return 0;
    /*ConvertMarioMapToImage(177, 14, "Mario_Maps/Playthrough/SummervilleMaps/VideoA/converted_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/A.bmp", "standard");
    ConvertMarioMapToImage(171, 14, "Mario_Maps/Playthrough/SummervilleMaps/VideoB/converted_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/B.bmp", "standard");
    ConvertMarioMapToImage(169, 14, "Mario_Maps/Playthrough/SummervilleMaps/VideoC/converted_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/C.bmp", "standard");
    ConvertMarioMapToImage(170, 14, "Mario_Maps/Playthrough/SummervilleMaps/VideoD/converted_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/D.bmp", "standard");
    ConvertMarioMapToImage(197, 14, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_1_Annotated_Path.txt", "/Users/sps74/Dropbox/shared\ Sam/DrexelPhd/papers/IJCAI17/E.bmp", "standard");
    
    
    
    ConvertMarioMapToImage(210, 14, "IJCAI17_Experiments/VLR_Playability/mario_1_Annotated_Path.txt", "IJCAI17_Experiments/VLR_Playability/mario_1_Annotated_Path.bmp", "standard");
    return 0;
    */
    
    
    //Cluster on the sample maps and then convert all the maps using the found clusters
    //ClusterAndConvert("CIG17_Experiments/Clustering/", "C_", 65, 4, 4, 30, &Shape, "Shape", true);
    
    /*vector<vector<vector<char> > > Medoids;
    
    //read in the medoids (high-level tiles)
    ifstream Medoid_File;
    string line;
    Medoid_File.open("CIG17_Experiments/Clustering/Shape_Medoids_30_4X4.txt");
    
    while(getline(Medoid_File, line)){
        vector<vector< char > > Block;
        for(int h = 0; h < 4; h++){
            getline(Medoid_File, line);
            vector< char > temp;
            for(int w=0; w<4; w++)
                temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(Medoid_File, line);
        Medoids.push_back(Block);
    }
    
    //output maps folder
    string OutputFolder = "CIG17_Experiments/Clustering/30-4X4-Shape";
    
    //convert the low-level maps into high-level maps, and put them in the newly created subfolder
    MapReader Converter = MapReader();
    Learner tileFinder = Learner(1, 0, true, false);
    tileFinder.FindAllTileTypes(250, "CIG17_Experiments/ProvidedPath/Path_4/deep_standard_", false);
    
    Converter.ConvertAllTileMapsToHier("deep_standard_", "CIG17_Experiments/ProvidedPath/Path_4/", OutputFolder+"/", 250, 4, 4, Medoids, &Shape, "Shape", vector<vector<float>>(), tileFinder.TileTypes);
    Medoid_File.close();
    Medoids.clear();
    */
    
    
    //void ClusterAndConvert(string lowLevelMapFolder, string lowLevelMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfClusters, float(*comparisonFunction) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), string comparisonFunctionName, bool removeDuplicates);
    
    
    //cout<<"Done clustering"<<endl;
    
    //compare the high level tile occurences between all the maps
    ofstream output("CIG17_Experiments/Clustering/PairwiseDifference.tsv");
    
    for(int i=0; i<6; i++){
        string prefix_1="";
        int numMaps_1=250;
        if(i==0){
            prefix_1 = "T_";
            numMaps_1 = 25;
        }
        else if(i==1){
            prefix_1 ="S_";
            numMaps_1 = 1000;
        }
        else if(i==2)
            prefix_1 = "1_";
        else if(i==3)
            prefix_1 = "2_";
        else if(i==4)
            prefix_1 = "3_";
        else if(i==5)
            prefix_1 = "4_";
        
        
        
        vector< char> types = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'};
        //load in the input map and count the number of tile of each type
        for(int j=1; j<=numMaps_1; j++){

            output<<prefix_1<<j;
            //read in the first map
            ifstream mapFile_1("CIG17_Experiments/Clustering/30-4X4-Shape/"+prefix_1+to_string(j)+".txt");
            //read in the map
            string line;
            vector<vector<char> > map_1;
            while(getline(mapFile_1, line)){
                vector<char> tmp;
                for(int k=0; k<line.length(); k++){
                    if(line[k]!= '\n' && line[k] != '\t' && line[k] !='\0' && line[k] != '\r')
                        tmp.push_back(line[k]);
                }
                map_1.push_back(tmp);
                tmp.clear();
            }
            
            vector<int> mapCounts_1(40, 0);
            for(int r_1=0; r_1<map_1.size(); r_1++){
                for(int c_1=0; c_1<map_1[r_1].size(); c_1++){
                    for(int t=0; t<types.size(); t++){
                        if(map_1[r_1][c_1] == types[t])
                            mapCounts_1[t]++;
                    }
                }
            }
            
            
            //compare the first map to all other maps
            for(int f=0; f<6; f++){
                string prefix_2="";
                int numMaps_2=250;
                if(f==0){
                    prefix_2 = "T_";
                    numMaps_2 = 25;
                }
                else if(f==1){
                    prefix_2 = "S_";
                    numMaps_2 = 1000;
                }
                else if(f==2)
                    prefix_2 = "1_";
                else if(f==3)
                    prefix_2 = "2_";
                else if(f==4)
                    prefix_2 = "3_";
                else if(f==5)
                    prefix_2 = "4_";
                
                
                for(int g=1; g<=numMaps_2; g++){
                    output<<'\t';
                    //read in the second map
                    ifstream mapFile_2("CIG17_Experiments/Clustering/30-4X4-Shape/"+prefix_2+to_string(g)+".txt");
                    //read in the map
                    vector<vector<char> > map_2;
                    while(getline(mapFile_2, line)){
                        vector<char> tmp;
                        for(int k=0; k<line.length(); k++){
                            if(line[k]!= '\n' && line[k] != '\t' && line[k] !='\0' && line[k] != '\r')
                                tmp.push_back(line[k]);
                        }
                        map_2.push_back(tmp);
                        tmp.clear();
                    }
                    
                    vector<int> mapCounts_2(40, 0);
                    for(int r_2=0; r_2<map_2.size(); r_2++){
                        for(int c_2=0; c_2<map_2[r_2].size(); c_2++){
                            for(int t=0; t<types.size(); t++){
                                if(map_2[r_2][c_2] == types[t])
                                    mapCounts_2[t]++;
                            }
                        }
                    }
                    
                    
                    //compute the Euclidean distance between the tile counts and print it to the output file
                    double squaredSum=0;
                    for(int t=0; t<types.size(); t++)
                        squaredSum += pow((mapCounts_1[t]-mapCounts_2[t]), 2);
                    
                    double eucDist = sqrt(squaredSum);
                    output<<eucDist;
                }
            }
            output<<'\n';
        }
        
    }
    output.close();
    
    return 0;
    //*/
    /*cout<<"Converting maps"<<endl;
    ifstream M_F;
    string line;
    M_F.open("IJCAI17_Experiments/LevelsForClustering/Shape_Medoids_30_4X4.txt");
    vector< vector< vector< char> > > M;
    while(!getline(M_F, line).eof()){
        vector<vector< char > > Block;
        for(int h = 0; h < 4; h++){
            getline(M_F, line);
            vector< char > temp;
            for(int w=0; w<4; w++)
                temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(M_F, line);
        M.push_back(Block);
    }
    
     vector<char> tileTypes = {'S', '-', 'e', '#', 'B', '?', 'p', 'P', 'o', 'b', '*'};
    //convert the low-level maps into high-level maps, and put them in the newly created subfolder
    MapReader Converter = MapReader();
    Converter.ConvertAllTileMapsToHier("converted_", "Mario_Maps/IJCAI17_Experiments/VLR_Likelihood_2/mario_", "IJCAI17_Experiments/LevelsForClustering/Converted/", 100, 4, 4, M, &Shape, "Shape", vector<vector<float>>(), tileTypes);
    //Converter.ConvertAllTileMapsToHier("converted_", "Mario_Maps/Playthrough/SummervilleMaps/VideoC/", "IJCAI17_Experiments/LevelsForClustering/Converted/", 100, 4, 4, M, &Shape, "Shape", vector<vector<float>>(), tileTypes);
    //Converter.ConvertAllTileMapsToHier("converted_", "Mario_Maps/Playthrough/SummervilleMaps/VideoD/", "IJCAI17_Experiments/LevelsForClustering/Converted/", 100, 4, 4, M, &Shape, "Shape", vector<vector<float>>(), tileTypes);
    //Converter.ConvertAllTileMapsToHier("M_", "IJCAI17_Experiments/LevelsForClustering/", "IJCAI17_Experiments/LevelsForClustering/Converted/", 4, 4, 4, M, &Shape, "Shape", vector<vector<float>>(), tileTypes);
    
    M_F.close();
    //Medoids.clear();
    */
    
    
    return 0;
    //*/
    
    
    
    //converting image to text
    //for(int i=18379; i<18912; i++)
    //    convertImagetoMarioMap(16, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Mario_Maps/Playthrough/MarioImageSequence/image_"+to_string(i)+".bmp", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "standard");
    
    
    //return 0;
    
    int windowWidth = 5;
    int windowHeight = 5;
    
    /*for(int i=460; i<=18911; i++){
        if((i > 1331 && i < 4330) || (i > 5154 && i < 10968) || (i > 11453 && i < 18379))
            continue;
        
        extractWindowFromFrame("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", windowWidth, windowHeight);
    }
    //3X3
    //vector< vector< float> > weights;
    //weights.push_back({0.5, 1.0, 0.5});
    //weights.push_back({1.0, 0.0, 1.0});
    //weights.push_back({0.5, 1.0, 0.5});
    
    //5X5
    vector< vector< float> > weights;
    weights.push_back({0.5, 0.7, .85, 0.7, 0.5});
    weights.push_back({0.7, .85, 1.0, .85, 0.7});
    weights.push_back({.85, 1.0, 0.0, 1.0, .85});
    weights.push_back({0.7, .85, 1.0, .85, 0.7});
    weights.push_back({0.5, 0.7, .85, 0.7, 0.5});
    
    //cluster the chunks weighting the sections nearer mario as higher importance
    
    vector<char> tileTypes = {'x', 'B', '#', '?', 'g', 'k', 'K', 'l', 't', '-', '&', 'p', 'P', 'o'};
    Clustering WindowClusterer = Clustering(windowWidth, windowHeight);
    WindowClusterer.ComputeDifferencesWithWeights("Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/Differences.csv", &Direct, tileTypes, weights);
    WindowClusterer.kMedoids("Mario_Maps/Playthrough/Converted/Windows_5X5/Differences.csv", 20);
    WindowClusterer.PrintMedoids("Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/medoids_20.txt");
    
    return 0;
    */
    
    //Read in the medoids
    /*vector<vector<vector<char> > > Medoids;
    ifstream Medoid_File("Mario_Maps/Playthrough/Converted/Windows_5X5/medoids_20.txt");
    string line;
    
    while(getline(Medoid_File, line)){
        vector<vector< char > > Block;
        for(int h = 0; h < windowHeight; h++){
            getline(Medoid_File, line);
            vector< char > temp;
            for(int w=0; w<windowWidth; w++)
                temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(Medoid_File, line);
        Medoids.push_back(Block);
    }
     
    
    double Agent_Move_Total=0;
    double Agent_State_Total=0;
    double Agent_MoveAndState_Total=0;

    double Player_Move_Total=0;
    double Player_State_Total=0;
    double Player_MoveAndState_Total=0;

    
    //Level 1-1
    //print out the path that the A* agent takes
    //MarioPlayabilityPath("Mario_Maps/Standard/standard_1");
    vector<int> movements;
    for(int i=460; i<1331; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    
    vector< vector< double> > table_Moves_1 = trainProbabilisticFSM(movements);
    
    
    
    vector< vector< double> > table_MovesAndState_1 = trainProbabilisticFSM(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/");
    vector< vector< double> > table_State_1 = trainProbabilisticFSMOnlyStates(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/", 460);
    
    
    //Level 2-1
    //MarioPlayabilityPath("Mario_Maps/Standard/standard_2");
    movements.clear();
    for(int i=4330; i<5154; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    
    vector<vector< double> > table_Moves_2 = trainProbabilisticFSM(movements);
    vector< vector< double> >table_MovesAndState_2 = trainProbabilisticFSM(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/");
    vector< vector< double> >table_State_2 = trainProbabilisticFSMOnlyStates(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/", 4330);
    
    
    //Level 4-1
    //MarioPlayabilityPath("Mario_Maps/Standard/standard_5");
    movements.clear();
    for(int i=10968; i<11453; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    
    vector<vector< double> > table_Moves_3 = trainProbabilisticFSM(movements);
    vector<vector< double> > table_MovesAndState_3 = trainProbabilisticFSM(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/");
    vector<vector< double> > table_State_3 = trainProbabilisticFSMOnlyStates(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/", 10968);
    
    
    
    //Level 6-2
    //MarioPlayabilityPath("Mario_Maps/Standard/standard_9");
    movements.clear();
    for(int i=18379; i<18911; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    
    //for(int i =0; i<movements.size(); i++)
    //    cout<<movements[i]<<endl;
    
    vector<vector< double> > table_Moves_4 = trainProbabilisticFSM(movements);
    vector<vector< double> > table_MovesAndState_4 = trainProbabilisticFSM(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/");
    vector<vector< double> > table_State_4 = trainProbabilisticFSMOnlyStates(movements, Medoids, (int)movements.size(), "Mario_Maps/Playthrough/Converted/Third/", 18379);
    
    //sum the counts (Moves Only)
    for(int i=0; i<table_Moves_1.size(); i++){
        for(int j=0; j<table_Moves_1[i].size(); j++){
            table_Moves_1[i][j] += table_Moves_2[i][j];
            table_Moves_1[i][j] += table_Moves_3[i][j];
            table_Moves_1[i][j] += table_Moves_4[i][j];
        }
    }
    
    
    vector<double> actionDistribution(table_Moves_1[0].size(),0);
    
    for(int i=0; i<table_Moves_1.size(); i++){
        double total = 0;
        for(int j=0; j<table_Moves_1[i].size(); j++){
            total+=table_Moves_1[i][j];
            if(j>0){
                actionDistribution[j-1]+=table_Moves_1[i][j];
                actionDistribution[actionDistribution.size()-1]+=table_Moves_1[i][j];
            }
        }
        double g=0;
        for(int j=0; j<table_Moves_1[i].size(); j++){
            table_Moves_1[i][j]/= total;
            cout<<table_Moves_1[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
    double total=0;
    for(int i=0; i<actionDistribution.size()-1; i++){
        actionDistribution[i] = actionDistribution[i]/actionDistribution[actionDistribution.size()-1];
        total+=actionDistribution[i];
        cout<<actionDistribution[i]<<"\t";
    }
    cout<<endl;
    cout<<total<<endl;
    //return 0;
    //sum the counts (States Only)
    for(int i=0; i<table_State_1.size(); i++){
        for(int j=0; j<table_State_1[i].size(); j++){
            table_State_1[i][j] += table_State_2[i][j];
            table_State_1[i][j] += table_State_3[i][j];
            table_State_1[i][j] += table_State_4[i][j];
        }
    }
    
    for(int i=0; i<table_State_1.size(); i++){
        double total = 0;
        for(int j=0; j<table_State_1[i].size(); j++){
            total+=table_State_1[i][j];
        }
        for(int j=0; j<table_State_1[i].size(); j++){
            table_State_1[i][j]/= total;
        }
    }
    
    //sum the counts (States and Moves)
    for(int i=0; i<table_MovesAndState_1.size(); i++){
        for(int j=0; j<table_MovesAndState_1[i].size(); j++){
            table_MovesAndState_1[i][j] += table_MovesAndState_2[i][j];
            table_MovesAndState_1[i][j] += table_MovesAndState_3[i][j];
            table_MovesAndState_1[i][j] += table_MovesAndState_4[i][j];
        }
    }
    
    for(int i=0; i<table_MovesAndState_1.size(); i++){
        double total = 0;
        for(int j=0; j<table_MovesAndState_1[i].size(); j++){
            total+=table_MovesAndState_1[i][j];
        }
        for(int j=0; j<table_MovesAndState_1[i].size(); j++){
            table_MovesAndState_1[i][j]/= total;
        }
    }
    */
    
    
    
    
    
    /*double max=0;
    for(int i=0; i<table_Moves_1.size(); i++){
        for(int j=0; j<table_Moves_1[i].size(); j++){
            if(table_Moves_1[i][j] > max){
                max = table_Moves_1[i][j];
                cout<<i<<" "<<j<<endl;
            }
        }
    }
    
    
    cout<<"Moves only max likelihood = "<<max<<endl;
    
    max=0;
    for(int i=0; i<table_State_1.size(); i++){
        for(int j=0; j<table_State_1[i].size(); j++){
            if(table_State_1[i][j] > max){
                max = table_State_1[i][j];
                cout<<i<<" "<<j<<endl;
            }
        }
    }
    cout<<"State only max likelihood = "<<max<<endl;
    
    max=0;
    for(int i=0; i<table_MovesAndState_1.size(); i++){
        for(int j=0; j<table_MovesAndState_1[i].size(); j++){
            if(table_MovesAndState_1[i][j] > max){
                max = table_MovesAndState_1[i][j];
                cout<<i<<" "<<j<<endl;
            }
            
        }
    }
    cout<<"Moves and State max likelihood = "<<max<<endl;
    return 0;
     */
    
    
    //compute likelihood of sampled maps (Or Summerville maps)
    //int numMaps=100;
    //ViolationLocationResampling(1);
    
    //long double x=getDistanceFromDistribution(table_Moves_1, actionDistribution, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_1_Path.txt");
    //long double x=getDistanceFromDistribution(table_Moves_1, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood/mario_0_Path.txt");
    //cout<<x<<endl;
    //return 0;
    
    
    //ViolationLocationResamplingWithLikelihood(numMaps, .13, 1, Medoids, table_MovesAndState_1, actionDistribution);
    /*for(int i=1; i<=numMaps; i++){
        ofstream out("Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+".txt");
        vector<vector< char> > map = simplifyMapRepresentation("Mario_Maps/Playthrough/SummervilleMaps/VideoAll/rawAG."+to_string(i), true, true, true, true);
        for(int r=0; r<map.size(); r++){
            for(int c=0; c<map[r].size(); c++){
                out<<map[r][c];
            }
            out<<endl;
        }
        out.close();
    }*/
    //return 0;
    long double likelihood_Moves=0;
    long double likelihood_States=0;
    long double likelihood_MovesAndStates=0;
    for(int i=1; i<=4; i++)
        MarioPlayabilityPath("IJCAI17_Experiments/LevelsForClustering/M_"+to_string(i));
    //MarioPlayabilityPath("Test");
    
    /*for(int i=0; i<numMaps; i++){
        likelihood_Moves += getPathLikelihood(table_Moves_1, "IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+"_Path.txt");
        likelihood_States += getPathLikelihoodOnlyStates(table_State_1, "IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+"_Path.txt", Medoids, "IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+"_Annotated_Path.txt");
        likelihood_MovesAndStates += getPathLikelihood(table_MovesAndState_1, "IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+"_Path.txt", Medoids,  "IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+"_Annotated_Path.txt");
    }*/
    
    //summerville's maps
    /*for(int i=1; i<=numMaps; i++){
        likelihood_Moves += getPathLikelihood(table_Moves_1, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+"_Path.txt");
        likelihood_States += getPathLikelihoodOnlyStates(table_State_1, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+"_Path.txt", Medoids, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+"_Annotated_Path.txt");
        likelihood_MovesAndStates += getPathLikelihood(table_MovesAndState_1, "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+"_Path.txt", Medoids,  "Mario_Maps/Playthrough/SummervilleMaps/VideoAll/converted_"+to_string(i)+"_Annotated_Path.txt");
    }
    *//*
    double dist=0;
    for(int i=1; i<=4; i++){
        //likelihood_Moves += getPathLikelihood(table_Moves_1, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
        //likelihood_States += getPathLikelihoodOnlyStates(table_State_1, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", Medoids, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
        //long double tmp =getPathLikelihood(table_MovesAndState_1, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", Medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
        //likelihood_MovesAndStates +=tmp;
        
        
        double tmp = getDistanceFromDistribution(table_Moves_1, actionDistribution, "IJCAI17_Experiments/LevelsForClustering/M_"+to_string(i)+"_Path.txt");
        cout<<i<<": "<<tmp<<endl;
        dist+=tmp;
    }
    
    //likelihood_Moves += getPathLikelihood(table_Moves_1, "Test_Path.txt");
    //likelihood_States += getPathLikelihoodOnlyStates(table_State_1, "Test_Path.txt", Medoids, "Test_Annotated_Path.txt");
    //likelihood_MovesAndStates += getPathLikelihood(table_MovesAndState_1, "Test_Path.txt", Medoids,  "Test_Annotated_Path.txt");
    
    
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<likelihood_Moves/numMaps<<endl;
    cout<<"Likelihood of A* agent's Path (States Only): "<<likelihood_States/numMaps<<endl;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<likelihood_MovesAndStates/numMaps<<endl;
    cout<<"Distance from action Distribution: "<<dist/4<<endl;
    */
    /*movements.clear();
    for(int i=460; i<1331; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    long double likelihood_Moves = getPathLikelihood(table_Moves_1, "Mario_Maps/Standard/standard_1_Path.txt");
    long double player_likelihood_Moves = getPathLikelihood(table_Moves_1, movements);
    
    long double player_likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    long double likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, "Mario_Maps/Standard/standard_1_Path.txt", Medoids, "Mario_Maps/Standard/standard_1_Annotated_Path.txt");
    
    long double player_likelihood_States = getPathLikelihoodOnlyStates(table_State_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    long double likelihood_States = getPathLikelihoodOnlyStates(table_State_1, "Mario_Maps/Standard/standard_1_Path.txt", Medoids, "Mario_Maps/Standard/standard_1_Annotated_Path.txt");
    
    cout<<"Level 1-1: "<<endl;
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<likelihood_Moves<<endl;                       Agent_Move_Total  += likelihood_Moves;
    cout<<"Likelihood of A* agent's Path (States Only): "<<likelihood_States<<endl;                     Agent_State_Total += likelihood_States;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<likelihood_MovesAndStates<<endl;        Agent_MoveAndState_Total+= likelihood_MovesAndStates;

    cout<<"Likelihood of Player's Path (Moves Only): "<<player_likelihood_Moves<<endl;                  Player_Move_Total += player_likelihood_Moves;
    cout<<"Likelihood of Player's Path (States Only): "<<player_likelihood_States<<endl;    /Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/IJCAI17_Experiments/VLR_Playability/mario_0.txt            Player_State_Total+= player_likelihood_States;
    cout<<"Likelihood of Player's Path (Moves and States): "<<player_likelihood_MovesAndStates<<endl;   Player_MoveAndState_Total+= player_likelihood_MovesAndStates;
    
    
    
    
    movements.clear();
    for(int i=4330; i<5154; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    likelihood_Moves = getPathLikelihood(table_Moves_1, "Mario_Maps/Standard/standard_2_Path.txt");
    player_likelihood_Moves = getPathLikelihood(table_Moves_1, movements);
    
    player_likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, "Mario_Maps/Standard/standard_2_Path.txt", Medoids, "Mario_Maps/Standard/standard_2_Annotated_Path.txt");
    
    player_likelihood_States = getPathLikelihoodOnlyStates(table_State_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_States = getPathLikelihoodOnlyStates(table_State_1, "Mario_Maps/Standard/standard_2_Path.txt", Medoids, "Mario_Maps/Standard/standard_2_Annotated_Path.txt");
    
    cout<<"Level 2-1: "<<endl;
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<likelihood_Moves<<endl;                       Agent_Move_Total  += likelihood_Moves;
    cout<<"Likelihood of A* agent's Path (States Only): "<<likelihood_States<<endl;                     Agent_State_Total += likelihood_States;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<likelihood_MovesAndStates<<endl;        Agent_MoveAndState_Total+= likelihood_MovesAndStates;
    
    cout<<"Likelihood of Player's Path (Moves Only): "<<player_likelihood_Moves<<endl;                  Player_Move_Total += player_likelihood_Moves;
    cout<<"Likelihood of Player's Path (States Only): "<<player_likelihood_States<<endl;                Player_State_Total+= player_likelihood_States;
    cout<<"Likelihood of Player's Path (Moves and States): "<<player_likelihood_MovesAndStates<<endl;   Player_MoveAndState_Total+= player_likelihood_MovesAndStates;
    
    
    
    movements.clear();
    for(int i=10968; i<11453; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    likelihood_Moves = getPathLikelihood(table_Moves_1, "Mario_Maps/Standard/standard_5_Path.txt");
    player_likelihood_Moves = getPathLikelihood(table_Moves_1, movements);
    
    player_likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, "Mario_Maps/Standard/standard_5_Path.txt", Medoids, "Mario_Maps/Standard/standard_5_Annotated_Path.txt");
    
    player_likelihood_States = getPathLikelihoodOnlyStates(table_State_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_States = getPathLikelihoodOnlyStates(table_State_1, "Mario_Maps/Standard/standard_5_Path.txt", Medoids, "Mario_Maps/Standard/standard_5_Annotated_Path.txt");
    
    cout<<"Level 4-1: "<<endl;
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<likelihood_Moves<<endl;                       Agent_Move_Total  += likelihood_Moves;
    cout<<"Likelihood of A* agent's Path (States Only): "<<likelihood_States<<endl;                     Agent_State_Total += likelihood_States;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<likelihood_MovesAndStates<<endl;        Agent_MoveAndState_Total+= likelihood_MovesAndStates;
    
    cout<<"Likelihood of Player's Path (Moves Only): "<<player_likelihood_Moves<<endl;                  Player_Move_Total += player_likelihood_Moves;
    cout<<"Likelihood of Player's Path (States Only): "<<player_likelihood_States<<endl;                Player_State_Total+= player_likelihood_States;
    cout<<"Likelihood of Player's Path (Moves and States): "<<player_likelihood_MovesAndStates<<endl;   Player_MoveAndState_Total+= player_likelihood_MovesAndStates;
    
    
    
    
    movements.clear();
    for(int i=18379; i<18911; i++)
        movements.push_back(findOverlap("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i+1)+".txt"));
    likelihood_Moves = getPathLikelihood(table_Moves_1, "Mario_Maps/Standard/standard_9_Path.txt");
    player_likelihood_Moves = getPathLikelihood(table_Moves_1, movements);
    
    player_likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_MovesAndStates = getPathLikelihood(table_MovesAndState_1, "Mario_Maps/Standard/standard_9_Path.txt", Medoids, "Mario_Maps/Standard/standard_9_Annotated_Path.txt");
    
    player_likelihood_States = getPathLikelihoodOnlyStates(table_State_1, movements, Medoids, "Mario_Maps/Playthrough/Converted/Third/");
    likelihood_States = getPathLikelihoodOnlyStates(table_State_1, "Mario_Maps/Standard/standard_9_Path.txt", Medoids, "Mario_Maps/Standard/standard_9_Annotated_Path.txt");
    
    cout<<"Level 6-2: "<<endl;
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<likelihood_Moves<<endl;                       Agent_Move_Total  += likelihood_Moves;
    cout<<"Likelihood of A* agent's Path (States Only): "<<likelihood_States<<endl;                     Agent_State_Total += likelihood_States;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<likelihood_MovesAndStates<<endl;        Agent_MoveAndState_Total+= likelihood_MovesAndStates;
    
    cout<<"Likelihood of Player's Path (Moves Only): "<<player_likelihood_Moves<<endl;                  Player_Move_Total += player_likelihood_Moves;
    cout<<"Likelihood of Player's Path (States Only): "<<player_likelihood_States<<endl;                Player_State_Total+= player_likelihood_States;
    cout<<"Likelihood of Player's Path (Moves and States): "<<player_likelihood_MovesAndStates<<endl;   Player_MoveAndState_Total+= player_likelihood_MovesAndStates;
    cout<<endl;
    
    cout<<"Averages: "<<endl;
    cout<<"Likelihood of A* agent's Path (Moves Only): "<<Agent_Move_Total/4<<endl;
    cout<<"Likelihood of A* agent's Path (States Only): "<<Agent_State_Total/4<<endl;
    cout<<"Likelihood of A* agent's Path (Moves and States): "<<Agent_MoveAndState_Total/4<<endl;
    
    cout<<"Likelihood of Player's Path (States Only): "<<Player_State_Total/4<<endl;
    cout<<"Likelihood of Player's Path (Moves Only): "<<Player_Move_Total/4<<endl;    
    cout<<"Likelihood of Player's Path (Moves and States): "<<Player_MoveAndState_Total/4<<endl;
    */
    
    //for(int i=4339; i<5155; i++)
    //    convertImagetoMarioMap(16, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Mario_Maps/Playthrough/MarioImageSequence/image_0"+to_string(i)+".bmp", "Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt",
    //        "standard");
    
    return 0;
    
    //convertImagetoMarioMap(16, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Mario_Maps/Playthrough/MarioImageSequence/image_00499.bmp", "Mario_Maps/Playthrough/Converted/Second/499.txt", "standard");
    
    
    
    //extract nXm windows around mario to use as chunks for clustering (done)
    /*int windowWidth = 5;
    int windowHeight = 5;
    
    for(int i=461; i<1332; i++)
        extractWindowFromFrame("Mario_Maps/Playthrough/Converted/Third/"+to_string(i)+".txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", windowWidth, windowHeight);
    
    //3X3
    //vector< vector< float> > weights;
    //weights.push_back({0.5, 1.0, 0.5});
    //weights.push_back({1.0, 0.0, 1.0});
    //weights.push_back({0.5, 1.0, 0.5});

    //5X5
    vector< vector< float> > weights;
    weights.push_back({0.1, 0.3, 0.5, 0.3, 0.1});
    weights.push_back({0.3, .75, 1.0, .75, 0.3});
    weights.push_back({0.5, 1.0, 0.0, 1.0, 0.5});
    weights.push_back({0.3, .75, 1.0, .75, 0.3});
    weights.push_back({0.1, 0.3, 0.5, 0.3, 0.1});
    
    //cluster the chunks weighting the sections nearer mario as higher importance
    
    vector<char> tileTypes = {'x', 'B', '#', '?', 'g', 'k', '-', '&', 'p', 'P'};
    Clustering WindowClusterer = Clustering(windowWidth, windowHeight);
    WindowClusterer.ComputeDifferencesWithWeights("Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/Differences.csv", &Markov, tileTypes, weights);
    WindowClusterer.kMedoids("Mario_Maps/Playthrough/Converted/Windows_5X5/Differences.csv", 20);
    WindowClusterer.PrintMedoids("Mario_Maps/Playthrough/Converted/Windows_5X5/Windows_5X5.txt", "Mario_Maps/Playthrough/Converted/Windows_5X5/medoids_20.txt");
    
    return 0;
    */
    //assign each frame a cluster
    
    
    //train a probabilistic FSM using the clusters as states and the frames for transitions between the states
    
    
    
    
    //likelihood experiments
    //Learner likelihoodComputer = TrainWithPartialMap("Mario_Maps/Deep/Standard/", "deep_standard_1", "Mario_Maps/Deep/Standard/deep_styandard_", 25, 3);
    
    
    
    
    
    
    //for(int m=1; m<=30; m++){
    /*int m=30;
    string mapFolder = "Mario_Maps/Deep/Standard/";
    string mapName = "deep_standard_";
    // Testing the effects of the amount of training data provided
    Learner model = Learner(1, 0, false, false);
    model.FindAllTileTypes(30, mapFolder+"deep_standard_", false);
    
    model.SetDepMatrix(3);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    model.SetTotalsTopLevel(mapFolder+mapName, m, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    
    for(int i=1; i<=30; i++){
        
        ifstream input(mapFolder+"deep_standard_"+to_string(i)+".txt");
        
        string line;
        vector< vector< char> > map;
        while(getline(input, line)){
            vector< char > row;
            for(int c=0; c<line.size(); c++){
                if(line[c] != '\n' && line[c] != '\t' && line[c] != '\r' && line[c] != '\0')
                   row.push_back(line[c]);
            }
            map.push_back(row);
        }
        
        pair<long double, double> output = MapLikelihood(map, model);
    
        cout<<output.first<<","<<output.second<<",";
    }
    cout<<endl;
//}*/
    //============================================================================================================================================
    //testing plaigarism metric
    
    //MeasurePlagiarism("New_Experiments/MoreExpressive/mario_1.txt", "Mario_Maps/Deep/All/deep_20.txt");
    
    //============================================================================================================================================
    
    //Experiments with more expressive representations
    //Learner placeholder = Learner(0,0,false, false);
   // MdMCExperiments("Mario_Maps/Deep/Standard/", "deep_standard_", "New_Experiments/MoreExpressive/Standard/", "mario_", 4, 3, 3, 30, 100, 14, 210, placeholder, 1);
    
    //for(int i=1; i<=100; i++)
    //    ConvertMarioMapToImage(210, 14, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Standard/mario_"+to_string(i)+".txt", "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Standard/mario_"+to_string(i)+".bmp", "standard");
    
    
    //return 0;
    
    
    
    
    //Classical Hierarchical Approach
    //ClusterAndConvert("Mario_Maps/Deep/Standard/", "deep_standard_", 30, 4, 4, 20, &Count, "Count", true);
    //TrainAndSample("Mario_Maps/Deep/Standard/", "20-4X4-Count/", "deep_standard_", "New_Experiments/MoreExpressive/Classic_Hierarchical/Count_20/", "H_mario_", "mario_", 30, 4, 4, 100, 50, 4, 2, 2, 1, 3, 3, false);
    //for(int i=0; i<100; i++)
    //    ConvertMarioMapToImage(200, 16, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Classic_Hierarchical/Count_20/mario_"+to_string(i)+".txt", "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Classic_Hierarchical/Count_20/mario_"+to_string(i)+".bmp", "standard");
    //return 0;
    
    
    //Multi-layered Hierarchies
    //ClusterAndConvert("Mario_Maps/Deep/All/", "deep_", 54, 2, 2, 8, &Shape, "Shape", true);
    //ClusterAndConvert("Mario_Maps/Deep/All/8-2X2-Shape/", "deep_", 54, 2, 2, 40, &Direct, "Direct", true);
    //ClusterAndConvert("Mario_Maps/Deep/All/8-2X2-Shape/40-2X2-Direct/", "deep_", 54, 2, 2, 30, &Direct, "Direct", true);
    //ClusterAndConvert("Mario_Maps/Deep/All/8-2X2-Shape/40-2X2-Direct/30-2X2-Direct/", "deep_", 54, 2, 2, 20, &Direct, "Direct", true);
    //ClusterAndConvert("Mario_Maps/Deep/All/8-2X2-Shape/40-2X2-Direct/30-2X2-Direct/20-2X2-Direct/", "deep_", 54, 9, 1, 5, &Direct, "Direct", true);
    
    
    //TrainAndSample("Mario_Maps/Deep/All/50-2X2-Direct/40-2X2-Direct/30-2X2-Direct/20-2X2-Direct/", "5-9X1-Direct/", "deep_", "New_Experiments/MoreExpressive/Hierarchical/", "H_5_mario_", "H_4_mario_", 54, 9, 1, 100, 2, 1, 3, 3, 1, 3, 3, false);
    
    //TrainAndSampleHighLevelProvided("Mario_Maps/Deep/All/50-2X2-Direct/40-2X2-Direct/30-2X2-Direct/", "20-2X2-Direct/", "deep_", "New_Experiments/MoreExpressive/Hierarchical/", "H_4_mario_", "H_3_mario_", 54, 2, 2, 100, 18, 1, 3, 3, 1, 3, 3, false);
    
    //TrainAndSampleHighLevelProvided("Mario_Maps/Deep/All/50-2X2-Direct/40-2X2-Direct/", "30-2X2-Direct/", "deep_", "New_Experiments/MoreExpressive/Hierarchical/", "H_3_mario_", "H_2_mario_", 54, 2, 2, 100, 36, 2, 3, 3, 1, 3, 3, false);
    
    //TrainAndSampleHighLevelProvided("Mario_Maps/Deep/All/50-2X2-Direct/", "40-2X2-Direct/", "deep_", "New_Experiments/MoreExpressive/Hierarchical/", "H_2_mario_", "H_1_mario_", 54, 2, 2, 100, 72, 4, 3, 3, 1, 3, 3, false);

    //TrainAndSampleHighLevelProvided("Mario_Maps/Deep/All/", "50-2X2-Direct/", "deep_", "New_Experiments/MoreExpressive/Hierarchical/", "H_1_mario_", "LL_mario_", 54, 2, 2, 100, 144, 8, 3, 6, 1, 3, 3, false);
    
    //for(int i=20; i<100; i++)
    //    ConvertMarioMapToImage(288, 16, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Hierarchical/LL_mario_"+to_string(i)+".txt", "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/MoreExpressive/Hierarchical/LL_mario_"+to_string(i)+".bmp");
    

    return 0;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //ConvertMarioMapToImage(75, 12, "/Users/sps74/Dropbox/shared Sam/DrexelPhd/Proposal/Mario_Constrained.txt", "/Users/sps74/Dropbox/shared Sam/DrexelPhd/Proposal/Mario_Constrained.bmp");
    //ConvertKIMapToImage(16, 53, "/Users/sps74/Dropbox/shared Sam/DrexelPhd/Proposal/KI_Constrained.txt", "/Users/sps74/Dropbox/shared Sam/DrexelPhd/Proposal/KI_Contrained.bmp");
    
    
    
//    vector<char> mapping{'S', '-', '-', '?', '#','#','e'};
    //ConvertSourceToTarget(mapping, "KidIcarus_Maps/Standard/kidicarus_", 6, "DomainAbstractionTests/KI_to_SMB_");
//    pair<int, int> ret = FindCommonTileMappings(vector<char>{'S', '-', 'T', '#', 'M', '#', 'T', '#', 'D'}, mapping,
//                                 vector<char>{'S', '-', 'e', '?', 'B', '#', 'p', 'P', 'c'}, vector<char>{'S', '#', '-', 'D', 'T', 'M', 'H'});
    
//    cout<<ret.first<<" "<<ret.second<<endl;
    
    /*for(int i=1; i<=150; i++){
        if(LoderunnerPlayabilityConstraint("Loderunner_Maps/Standard/Level "+to_string(i)+".txt", 0, 0))
            cout<<i<<endl;
    }*/
    
    //tetsing DA
    //KI->SMB
    //CompareRelativeTileFrequency("KidIcarus_Maps/Standard/kidicarus_", "Mario_Maps/Enemies/mario_", 6, 16, "DomainAbstractionTests/KI_SMB.txt", 10);
    
    //SMB->KI
    //CompareRelativeTileFrequency("Mario_Maps/Enemies/mario_", "KidIcarus_Maps/Standard/kidicarus_", 16, 6, "DomainAbstractionTests/SMB_KI.txt", 2);
    
    //LR->SMB
    //CompareRelativeTileFrequency("Loderunner_Maps/Standard/Level ", "Mario_Maps/Enemies/mario_", 150, 16, "DomainAbstractionTests/LR_SMB.txt", 1);

    //SMB->LR
    //CompareRelativeTileFrequency("Mario_Maps/Enemies/mario_", "Loderunner_Maps/Standard/Level ", 16, 150, "DomainAbstractionTests/SMB_LR.txt", 1);

    //KK->SMB
    //CompareRelativeTileFrequency("KidKool_Maps/Outside/kidkool_", "Mario_Maps/Enemies/mario_", 12, 16, "DomainAbstractionTests/KK_SMB.txt", 1);
    
    //SMB->KK
    //CompareRelativeTileFrequency("Mario_Maps/Enemies/mario_", "KidKool_Maps/Outside/kidkool_", 16, 12, "DomainAbstractionTests/SMB_KK.txt", 1);
    
    /*ManualConversion("Mario_Maps/Enemies/", "Manual2X2/", "mario_", 16, 2, 2);
    ManualConversion("Mario_Maps/Enemies/", "Manual3X3/", "mario_", 16, 3, 3);
    ManualConversion("Mario_Maps/Enemies/", "Manual4X4/", "mario_", 16, 4, 4);
    ManualConversion("Mario_Maps/Enemies/", "Manual6X6/", "mario_", 16, 6, 6);
    ManualConversion("Mario_Maps/Enemies/", "Manual12X12/", "mario_", 16, 12, 12);
    */
    
    //PlayabilityContraintSampling("/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Mario_Maps/Castle/", "mario-castle_", "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/CastleLevels/", "mario_", 12, 3, 3, 8, 10, 12, 210, 21);
    
    //for(int i=0; i<1; i++)
    //    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/CastleLevels/mario_"+to_string(i)+".txt", "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/CastleLevels/mario_"+to_string(i)+".bmp");
    //return 0;
    
    //ConvertKIMapToImage(16, 160, "Old_Experiments/TCIAIG_Experiments/MRF/KidIcarus/NoSmoothing/Sample_58.txt", "KIMRF.bmp");
    //return 0;
    
    /*for(int z=2; z<=12; z++){
        for(int lh=0; lh<=10; lh++){
            if(z == 5 || (z > 6 && z < 12) || lh == 4 || (lh > 5 && lh < 10))
                continue;
            else
                TrainAndSample("Mario_Maps/Enemies/", "Manual"+to_string(z)+"X"+to_string(z)+"/", "mario_", "Proposal_Experiments/HMdMC/Manual/SMB/"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"/", "HighLevel_", "LowLevel_", 16, z, z, 1000, 210/z, 12/z, 2, 3, 1, lh, 3, true);
        }
    }*/
    
    
    /*int playability=0;
    for(int i=0; i<1000; i++){
        if(KidIcarusPlayabilityConstraint("Old_Experiments/TCIAIG_Experiments/MRF/KidIcarus/NoSmoothing/Sample_"+to_string(i)+".txt", 0, 0)){
            cout<<i<<endl;
            playability++;
        }
        
    }
    cout<<playability<<endl;
    return 0;
    */
    /*
    int trainingMaps =16;
    string mapname = "Mario_Maps/Enemies/";
    string map = "mario_";
    
    int width = 210;
    int height = 12;
    
    for(int z=2; z<=12; z++){
        cout<<"Z= "<<z<<endl;
        for(int lh=0; lh<=10; lh++){
            if(z == 5 || (z > 6 && z < 12) || lh == 4 || (lh > 5 && lh < 10))
                continue;
            else
                MapLikelihood(<#vector<vector<char> > Map#>, <#Learner &model#>);
                //TrainAndSample("Mario_Maps/Enemies/", "Manual"+to_string(z)+"X"+to_string(z)+"/", "mario_", "Proposal_Experiments/HMdMC/Manual/SMB/"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"/", "HighLevel_", "LowLevel_", 16, z, z, 1000, width/z, height/z, 2, 3, 1, lh, 3, true);
        }
        
        cout<<"==============="<<endl;
    }
    
    
    //int z=4;
    //int lh=3;
    //int k=16;
    
    //TrainAndSample(mapname, to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-Direct/", map, "Proposal_Experiments/HMdMC/Clustering/SMB/"+to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"-Direct/", "HighLevel_", "LowLevel_", trainingMaps, z, z, 1000, width/z, height/z, 2, 3, 1, lh, 3, false);
    //TrainAndSample(mapname, to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-Count/", map, "Proposal_Experiments/HMdMC/Clustering/SMB/"+to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"-Count/", "HighLevel_", "LowLevel_", trainingMaps, z, z, 1000, width/z, height/z, 2, 3, 1, lh, 3, false);
    //TrainAndSample(mapname, to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-Markov/", map, "Proposal_Experiments/HMdMC/Clustering/SMB/"+to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"-Markov/", "HighLevel_", "LowLevel_", trainingMaps, z, z, 1000, width/z, height/z, 2, 3, 1, lh, 3, false);
    
    
    return 0;
    */
    
    /*int trainingmaps =6;
    string mapname = "KidIcarus_Maps/Standard/kidicarus_";

    Learner TopLevelLearner = Learner(1, 0, false, false);
    TopLevelLearner.FindAllTileTypes(trainingmaps, mapname, false);
    
    //for(int i=networkStructure; i>=0; i--)
    TopLevelLearner.SetDepMatrix(3);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    //for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
    TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    //   for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
    TopLevelLearner.SetTotalsTopLevel(mapname, trainingmaps, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    // for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
    TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);

    
    int z=4;
    int k=20;
    string d="Shape";
    int diff=0;
    int lh=3;
    
    
    for(int di=0; di<=2; di++){
        
        if(di==0)
            d="Direct";
        if(di==1)
            d="Count";
        if(di==2)
            d="Markov";
        if(di==3)
            d="Shape";
        
        
        float total_likelihood =0;
        float total_unseenstates=0;
        //cout<<"Proposal_Experiments/HMdMC/Clustering/LR/"+to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"-"+d+"/LowLevel_"<<endl;
        for (int i = 0; i < 1000; i++){
            ifstream map("Proposal_Experiments/HMdMC/Clustering/KI/"+to_string(k)+"-"+to_string(z)+"X"+to_string(z)+"-"+to_string(lh)+"-"+d+"/LowLevel_"+to_string(i)+".txt");
            string line;
            vector<vector<char> > Map;
            while(getline(map, line)){
                vector<char> row;
                for(int c=0; c<line.size(); c++){
                    row.push_back(line[c]);
                }
                Map.push_back(row);
            }
        
            pair<long double, float> tmp = MapLikelihood(Map, TopLevelLearner);
            total_likelihood+=tmp.first;
            total_unseenstates+=tmp.second;
        }
        cout<<d<<": "<<total_likelihood/1000.0<<" "<<total_unseenstates/1000.0<<endl;
    }

    
    return 0;
    
    
    
    
    
    /*ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 8, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 12, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 20, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 24, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 28, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Enemies/", "mario_", 16, 4, 4, 32, &Shape, "Shape");
    */
/*    ClusterAndConvert("Mario_Maps/Standard/", "kidicarus_", 6, 8, 8, 20, &Shape, "Shape");
    ClusterAndConvert("Mario_Maps/Standard/", "kidicarus_", 6, 16, 16, 20, &Shape, "Shape");
    ClusterAndConvert("Ki_Maps/Standard/", "kidicarus_", 6, 4, 4, 24, &Shape, "Shape");
    ClusterAndConvert("KidIcarus_Maps/Standard/", "kidicarus_", 6, 4, 4, 28, &Shape, "Shape");
    ClusterAndConvert("KidIcarus_Maps/Standard/", "kidicarus_", 6, 4, 4, 32, &Shape, "Shape");
    ClusterAndConvert("KidIcarus_Maps/Standard/", "kidicarus_", 6, 4, 4, 20, &Shape, "Shape");
  */
    
    
    //ConvertMarioMapToImage(208, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Proposal_Experiments/HMdMC/Clustering/SMB/12-4X4-3-Shape/LowLevel_1.txt", "Mario_3X3-clustering.bmp");
    //return 0;
    //for(int i=0; i<1000; i++){
    //if(LoderunnerPlayabilityConstraint("Proposal_Experiments/MdMC/LR/LookAhead/3/Level "+to_string(i)+".txt", 0, 0))
    //    cout<<i<<" is plalayble"<<endl;
    //}
    //return 0;
    //Learner placeholder = Learner(0,0,false, false);
    
   // MdMCExperiments(<#string trainingFolderName#>, <#string trainingMapName#>, <#string outputFolderName#>, <#string outputMapName#>, <#int rowSplits#>, <#int lookAhead#>, <#int networkStructure#>, <#int trainingMaps#>, <#int toGenerate#>, <#int height#>, <#int width#>, <#Learner model#>, <#int startingMap#>)
    //MdMCExperiments("KidIcarus_Maps/Standard/", "kidicarus_", "TestingWebPage/", "kidicarus_", 1, 0, 3, 6, 10, 170, 16, placeholder, 0);
    
/*    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/Final/S-PBec#X/mario_7.txt", "KI_B2.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Manual/S-##e##/mario_1.txt", "KI_M1.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Random/S-c#B?e/mario_2.txt", "KI_R23.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Random/Final/S-B#ppp/mario_3.txt", "KI_R12.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Best/Final/S-#ee---B#?-/mario_3.txt", "KK_B3.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Manual/S-#e#B?P--?c/mario_7.txt", "KK_M3.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Random/S-#eeBB?#B--/mario_4.txt", "KK_R23.bmp");
    ConvertMarioMapToImage(210, 12, "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Random/NewRandom/S-#e?#?P--?e/mario_5.txt", "KK_R11.bmp");
  */
    
    //Demo(12, 3, 16, "mario_", "Mario_Maps/Enemies/", 3, 210, 12, "Testing/", 10);
    
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/S-?Bec#/mario_0.txt", "KI_SMB_B.bmp");
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Random/S--?-Bc/mario_0.txt", "KI_SMB_R.bmp");
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Manual/S-##e##/mario_0.txt", "KI_SMB_M.bmp");
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Best/S-#ee-B?#---/mario_0.txt", "KK_SMB_B.bmp");
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Random/S-#e?ce?#B--/mario_0.txt", "KK_SMB_R.bmp");
    //ConvertMarioMapToImage(210, 12, "New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Manual/S-#e?B#pB--c/mario_0.txt", "KK_SMB_M.bmp");
    
    //ConvertMarioMapToImage(11, 11, "tiles.txt", "tiles.bmp");
    
    //ConvertMarioMapToImage(192, 12, "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/S-?Bec#/mario_0.txt", "Testing.bmp");
    
    
    //used to cluster chunks of maps together in order to find high-level tiles, then converts low-level maps into their high-level representation
    //ClusterAndConvert("Mario_Maps/DA_Clustering/", "mario_", 34, 4, 4, 40, &Count, "Count");
    
    
    //Thesis Proposal Experiments

    //bool TopDown = false;
    //bool Hierarchical = false;
    //int toGenerate = 1000;
    //int DepMatrix = 3;
    
    //SMB
    //int trainingMaps = 16;
    //string trainingMapName = "mario_";
    //string trainingFolderName = "Mario_Maps/Enemies/";
    //int width = 210;
    //int height = 12;
    //int rows = 6;
    //int lookahead = 2;
    //string outputFolder = "Proposal_Experiments/MdMC/SMB/LookAhead/";
    //string outputMapName= "mario_";
    
    
    //KI
    //int trainingMaps = 6;
    //string trainingMapName = "kidicarus_";
    //string trainingFolderName = "KidIcarus_Maps/Standard/";
    //int width = 16;
    //int height = 160;
    //int rows = 80;
    //int lookahead = 3;
    //string outputFolder = "Proposal_Experiments/MdMC/KI/LookAhead/";
    //string outputMapName= "kidicarus_";
    
    //LR
    //int trainingMaps = 150;
    //string trainingMapName = "Level ";
    //string trainingFolderName = "Loderunner_Maps/Standard/";
    //int width = 30;
    //int height = 20;
    //int rows = 10;
    //int lookahead=5;
    //string outputFolder = "Proposal_Experiments/MdMC/LR/LookAhead/";
    //string outputMapName= "Level ";
    
    
    
    
    
    //Likelihood computer
    /*Learner TopLevelLearner = Learner(1, 0, false, false);
    TopLevelLearner.FindAllTileTypes(trainingMaps, trainingFolderName+trainingMapName, false);
    
    //for(int i=networkStructure; i>=0; i--)
    TopLevelLearner.SetDepMatrix(3);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    //for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
    TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    //   for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
    TopLevelLearner.SetTotalsTopLevel(trainingFolderName+trainingMapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    // for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
    TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    //level generation
    for(int l=0; l<=10; l++){
        if(l==4 || (l>5 && l<10))
            continue;
        double total_likelihood=0;
        double total_unseenstates=0;
        
        //Train
        Learner L = Learner(rows, l, false, false);
        L.FindAllTileTypes(trainingMaps, trainingFolderName+trainingMapName, false);
    
        for(int i=3; i>=0; i--)
            L.SetDepMatrix(i);
    
        L.SetRows(false);
        L.ResizeVectors(false);
    
        //initializes the files necessary for storing the probabilities and totals
        for (int i = 0; i < L.Config.NumberMatrices; i++)
            L.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
        //records the encountered totals for each tile type
        for (int dep = 0; dep < L.Config.NumberMatrices; dep++)
            L.SetTotalsTopLevel(trainingFolderName+trainingMapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
        //sets the probabilities using the totals
        for (int dep = 0; dep < L.Config.NumberMatrices; dep++)
            L.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
        //Sample
        Generator TopGen = Generator(width, height);
        TopGen.CopyConfig(L.Config);
        TopGen.SetTileTypes(L.TileTypes, false);
        TopGen.ResizeVectors(false);
        
        for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
            TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
        
        #if defined(_WIN32)
            _mkdir(outputFolder+to_string(l)+"/");
        #else
            mkdir((outputFolder+to_string(l)+"/").c_str(), 0777);
        #endif
        
        float playable=0;
        
        for (int i = 0; i < toGenerate; i++){
            TopGen.GenerateTopLevel(outputFolder+to_string(l)+"/"+outputMapName + to_string(i) + ".txt");
                pair<long double, float> tmp = MapLikelihood(TopGen.Map, TopLevelLearner);
                total_likelihood+=tmp.first;
                total_unseenstates+=tmp.second;
            if(MarioPlayabilityConstraint(TopGen.Map, 0, 0))
            //if(KidIcarusPlayabilityConstraintSection(TopGen.Map, 0, 0))
            //if(LoderunnerPlayabilityConstraint(TopGen.Map, 0, 0))
                playable++;
            
        }
        cout<<playable/toGenerate<<" "<<total_likelihood/toGenerate<<" "<<total_unseenstates/toGenerate<<endl;
    }
    */
    
    
    
    //compute the likelihood of the maps
    /*for(int r=1; r<=12; r++){
        if(ceil(12.0/r) != 12.0/r)
            continue;
        double total_likelihood=0;
        double total_unseenStates=0;
        for(int i=0; i<100; i++){
            vector<vector<char>> map;
            string line;
            ifstream input("Proposal_Experiments/MdMC/SMB/RowSplits/"+to_string(r)+"/mario_"+to_string(i)+".txt");
            //ifstream input("Old_Experiments/TCIAIG_Experiments/MdMC_Mario_Standard/16Maps/123/Generated_"+to_string(i)+".txt");
            while(getline(input, line)){
                vector<char> row;
                for(int k=0; k<line.length(); k++)
                    row.push_back(line[k]);
                map.push_back(row);
                row.clear();
            }
            pair<double, int> tmp = MapLikelihood(map, TopLevelLearner);
            total_likelihood+= tmp.first;
            total_unseenStates+=tmp.second;
        }
    
        cout<<r<<": "<<total_likelihood/100.0<<" "<<total_unseenStates/100.0<<endl;
    }
    
    //cout<<"Done Learning"<<endl;
    */
    /*Generator TopGen = Generator(width, height);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    #if defined(_WIN32)
        _mkdir(outputFolder);
    #else
        mkdir(outputFolder.c_str(), 0777);
    #endif
    
    int playable=0;

    for (int i = 0; i < toGenerate; i++){
        TopGen.GenerateTopLevel(outputFolder+outputMapName + to_string(i) + ".txt");
        //if(MarioPlayabilityConstraint(TopGen.Map, 0, 0))
        if(KidIcarusPlayabilityConstraintSection(TopGen.Map, 0, 0))
        //if(LoderunnerPlayabilityConstraint(TopGen.Map, 0, 0))
            playable++;
    }
    cout<<playable<<endl;
    */
    
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //TILE MAPPING
    //Comparing two levels
    //count the number of each high-level tile type in each map
    //take the absolute difference between the two
    //average that total over the length of the map (or number of tiles?)
    
    //string in = "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/Mario_Maps/Enemies/";
    /*string in= "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/SingleLevel/";
    string out= "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/New_Experiments/DomainAdaptation/TileMapping/Renamed/";
    
    for(int i=0; i<100; i++){
        system(("cp "+in+"mario_"+to_string(i)+".txt "+out).c_str());
        rename((out+"mario_"+to_string(i)+".txt").c_str(), (out+"S_"+to_string(i)+".txt").c_str());
    }
    */
    
    //convert the low-level training maps into high-level training maps using the found cluster medoids
    
    //ClusterAndConvert("Mario_Maps/DA_Clustering/", "mario_", 40, 4, 4, 40, &Count, "Count");
    /*vector<vector<vector<char> > > Medoids;
    
    //read in the medoids (high-level tiles)
    ifstream Medoid_File;
    string line;
    Medoid_File.open("Mario_Maps/DA_Clustering/Count_Medoids_"+to_string(40)+"_"+to_string(4)+"X"+to_string(4)+".txt");
    
    while(!getline(Medoid_File, line).eof()){
        vector<vector< char > > Block;
        for(int h = 0; h < 4; h++){
            getline(Medoid_File, line);
            vector< char > temp;
            for(int w=0; w<4; w++)
                temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(Medoid_File, line);
        Medoids.push_back(Block);
    }
    Learner tileFinder = Learner(1, 0, true, false);
    tileFinder.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    //*/
    
    
    //9228
    /*ofstream output("New_Experiments/DomainAdaptation/TileMapping/PairwiseMarioMapDifferencesMoreTiles.tsv");
    ofstream output2("New_Experiments/DomainAdaptation/TileMapping/MarioMapDifferenceClasses.tsv");
    for(int domain1=0; domain1<4; domain1++){
        string domainName1="";
        if(domain1 == 0)
            domainName1 = "T_";
        else if(domain1 == 1)
            domainName1 = "S_";
        else if(domain1 == 2)
            domainName1 = "KI_";
        else if(domain1 == 3)
            domainName1 = "KK_";
        
        for(int method1 = 0; method1<4; method1++){
            string methodName1="";
            if(method1 == 0 && domain1 > 1)
                methodName1 = "B";
            else if(method1 == 1 && domain1 > 1)
                methodName1 = "M";
            else if(method1 == 2 && domain1 > 1)
                methodName1 = "UIR";
            else if(method1 == 3 && domain1 > 1)
                methodName1 = "PTR";
            else if(method1 > 0 && domain1 <= 1)
                continue;
            
            
            for(int mapping1 =1; mapping1 <4; mapping1++){
                string mappingName1="";
                if(domain1 > 1)
                    mappingName1 = to_string(mapping1)+"_";
                else if(mapping1 > 1 && domain1 <= 1)
                    continue;
                
                
                int numberOfMaps1 = 17;
                int start1=1;
                if(domain1 != 0){
                    numberOfMaps1 = 100;
                    start1=0;
                }
                for(int i=start1; i<numberOfMaps1; i++){
                    cout<<domainName1+methodName1+mappingName1<<i<<endl;
                    output<<domainName1+methodName1+mappingName1+to_string(i);
                    output2<<domainName1+methodName1+mappingName1+to_string(i)<<'\t'<<domainName1+methodName1+mappingName1<<endl;
                    //convert the low-level maps into high-level maps, and put them in the newly created subfolder
                    //MapReader Converter = MapReader();
                    //Converter.ConvertAllTileMapsToHier(domainName1+methodName1+mappingName1, "New_Experiments/DomainAdaptation/TileMapping/Renamed/", "New_Experiments/DomainAdaptation/TileMapping/Renamed/high_level_", numberOfMaps1, 4, 4, Medoids, &Count, "Count", vector<vector<float>>(), tileFinder.TileTypes);
                    ifstream input1("New_Experiments/DomainAdaptation/TileMapping/Renamed/high_level_"+domainName1+methodName1+mappingName1+to_string(i)+".txt");
                    vector<double> map1_counts(41, 0);
                    string line;
                    int total_tiles=0;
                    while(getline(input1, line)){
                        for(int k=0; k<line.length(); k++){
                            char curr = line[k];
                            if(curr == 'A'){
                                map1_counts[0]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'B'){
                                map1_counts[1]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'C'){
                                map1_counts[2]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'D'){
                                map1_counts[3]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'E'){
                                map1_counts[4]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'F'){
                                map1_counts[5]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'G'){
                                map1_counts[6]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'H'){
                                map1_counts[7]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'I'){
                                map1_counts[8]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'J'){
                                map1_counts[9]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'K'){
                                map1_counts[10]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'L'){
                                map1_counts[11]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'M'){
                                map1_counts[12]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'N'){
                                map1_counts[13]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'O'){
                                map1_counts[14]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'P'){
                                map1_counts[15]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'Q'){
                                map1_counts[16]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'R'){
                                map1_counts[17]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'S'){
                                map1_counts[18]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'T'){
                                map1_counts[19]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'U'){
                                map1_counts[20]++;
                                total_tiles++;
                                continue;
                            } else if(curr == 'V'){
                                map1_counts[21]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'W'){
                                map1_counts[22]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'X'){
                                map1_counts[23]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'Y'){
                                map1_counts[24]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'Z'){
                                map1_counts[25]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'a'){
                                map1_counts[26]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'b'){
                                map1_counts[27]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'c'){
                                map1_counts[28]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'd'){
                                map1_counts[29]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'e'){
                                map1_counts[30]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'f'){
                                map1_counts[31]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'g'){
                                map1_counts[32]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'h'){
                                map1_counts[33]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'i'){
                                map1_counts[34]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'j'){
                                map1_counts[35]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'k'){
                                map1_counts[36]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'l'){
                                map1_counts[37]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'm'){
                                map1_counts[38]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'n'){
                                map1_counts[39]++;
                                total_tiles++;
                                continue;
                            }else if(curr == 'o'){
                                map1_counts[40]++;
                                total_tiles++;
                                continue;
                            } else
                                cout<<"Tile type not found: "<<curr<<endl;
                        }
                    }
                    
                    for(int k=0; k<map1_counts.size(); k++){
                        map1_counts[k]/=total_tiles;
                    }
                    
                    //second map for comparison
                    for(int domain2=0; domain2<4; domain2++){
                        string domainName2="";
                        if(domain2 == 0)
                            domainName2 = "T_";
                        else if(domain2 == 1)
                            domainName2 = "S_";
                        else if(domain2 == 2)
                            domainName2 = "KI_";
                        else if(domain2 == 3)
                            domainName2 = "KK_";
                        
                        for(int method2 = 0; method2<4; method2++){
                            string methodName2="";
                            if(method2 == 0 && domain2 > 1)
                                methodName2 = "B";
                            else if(method2 == 1 && domain2 > 1)
                                methodName2 = "M";
                            else if(method2 == 2 && domain2 > 1)
                                methodName2 = "UIR";
                            else if(method2 == 3 && domain2 > 1)
                                methodName2 = "PTR";
                            else if(method2 > 0 && domain2 <= 1)
                                continue;
                            
                            
                            for(int mapping2 =1; mapping2 <4; mapping2++){
                                string mappingName2="";
                                if(domain2 > 1)
                                    mappingName2 = to_string(mapping2)+"_";
                                else if(mapping2 > 1 && domain2 <= 1)
                                    continue;
                                
                                
                                int numberOfMaps2 = 17;
                                int start2=1;
                                if(domain2 != 0){
                                    numberOfMaps2 = 100;
                                    start2=0;
                                }
                                cout<<"    "<<domainName2+methodName2+mappingName2<<endl;
                                for(int ii=start2; ii<numberOfMaps2; ii++){
                                   ifstream input2("New_Experiments/DomainAdaptation/TileMapping/Renamed/high_level_"+domainName2+methodName2+mappingName2+to_string(ii)+".txt");
                                    vector<double> map2_counts(41, 0);
                                    string line;
                                    total_tiles=0;
                                    while(getline(input2, line)){
                                        for(int k=0; k<line.length(); k++){
                                            if(line[k] =='\301')
                                                continue;
                                            char curr = line[k];
                                            if(curr == 'A'){
                                                map2_counts[0]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'B'){
                                                map2_counts[1]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'C'){
                                                map2_counts[2]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'D'){
                                                map2_counts[3]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'E'){
                                                map2_counts[4]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'F'){
                                                map2_counts[5]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'G'){
                                                map2_counts[6]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'H'){
                                                map2_counts[7]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'I'){
                                                map2_counts[8]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'J'){
                                                map2_counts[9]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'K'){
                                                map2_counts[10]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'L'){
                                                map2_counts[11]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'M'){
                                                map2_counts[12]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'N'){
                                                map2_counts[13]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'O'){
                                                map2_counts[14]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'P'){
                                                map2_counts[15]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'Q'){
                                                map2_counts[16]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'R'){
                                                map2_counts[17]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'S'){
                                                map2_counts[18]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'T'){
                                                map2_counts[19]++;
                                                total_tiles++;
                                                continue;
                                            } else if(curr == 'U'){
                                                map2_counts[20]++;
                                                total_tiles++;
                                                continue;
                                            }  else if(curr == 'V'){
                                                map2_counts[21]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'W'){
                                                map2_counts[22]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'X'){
                                                map2_counts[23]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'Y'){
                                                map2_counts[24]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'Z'){
                                                map2_counts[25]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'a'){
                                                map2_counts[26]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'b'){
                                                map2_counts[27]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'c'){
                                                map2_counts[28]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'd'){
                                                map2_counts[29]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'e'){
                                                map2_counts[30]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'f'){
                                                map2_counts[31]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'g'){
                                                map2_counts[32]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'h'){
                                                map2_counts[33]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'i'){
                                                map2_counts[34]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'j'){
                                                map2_counts[35]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'k'){
                                                map2_counts[36]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'l'){
                                                map2_counts[37]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'm'){
                                                map2_counts[38]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'n'){
                                                map2_counts[39]++;
                                                total_tiles++;
                                                continue;
                                            }else if(curr == 'o'){
                                                map2_counts[40]++;
                                                total_tiles++;
                                                continue;
                                            } else
                                                cout<<"Tile type not found: "<<curr<<endl;
                                        }
                                    }
                                    
                                    for(int k=0; k<map2_counts.size(); k++){
                                        map2_counts[k]/=total_tiles;
                                    }
                    
                                    double totalDistance=0;
                                    //Find the difference in the tile distributions
                                    for(int k=0; k<map1_counts.size(); k++){
                                        totalDistance+=abs(map1_counts[k] - map2_counts[k]);
                                    }
                                    output<<'\t'<<totalDistance;
                    
                                }
                            }
                        }
                    }
                    output<<endl;
                }
            }
        }
    }
    output.close();
    output2.close();
    
    //*/
    
    
    /*ofstream output("New_Experiments/DomainAdaptation/TileMapping/PairwiseMarioMapDifferences.tsv");
    
    for(int i=1; i<=16; i++){
        ifstream input1("Mario_Maps/Enemies/20-4-S/mario_"+to_string(i)+".txt");
        vector<double> map1_counts(21, 0);
        
        string line;
        int total_tiles=0;
        while(getline(input1, line)){
            vector<char> tmp;
            for(int k=0; k<line.length(); k++){
                char curr = line[k];
                if(curr == 'A'){
                    map1_counts[0]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'B'){
                    map1_counts[1]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'C'){
                    map1_counts[2]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'D'){
                    map1_counts[3]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'E'){
                    map1_counts[4]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'F'){
                    map1_counts[5]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'G'){
                    map1_counts[6]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'H'){
                    map1_counts[7]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'I'){
                    map1_counts[8]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'J'){
                    map1_counts[9]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'K'){
                    map1_counts[10]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'L'){
                    map1_counts[11]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'M'){
                    map1_counts[12]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'N'){
                    map1_counts[13]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'O'){
                    map1_counts[14]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'P'){
                    map1_counts[15]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'Q'){
                    map1_counts[16]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'R'){
                    map1_counts[17]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'S'){
                    map1_counts[18]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'T'){
                    map1_counts[19]++;
                    total_tiles++;
                    continue;
                } else if(curr == 'U'){
                    map1_counts[20]++;
                    total_tiles++;
                    continue;
                } else
                    cout<<"Tile type not found: "<<curr<<endl;
            }
        }
        
        for(int k=0; k<map1_counts.size(); k++){
            map1_counts[k]/=total_tiles;
        }
        
        for(int j=1; j<=16; j++){
            ifstream input2("Mario_Maps/Enemies/20-4-S/mario_"+to_string(j)+".txt");
            vector<double> map2_counts(21, 0);
            
            total_tiles=0;
            while(getline(input2, line)){
                for(int k=0; k<line.length(); k++){
                    char curr = line[k];
                    if(curr == 'A'){
                        map2_counts[0]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'B'){
                        map2_counts[1]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'C'){
                        map2_counts[2]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'D'){
                        map2_counts[3]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'E'){
                        map2_counts[4]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'F'){
                        map2_counts[5]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'G'){
                        map2_counts[6]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'H'){
                        map2_counts[7]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'I'){
                        map2_counts[8]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'J'){
                        map2_counts[9]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'K'){
                        map2_counts[10]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'L'){
                        map2_counts[11]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'M'){
                        map2_counts[12]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'N'){
                        map2_counts[13]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'O'){
                        map2_counts[14]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'P'){
                        map2_counts[15]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'Q'){
                        map2_counts[16]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'R'){
                        map2_counts[17]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'S'){
                        map2_counts[18]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'T'){
                        map2_counts[19]++;
                        total_tiles++;
                        continue;
                    } else if(curr == 'U'){
                        map2_counts[20]++;
                        total_tiles++;
                        continue;
                    } else
                        cout<<"Tile type not found: "<<curr<<endl;
                }
            }
            double totalDistance=0;
            //normalize the number of tiles
            for(int k=0; k<map2_counts.size(); k++){
                map2_counts[k]/=total_tiles;
            }
            
            
            //Find the difference in the tile distributions
            for(int k=0; k<map1_counts.size(); k++){
                totalDistance+=abs(map1_counts[k] - map2_counts[k]);
            }
            output<<totalDistance<<'\t';
        }
        output<<endl;
    }
    output.close();
    //*/
     
     
    
    //Tile mapping: Convert training maps
    
    //Find the best tile mappings
    //1) Assign a tile mapping
    //2) Sample 100 maps using the converted maps (only?)
    //3) Test the playability of the sampled maps
    /*vector<char> KI_Tiles =  {'S', '-', 'M', 'T', 'H', 'D', '#'};
    vector<char> SMB_Tiles = {'S', '-', 'p', 'P', 'B', '?', 'e', 'c', '#'};
    vector<char> LR_Tiles =  {'S', '.', '-', 'b', 'B', 'G', 'E', 'M', '#'};
    vector<char> KK_Tiles = {'S', '-', '#', 'e', 'B', 'b', 'M', 'H', '|', 'W', 'T', 'c'};
    
    //Kid Icarus to _____
    vector<int> tileMapping(7);
    
    //Mario to ______
    //vector<int> tileMapping(9);
    
    //Kid Kool to _____
    //vector<int> tileMapping(12);
    
    //Loderunner to _____
    //vector<int> tileMapping(9);
    
    
    //Start and empty tiles always map to each other
    tileMapping[0] = 0;
    tileMapping[1] = 1;
    
    
    //Rest of the tile start mapped to empty
    //tileMapping[2] = 1;
    //tileMapping[3] = 1;
    //tileMapping[4] = 1;
    //tileMapping[5] = 1;
    //tileMapping[6] = 1;
    //tileMapping[7] = 1;   //SMB
    //tileMapping[8] = 1;   //LR
    
    
    int numberOfPossibleTiles = (int)SMB_Tiles.size();
    int numberOfMapsToConvert=6;
    //int numberOfMapsAvailable=16;
    
    //for simple indexing into the tile types
    map<char, int> mapping;
    for(int i=0; i<KI_Tiles.size(); i++){
        mapping.insert(pair<char, int> (KI_Tiles[i], i));
    }
    
    
    string outputFolder = "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/Final/S-PBec#X/";
    string mapFolder = "Mario_Maps/DA_KidIcarus/Best/Final/S-PBec#X/";
    //string mapFolder = "Mario_Maps/DA_KidKool/Random/S-#eBe#-P?Pp/";

    
    //*/
    
    //string x = "Random/S-c#B?e/";
    
    
    
    
    
    //                           0    1    2    3    4    5    6    7    8
    //vector<char> SMB_Tiles = {'S', '-', 'p', 'P', 'B', '?', 'e', 'c', '#'};
    
    //                           0    1    2    3    4    5    6
    //vector<char> KI_Tiles =  {'S', '-', 'M', 'T', 'H', 'D', '#'};

    
    /*tileMapping[2] = 3;
    tileMapping[3] = 4;
    tileMapping[4] = 6;
    tileMapping[5] = 7;
    tileMapping[6] = 8;
    //tileMapping[7] = 1;
    //tileMapping[8] = 4;
    //tileMapping[9] = 8;
    //tileMapping[10] = 5;
    //tileMapping[11] = 1;
    
    //Convert the maps, train on all, sample
    for(int i=1; i<=numberOfMapsToConvert; i++){
        ifstream input("KidIcarus_Maps/Standard/kidicarus_"+to_string(i)+".txt");
        string line;
        vector<vector<char> > map;
        vector<vector<char> > convertedMap;
        while(getline(input, line)){
            vector<char> tmp;
            vector<char> tmp2;
            for(int i=0; i<line.length(); i++){
                if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0'){
                    tmp.push_back(line[i]);
                    tmp2.push_back('X');
                }
            }
            map.push_back(tmp);
            convertedMap.push_back(tmp2);
            tmp.clear();
            tmp2.clear();
        }
        
        for(int k=0; k<map.size(); k++){
            for(int j=0; j<map[k].size(); j++){
                convertedMap[k][j] = SMB_Tiles[tileMapping[mapping[map[k][j]]]];
            }
        }
        
        for(int j=1; j<7; j++){
            ofstream out(mapFolder+"mario_"+to_string(i+1)+".txt");
            for(int k=0; k<convertedMap.size(); k++){
                for(int j=0; j<convertedMap[k].size(); j++){
                    out<<convertedMap[k][j];
                }
                out<<endl;
            }
            out.close();
        }
    }
    //*/
    
    //KI->SMB
    //PlayabilityContraintSampling("Mario_Maps/DA_KidIcarus/Best/S-BecP#/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/S-BecP#/", "mario_", 12, 3, 3, 7, 100, 12, 210, 10);
    //MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe##/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe##/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    //MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe?#/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe?#/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    
    //KK->SMB
    /*MdMCExperiments("Mario_Maps/DA_KidIcarus/S-##e##/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-##e##/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe##/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe##/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe?#/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe?#/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    */
    
    //KI->SMB
    /*MdMCExperiments("Mario_Maps/DA_KidIcarus/S-##e##/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-##e##/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe##/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe##/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    MdMCExperiments("Mario_Maps/DA_KidIcarus/S-BBe?#/", "mario_", "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/S-BBe?#/", "mario_", 12, 3, 3, 22, 100, 12, 210, Learner(3,3,false,false));
    */
    
    //SMB->KI
    
    /*MdMCExperiments("KidIcarus_Maps/DA_Mario/S-####HH#/", "kidicarus_", "New_Experiments/DomainAdaptation/TileMapping/SMB_KI/S-####HH#/", "kidicarus_", 1, 3, 3, 22, 100, 170, 16, Learner(1,3,false,false));
    MdMCExperiments("KidIcarus_Maps/DA_Mario/S-##T#HH#/", "kidicarus_", "New_Experiments/DomainAdaptation/TileMapping/SMB_KI/S-##T#HH#/", "kidicarus_", 1, 3, 3, 22, 100, 170, 16, Learner(1,3,false,false));
    MdMCExperiments("KidIcarus_Maps/DA_Mario/S-DDT#HH#/", "kidicarus_", "New_Experiments/DomainAdaptation/TileMapping/SMB_KI/S-DDT#HH#/", "kidicarus_", 1, 3, 3, 22, 100, 170, 16, Learner(1,3,false,false));
    */
    //ofstream results("New_Experiments/DomainAdaptation/TileMapping/SMB_KI/Results_AvgLikelihood.txt");
    
    
    
    //DOMAIN ADAPTATION CODE
    
    
    
    //Finding and comparing the ordering of the tile type counts in an orginal map and the converted maps
    //find the tile distribution of the original maps
    

    /*vector<char> KI_Tiles = {'S', '-', 'M', 'T', 'H', 'D', '#'};
    vector<char> SMB_Tiles ={'S', '-', 'p', 'P', 'B', '?', 'e', 'c', '#'};
    vector<char> KK_Tiles = {'S', '-', '#', 'e', 'B', 'b', 'M', 'H', '|', 'W', 'T', 'c'};
    
    //Kid Icarus to _____
    //vector<int> tileMapping(7);

    //Mario to ______
    //vector<int> tileMapping(9);
    
    //Kid Kool to ______
    vector<int> tileMapping(12);
    
    
    //_____ to Kid Icarus
    //int numberOfPossibleTiles = 7;

    //_____ to Mario
    int numberOfPossibleTiles = 9;

    //_____ to Kid Kool
    //int numberOfPossibleTiles = 12;

    
    
    //Start and empty tiles always map to each other
    tileMapping[0] = 0;
    tileMapping[1] = 1;
    
    
    //Rest of the tile start mapped to empty
    tileMapping[2] = 8;
    tileMapping[3] = 6;
    tileMapping[4] = -1;
    tileMapping[5] = -1;
    tileMapping[6] = -1;
    tileMapping[7] = -1;   //SMB
    tileMapping[8] = -1;   //SMB
    tileMapping[9] = -1;   //KK
    tileMapping[10] = -1;  //KK
    tileMapping[11] = -1;  //KK
    tileMapping[12] = -1;  //KK
    
    /*Learner model = Learner(1, 0, false, false);
    model.TileTypes = SMB_Tiles;
    model.SetDepMatrix(0);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    //model.SetTotalsTopLevel("Mario_Maps/SingleLevel/mario_", 1, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    model.SetTotalsTopLevel("Mario_Maps/SingleLevel/mario_", 1, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    //model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    vector<int> originalOrdering(SMB_Tiles.size());
    vector<int> Totals = model.Totals[0][0];
    
    int position=0;
    int prevMax = -1;
    for(int j=0; j<Totals.size(); j++){
        int max=-1;
        int maxIndex=-1;
        for(int i=0; i<Totals.size();i++){
            if(Totals[i]>max){
                max = Totals[i];
                maxIndex=i;
            }
        }
        if(max < prevMax)
            position++;
        
        originalOrdering[maxIndex] = position;
        Totals[maxIndex] = -1;
        prevMax = max;
    }
    
    //for(int i=0; i<originalOrdering.size(); i++)
    //    cout<<originalOrdering[i]<<"\t"<<model.Totals[0][0][i]<<endl;
    
    //find the tile distribution of the maps to be converted, and then count the tile totals for converted maps
    Learner model2 = Learner(1, 0, false, false);
    model2.TileTypes = SMB_Tiles;
    model2.SetDepMatrix(0);
    model2.SetRows(false);
    model2.ResizeVectors(false);
    model2.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    //model2.SetTotalsTopLevel("KidIcarus_Maps/Standard/kidicarus_", numberOfMapsToConvert, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    model2.SetTotalsTopLevel("KidKool_Maps/Outside/kidkool_", 12, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);

    
    
    //check all possible combinations of tiles
    int combination=0;
    ofstream tileDistribution("New_Experiments/DomainAdaptation/TileMapping/KK_SMB/TileDistributionKK_SMB.txt");
    //for(int eleventh=1; eleventh<numberOfPossibleTiles; eleventh++){
    for(int tenth=1; tenth<numberOfPossibleTiles; tenth++){
    for(int ninth=1; ninth<numberOfPossibleTiles; ninth++){
    for(int eighth=1; eighth<numberOfPossibleTiles; eighth++){
    for(int seventh=1; seventh<numberOfPossibleTiles; seventh++){
    for(int sixth=1; sixth<numberOfPossibleTiles; sixth++){
        for(int fifth=1; fifth<numberOfPossibleTiles; fifth++){
            for(int fourth=1; fourth<numberOfPossibleTiles; fourth++){
                for(int third=1; third<numberOfPossibleTiles; third++){
                    //for(int second=1; second<numberOfPossibleTiles; second++){
                        //for(int first=1; first<numberOfPossibleTiles; first++){
            
                            //tileMapping[2] = first;
                            //tileMapping[3] = second;
                            tileMapping[4] = third;
                            tileMapping[5] = fourth;
                            tileMapping[6] = fifth;
                            tileMapping[7] = sixth;
                            tileMapping[8] = seventh;
                            tileMapping[9] = eighth;
                            tileMapping[10] = ninth;
                            tileMapping[11] = tenth;
                            //tileMapping[12] = eleventh;

                            
                            //Compares the orderings of the tile counts
                            
                            vector<int> newCounts(SMB_Tiles.size());
                            for(int q=0;q<newCounts.size(); q++)
                                newCounts[q]=0;
                            
                            for(int i=0; i<tileMapping.size(); i++)
                                tileDistribution<<SMB_Tiles[tileMapping[i]]<<"";
                            
                            tileDistribution<<",";
                            
                            
                            //count the converted tile map tile counts
                            for(int q=0; q<tileMapping.size(); q++){
                                newCounts[tileMapping[q]]+=model2.Totals[0][0][q];
                            }
                            
                            vector<int> convertedOrdering(SMB_Tiles.size());
                            vector<int> Totals = newCounts;
                            
                            
                            int position=0;
                            int prevMax = -1;
                            for(int j=0; j<Totals.size(); j++){
                                int max=-1;
                                int maxIndex=-1;
                                for(int i=0; i<Totals.size();i++){
                                    if(Totals[i]>max){
                                        max = Totals[i];
                                        maxIndex=i;
                                    }
                                }
                                if(max < prevMax)
                                    position++;
                                
                                convertedOrdering[maxIndex] = position;
                                Totals[maxIndex] = -1;
                                prevMax = max;
                            }
                            
                            //compare the rankings
                            
                            float totalDifferences=0;
                            for(int q=0; q<convertedOrdering.size(); q++){
                                totalDifferences+=abs(convertedOrdering[q] - originalOrdering[q]);
                            }
                            
                            float mean = totalDifferences/newCounts.size();
                            float variance=0;
                            for(int q=0; q<newCounts.size(); q++)
                                variance+=(mean-abs(convertedOrdering[q] - originalOrdering[q]))*(mean-abs(convertedOrdering[q] - originalOrdering[q]));
                            
                            variance/=newCounts.size();
                            float stdDev = sqrt(variance);

                            tileDistribution<<totalDifferences<<","<<mean<<","<<stdDev<<endl;
                            tileDistribution.flush();
                            //
                            
                        //}
                    //}
                }
            }
        }
    }
    }
    }
    }
    }
    //}
    //*/
    
    
    //Finding the likelihood of a the converted maps, using a designated set of conversions
    //print the tile mapping to a file and place it in both new directories
    /*ifstream input("Mario_Maps/DA_KidKool/TileMappingsOrdering.txt");
    ofstream output("New_Experiments/DomainAdaptation/TileMapping/KK_SMB/LikelihoodsOrderingThreshold.txt");
    
    //for simple indexing into the tile types
    map<char, int> mapping;
    for(int i=0; i<KK_Tiles.size(); i++){
        mapping.insert(pair<char, int> (KK_Tiles[i], i));
    }
    
    Learner model = Learner(1, 0, false, false);
    model.TileTypes = SMB_Tiles;
    //model.SetDepMatrix(3);
    model.SetDepMatrix(2);
    //model.SetDepMatrix(1);
    //model.SetDepMatrix(0);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    //for(int i=0; i<=3; i++)
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    //for(int i=0; i<=3; i++)
    model.SetTotalsTopLevel("Mario_Maps/SingleLevel/mario_", 1, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    //for(int i=0; i<=3; i++)
    model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    //*/
    
    /*vector<vector<int> > netStruct;
    vector<int> row1{0, 0, 0};
    vector<int> row2{1, 1, 1};
    vector<int> row3{1, 1, 1};
    vector<int> row4{1, 1, 1};
    vector<int> row5{0, 0, 0};
    
    netStruct.push_back(row1);
    netStruct.push_back(row2);
    netStruct.push_back(row3);
    netStruct.push_back(row4);
    netStruct.push_back(row5);
    
    vector<float> P= ComputeProbabilitiesConfigurations(netStruct, model.TileTypes, 9, (int)model.TileTypes.size(), 1, "Mario_Maps/SingleLevel/", "mario_");
    
    /*ifstream in("Mario_Maps/SingleLevel/mario_1.txt");
    string line;
    vector<vector<char> > map;
    while(getline(in, line)){
        vector< char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i]!= '\t' &&line[i]!= '\r' && line[i]!= '\0' &&line[i]!= '\n')
            row.push_back(line[i]);
        }
        map.push_back(row);
    }
    
    pair<double, int> ret = MapLikelihoodConfig(map, netStruct, model.TileTypes, P);
    cout<<ret.first<<" "<<ret.second<<endl;
    
    //*/
    
    
    //set to tile configuration probabilities instead of tile probabilities.
    /*vector<float> configProbabilities;
    float size = .1*(model.Totals[0].size()-1);
    float totalObservations = 0;
    for(int i=0; i<model.Totals[0].size(); i++){
        totalObservations += model.Totals[0][i][model.TileTypes.size()-1];
    }
    
    for(int i=0; i<model.Totals[0].size(); i++){
        float prob = ((float)model.Totals[0][i][model.TileTypes.size()-1]+.1)/(totalObservations+size);
        configProbabilities.push_back(prob);
    }
    */
    
    
    /*string line;
    int iter=1;
    while(getline(input, line)){
        //cout<<iter<<endl;
        iter++;
        for(int i=0; i<line.length(); i++){
            for(int j=0; j<SMB_Tiles.size(); j++){
                if(line.at(i) == SMB_Tiles[j]){
                    tileMapping[i] = j;
                }
            }
        }
        
        for(int i=0; i<tileMapping.size(); i++){
            cout<<tileMapping[i]<<" ";
        }
        cout<<endl;
        
        for(int i=0; i<tileMapping.size(); i++)
            output<<SMB_Tiles[tileMapping[i]];
        output<<",";
        
        //convert the maps
        double total_log_likelihood=0;
        int total_unseen_states=0;
        for(int i=1; i<=12; i++){
            ifstream input("KidKool_Maps/Outside/kidkool_"+to_string(i)+".txt");
            vector<vector<char> > map;
            vector<vector<char> > convertedMap;
            while(getline(input, line)){
                vector<char> tmp;
                vector<char> tmp2;
                for(int i=0; i<line.length(); i++){
                    if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0'){
                        tmp.push_back(line[i]);
                        tmp2.push_back('X');
                    }
                }
                map.push_back(tmp);
                convertedMap.push_back(tmp2);
                tmp.clear();
                tmp2.clear();
            }
     
            for(int k=0; k<map.size(); k++){
                for(int j=0; j<map[k].size(); j++){
                    convertedMap[k][j] = SMB_Tiles[tileMapping[mapping[map[k][j]]]];
                }
            }
            ofstream out("Mario_Maps/DA_KidKool/Kidkool_to_mario_"+to_string(i)+".txt");
            for(int k=0; k<convertedMap.size(); k++){
                for(int j=0; j<convertedMap[k].size(); j++){
                    out<<convertedMap[k][j];
                }
                out<<endl;
            }
            out.close();
     
            //compute the total_log_likelihood of all the maps
            //pair<double, int> tmp = MapLikelihoodConfig(convertedMap, netStruct, model.TileTypes, P);
            
            //cout<<convertedMap.size()<<" "<<convertedMap[0].size()<<endl;
            pair<double, int> tmp = MapLikelihood(convertedMap, model);
            total_log_likelihood+=tmp.first;
            total_unseen_states+=tmp.second;
        }
        output<<total_log_likelihood<<","<<total_unseen_states<<endl;
    }
    output.close();
    //*/
    
    //*/
    
    //Evaluating Map:
    
    //Likelihood
    //Unseen States
    //Linearity
    //Leniency
    //Expressive Range
    
    /*string inputMapBase = "mario_";
    string folder = "New_Experiments/DomainAdaptation/TileMapping/KI_SMB/Best/";
    string mapping = "S-eB?c#/";
    string loglikelihoodName = "LogLikelihoods.txt";
    string unseenStatesName = "UnseenStates.txt";
    string linearityName = "Linearity.txt";
    string leniencyName = "Leniency.txt";
    
    ofstream loglikelihoodFile(folder+mapping+loglikelihoodName);
    ofstream unseenStatesFile(folder+mapping+unseenStatesName);
    ofstream linearityFile(folder+mapping+linearityName);
    ofstream leniencyFile(folder+mapping+leniencyName);
    
    
    //train an MdMC on the training maps for the lieklihood and unseen states
    Learner model = Learner(1, 0, false, false);
    model.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    model.SetDepMatrix(3);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    model.SetTotalsTopLevel("Mario_Maps/Enemies/mario_", 16, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    
    //for each map
    vector<double> loglikelihoods;
    vector<double> unseenStates;
    vector<double> linearities;
    vector<double> leniencies;
    double playable=0;
    for(int i=0; i<1000; i++){
        ifstream mapFile(folder+mapping+inputMapBase+to_string(i)+".txt");
        //read in the map
        string line;
        vector<vector<char> > map;
        while(getline(mapFile, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                tmp.push_back(line[i]);
            }
            map.push_back(tmp);
            tmp.clear();
        }
        
        //compute the likelihood and unseen states
        pair<double, int> ret = MapLikelihood(map, model);
        loglikelihoods.push_back(ret.first);
        loglikelihoodFile<<ret.first<<endl;
        unseenStates.push_back(ret.second);
        unseenStatesFile<<ret.second<<endl;
        
        //compute linearity
        double linearity = ComputeLinearity(map);
        linearities.push_back(linearity);
        linearityFile<<linearity<<endl;
        
        //compute leniency
        double leniency = ComputeLeniency(map);
        leniencies.push_back(leniency);
        leniencyFile<<leniency<<endl;
        
        if(MarioPlayabilityConstraint(map, 0, 0))
            playable++;
    }
    
    cout<<"Log-Likelihood, Unseen States, Linearity, Leniency, Playable"<<endl;
    double likelihoodTotal=0;
    double unseenStatesTotal=0;
    double linearityTotal=0;
    double leniencyTotal=0;
    for(int i=0; i<1000; i++){
        likelihoodTotal+=loglikelihoods[i];
        unseenStatesTotal+=unseenStates[i];
        linearityTotal+=linearities[i];
        leniencyTotal+=leniencies[i];
    }
    
    cout<<likelihoodTotal/1000.0<<" "<<unseenStatesTotal/1000.0<<" "<<linearityTotal/1000.0<<" "<<leniencyTotal/1000.0<<" "<<playable/1000.0<<endl;
    */
    
    
    //Sampling maps for domain transfer
    
    //9227
    //Train the model for the likelihood calculations
    /*Learner TopLevelLearner = Learner(1, 0, false, false);
    TopLevelLearner.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    
    //rerun with 3
    TopLevelLearner.SetDepMatrix(3);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel("Mario_Maps/SingleLevel/mario_", 1, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    for(int i=0; i<1; i++){
        string mapping="";
        if(i==0)
            mapping = "S-#e?B#pB--c/";
        if(i==1)
            mapping = "S-#e##?pB-?c/";
        if(i==2)
            mapping = "S-#e#B?P--?c/";
        
        //string inputFolder = "Mario_Maps/DA_KidIcarus/Random/Final/"+mapping;
        string inputFolder = "Mario_Maps/SingleLevel/";
        //string outputFolder = "New_Experiments/DomainAdaptation/TileMapping/SingleLevel/";
        //string outputFolder= "New_Experiments/DomainAdaptation/TileMapping/KK_SMB/Manual/"+mapping;
        string mapName = "mario_";
        int splits =12;
        int lookahead=3;
        int toGen = 100;
        int height = 12;
        int width = 210;
        int sectionSize = 10;
        float maps=16;
        int trainingMaps = 1;
        //PlayabilityContraintSampling(inputFolder, mapName, outputFolder, mapName, splits, lookahead, 3, trainingMaps, toGen, height, width, sectionSize);
        //cout<<mapping<<"=============================="<<endl;
    
        //Evaluation:
        vector<double> likelihoods;
        vector<double> unseens;
        vector<double> linearities;
        vector<double> leniencies;
        
        
        
        
        for(int j=1; j<=maps; j++){
            //cout<<j<<endl;
            //read the map into a container
            //ifstream input(outputFolder+mapName+to_string(j)+".txt");
            //ifstream input("KidIcarus_Maps/Standard/kidicarus_"+to_string(j)+".txt");
            ifstream input("Mario_Maps/Enemies/mario_"+to_string(j)+".txt");
            string line;
            vector<vector<char> > map;
            while(getline(input, line)){
                vector<char> tmp;
                for(int i=0; i<line.length(); i++){
                    if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0')
                        tmp.push_back(line[i]);
                }
                map.push_back(tmp);
                tmp.clear();
            }
            pair<int, double> tmp = MapLikelihood(map, TopLevelLearner);
            likelihoods.push_back(tmp.first);
            unseens.push_back(tmp.second);
            //linearities.push_back(ComputeLinearity(map));
            //leniencies.push_back(ComputeLeniency(map));
        }
        
        //compute the average values, and ths std dev
        double avg_likelihood=0;
        double avg_unseens=0;
        double avg_linearity=0;
        double avg_leniency=0;
        for(int j=0; j<maps; j++){
            avg_likelihood+=likelihoods[j];
            avg_unseens+=unseens[j];
            //avg_linearity+=linearities[j];
            //avg_leniency+=leniencies[j];
        }
        avg_likelihood/=maps;
        avg_unseens/=maps;
        //avg_linearity/=maps;
        //avg_leniency/=maps;
        
        double dev_likelihood=0;
        double dev_unseens=0;
        //double dev_linearity=0;
        //double dev_leniency=0;
        
        for(int j=0; j<maps; j++){
            dev_likelihood+= pow(avg_likelihood-likelihoods[j], 2);
            dev_unseens+= pow(avg_unseens-unseens[j], 2);
            //dev_linearity+= pow(avg_linearity-linearities[j], 2);
            //dev_leniency+= pow(avg_leniency-leniencies[j], 2);
        }
        
        dev_likelihood = dev_likelihood/maps;
        dev_unseens = dev_unseens/maps;
        //dev_linearity = dev_linearity/maps;
        //dev_leniency = dev_leniency/maps;
        
        dev_likelihood = sqrt(dev_likelihood);
        dev_unseens = sqrt(dev_unseens);
        //dev_linearity = sqrt(dev_linearity);
        //dev_leniency = sqrt(dev_leniency);
        
        
        
        //cout<<"$PostTrimR_"<<to_string(i+1)<<"$ &$"<<avg_likelihood<<"\\pm"<<dev_likelihood<<"$ &$";
        //cout<<avg_unseens<<"\\pm"<<dev_unseens<<"$ &$"<<avg_linearity<<"\\pm"<<dev_linearity;
        //cout<<"$ &$"<<avg_leniency<<"\\pm"<<dev_leniency<<"$ \\\\"<<endl;
        
        //cout<<"$"<<avg_leniency<<"\\pm"<<dev_leniency<<"$ \\\\"<<endl;
        
        cout<<"Likelihood: "<<avg_likelihood<<"\\pm"<<dev_likelihood<<endl;
        //cout<<"Unseen: "<<avg_unseens<<"\t"<<dev_unseens<<endl;
        //cout<<"Linearity: "<<avg_linearity<<"\t"<<dev_linearity<<endl;
        //cout<<"Leniency: "<<avg_leniency<<"\t"<<dev_leniency<<endl;
     //*/
    //}
     
     
     
    //BASELINES: 1 Training Map
    //Learner l = Learner(1, 1, false, false);
    //MdMCExperiments("Mario_Maps/Enemies/", "mario_", "New_Experiments/DomainAdaptation/Baselines/AllMaps/Mario/", "mario_", 12, 3, 3, 16, 1000, 12, 210, l);
    
    
    
    
    
    
    //EVALUATING LEVI'S MAPS
    //Fun Coefficients
    /*float LinCoeff = 0;
    float LenCoeff = 72.0045;
    float LikCoeff = -87.4372;
    float USCoeff = 0;
    float ACoeff = 0;
    float BCoeff = 5.7561;
    float CCoeff = 0;
    float DCoeff = 18.031;
    float ECoeff = 0;
    float FCoeff = 16.5765;
    float GCoeff = 0;
    float HCoeff = 12.9771;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 3.1662;
    float LCoeff = 0;
    float MCoeff = 35.2595;
    float NCoeff = 11.3542;
    float OCoeff = 0;
    float PCoeff = 95.9749;
    float constant = 77.1454;
    */
    
    
    //Training on Short -------------------------------
    
    //Difficulty
    /*float LinCoeff = -0.6656;
    float LenCoeff = 461.079;
    float LikCoeff = -66.4928;
    float USCoeff = 54.069;
    float ACoeff = 0;
    float BCoeff = 5.362;
    float CCoeff = 8.7136;
    float DCoeff = 39.0804;
    float ECoeff = 0;
    float FCoeff = 20.926;
    float GCoeff = -8.442;
    float HCoeff = -4.2385;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 2.7108;
    float LCoeff = 0;
    float MCoeff = 0;
    float NCoeff = 20.1057;
    float OCoeff = 0;
    float PCoeff = 0;
    float constant = 68.4606;
    */
    
    //Aesthetic Coefficients
    /*float LinCoeff = 0.1893;
    float LenCoeff = 0;
    float LikCoeff = -59.9755;
    float USCoeff = 0;
    float ACoeff = 0;
    float BCoeff = 4.199;
    float CCoeff = 0;
    float DCoeff = 14.7262;
    float ECoeff = 0;
    float FCoeff = 12.4024;
    float GCoeff = 0;
    float HCoeff = 15.9167;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 2.0019;
    float LCoeff = 0;
    float MCoeff = 45.5733;
    float NCoeff = 0;
    float OCoeff = 0;
    float PCoeff = 0;
    float constant = 48.8005;
    */
    
    //Fun Coefficients
    /*float LinCoeff = 0;
    float LenCoeff = 72.0045;
    float LikCoeff = -87.4372;
    float USCoeff = 0;
    float ACoeff = 0;
    float BCoeff = 5.7561;
    float CCoeff = 0;
    float DCoeff = 18.031;
    float ECoeff = 0;
    float FCoeff = 16.5765;
    float GCoeff = 0;
    float HCoeff = 12.9771;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 3.1662;
    float LCoeff = 0;
    float MCoeff = 35.2595;
    float NCoeff = 11.3542;
    float OCoeff = 0;
    float PCoeff = 95.9749;
    float constant = 77.1454;
    */
    
    //Training on Long Levels
    
    //Aesthetics
    /*float LinCoeff = 0;
    float LenCoeff = 0;
    float LikCoeff = -103.5974;
    float USCoeff = -250.1632;
    float ACoeff = 0;
    float BCoeff = -21.3024;
    float CCoeff = 0;
    float DCoeff = 0;
    float ECoeff = -64.2221;
    float FCoeff = -30.2159;
    float GCoeff = -39.5733;
    float HCoeff = 0;
    float ICoeff = 0;
    float JCoeff = -14.2717;
    float KCoeff = 0;
    float LCoeff = 0;
    float MCoeff = -38.0624;
    float NCoeff = -45.9998;
    float OCoeff = 0;
    float PCoeff = 0;
    float constant = 107.862;
    */
    
    //Difficulty
    /*float LinCoeff = 0;
    float LenCoeff = 0;
    float LikCoeff = 0;
    float USCoeff = 0;
    float ACoeff = 0;
    float BCoeff = 0;
    float CCoeff = 0;
    float DCoeff = 0;
    float ECoeff = 0;
    float FCoeff = 21.8903;
    float GCoeff = 0;
    float HCoeff = 0;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 0;
    float LCoeff = 0;
    float MCoeff = 0;
    float NCoeff = 0;
    float OCoeff = 0;
    float PCoeff = 0;
    float constant = 3.3429;
    */
    
    //Fun
    /*float LinCoeff = 0.4764;
    float LenCoeff = 0;
    float LikCoeff = 0;
    float USCoeff = 0;
    float ACoeff = 84.4786;
    float BCoeff = -15.3037;
    float CCoeff = 0;
    float DCoeff = 0;
    float ECoeff = 0;
    float FCoeff = 0;
    float GCoeff = 26.0285;
    float HCoeff = 0;
    float ICoeff = 0;
    float JCoeff = 0;
    float KCoeff = 19.3823;
    float LCoeff = 0;
    float MCoeff = 0;
    float NCoeff = 0;
    float OCoeff = 0;
    float PCoeff = 0;
    float constant = 3.7251;
    

    //Likelihood
    //First train the model
    Learner model = Learner(1, 0, false, false);
    model.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    model.SetDepMatrix(3);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    model.SetTotalsTopLevel("Mario_Maps/Enemies/mario_", 16, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    //perform predictions for the long levels, using the aggregated predictions of the sliding windows over the longer level
    //and the model trained using the shorter levels
    
    cout<<"Arithmetic Mean, Minimum, Maximum, Geometric Mean"<<endl;
    for(int num=1; num<=111; num++){
        //read in the long level, break into smaller sections later
        ifstream inputL("Levis_Maps/LongLevels/tela"+to_string(num)+".txt");
        ifstream inputH("Levis_Maps/LongLevels/High_Level/16-4X4-Count/tela"+to_string(num)+".txt");
        //ifstream scores("Levis_Maps/labels7likert/tela"+to_string(num));
        ifstream scores("Levis_Maps/LongLevels/scores_tela"+to_string(num)+".txt");
        if(!inputL || !inputH)
            continue;
        string line;
        vector<vector<char> > Lmap;
        while(getline(inputL, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                tmp.push_back(line[i]);
            }
            Lmap.push_back(tmp);
            tmp.clear();
        }
        vector<vector<char> > Hmap;
        while(getline(inputH, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                tmp.push_back(line[i]);
            }
            Hmap.push_back(tmp);
            tmp.clear();
        }
    
        //slide the window the distance of one high-level tile each iteration
        double finalPrediction;
        vector<double> predictions;
    
        for(int k=0; k<=Lmap[0].size()-180; k+=4){
            vector<vector<char>> LmapSection;
            vector<vector<char>> HmapSection;
        
            float linearity=0;
            float leniency=0;
            double likelihood=0;
            float unseenStates=0;
            float A=0;
            float B=0;
            float C=0;
            float D=0;
            float E=0;
            float F=0;
            float G=0;
            float H=0;
            float I=0;
            float J=0;
            float K=0;
            float L=0;
            float M=0;
            float N=0;
            float O=0;
            float P=0;
            
        
            for(int y=0;y<Lmap.size(); y++){
                vector<char> tmp;
                for(int x=k; x<k+180; x++){
                    tmp.push_back(Lmap[y][x]);
                }
                LmapSection.push_back(tmp);
                tmp.clear();
            }
        
            for(int y=0;y<Hmap.size(); y++){
                vector<char> tmp;
                for(int x=k/4; x<(k+180)/4; x++){
                    tmp.push_back(Hmap[y][x]);
                }
                HmapSection.push_back(tmp);
                tmp.clear();
            }

            //compute the linearity
            linearity = ComputeLinearity(LmapSection);
        
            //compute leniency
            leniency = ComputeLeniency(LmapSection);
        
            //compute Likelihood and Unseen States
            pair<double, int> tmp = MapLikelihood(LmapSection, model);
            likelihood = tmp.first/(LmapSection.size()*LmapSection[0].size());
            unseenStates = (float)tmp.second/(LmapSection.size()*LmapSection[0].size());

        
            //count each high-level tile
            float total=0;
            for(int y=0; y<HmapSection.size(); y++){
                for(int x=0; x<HmapSection[y].size(); x++){
                    if(HmapSection[y][x] == 'A'){
                        A++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'B'){
                        B++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'C'){
                        C++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'D'){
                        D++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'E'){
                        E++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'F'){
                        F++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'G'){
                        G++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'H'){
                        H++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'I'){
                        I++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'J'){
                        J++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'K'){
                        K++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'L'){
                        L++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'M'){
                        M++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'N'){
                        N++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'O'){
                        O++;
                        total++;
                    }
                    else if(HmapSection[y][x] == 'P'){
                        P++;
                        total++;
                    }
                }
            }
        
            A/=total;
            B/=total;
            C/=total;
            D/=total;
            E/=total;
            F/=total;
            G/=total;
            H/=total;
            I/=total;
            J/=total;
            K/=total;
            L/=total;
            M/=total;
            N/=total;
            O/=total;
            P/=total;
        
            predictions.push_back(LinCoeff*linearity + LenCoeff*leniency + LikCoeff*likelihood + USCoeff*unseenStates + ACoeff*A +
                                  BCoeff*B + CCoeff*C + DCoeff*D + ECoeff*E + FCoeff*F + GCoeff*G +HCoeff*H + ICoeff*I + JCoeff*J +
                                  KCoeff*K + LCoeff*L + MCoeff*M + NCoeff*N + OCoeff*O + PCoeff*P + constant);
        }
    
        double min= 100;
        double max=-100;
        double geoMean = 1;
        for(int i=0; i<predictions.size(); i++){
            finalPrediction+=predictions[i];
            geoMean*=predictions[i];
            if(predictions[i] < min)
                min = predictions[i];
            if(predictions[i] > max)
                max = predictions[i];
        }
        
        
        
        //cout<<endl;
        double value;
        //scores>>value;  //difficulty
        //scores>>value;  //aesthetics
        //scores>>value;  //fun
        
        //long levels
        getline(scores, line);  int offset = 19;    //aesthetic
        getline(scores, line);  offset = 12;        //difficulty
        getline(scores, line);  offset = 5;         //fun
        
        cout<<finalPrediction/predictions.size()<<" "<<min<<" "<<max<<" "<<pow(geoMean, 1.0/predictions.size())<<" "//<<endl;
        cout<<line.substr(offset)<<endl;
        finalPrediction=0;
        predictions.clear();
    
    }
    //*/
    //ofstream output("Levis_Maps/LongLevels/Linearity.txt");
    //ofstream output2("Levis_Maps/LongLevels/Leniency.txt");
    
    
    /*Learner model = Learner(1, 0, false, false);
    model.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    model.SetDepMatrix(3);
    
    model.SetRows(false);
    model.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    model.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    model.SetTotalsTopLevel("Mario_Maps/Enemies/mario_", 16, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    
    //sets the probabilities using the totals
    model.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(0) + ".txt", false, 0);
    
    ofstream output1("Levis_Maps/Final_HighLevelTileCounts.txt");
    ofstream output2("Levis_Maps/Final_UnseenStates.txt");
    ofstream output3("Levis_Maps/Final_Avg_Likelihood.txt");
    ofstream output4("Levis_Maps/Final_Linearity.txt");
    ofstream output5("Levis_Maps/Final_Leniency.txt");

    
    for(int i=0; i<=2604; i++){
        ifstream input("Levis_Maps/tela"+to_string(i)+".txt");
        if(input){
            string line;
            vector<vector<char> > map;
            while(getline(input, line)){
                vector<char> tmp;
                for(int i=0; i<line.length(); i++){
                    if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0'){
                        tmp.push_back(line[i]);
                    }
                }
                map.push_back(tmp);
                tmp.clear();
            }
            output4<<"tela"+to_string(i)+"\t"<<ComputeLinearity(map)<<endl;
            output5<<"tela"+to_string(i)+"\t"<<ComputeLeniency(map)<<endl;
            pair<long double, int> p = MapLikelihood(map, model);
            //output<<p.first<<"\t"<<p.second<<endl;
            output2<<"tela"+to_string(i)+"\t"<<(float)p.second/(map.size()*map[0].size())<<endl;
            output3<<"tela"+to_string(i)+"\t"<<(float)p.first/(map.size()*map[0].size())<<endl;
            
            ifstream Hinput("Levis_Maps/High_Level/16-4X4-Count/tela"+to_string(i)+".txt");
            
            vector<float> counts;
            int total=0;
            for(int j=0; j<16; j++)
                counts.push_back(0);
            while(getline(Hinput, line)){
                for(int k=0; k<line.length(); k++){
                    if(line.at(k) == 'A'){
                        counts[0]++;
                        total++;
                    } else if(line.at(k) == 'B'){
                        counts[1]++;
                        total++;
                    } else if(line.at(k) == 'C'){
                        counts[2]++;
                        total++;
                    } else if(line.at(k) == 'D'){
                        counts[3]++;
                        total++;
                    } else if(line.at(k) == 'E'){
                        counts[4]++;
                        total++;
                    } else if(line.at(k) == 'F'){
                        counts[5]++;
                        total++;
                    } else if(line.at(k) == 'G'){
                        counts[6]++;
                        total++;
                    } else if(line.at(k) == 'H'){
                        counts[7]++;
                        total++;
                    } else if(line.at(k) == 'I'){
                        counts[8]++;
                        total++;
                    } else if(line.at(k) == 'J'){
                        counts[9]++;
                        total++;
                    } else if(line.at(k) == 'K'){
                        counts[10]++;
                        total++;
                    } else if(line.at(k) == 'L'){
                        counts[11]++;
                        total++;
                    } else if(line.at(k) == 'M'){
                        counts[12]++;
                        total++;
                    } else if(line.at(k) == 'N'){
                        counts[13]++;
                        total++;
                    } else if(line.at(k) == 'O'){
                        counts[14]++;
                        total++;
                    } else if(line.at(k) == 'P'){
                        counts[15]++;
                        total++;
                    }
                }
            }
            cout<<total<<endl;

            output1<<"tela"+to_string(i)+'\t';
            for(int h=0; h<counts.size();h++)
                output1<<counts[h]/total<<'\t';
            output1<<endl;
            
        }
    }//*/
    
    //Combine our evaluation with the human evaluation to create an ARFF for regression
    /*ifstream autoEvalLen("Levis_Maps/LongLevels/Leniency.txt");
    ifstream autoEvalLin("Levis_Maps/LongLevels/Linearity.txt");
    ifstream autoEvalLik("Levis_Maps/LongLevels/Likelihood.txt");
    ifstream autoEvalUS("Levis_Maps/LongLevels/UnseenStates.txt");
    ifstream humanEval;
    
    //string metric = "Difficulty"; int maxIters=2; int offset =12;
    //string metric = "Aesthetics"; int maxIters=1; int offset =19;
    string metric = "Fun"; int maxIters=3; int offset =5;
    
    
    
    ofstream arff("Levis_Maps/LongLevels/All_Pred_"+metric+".arff");
    
    arff<<"@RELATION All"<<endl;
    arff<<endl;
    arff<<"@ATTRIBUTE Linearity NUMERIC"<<endl;
    arff<<"@ATTRIBUTE Leniency NUMERIC"<<endl;
    arff<<"@ATTRIBUTE Likelihood NUMERIC"<<endl;
    arff<<"@ATTRIBUTE UnseenStates NUMERIC"<<endl;
    arff<<"@ATTRIBUTE A NUMERIC"<<endl;
    arff<<"@ATTRIBUTE B NUMERIC"<<endl;
    arff<<"@ATTRIBUTE C NUMERIC"<<endl;
    arff<<"@ATTRIBUTE D NUMERIC"<<endl;
    arff<<"@ATTRIBUTE E NUMERIC"<<endl;
    arff<<"@ATTRIBUTE F NUMERIC"<<endl;
    arff<<"@ATTRIBUTE G NUMERIC"<<endl;
    arff<<"@ATTRIBUTE H NUMERIC"<<endl;
    arff<<"@ATTRIBUTE I NUMERIC"<<endl;
    arff<<"@ATTRIBUTE J NUMERIC"<<endl;
    arff<<"@ATTRIBUTE K NUMERIC"<<endl;
    arff<<"@ATTRIBUTE L NUMERIC"<<endl;
    arff<<"@ATTRIBUTE M NUMERIC"<<endl;
    arff<<"@ATTRIBUTE N NUMERIC"<<endl;
    arff<<"@ATTRIBUTE O NUMERIC"<<endl;
    arff<<"@ATTRIBUTE P NUMERIC"<<endl;
//
    arff<<"@ATTRIBUTE "+metric+" NUMERIC"<<endl;
    arff<<endl;
    arff<<"@DATA"<<endl;
  
    for(int i=1; i<=111; i++){
        
        humanEval.open("Levis_Maps/LongLevels/scores_tela"+to_string(i)+".txt");
        
        string line;
        
        if(humanEval){
            getline(autoEvalLin, line);
            arff<<line<<", ";
            getline(autoEvalLen, line);
            arff<<line<<", ";
            getline(autoEvalLik, line);
            arff<<line<<", ";
            getline(autoEvalUS, line);
            arff<<line<<", ";
            
            
            ifstream map("Levis_Maps/LongLevels/High_Level/16-4X4-Count/tela"+to_string(i)+".txt");
            vector<float> counts;
            int total=0;
            for(int j=0; j<16; j++)
                counts.push_back(0);
            while(getline(map, line)){
                for(int k=0; k<line.length(); k++){
                    if(line.at(k) == 'A'){
                        counts[0]++;
                        total++;
                    } else if(line.at(k) == 'B'){
                        counts[1]++;
                        total++;
                    } else if(line.at(k) == 'C'){
                        counts[2]++;
                        total++;
                    } else if(line.at(k) == 'D'){
                        counts[3]++;
                        total++;
                    } else if(line.at(k) == 'E'){
                        counts[4]++;
                        total++;
                    } else if(line.at(k) == 'F'){
                        counts[5]++;
                        total++;
                    } else if(line.at(k) == 'G'){
                        counts[6]++;
                        total++;
                    } else if(line.at(k) == 'H'){
                        counts[7]++;
                        total++;
                    } else if(line.at(k) == 'I'){
                        counts[8]++;
                        total++;
                    } else if(line.at(k) == 'J'){
                        counts[9]++;
                        total++;
                    } else if(line.at(k) == 'K'){
                        counts[10]++;
                        total++;
                    } else if(line.at(k) == 'L'){
                        counts[11]++;
                        total++;
                    } else if(line.at(k) == 'M'){
                        counts[12]++;
                        total++;
                    } else if(line.at(k) == 'N'){
                        counts[13]++;
                        total++;
                    } else if(line.at(k) == 'O'){
                        counts[14]++;
                        total++;
                    } else if(line.at(k) == 'P'){
                        counts[15]++;
                        total++;
                    }
                }
            }
        
            for(int k=0; k<16; k++){
                arff<<counts[k]/total<<", ";
            }
     
            for(int iter=0; iter<maxIters; iter++)
                getline(humanEval, line);
            //arff<<line.substr(offset)<<endl;
            cout<<line.substr(offset)<<endl;
            
            humanEval.close();
        }
        
    }
//    arff.close();
    //*/
    
    
    
    
    
    
    
    //Perform clustering, and convert Levi's Maps
    /*Learner tileFinder = Learner(1, 0, true, false);
    tileFinder.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    
    
    //first we collect all the chunks of the training maps that are of the proper size
    MapReader Gatherer = MapReader();
    Gatherer.GatherAllBlocks("Mario_Maps/Enemies/", "mario_", 16, 4, 4);
    Clustering Clusterer = Clustering(4, 4);
    
    //we then remove all the duplicate chunks, so as to speed up clustering
    Clusterer.RemoveDuplicates("Mario_Maps/Enemies/",to_string(4)+"X"+to_string(4)+"_Blocks.txt");
    
    //next, compute the distances between each of the chunks using the desired comparisonFunction
    Clusterer.ComputeDifferences("Mario_Maps/Enemies/Duplicates_Removed_"+to_string(4)+"X"+to_string(4)+"_Blocks.txt", "Mario_Maps/Enemies/Direct_Differences_"+to_string(4)+"X"+to_string(4)+".csv", &Direct, vector<vector<float>>(), tileFinder.TileTypes);
    
    
    //Perform k-Medoids Clustering on the collected chunks, using the computed distance matrix
    string DifferenceFile = "Mario_Maps/Enemies/Direct_Differences_"+to_string(4)+"X"+to_string(4)+".csv";
    Clusterer.kMedoids(DifferenceFile, 16);
    Clusterer.PrintMedoids("Mario_Maps/Enemies/Duplicates_Removed_"+to_string(4)+"X"+to_string(4)+"_Blocks.txt", "Mario_Maps/Enemies/Direct_Medoids_"+to_string(16)+"_"+to_string(4)+"X"+to_string(4)+".txt");
    
    //convert the low-level training maps into high-level training maps using the found cluster medoids
    vector<vector<vector<char> > > Medoids;
    
    //read in the medoids (high-level tiles)
    ifstream Medoid_File;
    string line;
    Medoid_File.open("Mario_Maps/Enemies/Count_Medoids_"+to_string(16)+"_"+to_string(4)+"X"+to_string(4)+".txt");
    
    while(!getline(Medoid_File, line).eof()){
        vector<vector< char > > Block;
        for(int h = 0; h < 4; h++){
            getline(Medoid_File, line);
            vector< char > temp;
            for(int w=0; w<4; w++)
                temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(Medoid_File, line);
        Medoids.push_back(Block);
    }
    
    //output maps folder
    string OutputFolder = "Levis_Maps/LongLevels/High_Level/"+to_string(16)+"-"+to_string(4)+"X"+to_string(4)+"-Count";
    
#if defined(_WIN32)
    _mkdir(OutputFolder.c_str());
#else
    mkdir(OutputFolder.c_str(), 0777);
#endif
    
    //convert the low-level maps into high-level maps, and put them in the newly created subfolder
    MapReader Converter = MapReader();
    Converter.ConvertAllTileMapsToHier("tela", "Levis_Maps/LongLevels/", OutputFolder+"/", 111, 4, 4, Medoids, &Count, "Count", vector<vector<float>>(), tileFinder.TileTypes);
    Medoid_File.close();
    Medoids.clear();
    
    //*/
    
    
    
    
    //ifstream input("New_Experiments/ConstraintSatisfaction/mario_0.txt");
    /*int numMaps =6;
    vector<float> vals;
    
    
    
    
    float sum=0;
    float stddev=0;
    float variance=0;
    float avg=0;
    float min = 999999;
    float max=-1;
    
    
    for(int k=1; k<=numMaps; k++){
        ifstream input("KidIcarus_Maps/kidicarus_"+to_string(1)+".txt");
        string line;
        vector<vector<char> > map;
        while(getline(input, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0')
                    tmp.push_back(line[i]);
            }
            map.push_back(tmp);
            tmp.clear();
        }
    ConstraintChecker t;
    vector<int> v;// = t.SlidingWindowViolationDetection(map, false, 10);
    v = t.SectionBySectionViolationDetection(map, false, 10, 0, 0, &KidIcarusPlayabilityConstraintSection);
    for (int i=0; i<v.size(); i++) {
        cout<<v[i]<<" ";
    }
    cout<<endl;
    
    cout<<KidIcarusPlayabilitySection(map)<<endl;
    
    return 0;
        for (int i=0; i<100; i++) {
            if(KidIcarusNumberOfHazardsConstraint(map, i, i)){
                double perRow = i/(float)map.size();
                sum+=perRow;
                if(perRow < min)
                    min = perRow;
                if(perRow > max)
                    max= perRow;
                
                vals.push_back(perRow);
            }
        
        }
    }

    avg = sum/numMaps;
    for (int i=0; i<vals.size(); i++) {
        variance+= (avg-vals[i])*(avg-vals[i]);
    }
    
    variance = variance/numMaps;
    stddev = sqrt(variance);
    
    
    cout<<min<<" "<<avg<<" "<<stddev<<" "<<max<<endl;
    cout<<min*170<<" "<<avg*170<<" "<<stddev*170<<" "<<max*170<<endl;
    
    //*/
    
    
   
    
    
    //=================================================================================================================
    
    /*Generator TopGen = Generator(210, 12);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    
    int numConfigs = TopGen.TileTypes.size()*TopGen.TileTypes.size()*TopGen.TileTypes.size()*12;
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++){
        for(int config=0; config<numConfigs; config++){
            vector<float> tmp;
            for(int tile=0; tile<TopGen.TileTypes.size(); tile++){
                tmp.push_back(1.0/TopGen.TileTypes.size());
            }
            TopGen.Probabilities[dep].push_back(tmp);
            tmp.clear();
        }
    }
        
    
    
    //cout<<"Starting to generate"<<endl;
    //cout<<outputFolderName+outputMapName<<endl;
    //clock_t start;
    //start = clock();
    int playable=0;
    //cout<<"Config: "<<rowSplits<<" "<<lookAhead<<": ";
    //ConstraintChecker t;
    
    pair<long double, float> ret;
    
    chrono::time_point<std::chrono::system_clock> start, end, mapStart, mapEnd;
    start = chrono::system_clock::now();
    for (int i = 0; i < 100; i++){
        TopGen.GenerateTopLevel("UniformDistribution/NoEnforcement/mario_"+ to_string(i) + ".txt");
    }
    
    */
    
    
    
    
    
    //Baseline
    //MdMCExperiments("Mario_Maps/Enemies/","mario_", "New_Experiments/ConstraintSatisfaction/NoEnforcement/Mario/","mario_", 12, 3, 3, 16, 50, 12, 210);
    //MdMCExperiments("Loderunner_Maps/Standard/","Level ", "New_Experiments/ConstraintSatisfaction/NoEnforcement/Loderunner/","Level ", 11, 3, 3, 150, 100, 22, 32);
    //MdMCExperiments("KidIcarus_Maps/","kidicarus_", "New_Experiments/ConstraintSatisfaction/NoEnforcement/KidIcarus/","kidicarus_", 10, 3, 3, 6, 100, 170, 16);
    //EASY
    
    /*int maxPipes = 13;
    int minPipes = 1;
    int maxLength = 8;
    int minLength = 4;
    int maxGaps = 12;
    int minGaps = 4;
    int maxEnemies = 31;
    int minEnemies = 11;
    int minLinearity = 280;
    int maxLinearity = 566;
    int minLeniency = 14;
    int maxLeniency = 46;
    *///
    
    //STRETCH
    /*
    int maxPipes = 19;
    int minPipes = 7;
    int maxLength = 10;
    int minLength = 6;
    int maxGaps = 16;
    int minGaps = 8;
    int maxEnemies = 41;
    int minEnemies = 21;
    int minLinearity = 423;
    int maxLinearity = 709;
    int minLeniency = 30;
    int maxLeniency = 62;
     //*/
    //EASY
    /*
     int minHazards = 0;
     int maxHazards = 27;
     int minGap = 10;
     int maxGap = 12;
     int minAvgPlat = 3;
     int maxAvgPlat = 4;
     //*/
    //STRETCH
    
    /*int minHazards = 13;
    int maxHazards = 41;
    int minGap = 11;
    int maxGap = 13;
    int minAvgPlat = 4;
    int maxAvgPlat = 5;
     //*/
    /*
    Learner model = Learner(12, 3, false, false);
    model.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    model.SetDepMatrix(3);
    model.SetRows(false);
    model.ResizeVectors(false);
    
    Generator TopGen = Generator(210, 12);
    TopGen.CopyConfig(model.Config);
    TopGen.SetTileTypes(model.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    
    int numConfigs = (int)(TopGen.TileTypes.size()*TopGen.TileTypes.size()*TopGen.TileTypes.size()*12);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++){
        for(int config=0; config<numConfigs; config++){
            vector<float> tmp;
            for(int tile=0; tile<TopGen.TileTypes.size(); tile++){
                tmp.push_back(1.0/TopGen.TileTypes.size());
            }
            TopGen.Probabilities[dep].push_back(tmp);
            tmp.clear();
        }
    }
    
    int mapsSucceeded=0;
    for(int k=0; k<100; k++){
        //cout<<k<<endl;
        int i=0;
        do{
            TopGen.GenerateTopLevel("UniformDistribution/GenAndTest/mario_"+to_string(k)+".txt");
            i++;
        } while(i<=50 && (!MarioNoIllFormedPipesConstraint(TopGen.Map, 0, 0) || !MarioPlayabilityConstraint(TopGen.Map, 0, 0)));
        
        
        if(i<=50)
            mapsSucceeded++;
        /*if(!MarioNoIllFormedPipesConstraint(TopGen.Map, 0, 0)){
            //cout<<"ill-formed pipes"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioPlayabilityConstraint(TopGen.Map, 0, 0)){
            //cout<<"not playable"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLongestGapConstraint(map, minLength, maxLength)){
            cout<<"gap not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfEnemiesConstraint(map, minEnemies, maxEnemies)){
            cout<<"number of enemies not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfGapsConstraint(map, minGaps, maxGaps)){
            cout<<"number of gas not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfPipesConstraint(map, minPipes, maxPipes)){
            cout<<"number of pipes not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLeniencyBoundConstraint(map, minLeniency, maxLeniency)){
            cout<<"leniency not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLinearityBoundConstraint(map, minLinearity, maxLinearity)){
            cout<<"linearity not in range"<<endl;
            mapsFailed++;
            continue;
        }
        
    }
    cout<<mapsSucceeded<<endl;
    */
    
    
    
    
    
    
    /*int mapsFailed=0;
    for(int k=0; k<100; k++){
        cout<<k<<endl;
        ifstream input("New_Experiments/ConstraintSatisfaction/NoEnforcement/KidIcarus/kidicarus_"+to_string(k)+".txt");
        string line;
        vector<vector<char> > map;
        while(getline(input, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0')
                    tmp.push_back(line[i]);
            }
            map.push_back(tmp);
            tmp.clear();
        }
        
        
        ConstraintChecker t;
        //vector<int> v = t.SlidingWindowViolationDetection(map, true, 10);
        if(!KidIcarusPlayabilityConstraintSection(map, 0, 0)){
            cout<<"not playable"<<endl;
            mapsFailed++;
            continue;
        }/* else if(!KidIcarusLongestGapConstraint(map, minGap, maxGap)){
            cout<<"gap not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!KidIcarusAveragePlatformSizeConstraint(map, minAvgPlat, maxAvgPlat)){
            cout<<"Avg plat length not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!KidIcarusNumberOfHazardsConstraint(map, minHazards, maxHazards)){
            cout<<"number of hazards not in range"<<endl;
            mapsFailed++;
            continue;
        }*/
        
        
        
        /*if(!MarioNoIllFormedPipesConstraint(map, 0, 0)){
            cout<<"ill-formed pipes"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioPlayabilityConstraint(map, 0, 0)){
            cout<<"not playable"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLongestGapConstraint(map, minLength, maxLength)){
            cout<<"gap not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfEnemiesConstraint(map, minEnemies, maxEnemies)){
            cout<<"number of enemies not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfGapsConstraint(map, minGaps, maxGaps)){
            cout<<"number of gas not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioNumberOfPipesConstraint(map, minPipes, maxPipes)){
            cout<<"number of pipes not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLeniencyBoundConstraint(map, minLeniency, maxLeniency)){
            cout<<"leniency not in range"<<endl;
            mapsFailed++;
            continue;
        } else if(!MarioLinearityBoundConstraint(map, minLinearity, maxLinearity)){
            cout<<"linearity not in range"<<endl;
            mapsFailed++;
            continue;
        }
    }
    
    cout<<mapsFailed<<endl;
    
    
    //Generate->Test->Resample
    MdMCExperimentsWithConstraints("Mario_Maps/Enemies/","mario_", "UniformDistribution/ViolationDetection/","mario_", 12, 3, 3, 16, 100, 12, 210, 10);
    
    //MdMCExperimentsWithConstraints("KidIcarus_Maps/","kidicarus_", "New_Experiments/ConstraintSatisfaction/ViolationDetectionWithSmoothingAttemptLimit/KidIcarus/IS/","kidicarus_", 10, 3, 3, 6, 100, 170, 16, 10);
    
    //MdMCExperimentsWithConstraints("Loderunner_Maps/Standard/","Level ", "New_Experiments/ConstraintSatisfaction/GenerateAndTestAttemptLimit/Loderunner/Stretch/","Level ", 11, 3, 3, 150, 100, 22, 32, 10);
    
    //MdMCExperimentsWithConstraints("KidIcarus_Maps/","kidicarus_", "New_Experiments/ConstraintSatisfaction/KidIcarus/","kidicarus_", 1, 0, 3, 6, 5, 170, 16);
    
    /*ConstraintChecker test;
    int playable=0;
    for(int i=1; i<=100; i++){
        if(test.MarioPlayabilityConstraint("TCIAIG_Experiments/MRF/Mario/Sampling_Function/Sample "+to_string(i)+".txt"))
            playable++;
        //else
        //    cout<<i<<endl;
    }
    
    cout<<"Number Playable: "<<playable<<endl;
    */
    
    
    //STANDARD EXPERIMENTS
    //int r=22;
    //int l=0;
    //MdMCExperiments("KidIcarus_Maps/","kidicarus_", "TCIAIG_Experiments/1000Maps/MdMC/KidIcarus/"+to_string(r)+"-"+to_string(l)+"/","kidicarus_", r, l, 3, 6, 1000, 170, 16);

    
    /*ConstraintChecker t;
    //int z=4;
    //int l=3;
    for(int z=2; z<=12; z++){
        if(z==5 || z==7 || z==8 || z==9 || z==10 || z==11)
            continue;
            for(int l=0; l<=10; l++){
                if(l==4 || (l>5 && l<10))
                    continue;
                int playable=0;
                cout<<to_string(z)+"-"+to_string(l)+": ";
                for(int i=0; i<100; i++){
                    if(t.MarioPlayabilityConstraint("TCIAIG_Experiments/ReRun/MHMdMC/Mario/Manual-"+to_string(z)+"-"+to_string(l)+"/LowLevel_"+to_string(i)+".txt"))
                        playable++;
                }
                cout<<playable<<endl;
                
                TrainAndSample("Mario_Maps/Standard/", to_string(28)+"-"+to_string(4)+"-W"+"/", "mario_", "TCIAIG_Experiments/ReRun/HMdMC/Mario/"+to_string(28)+"-"+to_string(4)+"-"+to_string(3)+"-W/", "HighLevelTIME_", "LowLevelTIME_", 16, 4, 4, 100, 210/4, 12/4, 2, 3, 1, 3, 3, false);
            }
    }
    */
    //ClusterAndConvert("Mario_Maps/DA_Clustering/", "mario_", 40, 4, 4, 40, &Count, "Count");
    
    
    //NewMRFexperiments(150, 22, 32, 400000, 10, "Loderunner_Maps/Standard/Level ", "TestingWebPage/");
    //NewMRFexperiments(150, 22, 32, 50000, 900, "Loderunner_Maps/Standard/Level ", "TCIAIG_Experiments/MRF/Loderunner/NoSmoothing/");
    
    /*int playability=0;
    for(int i=1; i<=150; i++){
        ifstream input("Loderunner_Maps/Standard/Level "+to_string(i)+".txt");
        string line;
        vector<vector<char> > map;
        while(getline(input, line)){
            vector<char> tmp;
            for(int i=0; i<line.length(); i++){
                if(line[i]!='\n' && line[i]!='\r' && line[i]!='\t' && line[i]!='\0')
                    tmp.push_back(line[i]);
            }
            map.push_back(tmp);
            tmp.clear();
        }
        if(LoderunnerPlayabilityConstraint(map, 0, 0)){
            playability++;
            cout<<i<<endl;
        }
    }
    
    cout<<"Playable: "<<playability<<endl;
    */
    /*string D="D";
    int k=20;
    int z=4;
    int l=3;
    cout<<k<<"-"<<z<<"-"<<l<<"-"<<D<<": ";
    TrainAndSample("KidIcarus_Maps/", to_string(k)+"-"+to_string(z)+"-"+D+"/", "kidicarus_", "TCIAIG_Experiments/1000Maps/HMdMC/KidIcarus/"+to_string(k)+"-"+to_string(z)+"-"+to_string(l)+"-"+D+"/", "HighLevel_", "LowLevel_", 6, z, z, 1000, 16/z, 170/z, 2, 3, 1, 3, l, false);
    */
    //ManualConversion("Mario_Maps/Standard/", "Manual_3X3/", "mario_", 16, 3, 3);
    /*for (int z=2; z<=12; z++) {
        if(z==5 || (z>6 && z<12))
            continue;
        for(int l=0; l<=10; l++){
            if(l==4 || (l>5 && l<10))
                continue;
            cout<<z<<"-"<<l<<endl;
            TrainAndSample("Mario_Maps/Standard/", "Manual_"+to_string(z)+"X"+to_string(z)+"/", "mario_", "TCIAIG_Experiments/1000Maps/MHMdMC/Mario/"+to_string(z)+"-"+to_string(l)+"/", "HighLevel_", "LowLevel_", 16, z, z, 1000, 210/z, 12/z, 2, 3, 1, 3, l, true);
        }
    }*/
    //NewMRFexperiments(16, 12, 210, 100, 1, "Mario_Maps/Standard/mario_", "TCIAIG_Experiments/");
    
    //Linearity and Leniency Experiments for Expressive ranges
    
    //ComputeLinVSLen("Proposal_Experiments/MdMC/SMB/RowSplits/12/", "MdMC", "MdMC", "mario_", 1000);
    //ComputeLinVSLen("Proposal_Experiments/HMdMC/Manual/SMB/4X4-2/", "Manual", "Manual", "LowLevel_", 1000);
    //ComputeLinVSLen("Proposal_Experiments/HMdMC/Clustering/SMB/28-4X4-3-Shape/", "Clustering", "Clustering", "LowLevel_", 1000);
    //ComputeLinVSLen("Proposal_Experiments/MRF/SMB/", "MRF", "MRF", "Sample ", 1000);

    //ComputeLinVSLen("New_Experiments/DomainAdaptation/Baselines/AllMaps/Mario/", "Mario_AllMaps", "Mario_AllMaps", "mario_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/ReRun/TrainingData/Left/", "TD_Left", "TD_Left", "mario_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/ReRun/TrainingData/Top/", "TD_Top", "TD_Top", "mario_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/ReRun/MdMC/Mario/12-3/", "MdMC", "MdMC", "marioEXP_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/ReRun/HMdMC/Mario/28-4-3-W/", "HMdMC", "HMdMC", "LowLevelEXP_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/ReRun/MHMdMC/Mario/Manual-4-1/", "MHMdMC", "MHMdMC", "LowLevelEXP_", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/MRF/Mario/NoSmoothing/", "MRF", "MRF", "Sample ", 1000);
    //ComputeLinVSLen("TCIAIG_Experiments/MRF/Mario/NoSmoothing/", "MRF", "MRF", "Sample ", 1000);
    
    //ComputeLinVSLen("TCIAIG_Experiments/MRF/Mario/NoSmoothing/", "MRF", "MRF", "Sample ", 1000);
    ComputeLinVSLen("Mario_Maps/Enemies/", "Training", "Training", "mario_", 16);
    
    
    
    return 0;
}

void MdMCExperimentsWithConstraints(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, int sectionSize){
    
    /*Learner TopLevelLearner = Learner(rowSplits, lookAhead, false, false);
    TopLevelLearner.FindAllTileTypes(trainingMaps, trainingFolderName+trainingMapName, false);
    
    for(int i=networkStructure; i>=0; i--)
        TopLevelLearner.SetDepMatrix(i);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel(trainingFolderName+trainingMapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(width, height);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    TopGen.Config.LookAhead=lookAhead;
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
#if defined(_WIN32)
    _mkdir(outputFolderName);
#else
    mkdir(outputFolderName.c_str(), 0777);
#endif
    
    */
    
    
    
    Learner model = Learner(12, 3, false, false);
    model.FindAllTileTypes(16, "Mario_Maps/Enemies/mario_", false);
    model.SetDepMatrix(3);
    model.SetRows(false);
    model.ResizeVectors(false);
    
    Generator TopGen = Generator(210, 12);
    TopGen.CopyConfig(model.Config);
    TopGen.SetTileTypes(model.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    
    int numConfigs = (int)(TopGen.TileTypes.size()*TopGen.TileTypes.size()*TopGen.TileTypes.size()*12);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++){
        for(int config=0; config<numConfigs; config++){
            vector<float> tmp;
            for(int tile=0; tile<TopGen.TileTypes.size(); tile++){
                tmp.push_back(1.0/TopGen.TileTypes.size());
            }
            TopGen.Probabilities[dep].push_back(tmp);
            tmp.clear();
        }
    }

    
    //MARIO CONSTRAINTS
    //Easy
    
    int maxPipes = 13;
    int minPipes = 1;
    int maxLength = 8;
    int minLength = 4;
    int maxGaps = 12;
    int minGaps = 4;
    int maxEnemies = 31;
    int minEnemies = 11;
     //*/
    
    //Stretch
    /*int maxPipes = 19;
    int minPipes = 7;
    int maxLength = 10;
    int minLength = 6;
    int maxGaps = 16;
    int minGaps = 8;
    int maxEnemies = 41;
    int minEnemies = 21;
     //*/

    
    //KID ICARUS CONSTRAINTS
    //EASY
    /*
    int minHazards = 0;
    int maxHazards = 27;
    int minGap = 10;
    int maxGap = 12;
    int minAvgPlat = 3;
    int maxAvgPlat = 4;
    //*/
    //STRETCH
    
    /*int minHazards = 13;
    int maxHazards = 41;
    int minGap = 11;
    int maxGap = 13;
    int minAvgPlat = 4;
    int maxAvgPlat = 5;
    //*/
    
    //LODERUNNER CONSTRAINTS
    //EASY
    /*
    int minGold = 8;
    int maxGold = 30;
    int minGuards = 2;
    int maxGuards = 4;
    float minDensity = .22;
    float maxDensity = .46;
    //*/
    //STRETCH
    /*
    int minGold = 19;
    int maxGold = 41;
    int minGuards = 3;
    int maxGuards = 5;
    float minDensity = .34;
    float maxDensity = .58;
    //*/
    
    cout<<"Starting to generate"<<endl;
    //cout<<outputFolderName+outputMapName<<endl;
    //vector<int> timedOutMaps;
    //vector<double> timedOutMapsTimes;
    //vector<int> attempts;
    //chrono::time_point<std::chrono::system_clock> start, end, mapStart, mapEnd;
    //start = chrono::system_clock::now();
    
    //Generate and test
    /*int totalAttempts = 0;
    int incompleteMaps =0;
    for (int i = 0; i < toGenerate; i++){
        int attempt=1;

        //mapStart = chrono::system_clock::now();
        //mapEnd = chrono::system_clock::now();
        do{
            
            //stop sampling if at takes more than 50 attempts
            if(attempt==50){
                incompleteMaps++;
                break;
            }
            
            //stop trying to sample a map if it takes more than 20 seconds
            /*chrono::duration<double> t = mapEnd-mapStart;
            if(t.count() >= 20){
                timedOutMaps.push_back(i);
                timedOutMapsTimes.push_back(t.count());
                break;
            }*/
      /*      TopGen.GenerateTopLevel(outputFolderName+outputMapName + to_string(i) + ".txt");
            //mapEnd = chrono::system_clock::now();
            attempt++;
        }while(/*!MarioPlayabilityConstraint(outputFolderName+outputMapName+to_string(i)+".txt", 0, 0)
               || !MarioNoIllFormedPipesConstraint(TopGen.Map, 0, 0));
               || !MarioNumberOfPipesConstraint(TopGen.Map, minPipes, maxPipes)
               || !MarioNumberOfGapsConstraint(TopGen.Map, minGaps, maxGaps)
               || !MarioNumberOfEnemiesConstraint(TopGen.Map, minEnemies, maxEnemies)
               || !MarioLinearityBoundConstraint(TopGen.Map, 423, 709)
               || !MarioLeniencyBoundConstraint(TopGen.Map, 30, 62)
               || !MarioLongestGapConstraint(TopGen.Map, minLength, maxLength));*/
     /*          KidIcarusPlayabilitySection(TopGen.Map) >= 5);// ||
               //!KidIcarusAveragePlatformSizeConstraint(TopGen.Map, minAvgPlat, maxAvgPlat) ||
               //!KidIcarusLongestGapConstraint(TopGen.Map, minGap, maxGap) ||
               //!KidIcarusNumberOfHazardsConstraint(TopGen.Map, minHazards, maxHazards)*/
              /* !LoderunnerPlayabilityConstraint(TopGen.Map, 0, 0) ||
               !LoderunnerNumberOfTreasuresConstraint(TopGen.Map, minGold, maxGold) ||
               !LoderunnerNumberOfGuardsConstraint(TopGen.Map, minGuards, maxGuards) ||
               !LoderunnerDensityBoundConstraint(TopGen.Map, minDensity, maxDensity)
               );*/
    /*    totalAttempts+=attempt;
    }
    
               
    /*end = chrono::system_clock::now();
    
    chrono::duration<double> tm = end-start;
    cout<<"Time in seconds taken to generate all levels: "<<tm.count()<<" s"<<endl;
    
    double tm2=0;
    for (int i=0; i<(int)timedOutMapsTimes.size(); i++) {
        tm2+=timedOutMapsTimes[i];
    }
    
    cout<<"Time in seconds taken to generate all proper levels: "<<tm.count()-tm2<<" s"<<endl;
    
    int total_attempts = 0;
    for (int i=0; i<attempts.size(); i++) {
        total_attempts+=attempts[i];
    }
    
    float avg1 = total_attempts/(float)attempts.size();
    float avg2=-5;
    if(timedOutMaps.size()>0){
        float num=0;
        for (int i=0; i<attempts.size(); i++) {
            int iter=0;
            if(iter < timedOutMaps.size()-1 && i != timedOutMaps[iter]){
                total_attempts+=attempts[i];
                num++;
            } else if(iter < timedOutMaps.size()-1 && i == timedOutMaps[iter]){
                iter++;
            }
        }
        avg2 = total_attempts/num;
    }*/
    
    /*
    cout<<"Avg attempts per all maps: "<<(totalAttempts*17)/100.0<<endl;
    cout<<"Avg attempts per all proper maps: "<<(17*(totalAttempts-(50*incompleteMaps)))/(100.0-incompleteMaps)<<endl;
    cout<<"Number of maps timed out: "<<incompleteMaps<<endl;
    //*/
    
  /*
    ConstraintChecker t;
    //violation detection
    int incompleteMaps=0;
    int totalAttempts=0;
    for (int i=0; i<100; i++) {
        //mapStart = chrono::system_clock::now();
        //mapEnd = chrono::system_clock::now();
        //generate the level
        //TopGen.GenerateTopLevel(outputFolderName+outputMapName + to_string(i) + ".txt");
        TopGen.GenerateTopLevel("UniformDistribution/ViolationDetection/mario_"+ to_string(i) + ".txt");

        //fix the level while there are sections to be edited
        vector<int> ret1; //=t.SlidingWindowViolationDetection(TopGen.Map, true, 10);
        vector<int> ret2;
        ret2.push_back(0);
        bool rerun;
        //bool timedOut=false;
        int chunksChanged=0;
        int attempt=17;
        //cout<<"Generating Map "<<i<<endl;
        do{
            //chrono::duration<double> tm = mapEnd - mapStart;
            /*if (tm.count()> 20) {
                timedOutMaps.push_back(i);
                timedOutMapsTimes.push_back(tm.count());
                cout<<i<<endl;
                changesR.push_back(chunksChanged);
                timedOut = true;
                break;
            }*/
/*            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            
            rerun=false;
            //first check the hard constraints, playability and ill formed structures
            for (int r=0; r<ret1.size(); r+=4){
                //cout<<r<<" "<<ret1.size()<<endl;
                //cout<<"RET: "<<ret1[r]<<" "<<ret1[r+3]<<" "<<ret1[r+2]<<" "<<ret1[r+1]<<endl;
                //TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret1[r], ret1[r+3], ret1[r+2], ret1[r+1]);
                TopGen.RegenerateTopLevelSectionWithSmoothing("UniformDistribution/ViolationDetection/mario_"+ to_string(i) + ".txt", ret1[r], ret1[r+3], ret1[r+2], ret1[r+1]);
                attempt++;
                if(attempt >=1050){
                    incompleteMaps++;
                    attempt = 1050;
                    break;
                }
                //cout<<"Playability and ill formed pipes"<<endl;
            }
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }

            int maxAttempts = 5;
/*
            //KID ICARUS
            int longestGap = KidIcarusLongestGapCount(TopGen.Map);
            int numHazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
            float avgPlat  = KidIcarusAveragePlatformSizeCount(TopGen.Map);
            
            bool gapTooShort    = false;
            bool gapTooLong     = false;
            bool tooFewHazards  = false;
            bool tooManyHazards = false;
            bool avgPlatTooShort= false;
            bool avgPlatTooLong = false;
            
            if(longestGap < minGap)
                gapTooShort = true;
            if(longestGap > maxGap)
                gapTooLong = true;
            
            if(numHazards < minHazards)
                tooFewHazards = true;
            if(numHazards > maxHazards)
                tooManyHazards = true;
            
            if(avgPlat < minAvgPlat)
                avgPlatTooShort = true;
            if(avgPlat > maxAvgPlat)
                avgPlatTooLong = true;
            
            
            vector<vector<char> > fallbackMap;
            for (int i=0; i<TopGen.Map.size(); i++) {
                vector<char> tmp;
                for (int j=0; j<TopGen.Map[i].size(); j++) {
                    tmp.push_back(TopGen.Map[i][j]);
                }
                fallbackMap.push_back(tmp);
                tmp.clear();
            }
            
            bool retry=false;
            bool chooseNewSection = true;
            int attempts=0;
            int iter=0;
            while(gapTooShort || retry ){
                cout<<"gap too short"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusLongestGapCount, false);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(avgPlatTooShort && plat < avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(avgPlatTooLong && plat > avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!avgPlatTooLong && !avgPlatTooShort && (plat < minAvgPlat || plat > maxAvgPlat)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(tooFewHazards && hazards < numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyHazards && hazards > numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewHazards && !tooManyHazards && (hazards < minHazards || hazards > maxHazards)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(length > longestGap && length < minGap){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(length >= minGap){
                    gapTooShort = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                avgPlat = plat;
                numHazards  = hazards;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            
            
            retry=false;
            chooseNewSection = true;
            attempts=0;
            iter=0;
            while(gapTooLong || retry ){
                cout<<"gap too long"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusLongestGapCount, true);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(avgPlatTooShort && plat < avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(avgPlatTooLong && plat > avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!avgPlatTooLong && !avgPlatTooShort && (plat < minAvgPlat || plat > maxAvgPlat)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(tooFewHazards && hazards < numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyHazards && hazards > numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewHazards && !tooManyHazards && (hazards < minHazards || hazards > maxHazards)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(length < longestGap && length > maxGap){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(length <= maxGap){
                    gapTooLong = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                avgPlat = plat;
                numHazards  = hazards;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            
            
            retry=false;
            chooseNewSection = true;
            attempts=0;
            iter=0;
            while(tooFewHazards || retry ){
                cout<<"too few hazards"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusNumberOfHazardsCount, false);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(avgPlatTooShort && plat < avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(avgPlatTooLong && plat > avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!avgPlatTooLong && !avgPlatTooShort && (plat < minAvgPlat || plat > maxAvgPlat)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(gapTooShort && length < minGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooShort && length > maxGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooShort && !gapTooLong && (length < minGap || length > maxGap)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(hazards > numHazards && hazards < minHazards){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(hazards >= minHazards){
                    tooFewHazards = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                avgPlat = plat;
                longestGap = length;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            
            retry=false;
            chooseNewSection = true;
            attempts=0;
            iter=0;
            while(tooManyHazards || retry ){
                cout<<"too many hazards"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusNumberOfHazardsCount, true);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(avgPlatTooShort && plat < avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(avgPlatTooLong && plat > avgPlat){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!avgPlatTooLong && !avgPlatTooShort && (plat < minAvgPlat || plat > maxAvgPlat)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(gapTooShort && length < minGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooShort && length > maxGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooShort && !gapTooLong && (length < minGap || length > maxGap)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(hazards < numHazards && hazards > maxHazards){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(hazards <= maxHazards){
                    tooManyHazards = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                avgPlat = plat;
                longestGap = length;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            
            retry=false;
            chooseNewSection = true;
            attempts=0;
            iter=0;
            while(avgPlatTooShort || retry ){
                cout<<"average platform too short"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusAveragePlatformSizeCount, false);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                
                if(gapTooShort && length < minGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooShort && length > maxGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooShort && !gapTooLong && (length < minGap || length > maxGap)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(tooFewHazards && hazards < numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyHazards && hazards > numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewHazards && !tooManyHazards && (hazards < minHazards || hazards > maxHazards)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(plat > avgPlat && plat < minAvgPlat){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(plat >= minAvgPlat){
                    avgPlatTooShort = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                longestGap = length;
                numHazards  = hazards;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            
            retry=false;
            chooseNewSection = true;
            attempts=0;
            iter=0;
            while(avgPlatTooLong || retry ){
                cout<<"average platform too long"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, false, sectionSize, &KidIcarusAveragePlatformSizeCount, true);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+3], ret2[iter+2],ret2[iter+1]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = KidIcarusLongestGapCount(TopGen.Map);
                float plat = KidIcarusAveragePlatformSizeCount(TopGen.Map);
                int hazards = KidIcarusNumberOfHazardsCount(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                
                if(gapTooShort && length < minGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooShort && length > maxGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooShort && !gapTooLong && (length < minGap || length > maxGap)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                if(tooFewHazards && hazards < numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyHazards && hazards > numHazards){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewHazards && !tooManyHazards && (hazards < minHazards || hazards > maxHazards)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(plat < avgPlat && plat > maxAvgPlat){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                
                if(plat <= maxAvgPlat){
                    avgPlatTooLong = false;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                }
                
                longestGap = length;
                numHazards  = hazards;
            }
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
*/
            //then record how much of each thing we have
            //MARIO
/*            int longestGap = MarioCountGapLength(TopGen.Map);
            int numberOfPipes = MarioCountPipes(TopGen.Map);
            int numberOfGaps = MarioCountGaps(TopGen.Map);
            int numberOfEnemies = MarioCountEnemies(TopGen.Map);
            
            //Next, determine which constraints are not satisfied (and in which direction: too many or too few)
            bool gapTooLong     = false;
            bool gapTooShort    = false;
            bool tooManyPipes   = false;
            bool tooFewPipes    = false;
            bool tooManyGaps    = false;
            bool tooFewGaps     = false;
            bool tooManyEnemies = false;
            bool tooFewEnemies  = false;
            
            if(longestGap < minLength)
                gapTooShort = true;
            else if(longestGap > maxLength)
                gapTooLong = true;
            
            if(numberOfPipes < minPipes)
                tooFewPipes = true;
            else if(numberOfPipes > maxPipes)
                tooManyPipes = true;
            
            if(numberOfGaps < minGaps)
                tooFewGaps = true;
            else if(numberOfGaps > maxGaps)
                tooManyGaps = true;
            
            if(numberOfEnemies < minEnemies)
                tooFewEnemies = true;
            else if(numberOfEnemies > maxEnemies)
                tooManyEnemies = true;
            vector<vector<char> > fallbackMap;
            for (int i=0; i<TopGen.Map.size(); i++) {
                vector<char> tmp;
                for (int j=0; j<TopGen.Map[i].size(); j++) {
                    tmp.push_back(TopGen.Map[i][j]);
                }
                fallbackMap.push_back(tmp);
                tmp.clear();
            }
            
            bool retry=false;
            bool chooseNewSection = true;
            int attempts=0;
            int iter=0;
            while(gapTooShort || retry ){
                cout<<"gap too short"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountGapLength, false);
                    chooseNewSection = false;
                }
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }

                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                attempt++;
                chunksChanged++;
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                //then see if we helped the original constraint
                //This is only reached if other constraints are not worsened
                if(length > longestGap && length < minLength){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }

                }
                
                if(length >= minLength)
                    gapTooShort = false;
                
                numberOfPipes = pipes;
                numberOfGaps  = gaps;
                numberOfEnemies = enemies;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(gapTooLong || retry){
                cout<<"gap too long"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountGapLength, true);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                attempt++;
                chunksChanged++;
                    
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(length < longestGap && length > maxLength){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(length <= maxLength)
                    gapTooLong = false;
                
                numberOfPipes = pipes;
                numberOfGaps  = gaps;
                numberOfEnemies = enemies;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(tooFewPipes || retry){
                
                cout<<"Too few pipes"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountPipes, false);
                    chooseNewSection = false;
                }
            
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                attempt++;
                chunksChanged++;
                
            
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(pipes > numberOfPipes && pipes < minPipes){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(pipes >= minPipes)
                    tooFewPipes = false;
                
                longestGap = length;
                numberOfGaps  = gaps;
                numberOfEnemies = enemies;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(tooManyPipes || retry){
                
                cout<<"too many pipes"<<endl;
                
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountPipes, true);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                chunksChanged++;
                attempt++;
                
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(pipes < numberOfPipes && pipes > maxPipes){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(pipes <= maxPipes)
                    tooManyPipes = false;
                
                longestGap = length;
                numberOfGaps  = gaps;
                numberOfEnemies = enemies;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(tooFewGaps || retry){
                
                cout<<"too few gaps"<<endl;
                
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountGaps, false);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                chunksChanged++;
                attempt++;
                 
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(gaps > numberOfGaps && gaps < minGaps){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(gaps >= minGaps)
                    tooFewGaps = false;
                
                longestGap = length;
                numberOfPipes  = pipes;
                numberOfEnemies = enemies;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(tooManyGaps || retry){
                
                cout<<"too many gaps"<<endl;
                
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountGaps, true);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                chunksChanged++;
                attempt++;
                
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewEnemies && enemies < numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyEnemies && enemies > numberOfEnemies){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewEnemies && !tooManyEnemies && (enemies<minEnemies || enemies >maxEnemies)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(gaps < numberOfGaps && gaps > maxGaps){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(gaps <= maxGaps)
                    tooManyGaps = false;
                
                longestGap = length;
                numberOfPipes  = pipes;
                numberOfEnemies = enemies;
            }
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection=true;
            while(tooFewEnemies || retry){
                
                cout<<"too few enemies"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountEnemies, false);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                chunksChanged++;
                attempt++;
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(enemies > numberOfEnemies && enemies < minEnemies){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(enemies >= minEnemies)
                    tooFewEnemies = false;
                
                numberOfPipes = pipes;
                numberOfGaps  = gaps;
                longestGap    = length;
            }
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            //Now the next constraint
            retry = false;
            attempts=0;
            iter=0;
            chooseNewSection = true;
            while(tooManyEnemies || retry){
                //cout<<"too many enemies"<<endl;
                if(chooseNewSection){
                    ret2 = t.SlidingWindowViolationDetectionCounts(TopGen.Map, true, sectionSize, &MarioCountEnemies, true);
                    chooseNewSection = false;
                }
                
                if(attempts >= maxAttempts){
                    attempts=0;
                    iter+=4;
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    
                }
                
                if(iter>=ret2.size()){
                    //revert the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            TopGen.Map[h][w] = fallbackMap[h][w];
                        }
                    }
                    break;
                }
                
                TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret2[iter+0], ret2[iter+1], ret2[iter+2],ret2[iter+3]);
                chunksChanged++;
                attempt++;
                retry = false;
                
                int length = MarioCountGapLength(TopGen.Map);
                int gaps = MarioCountGaps(TopGen.Map);
                int pipes = MarioCountPipes(TopGen.Map);
                int enemies = MarioCountEnemies(TopGen.Map);
                
                //Make sure we are not worsening any other constraints
                if(gapTooShort && length < longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(gapTooLong && length > longestGap){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!gapTooLong && !gapTooShort && (length<minLength || length >maxLength)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewPipes && pipes < numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyPipes && pipes > numberOfPipes){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewPipes && !tooManyPipes && (pipes<minPipes || pipes >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooFewGaps && gaps < numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(tooManyGaps && gaps > numberOfGaps){
                    retry = true;
                    attempts++;
                    continue;
                }
                if(!tooFewGaps && !tooManyGaps && (gaps<minGaps || gaps >maxPipes)){
                    retry = true;
                    attempts++;
                    continue;
                }
                
                //then see if we helped the original constraint
                //This is only reache dif other cosntraints are not worsened
                if(enemies < numberOfEnemies && enemies > maxEnemies){
                    attempts=0;
                    iter+=4;
                    //save the map
                    for(int h=0; h<TopGen.Map.size(); h++){
                        for (int w=0; w<TopGen.Map[h].size(); w++) {
                            fallbackMap[h][w] = TopGen.Map[h][w];
                        }
                    }
                    
                }
                if(enemies <= maxEnemies)
                    tooManyEnemies = false;
                
                longestGap = length;
                numberOfPipes  = pipes;
                numberOfGaps = gaps;
            }
            
            longestGap = MarioCountGapLength(TopGen.Map);
            numberOfPipes = MarioCountPipes(TopGen.Map);
            numberOfGaps = MarioCountGaps(TopGen.Map);
            numberOfEnemies = MarioCountEnemies(TopGen.Map);
            
            cout<<"Longest Gap = "<<longestGap<<endl;
            cout<<"Number of Pipes = "<<numberOfPipes<<endl;
            cout<<"Number of Gaps = "<<numberOfGaps<<endl;
            cout<<"Number of Enemies = "<<numberOfEnemies<<endl;
            
            if(attempt >=1050){
                incompleteMaps++;
                attempt = 1050;
                break;
            }
            if(longestGap < minLength || longestGap > maxLength || numberOfPipes <minPipes || numberOfPipes > maxPipes || numberOfGaps < minGaps || numberOfGaps > maxGaps || numberOfEnemies < minEnemies || numberOfEnemies > maxEnemies){
                rerun=true;
                cout<<"Rerun whole thing"<<endl;
            }
             
            //*/
            
      /*      cout<<"Longest Gap = "<<longestGap<<endl;
            cout<<"Number of Hazards = "<<numHazards<<endl;
            cout<<"Average Platform Length = ="<<avgPlat<<endl;
            
            if(attempt >=850){
                incompleteMaps++;
                attempt = 850;
                break;
            }
            if(longestGap < minGap || longestGap > maxGap || numHazards < minHazards || numHazards > maxHazards || avgPlat < minAvgPlat || avgPlat > maxAvgPlat){
                rerun=true;
                cout<<"Repeat"<<endl;
            }
        */
         /*   if(!MarioPlayabilityConstraint(TopGen.Map, 0, 0))
                ret1 = t.SectionBySectionViolationDetection(TopGen.Map, false, sectionSize, 0, 0, &MarioPlayabilityConstraint);
            else
                ret1.clear();
            //mapEnd = chrono::system_clock::now();
        } while(!ret1.empty() || rerun);
        
        cout<<attempt<<" "<<chunksChanged<<endl;
        totalAttempts+=attempt;
        //if(!timedOut)
        //    changesA.push_back(chunksChanged);
    }
    */
    //end = chrono::system_clock::now();
    //chrono::duration<double> tm = end-start;
    //cout<<"Time in seconds taken to generate all levels: "<<tm.count()<<" s"<<endl;
    
    //double tm2=0;
    //for (int i=0; i<(int)timedOutMapsTimes.size(); i++) {
    //    tm2+=timedOutMapsTimes[i];
    //}
    
    //cout<<"Time in seconds taken to generate all proper levels: "<<tm.count()-tm2<<" s"<<endl;
    //cout<<"Maps timed out: "<<timedOutMaps.size()<<endl;
    
    //float avgT;
    //for (int i=0; i<changesA.size(); i++) {
    //    avgT+=changesA[i];
    //}
    /*cout<<"Average number of sections changed for all maps: "<<totalAttempts/100.0<<endl;
    cout<<"Average number of sections changed for the proper maps: "<<(totalAttempts-850*incompleteMaps)/(100.0-incompleteMaps)<<endl;
    cout<<"Incomplete maps: "<<incompleteMaps<<endl;
    //*/
    
    
//    int sectionsResampledTotal=0;
//    int sectionsResampledIncomp =0;
//    start = chrono::system_clock::now();
    int currAttempts=0;
    int attemptsPerMap =0;
    int incompleteMaps =0;
    int totalAttempts=0;
    int numSections = TopGen.LengthOfLevel/(10.0);
    for (int i=0; i<toGenerate; i++) {
        attemptsPerMap =0;
        //initialize the map to be generated
        TopGen.Map.resize(TopGen.HeightOfLevel);
        for (int i = 0; i < TopGen.HeightOfLevel; i++){
            TopGen.Map[i].resize(TopGen.LengthOfLevel);
            for (int j = 0; j < TopGen.LengthOfLevel; j++){
                TopGen.Map[i][j] = 'X';
            }
        }
        
        for (int j=1; j<=numSections; j++) {
            
        //for (int j=TopGen.HeightOfLevel-1; j>=0; j--) {
            //stop trying to sample a map if it takes more than a predefined number of section attempts
            
            if(attemptsPerMap >= 1050){
                incompleteMaps++;
                attemptsPerMap=1050;
                break;
            }
            /*chrono::duration<double> t = mapEnd-mapStart;
            if(t.count() >= 20){
                timedOutMaps.push_back(i);
                timedOutMapsTimes.push_back(t.count());
                sectionsResampledIncomp+=currAttempts;
                break;
            }*/
            
            currAttempts=TopGen.GenerateTopLevelSectionWithConstraints(outputFolderName+outputMapName+to_string(i)+".txt", (j-1)*10, 0, j*10, 12);
            //currAttempts = TopGen.GenerateTopLevelRowWithConstraints(outputFolderName+outputMapName+to_string(i)+".txt", j, 'K');
            
            //gives the algorithm a chance to break if time eaches too high
            if(currAttempts == 6){
                j++;
                attemptsPerMap+=currAttempts;
                continue;
            } else{
                attemptsPerMap+=currAttempts;
            }
            
        }
        cout<<i<<" "<<attemptsPerMap<<endl;
        totalAttempts+=attemptsPerMap;
    }
    
    
    cout<<"Average number of sections changed for all maps: "<<totalAttempts/100.0<<endl;
    cout<<"Average number of sections changed for the proper maps: "<<(totalAttempts-1050*incompleteMaps)/(100.0-incompleteMaps)<<endl;
    cout<<"Incomplete maps: "<<incompleteMaps<<endl;
    
    
    //end = chrono::system_clock::now();
    /*chrono::duration<double> tm = end-start;
    cout<<"Time in seconds taken to generate all levels: "<<tm.count()<<" s"<<endl;
    
    double tm2=0;
    for (int i=0; i<(int)timedOutMapsTimes.size(); i++) {
        tm2+=timedOutMapsTimes[i];
    }
    
    cout<<"Time in seconds taken to generate all proper levels: "<<tm.count()-tm2<<" s"<<endl;
    
    cout<<"Total number of sections resampled for all maps: "<<sectionsResampledTotal<<endl;
    cout<<"Total number of sections resampled for all proper maps: "<<sectionsResampledTotal- sectionsResampledIncomp<<endl;
    
    cout<<"Maps timed out: "<<timedOutMaps.size()<<endl;


    */
}

void PlayabilityContraintSampling(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, int sectionSize){
    
    
    //Train the model
    Learner TopLevelLearner = Learner(rowSplits, lookAhead, false, false);
    TopLevelLearner.FindAllTileTypes(trainingMaps, trainingFolderName+trainingMapName, false);
    
    for(int i=networkStructure; i>=0; i--)
        TopLevelLearner.SetDepMatrix(i);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel(trainingFolderName+trainingMapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    //cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(width, height);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    TopGen.Config.LookAhead=lookAhead;
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    ConstraintChecker cc= ConstraintChecker();
    
    float totalAttempts=0;
    int incompleteMaps=0;
    for(int i=0; i<toGenerate; i++){
        float attemptsPerMap=21;
        bool failed=false;
        
        //violation detection resampling
        TopGen.GenerateTopLevel(outputFolderName+outputMapName + to_string(i) + ".txt");
        
        
        int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        vector<int> ret1;
        if(pos == -1){
            cout<<"Playability returned -1. Exiting with error"<<endl;
            return;
        }
        if(pos >2 && pos<209){
            ret1.push_back(max(pos-4, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+5, 209));
            ret1.push_back(11);
        }
        
        //fix the level while there are sections to be edited
        while(ret1.size()>0){
            //cout<<pos<<"("<<ret1[0]<<", "<<ret1[1]<<") ("<<ret1[2]<<", "<<ret1[3]<<")"<<endl;
            //check the hard constraints, playability and ill formed structures
            for (int r=0; r<ret1.size(); r+=4){
                float tilesSampled = TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+outputMapName+to_string(i)+".txt", ret1[r], ret1[r+1], ret1[r+2], ret1[r+3]);
                
                attemptsPerMap+= tilesSampled/(120.0);
                if(attemptsPerMap >=1050){
                    incompleteMaps++;
                    attemptsPerMap = 1050;
                    failed=true;
                    break;
                }
            }
            if(failed)
                break;
        
            ret1.clear();
            int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
            vector<int> ret1;
            if(pos == -1){
                cout<<"Playability returned -1. Exiting with error"<<endl;
                return;
            }
            if(pos >2 && pos<209){
                ret1.push_back(max(pos-4, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+5, 209));
                ret1.push_back(11);
            }
        }
        if(!failed)
            totalAttempts+=attemptsPerMap;
        else
            rename((outputFolderName+outputMapName+to_string(i)+".txt").c_str(), (outputFolderName+"failed_"+outputMapName+to_string(i)+".txt").c_str());
        
        //cout<<i<<" "<<attemptsPerMap<<endl;
    }
    //cout<<"Average number of sections changed for all proper maps: "<<totalAttempts/100.0<<endl;
    //cout<<"Average number of sections changed for the proper maps: "<<(totalAttempts-1050*incompleteMaps)/(100.0-incompleteMaps)<<endl;
    //cout<<"Incomplete maps: "<<incompleteMaps<<endl;
}

void ViolationLocationResamplingWithLikelihood(int numToGenerate, double minLikelihood, double maxLikelihood, vector<vector<vector< char> > > medoids, vector<vector< double> > transitionTable){

    //train on the input maps
    Learner TopLevelLearner = Learner(14, 3, false, false);
    TopLevelLearner.FindAllTileTypes(4, "Mario_Maps/Playthrough/InputMaps/standard_", false);
    
    for(int i=3; i>=0; i--){
        TopLevelLearner.SetDepMatrix(i);
    }
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel("Mario_Maps/Playthrough/InputMaps/standard_", 4, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    //cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(210, 14);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    for(int i=1; i<numToGenerate; i++){
        
        //generate a level
        TopGen.GenerateTopLevel("IJCAI17_Experiments/VLR_Likelihood/mario_" + to_string(i) + ".txt");
        
        
        //0 = not resampling
        //1 = playability issues
        //2 = likelihood issues
        int resamplingReason=0;
        
        
        
        //test the playability constraint
        vector<int> ret;
        int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        if(pos == -1){
            cout<<"Playability returned -1. Exiting with error"<<endl;
            pos = 0;
        }
        if(pos >2 && pos<209){
            ret.push_back(max(pos-4, 0));
            ret.push_back(0);
            ret.push_back(min(pos+5, 209));
            ret.push_back(13);
        }
        
        if(ret.size() >0)
            resamplingReason = 1;
        else
            resamplingReason = 2;
        
        while(resamplingReason > 0){
            
            
            //resample unplayable sections
            while(ret.size()>0 && resamplingReason == 1){
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                
            
                ret.clear();
                int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
                cout<<"Distance into map: "<<pos<<endl;
                vector<int> ret1;
                if(pos == -1){
                    cout<<"Playability returned -1. Exiting with error"<<endl;
                    pos=0;;
                }
                if(pos >2 && pos<209){
                    ret1.push_back(max(pos-4, 0));
                    ret1.push_back(0);
                    ret1.push_back(min(pos+5, 209));
                    ret1.push_back(13);
                }
            }
            
            //poop
            MarioPlayabilityPath("IJCAI17_Experiments/VLR_Likelihood/mario_" + to_string(i));
            
            //determine likelihood of that level (either with states, moves, or both)
            double likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Annotated_Path.txt");;
            cout<<"Likelihood before looping: "<<likelihood<<endl;
            while(likelihood == 0 || likelihood == 1 || isnan(likelihood)){
                likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Annotated_Path.txt");
            }
            cout<<"First likelihood check: "<<likelihood<<endl;
            
            if(likelihood < minLikelihood || likelihood > maxLikelihood){
                resamplingReason = 2;
                cout<<"Need to resample for likelihood"<<endl;
            }
            else{
                resamplingReason = 0;
                cout<<"Likelihood is within range"<<endl;
            }
            while(resamplingReason == 2){
            
                string line;
                ifstream in("IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Path.txt");
                vector<pair<int, int> > trace;
                while(getline(in, line)){
                    stringstream s;
                    s.str(line);
                    string word;
                    pair<int, int> pos;
                    
                    getline(s, word, '\t');
                    pos.first = stoi(word);
                    getline(s, word, '\t');
                    pos.second = stoi(word);
                    
                    trace.push_back(pos);
                }
                
                
                //find lowest likelihood section and resample
                double lowest_likelihood=1;
                for(int c=0; c<210-16; c++){
                    ofstream tmp("IJCAI17_Experiments/VLR_Likelihood/SectionPath.txt");
                    int start=0;
                    int end=0;
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c){
                            start = k;
                            break;
                        }
                    }
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c+16){
                            end = k;
                            break;
                        }
                    }
                    
                    for(int k=start; k<=end; k++){
                        tmp<<trace[k].first<<"\t"<<trace[k].second<<endl;
                    }
                    tmp.close();
                
                
                    double tmpLikelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood/SectionPath.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Annotated_Path.txt");
                    
                    //cout<<"Section "<<c<<" likelihood: "<<tmpLikelihood<<", current min likelihood: "<<lowest_likelihood<<endl;
                    if(tmpLikelihood < lowest_likelihood){
                        lowest_likelihood = tmpLikelihood;
                        pos = c;
                    }
                }
                
                if(pos >=0 && pos<209){
                    ret.clear();
                    ret.push_back(pos);
                    ret.push_back(0);
                    ret.push_back(pos+16);
                    ret.push_back(13);
                }

                cout<<"position to resample is: "<<ret[0]<<endl;
                //keep the old map in case it is better
                vector< vector< char> > OldMap = TopGen.Map;
                
                
                //resample the offending section
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                
                while(!MarioPlayabilityPath("IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i))){
                    TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                    cout<<"Regenerating a section for likelihood"<<endl;
                }
                
                
                
                //determine likelihood of that level (either with states, moves, or both)
                double new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Annotated_Path.txt");
                while(new_likelihood == 0 || new_likelihood == 1 || isnan(new_likelihood)){
                    new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood/mario_"+to_string(i)+"_Annotated_Path.txt");
                }
                
                cout<<"New Likelihood: "<<new_likelihood<<", Current Likelihood: "<<likelihood<<endl;
                
                if(new_likelihood > likelihood){
                    likelihood = new_likelihood;
                } else{
                    TopGen.Map = OldMap;
                    cout<<"Is old map playable: "<<MarioPlayabilityPath(TopGen.Map)<<endl;
                }
                
                if(likelihood>= minLikelihood && likelihood <= maxLikelihood){
                    resamplingReason = 0;
                    cout<<"Map is ready"<<endl;
                    break;
                }
            }
        }
    }
}

void ViolationLocationResamplingWithLikelihood(int numToGenerate, double minLikelihood, double maxLikelihood, vector<vector<vector< char> > > medoids, vector<vector< double> > transitionTable, vector<double> actionDistribution){
    
    double goalDistributionDistance = .4;
    
    //train on the input maps
    Learner TopLevelLearner = Learner(14, 3, false, false);
    TopLevelLearner.FindAllTileTypes(4, "Mario_Maps/Playthrough/InputMaps/standard_", false);
    
    for(int i=3; i>=0; i--){
        TopLevelLearner.SetDepMatrix(i);
    }
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel("Mario_Maps/Playthrough/InputMaps/standard_", 4, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    //cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(210, 14);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    for(int i=94; i<96; i++){

        //generate a level
        TopGen.GenerateTopLevel("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i) + ".txt");
        
        
        //0 = not resampling
        //1 = playability issues
        //2 = likelihood issues
        int resamplingReason=0;
        
        
        
        //test the playability constraint
        vector<int> ret;
        int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        if(pos == -1){
            cout<<"Playability returned -1. Exiting with error"<<endl;
            pos = 0;
        }
        if(pos >2 && pos<209){
            ret.push_back(max(pos-4, 0));
            ret.push_back(0);
            ret.push_back(min(pos+5, 209));
            ret.push_back(13);
        }
        
        if(ret.size() >0)
            resamplingReason = 1;
        else
            resamplingReason = 2;
        
        while(resamplingReason > 0){
            
            
            //resample unplayable sections
            while(ret.size()>0 && resamplingReason == 1){
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                
                
                ret.clear();
                int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
                cout<<"Distance into map: "<<pos<<endl;
                vector<int> ret1;
                if(pos == -1){
                    cout<<"Playability returned -1. Exiting with error"<<endl;
                    pos=0;;
                }
                if(pos >2 && pos<209){
                    ret1.push_back(max(pos-4, 0));
                    ret1.push_back(0);
                    ret1.push_back(min(pos+5, 209));
                    ret1.push_back(13);
                }
            }
            
            MarioPlayabilityPath("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i));
            
            //determine likelihood of that level (either with states, moves, or both)
            double likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
            double dist = getDistanceFromDistribution(transitionTable, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
            cout<<"Likelihood before looping: "<<likelihood<<endl;
            while(likelihood == 0 || likelihood == 1 || isnan(likelihood)){
                likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                dist = getDistanceFromDistribution(transitionTable, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
            }
            cout<<"First likelihood check: "<<likelihood<<endl;
            
            if(likelihood < minLikelihood || likelihood > maxLikelihood){
                resamplingReason = 2;
                cout<<"Need to resample for likelihood"<<endl;
            }
            else{
                resamplingReason = 3;
                cout<<"Likelihood is within range"<<endl;
            }
            while(resamplingReason == 2){
                
                string line;
                ifstream in("IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                vector<pair<int, int> > trace;
                while(getline(in, line)){
                    stringstream s;
                    s.str(line);
                    string word;
                    pair<int, int> pos;
                    
                    getline(s, word, '\t');
                    pos.first = stoi(word);
                    getline(s, word, '\t');
                    pos.second = stoi(word);
                    
                    trace.push_back(pos);
                }
                
                
                //find lowest likelihood section and resample
                double lowest_likelihood=1;
                for(int c=0; c<210-16; c++){
                    ofstream tmp("IJCAI17_Experiments/VLR_Likelihood_2/SectionPath.txt");
                    int start=0;
                    int end=0;
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c){
                            start = k;
                            break;
                        }
                    }
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c+16){
                            end = k;
                            break;
                        }
                    }
                    
                    for(int k=start; k<=end; k++){
                        tmp<<trace[k].first<<"\t"<<trace[k].second<<endl;
                    }
                    tmp.close();
                    
                    
                    double tmpLikelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/SectionPath.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                    
                    //cout<<"Section "<<c<<" likelihood: "<<tmpLikelihood<<", current min likelihood: "<<lowest_likelihood<<endl;
                    if(tmpLikelihood < lowest_likelihood){
                        lowest_likelihood = tmpLikelihood;
                        pos = c;
                    }
                }
                
                if(pos >=0 && pos<209){
                    ret.clear();
                    ret.push_back(pos);
                    ret.push_back(0);
                    ret.push_back(pos+16);
                    ret.push_back(13);
                }
                
                cout<<"position to resample is: "<<ret[0]<<endl;
                //keep the old map in case it is better
                vector< vector< char> > OldMap = TopGen.Map;
                
                
                //resample the offending section
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                
                while(!MarioPlayabilityPath("IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i))){
                    TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                    cout<<"Regenerating a section for likelihood"<<endl;
                }
                
                
                
                //determine likelihood of that level (either with states, moves, or both)
                double new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                double newDist = getDistanceFromDistribution(transitionTable, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                //double newDist=0;
                while(new_likelihood == 0 || new_likelihood == 1 || isnan(new_likelihood)){
                    new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                    newDist = getDistanceFromDistribution(transitionTable, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                }
                //dist = newDist;
                
                cout<<"New Likelihood: "<<new_likelihood<<", Current Likelihood: "<<likelihood<<", New Dist: "<<newDist<<", Current Dist: "<<dist<<endl;
                
                if(new_likelihood > likelihood && (newDist <= goalDistributionDistance || newDist < dist)){
                    likelihood = new_likelihood;
                    dist = newDist;
                } else{
                    TopGen.Map = OldMap;
                    cout<<"Is old map playable: "<<MarioPlayabilityPath(TopGen.Map)<<endl;
                }
                
                if(likelihood>= minLikelihood){
                    resamplingReason = 3;
                    cout<<"Moving onto raw distribution of Actions constraint"<<endl;
                }
                in.close();
            }
            
            while(resamplingReason == 3){
                
                
                cout<<"In third loop"<<endl;
                string line;
                ifstream in("IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                vector<pair<int, int> > trace;
                while(getline(in, line)){
                    stringstream s;
                    s.str(line);
                    string word;
                    pair<int, int> pos;
                    
                    getline(s, word, '\t');
                    pos.first = stoi(word);
                    getline(s, word, '\t');
                    pos.second = stoi(word);
                    
                    trace.push_back(pos);
                }
                
                
                //find section with highest distance
                double lowest_dist =999;
                double curr_likelihood=0;
                vector<int> mapDistribution = countMovements(actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                for(int c=3; c<210-16; c++){
                    ofstream tmp("IJCAI17_Experiments/VLR_Likelihood_2/SectionPath.txt");
                    int start=0;
                    int end=0;
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c){
                            start = k;
                            break;
                        }
                    }
                    for(int k=0; k<trace.size(); k++){
                        if(trace[k].second >= c+16){
                            end = k;
                            break;
                        }
                    }
                    
                    for(int k=start; k<=end; k++){
                        tmp<<trace[k].first<<"\t"<<trace[k].second<<endl;
                    }
                    tmp.close();
                    
                    
                    vector<int> secDistribution = countMovements(actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/SectionPath.txt");
                    
                    vector<double> tmpDistribution(secDistribution.size(), 0);
                    double sum =0;
                    for(int v=0; v<secDistribution.size(); v++){
                        tmpDistribution[v] = mapDistribution[v] - secDistribution[v];
                        sum += tmpDistribution[v];
                    }
                    for(int v=0; v<secDistribution.size(); v++){
                        tmpDistribution[v] = tmpDistribution[v]/sum;
                    }
                    
                    
                    
                    double tmpDist = getDistanceFromDistribution(transitionTable, actionDistribution, tmpDistribution);
                    //curr_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/SectionPath.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                    
                    //cout<<"Section "<<c<<" likelihood: "<<tmpLikelihood<<", current min likelihood: "<<lowest_likelihood<<endl;
                    if(tmpDist < lowest_dist){
                        lowest_dist = tmpDist;
                        pos = c;
                    }
                }
                cout<<pos<<" position, distance: "<<lowest_dist<<endl;
                //double dist = lowest_dist;
                
                if(pos >=0 && pos<209){
                    ret.clear();
                    ret.push_back(pos);
                    ret.push_back(0);
                    ret.push_back(pos+16);
                    ret.push_back(13);
                }
                
                //cout<<"position to resample is: "<<ret[0]<<endl;
                //keep the old map in case it is better
                vector< vector< char> > OldMap = TopGen.Map;
                
                
                //resample the offending section
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                
                while(!MarioPlayabilityPath("IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i))){
                    TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Likelihood_2/mario_" + to_string(i)+".txt", ret[0], ret[1], ret[2], ret[3]);
                    //cout<<"Regenerating a section for distribution distance"<<endl;
                }
                
                
                
                //determine likelihood of that level (either with states, moves, or both)
                double new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                double newDist = getDistanceFromDistribution(transitionTable, actionDistribution, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt");
                while(new_likelihood == 0 || new_likelihood == 1 || isnan(new_likelihood)){
                    
                    new_likelihood = getPathLikelihood(transitionTable, "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Path.txt", medoids,  "IJCAI17_Experiments/VLR_Likelihood_2/mario_"+to_string(i)+"_Annotated_Path.txt");
                }
                
                cout<<"New distance: "<<newDist<<", Old distance: "<<dist<<", New Likelihood: "<<new_likelihood<<", Old Likelihood: "<<likelihood<<endl;
                
                if(newDist < dist && new_likelihood >= minLikelihood){
                    likelihood = new_likelihood;
                    dist = newDist;
                } else{
                    TopGen.Map = OldMap;
                    cout<<"Is old map playable: "<<MarioPlayabilityPath(TopGen.Map)<<endl;
                }
                
                if(likelihood >= minLikelihood && likelihood <= maxLikelihood && dist <= goalDistributionDistance){
                    resamplingReason = 0;
                    cout<<"Map is done"<<endl;
                    break;
                }
            }
        }
    }
}


void ViolationLocationResampling(int numToGenerate){
    

    //train on the input maps
    Learner TopLevelLearner = Learner(14, 3, false, false);
    TopLevelLearner.FindAllTileTypes(4, "Mario_Maps/Playthrough/InputMaps/standard_", false);
    
    for(int i=3; i>=0; i--){
        TopLevelLearner.SetDepMatrix(i);
    }
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel("Mario_Maps/Playthrough/InputMaps/standard_", 4, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    //cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(210, 14);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    for(int i=0; i<numToGenerate; i++){
    
        //generate a level
        TopGen.GenerateTopLevel("IJCAI17_Experiments/VLR_Playability/mario_" + to_string(i) + ".txt");
        
        
        //test the playability constraint
        vector<int> ret;
        int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        if(pos == -1){
            cout<<"Playability returned -1. Exiting with error"<<endl;
            pos = 0;
        }
        if(pos >2 && pos<209){
            ret.push_back(max(pos-4, 0));
            ret.push_back(0);
            ret.push_back(min(pos+5, 209));
            ret.push_back(13);
        }
        
        
        //resample unplayable sections
        while(ret.size()>0){
            for (int r=0; r<ret.size(); r+=4){
                TopGen.RegenerateTopLevelSectionWithSmoothing("IJCAI17_Experiments/VLR_Playability/mario_"+to_string(i)+".txt", ret[r], ret[r+1], ret[r+2], ret[r+3]);
            }

            ret.clear();
            int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
            vector<int> ret1;
            if(pos == -1){
                cout<<"Playability returned -1. Exiting with error"<<endl;
                pos=0;;
            }
            if(pos >2 && pos<209){
                ret1.push_back(max(pos-4, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+5, 209));
                ret1.push_back(13);
            }
        }
    }
}


pair<double, float> MdMCExperiments(string trainingFolderName, string trainingMapName, string outputFolderName, string outputMapName, int rowSplits, int lookAhead, int networkStructure, int trainingMaps, int toGenerate, int height, int width, Learner model, int startingMap){
    
    Learner TopLevelLearner = Learner(rowSplits, lookAhead, false, false);
    TopLevelLearner.FindAllTileTypes(trainingMaps, trainingFolderName+trainingMapName, false);
    
    for(int i=networkStructure; i>=0; i--){
        TopLevelLearner.SetDepMatrix(i);
        //skip over the other dep atrices to get to the good one
        if(i == 6 || i == 7)
            i=4;
    }
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel(trainingFolderName+trainingMapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    //cout<<"Done Learning"<<endl;
    
    Generator TopGen = Generator(width, height);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    #if defined(_WIN32)
        _mkdir(outputFolderName);
    #else
        mkdir(outputFolderName.c_str(), 0777);
    #endif
    
    //cout<<"Starting to generate"<<endl;
    //cout<<outputFolderName+outputMapName<<endl;
    //clock_t start;
    //start = clock();
    int playable=0;
    //cout<<"Config: "<<rowSplits<<" "<<lookAhead<<": ";
    //ConstraintChecker t;
    
    pair<long double, float> ret;
    
    //chrono::time_point<std::chrono::system_clock> start, end, mapStart, mapEnd;
    //start = chrono::system_clock::now();
    cout<<outputFolderName+outputMapName<<endl;
    for (int i = 0+startingMap; i < toGenerate+startingMap; i++){
        TopGen.GenerateTopLevel(outputFolderName+outputMapName + to_string(i) + ".txt");
    //    pair<long double, float> tmp = MapLikelihood(TopGen.Map, model);
    //    ret.first+=tmp.first;
    //    ret.second+=tmp.second;
        if(MarioPlayabilityConstraint(TopGen.Map, 0, 0)){
            cout<<i<<endl;
            playable++;
        }
        //if(KidIcarusPlayabilityConstraintSection(TopGen.Map, 0, 0))
        //if(LoderunnerPlayabilityConstraint(TopGen.Map, 0, 0))
        //    playable++;
    }
    cout<<playable<<endl;
    //end = chrono::system_clock::now();
    //chrono::duration<double> tm = end - start;
    //cout << "Time taken in seconds to generate all levels: " << tm.count()<<endl;
    //cout<<playable<<endl;
    //ret.first/=100;
    //ret.second/=100;
    return ret;
    //cout << "Time in ms taken to generate all levels: " << (clock() - start) / (double)(CLOCKS_PER_SEC / 1000.0) << " ms" << endl;
    
}

void ClusterAndConvert(string lowLevelMapFolder, string lowLevelMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfClusters, float(*comparisonFunction) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), string comparisonFunctionName, bool removeDuplicates){
    
    Learner tileFinder = Learner(1, 0, true, false);
    tileFinder.FindAllTileTypes(numberOfTrainingMaps, lowLevelMapFolder+lowLevelMapName, false);
    cout<<tileFinder.TileTypes.size()<<endl;
    
    for(int i=0; i<tileFinder.TileTypes.size(); i++){
        cout<<tileFinder.TileTypes[i]<<endl;
    }
    
    //first we collect all the chunks of the training maps that are of the proper size
    MapReader Gatherer = MapReader();
    Gatherer.GatherAllBlocks(lowLevelMapFolder, lowLevelMapName, numberOfTrainingMaps, widthOfHighLevelTiles, heightOfHighLevelTiles);
    Clustering Clusterer = Clustering(widthOfHighLevelTiles, heightOfHighLevelTiles);
    
    //we then remove all the duplicate chunks, so as to speed up clustering
    string dupRemoved="";
    if(removeDuplicates){
        Clusterer.RemoveDuplicates(lowLevelMapFolder,to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+"_Blocks.txt");
        dupRemoved = "Duplicates_Removed_";
    }
    //next, compute the distances between each of the chunks using the desired comparisonFunction
    Clusterer.ComputeDifferences(lowLevelMapFolder+dupRemoved+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+"_Blocks.txt", lowLevelMapFolder+comparisonFunctionName+"_Differences_"+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+".csv", comparisonFunction, vector<vector<float>>(), tileFinder.TileTypes);
    
    
    //Perform k-Medoids Clustering on the collected chunks, using the computed distance matrix
    string DifferenceFile = lowLevelMapFolder+comparisonFunctionName+"_Differences_"+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+".csv";
    Clusterer.kMedoids(DifferenceFile, numberOfClusters);
    Clusterer.PrintMedoids(lowLevelMapFolder+dupRemoved+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+"_Blocks.txt", lowLevelMapFolder+comparisonFunctionName+"_Medoids_"+to_string(numberOfClusters)+"_"+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+".txt");
    
    //convert the low-level training maps into high-level training maps using the found cluster medoids
    vector<vector<vector<char> > > Medoids;
    
    //read in the medoids (high-level tiles)
    ifstream Medoid_File;
    string line;
    Medoid_File.open(lowLevelMapFolder+comparisonFunctionName+"_Medoids_"+to_string(numberOfClusters)+"_"+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+".txt");
    
    while(!getline(Medoid_File, line).eof()){
        vector<vector< char > > Block;
        for(int h = 0; h < heightOfHighLevelTiles; h++){
            getline(Medoid_File, line);
            vector< char > temp;
            for(int w=0; w<widthOfHighLevelTiles; w++)
            temp.push_back(line.at(w));
            
            Block.push_back(temp);
        }
        getline(Medoid_File, line);
        Medoids.push_back(Block);
    }
    
    //output maps folder
    string OutputFolder = lowLevelMapFolder+to_string(numberOfClusters)+"-"+to_string(widthOfHighLevelTiles)+"X"+to_string(heightOfHighLevelTiles)+"-"+comparisonFunctionName;
    
#if defined(_WIN32)
    _mkdir(OutputFolder.c_str());
#else
    mkdir(OutputFolder.c_str(), 0777);
#endif
    
    //convert the low-level maps into high-level maps, and put them in the newly created subfolder
    MapReader Converter = MapReader();
    Converter.ConvertAllTileMapsToHier(lowLevelMapName, lowLevelMapFolder, OutputFolder+"/", numberOfTrainingMaps, widthOfHighLevelTiles, heightOfHighLevelTiles, Medoids, comparisonFunction, comparisonFunctionName, vector<vector<float>>(), tileFinder.TileTypes);
    Medoid_File.close();
    Medoids.clear();
}

void ManualConversion(string inputMapFolder, string outputMapFolder, string mapName, int numberOfMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles){
    
#if defined(_WIN32)
    _mkdir((inputMapFolder+outputMapFolder).c_str());
#else
    mkdir((inputMapFolder+outputMapFolder).c_str(), 0777);
#endif
    
    MapReader Converter;
    cout<<inputMapFolder+mapName<<" "<<inputMapFolder+outputMapFolder+mapName<<endl;
    Converter.ConvertAllTileMapsToHier(inputMapFolder+mapName, inputMapFolder+outputMapFolder+mapName, numberOfMaps, widthOfHighLevelTiles, heightOfHighLevelTiles);
}


void TrainAndSample(string trainingMapFolder, string highLevelMapFolder, string mapName, string outputMapFolder, string highLevelOutputMapName, string lowLevelOutputMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfOutputMaps, int widthOfHighLevelOutputMap, int heightOfHighLevelOutputMap, int highLevelDependencyMatrix, int lowLevelDependencyMatrix, int topLevelRowSplits, int highLevelLookAhead, int lowLevelLookAhead, bool manualConversion){
    
    //initialize the high-level training model
    Learner TopLevelLearner = Learner(topLevelRowSplits, highLevelLookAhead, false, true);
    TopLevelLearner.Config.width = widthOfHighLevelTiles;
    TopLevelLearner.Config.height = heightOfHighLevelTiles;
    
    //find all the high-level tie types from the high-level training maps
    TopLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+highLevelMapFolder+mapName, true);
    
    cout<<trainingMapFolder+highLevelMapFolder+mapName<<endl;
    cout<<"Number of tile types: "<<TopLevelLearner.HierTileTypes.size()<<endl;
    
    
    //pushes the chosen dependency matrix as well as the subsequent fallback dependency matrices into the model for use
    for(int depMatrix = highLevelDependencyMatrix; depMatrix>=0; depMatrix--)
        TopLevelLearner.SetDepMatrix(depMatrix);
    
    //sets up the number of rows each probability table table will need, and resizes them accordingly
    TopLevelLearner.SetRows(true);
    TopLevelLearner.ResizeVectors(false);
    
    //for each dependency matrix, intialize the files used to store the porbabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", true, i);
    
    cout<<"Hight level tables initialized"<<endl;
    
    //train a separate probability table for each dependency matrix
    //performs the counting
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++){
        TopLevelLearner.SetTotalsTopLevel(trainingMapFolder + highLevelMapFolder + mapName, numberOfTrainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", true, dep);
    }
    
    cout<<"High level totals counted"<<endl;
    
    //sets the probabilities according to the counts
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbs("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", true, dep);
    
    
    cout<<"Done training at the high level"<<endl;
    
    //initialize the low-level training model
    Learner BottomLevelLearner = Learner(1, lowLevelLookAhead, false, false);
    BottomLevelLearner.Config.width = widthOfHighLevelTiles;
    BottomLevelLearner.Config.height = heightOfHighLevelTiles;
    
    //find all the high-level tile types, and low-level tile types
    BottomLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+highLevelMapFolder+mapName, true);
    BottomLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+mapName, false);
    
    
    //set the dependency matrix and the fallback dependency matrices
    for(int depMatrix = lowLevelDependencyMatrix; depMatrix>=0; depMatrix--)
        BottomLevelLearner.SetDepMatrix(depMatrix);
    
    //determine the number of rows each probability table needs, and resize them accordingly
    BottomLevelLearner.SetRows(false);
    BottomLevelLearner.ResizeVectors(true);
    
    
    
    //initialize the files used for storing the totals and probabilities for the low-level model
    for (int i = 0; i < BottomLevelLearner.Config.NumberMatrices; i++)
        BottomLevelLearner.InitFiles("", "LowLevel_Totals" + to_string(i) + ".txt", "", "LowLevel_Probabilities" + to_string(i) + ".txt", false, i);
    
    cout<<"Low level tables initialized"<<endl;
    
    //train a separate model for each high-level tile type, and for each dependency matrix
    for (int dep = 0; dep < BottomLevelLearner.Config.NumberMatrices; dep++){
        for (int i = 1; i <=numberOfTrainingMaps; i++){
            cout<<"\t"<<trainingMapFolder+highLevelMapFolder+mapName+to_string(i) + ".txt"<<" "<<trainingMapFolder+mapName+to_string(i) + ".txt"<<endl;
            BottomLevelLearner.SetTotalsBottomLevel(trainingMapFolder+highLevelMapFolder+mapName+to_string(i) + ".txt", trainingMapFolder+mapName+to_string(i) + ".txt", "LowLevel_Totals"+ to_string(dep) + ".txt", dep, manualConversion);
                    }
        cout<<"Totals counted for dep: "<<dep<<endl;
    }
    
    cout<<"Low level totals counted"<<endl;
    
    //set the probabilities according to the observed totals
    for (int dep = 0; dep < BottomLevelLearner.Config.NumberMatrices; dep++)
        BottomLevelLearner.SetProbs("LowLevel_Probabilities"+ to_string(dep) +".txt", false, dep);
    
    cout<<"Done training at the low-level"<<endl;

    
    //initialize the high-level map generator
    Generator TopGen = Generator(widthOfHighLevelOutputMap, heightOfHighLevelOutputMap);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.HierTileTypes, false);
    TopGen.ResizeVectors(false);
    
    //copy the probabilities from the probability file
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    //initialize the low-level map generator
    Generator BottomGen = Generator(widthOfHighLevelOutputMap*widthOfHighLevelTiles, heightOfHighLevelOutputMap*heightOfHighLevelTiles);
    BottomGen.CopyConfig(BottomLevelLearner.Config);
    BottomGen.SetTileTypes(BottomLevelLearner.TileTypes, false);
    BottomGen.SetTileTypes(BottomLevelLearner.HierTileTypes, true);
    BottomGen.ResizeVectors(true);
    BottomGen.Config.NumberMatrices = BottomLevelLearner.Config.NumberMatrices;
    
    for (int dep = 0; dep < BottomGen.Config.NumberMatrices; dep++)
        BottomGen.CopyProbs("LowLevel_Probabilities"+ to_string(dep) +".txt", true, dep);
    
    //crate the directory to store the maps if one does not already exist
    #if defined(_WIN32)
        _mkdir(outputMapFolder.c_str());
    #else
        mkdir(outputMapFolder.c_str(), 0777);
    #endif
    
    clock_t    start;
    start = clock();
    for (int i = 0; i < numberOfOutputMaps; i++)
        TopGen.GenerateTopLevel(outputMapFolder+highLevelOutputMapName+to_string(i)+".txt");
    
    int playability=0;
    for (int i = 0; i < numberOfOutputMaps; i++){
        BottomGen.GenerateBottomLevel(outputMapFolder+lowLevelOutputMapName + to_string(i) + ".txt",
                                  outputMapFolder + highLevelOutputMapName + to_string(i) + ".txt", manualConversion);
        //if(MarioPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
        //if(LoderunnerPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
        //if(KidIcarusPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
    }
    //cout<<playability<<endl;
}

void TrainAndSampleHighLevelProvided(string trainingMapFolder, string highLevelMapFolder, string mapName, string outputMapFolder, string highLevelOutputMapName, string lowLevelOutputMapName, int numberOfTrainingMaps, int widthOfHighLevelTiles, int heightOfHighLevelTiles, int numberOfOutputMaps, int widthOfHighLevelOutputMap, int heightOfHighLevelOutputMap, int highLevelDependencyMatrix, int lowLevelDependencyMatrix, int topLevelRowSplits, int highLevelLookAhead, int lowLevelLookAhead, bool manualConversion){
    
    //initialize the high-level training model
    Learner TopLevelLearner = Learner(topLevelRowSplits, highLevelLookAhead, false, true);
    TopLevelLearner.Config.width = widthOfHighLevelTiles;
    TopLevelLearner.Config.height = heightOfHighLevelTiles;
    
    //find all the high-level tie types from the high-level training maps
    TopLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+highLevelMapFolder+mapName, true);
    
    cout<<trainingMapFolder+highLevelMapFolder+mapName<<endl;
    cout<<"Number of tile types: "<<TopLevelLearner.HierTileTypes.size()<<endl;
    
    
    //pushes the chosen dependency matrix as well as the subsequent fallback dependency matrices into the model for use
    for(int depMatrix = highLevelDependencyMatrix; depMatrix>=0; depMatrix--)
        TopLevelLearner.SetDepMatrix(depMatrix);
    
    //sets up the number of rows each probability table table will need, and resizes them accordingly
    TopLevelLearner.SetRows(true);
    TopLevelLearner.ResizeVectors(false);
    
    //for each dependency matrix, intialize the files used to store the porbabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", true, i);
    
    cout<<"Hight level tables initialized"<<endl;
    
    //train a separate probability table for each dependency matrix
    //performs the counting
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++){
        TopLevelLearner.SetTotalsTopLevel(trainingMapFolder + highLevelMapFolder + mapName, numberOfTrainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", true, dep);
    }
    
    cout<<"High level totals counted"<<endl;
    
    //sets the probabilities according to the counts
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbs("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", true, dep);
    
    
    cout<<"Done training at the high level"<<endl;
    
    //initialize the low-level training model
    Learner BottomLevelLearner = Learner(1, lowLevelLookAhead, false, false);
    BottomLevelLearner.Config.width = widthOfHighLevelTiles;
    BottomLevelLearner.Config.height = heightOfHighLevelTiles;
    
    //find all the high-level tile types, and low-level tile types
    BottomLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+highLevelMapFolder+mapName, true);
    BottomLevelLearner.FindAllTileTypes(numberOfTrainingMaps, trainingMapFolder+mapName, false);
    
    
    //set the dependency matrix and the fallback dependency matrices
    for(int depMatrix = lowLevelDependencyMatrix; depMatrix>=0; depMatrix--)
        BottomLevelLearner.SetDepMatrix(depMatrix);
    
    //determine the number of rows each probability table needs, and resize them accordingly
    BottomLevelLearner.SetRows(false);
    BottomLevelLearner.ResizeVectors(true);
    
    
    
    //initialize the files used for storing the totals and probabilities for the low-level model
    for (int i = 0; i < BottomLevelLearner.Config.NumberMatrices; i++)
        BottomLevelLearner.InitFiles("", "LowLevel_Totals" + to_string(i) + ".txt", "", "LowLevel_Probabilities" + to_string(i) + ".txt", false, i);
    
    cout<<"Low level tables initialized"<<endl;
    
    //train a separate model for each high-level tile type, and for each dependency matrix
    for (int dep = 0; dep < BottomLevelLearner.Config.NumberMatrices; dep++){
        for (int i = 1; i <=numberOfTrainingMaps; i++){
            cout<<"\t"<<trainingMapFolder+highLevelMapFolder+mapName+to_string(i) + ".txt"<<" "<<trainingMapFolder+mapName+to_string(i) + ".txt"<<endl;
            BottomLevelLearner.SetTotalsBottomLevel(trainingMapFolder+highLevelMapFolder+mapName+to_string(i) + ".txt", trainingMapFolder+mapName+to_string(i) + ".txt", "LowLevel_Totals"+ to_string(dep) + ".txt", dep, manualConversion);
        }
        cout<<"Totals counted for dep: "<<dep<<endl;
    }
    
    cout<<"Low level totals counted"<<endl;
    
    //set the probabilities according to the observed totals
    for (int dep = 0; dep < BottomLevelLearner.Config.NumberMatrices; dep++)
        BottomLevelLearner.SetProbs("LowLevel_Probabilities"+ to_string(dep) +".txt", false, dep);
    
    cout<<"Done training at the low-level"<<endl;
    
    
    //initialize the high-level map generator
    //Generator TopGen = Generator(widthOfHighLevelOutputMap, heightOfHighLevelOutputMap);
    //TopGen.CopyConfig(TopLevelLearner.Config);
    //TopGen.SetTileTypes(TopLevelLearner.HierTileTypes, false);
    //TopGen.ResizeVectors(false);
    
    //copy the probabilities from the probability file
    //for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
    //    TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    //initialize the low-level map generator
    Generator BottomGen = Generator(widthOfHighLevelOutputMap*widthOfHighLevelTiles, heightOfHighLevelOutputMap*heightOfHighLevelTiles);
    BottomGen.CopyConfig(BottomLevelLearner.Config);
    BottomGen.SetTileTypes(BottomLevelLearner.TileTypes, false);
    BottomGen.SetTileTypes(BottomLevelLearner.HierTileTypes, true);
    BottomGen.ResizeVectors(true);
    BottomGen.Config.NumberMatrices = BottomLevelLearner.Config.NumberMatrices;
    
    for (int dep = 0; dep < BottomGen.Config.NumberMatrices; dep++)
        BottomGen.CopyProbs("LowLevel_Probabilities"+ to_string(dep) +".txt", true, dep);
    
    //crate the directory to store the maps if one does not already exist
#if defined(_WIN32)
    _mkdir(outputMapFolder.c_str());
#else
    mkdir(outputMapFolder.c_str(), 0777);
#endif
    
    for (int i = 0; i < numberOfOutputMaps; i++){
        BottomGen.GenerateBottomLevel(outputMapFolder+lowLevelOutputMapName + to_string(i) + ".txt",
                                      outputMapFolder + highLevelOutputMapName + to_string(i) + ".txt", manualConversion);
        //if(MarioPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
        //if(LoderunnerPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
        //if(KidIcarusPlayabilityConstraint(BottomGen.Map, 0, 0))
        //    playability++;
    }
    //cout<<playability<<endl;
}



int CountPlayers(string FileName){
    
    ifstream Input(FileName.c_str());
    string line;
    int NumPlayers =0;
    while(!getline(Input, line).eof()){
        for(int i=0; i<line.length(); i++){
            if(line.at(i) == 'M')
            NumPlayers++;
        }
    }
    return NumPlayers;
}

bool IsGroundMisplaced(string MapName){
    
    ifstream Input;
    Input.open(MapName.c_str());
    
    vector< vector< char> > Map;
    vector<char> tmp;
    string line = "";
    while (getline(Input, line)){
        for (int i = 0; i < line.size(); i++){
            tmp.push_back(line.at(i));
        }
        Map.push_back(tmp);
        tmp.clear();
    }
    for (int i = 0; i < Map.size(); i++){
        for (int j = 0; j < Map.at(0).size(); j++){
            if ((Map[i][j] == 'G' || Map[i][j] == 'A') && i != Map.size() - 1)
            return true;
        }
    }
    
    Input.close();
    return false;
}

int FindBadPipes(string filename){
    ifstream map;
    map.open(filename.c_str());
    string line1;
    string line2;
    
    char curr;
    char right;
    char under;
    char left;
    map>>line1;
    int BadPipes=0;
    for(int y=0; y<13; y++){
        map>>line2;
        curr = 'X';
        right= 'X';
        under= 'X';
        left = 'X';
        for(int x=0; x<210; x++){
            if(line1.at(x) == 'p' || line1.at(x) == 'P'){
                curr = line1.at(x);
                if(y<13)
                under = line2.at(x);
                if(x<209)
                right = line1.at(x+1);
                if(x>0)
                left = line1.at(x-1);
                
                //by putting all the bad options in one if statement, if a pipe is bad in multiple ways, it only gets counted once
                if((curr == 'p' && x < 209 && right != 'P') ||					//right pipe did not follow left pipe
                   (curr == 'p' && y < 13 && under != 'p' && under != '#') ||	//left pipe is floating, or on invalid piece
                   (curr == 'P' && y < 13 && under != 'P' && under != '#') ||	//right pipe is floating or on invalid piece
                   (curr == 'P' && x > 0  && left  != 'p')					||	//left pipe did not precede right pipe
                   (y < 13 && (under == 'p' || under == 'P') && curr != '-' && curr != under)) //nonempty tile on top of pipe
                BadPipes++;
            }
            else
            continue;
        }
        line1 =line2;
    }
    
    return BadPipes;
}

void NewMRFexperiments(int trainingMaps, int height, int width, int iterations, int toGenerate, string inputFileBase, string outputFolder){
    
    //MRF experiments
    
    //no smoothing
    //standard, complexity 4
    /*MRF model = MRF(1);
     model.SetDependencyMatrix(0);
     */
    
    //smoothing
    //standard, complexity 4
    MRF model = MRF(1);
    
    //fallback, complexity 2
    /*model.SetDependencyMatrix(6);
     model.SetDependencyMatrix(7);
     
     //fallback, complexity 1
     model.SetDependencyMatrix(8);
     model.SetDependencyMatrix(9);
     model.SetDependencyMatrix(10);
     model.SetDependencyMatrix(11);
     */
    //fallback, complexity 0
    model.SetDependencyMatrix(0);
    
    //end smoothing
    
    
    
    /*cout<<"======"<<endl;
     cout<<model.Dependency_Matrices.size()<<endl;
     cout<<model.Dependency_Complexity[0]<<" "<<model.Dependency_Complexity[1]<<endl;
     cout<<"Dependency matrix: "<<endl;
     for(int i=0; i<model.Dependency_Matrices[0].size(); i++){
     for(int j=0; j<model.Dependency_Matrices[0][i].size(); j++){
     cout<<model.Dependency_Matrices[0][i][j]<<" ";
     }
     cout<<endl;
     }
     cout<<endl;
     cout<<"Complexity: "<<model.Dependency_Complexity[0]<<endl;
     cout<<endl;
     */
    model.FindAllTileTypes(inputFileBase, trainingMaps);
    
    /*for(int i=0; i<model.TileTypes.size(); i++)
     cout<<model.TileTypes[i]<<" ";
     cout<<endl;
     */
    model.InitializeDistributions();
    
    for(int i=0; i<model.Dependency_Matrices.size(); i++)
        model.SetAllTotals(inputFileBase, i, trainingMaps);
    
    //smoothing
    /*vector<vector<vector< int >>> FallbackMatrices;
     vector<vector<vector< Totals_Pair >>> FallbackTotals;
     for(int i=1; i<model.Dependency_Matrices.size(); i++){
     FallbackMatrices.push_back(model.Dependency_Matrices[i]);
     FallbackTotals.push_back(model.Totals[i]);
     }
     
     model.SetProbabilities((int)model.Dependency_Matrices.size()-1);
     model.Probabilities[0] = model.SmoothAll();
     //end smoothing
     */
    
    //no smoothing
    //set the probabilities of the model, as well as the raw distribution for the initial sample
    model.SetProbabilities(0);
    model.SetProbabilities(1);
    
    /*for(int i=1; i<model.Probabilities.back()[0].size(); i++){
     Probabilities_Pair tmp;
     tmp.type = model.Probabilities.back()[0][i].type;
     }*/
    
    /*for(int i=0; i<model.Probabilities[0].size(); i++){
     for(int j=0; j<model.Probabilities[0][i].size(); j++){
     cout<<model.Probabilities[0][i][j].probability<<" ";
     }
     cout<<endl;
     }*/
    
    
    
    string path = "/Users/sps74/Library/Developer/Xcode/DerivedData/PCG-bfnwolkqcayvxyfqfcmzwfuvtoxc/Build/Products/Debug/";
    
    
    clock_t    start;
    start = clock();
    for(int i=0; i<toGenerate; i++){
        cout<<i<<endl;
        model.GenerateLevel(outputFolder+"Sample "+to_string(i)+".txt", width, height, iterations, 0);
    }
    cout << (clock() - start) / (double)(CLOCKS_PER_SEC / 1000.0) <<endl;
}

//Computes Linearity/Leniency and puts points for a heatmap into a dat file
void ComputeLinVSLen(string folder, string CSVfileName, string model, string mapNameBase, int maps){
    //need to modify to keep track of the global mins and maxes
    vector<double> RawLinearity;
    vector<double> RawLeniency;
    
    float minLin=0.687747;
    float maxLin=4.2421;
    float minLen=11;
    float maxLen=153;
    
    for(int i=1; i<=maps; i++){
        RawLinearity.push_back(ComputeLinearity(folder+mapNameBase+to_string(i)+".txt", folder+"Points.txt"));
        RawLeniency.push_back(ComputeLeniency(folder+mapNameBase+to_string(i)+".txt"));
        if(RawLinearity.back() < minLin)
            minLin = RawLinearity.back();
        if(RawLinearity.back() > maxLin)
            maxLin = RawLinearity.back();
        
        if(RawLeniency.back() < minLen)
            minLen = RawLeniency.back();
        if(RawLeniency.back() > maxLen)
            maxLen = RawLeniency.back();
        
    }
    
    cout<<minLin<<" "<<maxLin<<" "<<minLen<<" "<<maxLen<<endl;
    
    ofstream Output(CSVfileName+".csv");
    for(int i=0; i<RawLinearity.size(); i++){
        Output<<((RawLinearity[i]-minLin)/(maxLin-minLin))<<",";
        Output<<((RawLeniency[i]-minLen)/(maxLen-minLen))<<endl;
    }
    Output.close();
    
    
    ifstream Input(CSVfileName+".csv");
    vector< vector<int> > HeatmapHits;
    float section = .02;
    HeatmapHits.resize(1.0/section+1);
    for(int i=0; i<HeatmapHits.size(); i++)
    HeatmapHits[i].resize(1.0/section+1, 0);
    
    string line;
    int k=0;
    while(getline(Input, line, ',')){
        k++;
        double lin = stod(line);
        getline(Input, line);
        double len = stod(line);
        
        int lin_index=-1;
        int len_index=-1;
        for(int i=0; i<HeatmapHits.size()-1; i++){
            if(lin>=i*section && lin<(i+1)*section)
            lin_index = i;
            if(len>=i*section && len<(i+1)*section)
            len_index = i;
        }
        if(lin_index == -1)
            lin_index = (int)HeatmapHits.size()-1;
        if(len_index == -1)
            len_index = (int)HeatmapHits.size()-1;
        HeatmapHits[lin_index][len_index]++;
    }
    
    Input.close();
    ofstream nOutput(model+"HeatmapPoints.dat");
    for(int i=0; i<HeatmapHits.size(); i++){
        for(int j=0; j<HeatmapHits[i].size(); j++){
            nOutput<<i*section<<" "<<j*section<<" "<<HeatmapHits[i][j]<<endl;
        }
        nOutput<<endl;
    }
    nOutput.close();
    
}

double ComputeLinearity(string MapFile, string PointsFile){
    
    ifstream input(MapFile.c_str());
    ofstream output(PointsFile.c_str());
    
    //read in the map
    vector<vector<char>> Map;
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            row.push_back(line.at(i));
        }
        Map.push_back(row);
    }

    vector<int> xVals;
    vector<int> yVals;
    
    for(int j=0; j<Map[0].size(); j++){
        for(int i=(int)Map.size()-1; i>=0; i--){
            
            if(Map[i][j] == '-' && i<(Map.size()-1) && Map[i+1][j] != '-'){
                xVals.push_back(j);
                yVals.push_back((int)Map.size()-1-i);
                output<<j<<","<<Map.size()-1-i<<endl;
            }
        }
    }
    output.close();
    
    //create arrays of doubles for the linear regression function
    double *xArray = new double[xVals.size()];
    double *yArray = new double[yVals.size()];
    
    for(int i=0; i<xVals.size(); i++){
        xArray[i] = xVals[i];
        yArray[i] = yVals[i];
    }
    
    double c0, c1, cov00, cov01, cov11, sumsq;
    
    gsl_fit_linear(xArray, 1, yArray, 1, xVals.size(), &c0, &c1, &cov00, &cov01, &cov11, &sumsq);
    
    double dist=0;
    for(int i=0; i<xVals.size(); i++){
        dist+=abs(yVals[i] - (c1*xVals[i]+c0));
    }
    
    //scale by length of level
    return dist/Map[0].size();
    
}

bool isEnemy(char t){
    if(t =='e' || t == 'g' || t == 'k' || t == 'K' || t == 't' || t == 'l' || t == 'C' || t == 'h' || t == '!')
        return true;
    else
        return false;
}

bool isPowerup(char t){
    if(t =='M' || t == '+' || t == '*')
        return true;
    else
        return false;
}

double ComputeLeniency(string MapFile){
    
    ifstream input(MapFile.c_str());
    
    //read in the map
    vector<vector<char>> Map;
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            row.push_back(line.at(i));
        }
        Map.push_back(row);
    }
  
    int prevHeight=0;
    float Leniency=0;
    for(int w=0; w<Map[0].size()-1; w++){
        
        if(Map[Map.size()-1][w] == '-' && Map[Map.size()-2][w] == '-' && Map[Map.size()-3][w] == '-')
            Leniency++;
    }
    for(int y=0; y<Map.size(); y++){
        for(int x=0; x<Map[y].size(); x++){
            if(isEnemy(Map[y][x]))
               Leniency+=.5;
            else if(isPowerup(Map[y][x]))
                Leniency-=.5;
        }
    }
    
    //normalize by length of map
    float scaledLeniency = (float)Leniency/(/*Map.size()**/Map[0].size());
    
    return Leniency;
}

pair<long double, double> MapLikelihood(vector<vector<char> > Map, Learner& model){
    long double likelihood=0;
    int unseenStates=0;
    
    int rowsPerSplit = Map.size()/model.Config.Rowsplits;
    
    
    for (int h=(int)Map.size()-1; h>=0; h--){
        int currRow = h/rowsPerSplit;
        if(currRow >= model.Config.Rowsplits)
            currRow = model.Config.Rowsplits-1;
        
        for (int w = 0; w < Map[h].size(); w++){
            
            int ID = 0;
            int id = 0;
            int P = 0;
            for (int i = 0; i<model.Config.DependencyMatrices[0].size(); i++){
                for (int j =0; j <model.Config.DependencyMatrices[0][i].size(); j++){
                    if (model.Config.DependencyMatrices[0][i][j] == 1){
                        
                        if (((h + i - 2) < 0) || ((h + i - 2) >= Map.size()) || ((w + j - 2) < 0)){
                            id=0;
                        }
                        else{
                            for (int m = 0; m < model.TileTypes.size(); m++){
                                if (model.TileTypes.at(m) == Map[h + i - 2][w + j - 2]){
                                    id = m;
                                    break;
                                }
                            }
                        }
                        ID += id*pow(model.TileTypes.size(), P);
                        P++;
                    }
                }
            }
            
            for (int m = 0; m < model.TileTypes.size(); m++){
                if (model.TileTypes.at(m) == Map[h][w]){
                    id = m;
                    break;
                }
            }
            
            //float t=0;
            //for(int q=0; q<model.Totals[0][ID+model.Config.Rows[0]*currRow].size(); q++)
            //    t+=model.Totals[0][ID+model.Config.Rows[0]*currRow][q];
            //cout<<q<<" "<<ID<<" "<<model.Config.Rows[q]*currRow<<" "<<id<<endl;
            if(model.Probabilities[0][ID+model.Config.Rows[0]*currRow][id] == 0){
                unseenStates++;
            }
            //if(t==0)
            //    unseenStates++;
            else{
                likelihood += log10(model.Probabilities[0][ID+model.Config.Rows[0]*currRow][id]);
                //likelihood +=model.Probabilities[0][ID+model.Config.Rows[0]*currRow][id];
            }
        }
    }
    //likelihood= (likelihood)/(Map.size()*Map[0].size());
    return pair<long double, double>(likelihood/Map[0].size(), (double)unseenStates/Map[0].size());
}

cimg_library::CImg<float> ConvertMarioMapToImage(int width, int height, string mapName, string imageName, string archetype){
    
    //first, read in the map to be translated
    ifstream input(mapName);
    vector< vector< char> > map;
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i]!= '\0' && line[i]!= '\n' && line[i]!= '\t' &&
               line[i]!= '\r'){
                row.push_back(line[i]);
            }
        }
        map.push_back(row);
        row.clear();
    }
    
    //initialize an image of the proper size
    cimg_library::CImg<float> ret(16*width, 16*height, 1, 3);
    
    //then, for each tile in the map, set theappropriate pixel values in the output image
    for(int r=height-1; r>=0; r--){
        for(int c=0; c<width; c++){
            
            cimg_library::CImg<float> tile;
            
            
            if(archetype == "standard"){
                if(map[r][c] == '-'){
                    tile = cimg_library::CImg<float>("MarioTiles/Empty.bmp");
                } else if(map[r][c] == '#' && r== height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/Solid.bmp");
                } else if(map[r][c] == '#' && r<height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/Solid2.bmp");
                } else if(map[r][c] == 'B'){
                    tile = cimg_library::CImg<float>("MarioTiles/Block.bmp");
                } else if(map[r][c] == '?'){
                    tile = cimg_library::CImg<float>("MarioTiles/QuestionBlock.bmp");
                } else if(map[r][c] == '{'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownLeft.bmp");
                } else if(map[r][c] == '}'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownRight.bmp");
                } else if(map[r][c] == ']'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeTopRight.bmp");
                } else if(map[r][c] == '['){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopLeft.bmp");
                } else if(map[r][c] == 'p' || map[r][c] == 'd'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeLeft.bmp");
                } else if(map[r][c] == 'P' || map[r][c] == 'D'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeRight.bmp");
                } else if(map[r][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonTop.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonMid.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'c'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonBot.bmp");
                } else if(map[r][c] == 'g'){
                    tile = cimg_library::CImg<float>("MarioTiles/Goomba.bmp");
                } else if(map[r][c] == 'k'){
                    tile = cimg_library::CImg<float>("MarioTiles/Koopa.bmp");
                } else if(map[r][c] == 'K'){
                    tile = cimg_library::CImg<float>("MarioTiles/ParaKoopa.bmp");
                } else if(map[r][c] == 't'){
                    tile = cimg_library::CImg<float>("MarioTiles/Turtle.bmp");
                } else if(map[r][c] == 'l'){
                    tile = cimg_library::CImg<float>("MarioTiles/Lakitu.bmp");
                } else if(map[r][c] == 'h'){
                    tile = cimg_library::CImg<float>("MarioTiles/HammerBro.bmp");
                } else if(map[r][c] == 'V'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaUp.bmp");
                } else if(map[r][c] == 'X'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaDown.bmp");
                } else if(map[r][c] == 'o'){
                    tile = cimg_library::CImg<float>("MarioTiles/Coin.bmp");
                } else if(map[r][c] == 'O'){
                    tile = cimg_library::CImg<float>("MarioTiles/CoinBlock.bmp");
                } else if(map[r][c] == 'M'){
                    tile = cimg_library::CImg<float>("MarioTiles/PowerUp.bmp");
                } else if(map[r][c] == '+'){
                    tile = cimg_library::CImg<float>("MarioTiles/ExtraLife.bmp");
                } else if(map[r][c] == '*'){
                    tile = cimg_library::CImg<float>("MarioTiles/Star.bmp");
                } else if(map[r][c] == '<' || map[r][c] == '>' || map[r][c] == 'v' || map[r][c] == '^'){
                    tile = cimg_library::CImg<float>("MarioTiles/Platform.bmp");
                } else if(map[r][c] == 'f'){
                    tile = cimg_library::CImg<float>("MarioTiles/Fire.bmp");
                } else if(map[r][c] == 'Y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringTop.bmp");
                } else if(map[r][c] == 'y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringBot.bmp");
                } else if(map[r][c] == 'H'){
                    tile = cimg_library::CImg<float>("MarioTiles/Vine.bmp");
                } else if(map[r][c] == '|' && (r-1) >= 0 && map[r-1][c] != '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/PoleTop.bmp");
                } else if(map[r][c] == '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/PoleBot.bmp");
                } else if(map[r][c] == 'z' && (c-1) >= 0 && map[r][c-1] != 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop1.bmp");
                } else if(map[r][c] == 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop2.bmp");
                } else if(map[r][c] == '=' && (c-1) >= 0 && map[r][c-1] != '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot1.bmp");
                } else if(map[r][c] == '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot2.bmp");
                } else if(map[r][c] == 'x'){
                    tile = cimg_library::CImg<float>("MarioTiles/smallMario.bmp");
                } else if(map[r][c] == '!'){
                    tile = cimg_library::CImg<float>("MarioTiles/PoisonMushroom.bmp");
                }
            } else if(archetype == "underground"){
                if(map[r][c] == '-'){
                    tile = cimg_library::CImg<float>("MarioTiles/Black.bmp");
                } else if(map[r][c] == '#' && r== height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/SolidBlack.bmp");
                } else if(map[r][c] == '#' && r<height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/SolidBlack2.bmp");
                } else if(map[r][c] == 'B'){
                    tile = cimg_library::CImg<float>("MarioTiles/BrickBlack.bmp");
                } else if(map[r][c] == '?'){
                    tile = cimg_library::CImg<float>("MarioTiles/QuestionBlock.bmp");
                } else if(map[r][c] == '{'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownLeft.bmp");
                } else if(map[r][c] == '}'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownRight.bmp");
                } else if(map[r][c] == ']'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopRight.bmp");
                } else if(map[r][c] == '['){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopLeft.bmp");
                } else if(map[r][c] == 'p' || map[r][c] == 'd'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeLeft.bmp");
                } else if(map[r][c] == 'P' || map[r][c] == 'D'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeRight.bmp");
                } else if(map[r][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonTop.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonMid.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'c'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonBot.bmp");
                } else if(map[r][c] == 'g'){
                    tile = cimg_library::CImg<float>("MarioTiles/GoombaBlack.bmp");
                } else if(map[r][c] == 'k'){
                    tile = cimg_library::CImg<float>("MarioTiles/KoopaBlack.bmp");
                } else if(map[r][c] == 'K'){
                    tile = cimg_library::CImg<float>("MarioTiles/ParaKoopaBlack.bmp");
                } else if(map[r][c] == 't'){
                    tile = cimg_library::CImg<float>("MarioTiles/TurtleBlack.bmp");
                } else if(map[r][c] == 'l'){
                    tile = cimg_library::CImg<float>("MarioTiles/Lakitu.bmp");
                } else if(map[r][c] == 'h'){
                    tile = cimg_library::CImg<float>("MarioTiles/HammerBroBlack.bmp");
                } else if(map[r][c] == 'V'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaUpBlack.bmp");
                } else if(map[r][c] == 'X'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaDownBlack.bmp");
                } else if(map[r][c] == 'o'){
                    tile = cimg_library::CImg<float>("MarioTiles/CoinBlack.bmp");
                } else if(map[r][c] == 'O'){
                    tile = cimg_library::CImg<float>("MarioTiles/CoinBlock.bmp");
                } else if(map[r][c] == 'M'){
                    tile = cimg_library::CImg<float>("MarioTiles/PowerUp.bmp");
                } else if(map[r][c] == '+'){
                    tile = cimg_library::CImg<float>("MarioTiles/ExtraLife.bmp");
                } else if(map[r][c] == '*'){
                    tile = cimg_library::CImg<float>("MarioTiles/Star.bmp");
                } else if(map[r][c] == '<' || map[r][c] == '>' || map[r][c] == 'v' || map[r][c] == '^'){
                    tile = cimg_library::CImg<float>("MarioTiles/PlatformBlack.bmp");
                } else if(map[r][c] == 'f'){
                    tile = cimg_library::CImg<float>("MarioTiles/Fire.bmp");
                } else if(map[r][c] == 'Y' && (r+1) < height && map[r+1][c] != 'Y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringBot.bmp");
                } else if(map[r][c] == 'Y' && (r+1) < height && map[r+1][c] == 'Y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringTop.bmp");
                } else if(map[r][c] == 'H'){
                    tile = cimg_library::CImg<float>("MarioTiles/Vine.bmp");
                } else if(map[r][c] == '|' && (r-1) >= 0 && map[r-1][c] != '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/Flag.bmp");
                } else if(map[r][c] == '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/PoleBot.bmp");
                } else if(map[r][c] == 'z' && (c-1) >= 0 && map[r][c-1] != 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop1.bmp");
                } else if(map[r][c] == 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop2.bmp");
                } else if(map[r][c] == '=' && (c-1) >= 0 && map[r][c-1] != '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot1.bmp");
                } else if(map[r][c] == '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot2.bmp");
                }
            } else if(archetype == "castle"){
                if(map[r][c] == '-'){
                    tile = cimg_library::CImg<float>("MarioTiles/Black.bmp");
                } else if(map[r][c] == '#' && r== height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/SolidCastle.bmp");
                } else if(map[r][c] == '#' && r<height-1){
                    tile = cimg_library::CImg<float>("MarioTiles/SolidCastle.bmp");
                } else if(map[r][c] == 'B'){
                    tile = cimg_library::CImg<float>("MarioTiles/BrickCastle.bmp");
                } else if(map[r][c] == '?'){
                    tile = cimg_library::CImg<float>("MarioTiles/QuestionBlock.bmp");
                } else if(map[r][c] == '{'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownLeft.bmp");
                } else if(map[r][c] == '}'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownRight.bmp");
                } else if(map[r][c] == ']'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopRight.bmp");
                } else if(map[r][c] == '['){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeTopLeft.bmp");
                } else if(map[r][c] == 'p' || map[r][c] == 'd'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeLeft.bmp");
                } else if(map[r][c] == 'P' || map[r][c] == 'D'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeRight.bmp");
                } else if(map[r][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonTop.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'C'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonMid.bmp");
                } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'c'){
                    tile = cimg_library::CImg<float>("MarioTiles/CannonBot.bmp");
                } else if(map[r][c] == 'g'){
                    tile = cimg_library::CImg<float>("MarioTiles/GoombaBlack.bmp");
                } else if(map[r][c] == 'k'){
                    tile = cimg_library::CImg<float>("MarioTiles/KoopaBlack.bmp");
                } else if(map[r][c] == 'K'){
                    tile = cimg_library::CImg<float>("MarioTiles/ParaKoopaBlack.bmp");
                } else if(map[r][c] == 't'){
                    tile = cimg_library::CImg<float>("MarioTiles/TurtleBlack.bmp");
                } else if(map[r][c] == 'l'){
                    tile = cimg_library::CImg<float>("MarioTiles/Lakitu.bmp");
                } else if(map[r][c] == 'h'){
                    tile = cimg_library::CImg<float>("MarioTiles/HammerBroBlack.bmp");
                } else if(map[r][c] == 'V'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaUpBlack.bmp");
                } else if(map[r][c] == 'X'){
                    tile = cimg_library::CImg<float>("MarioTiles/PiranhaDownBlack.bmp");
                } else if(map[r][c] == 'o'){
                    tile = cimg_library::CImg<float>("MarioTiles/CoinBlack.bmp");
                } else if(map[r][c] == 'O'){
                    tile = cimg_library::CImg<float>("MarioTiles/CoinBlock.bmp");
                } else if(map[r][c] == 'M'){
                    tile = cimg_library::CImg<float>("MarioTiles/PowerUp.bmp");
                } else if(map[r][c] == '+'){
                    tile = cimg_library::CImg<float>("MarioTiles/ExtraLife.bmp");
                } else if(map[r][c] == '*'){
                    tile = cimg_library::CImg<float>("MarioTiles/Star.bmp");
                } else if(map[r][c] == '<' || map[r][c] == '>' || map[r][c] == 'v' || map[r][c] == '^'){
                    tile = cimg_library::CImg<float>("MarioTiles/PlatformBlack.bmp");
                } else if(map[r][c] == 'f'){
                    tile = cimg_library::CImg<float>("MarioTiles/Fire.bmp");
                } else if(map[r][c] == 'Y' && (r+1) < height && map[r+1][c] != 'Y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringBot.bmp");
                } else if(map[r][c] == 'Y' && (r+1) < height && map[r+1][c] == 'Y'){
                    tile = cimg_library::CImg<float>("MarioTiles/SpringTop.bmp");
                } else if(map[r][c] == 'H'){
                    tile = cimg_library::CImg<float>("MarioTiles/Vine.bmp");
                } else if(map[r][c] == '|' && (r-1) >= 0 && map[r-1][c] != '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/Flag.bmp");
                } else if(map[r][c] == '|'){
                    tile = cimg_library::CImg<float>("MarioTiles/PoleBot.bmp");
                } else if(map[r][c] == 'z' && (c-1) >= 0 && map[r][c-1] != 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop1.bmp");
                } else if(map[r][c] == 'z'){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop2.bmp");
                } else if(map[r][c] == '=' && (c-1) >= 0 && map[r][c-1] != '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot1.bmp");
                } else if(map[r][c] == '='){
                    tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot2.bmp");
                } else if(map[r][c] == 'a'){
                    tile = cimg_library::CImg<float>("MarioTiles/Fireball.bmp");
                } else if(map[r][c] == 'A'){
                    tile = cimg_library::CImg<float>("MarioTiles/Axe.bmp");
                } else if(map[r][c] == 'q'){
                    tile = cimg_library::CImg<float>("MarioTiles/CheepBlack.bmp");
                } else if(map[r][c] == 'b'){
                    tile = cimg_library::CImg<float>("MarioTiles/Bridge.bmp");
                } else if(map[r][c] == 'G'){
                    tile = cimg_library::CImg<float>("MarioTiles/BowserTL.bmp");
                }/* else if(map[r][c] == 'G' && (r-1) >= 0 && (c-1) >= 0 && map[r-1][c] != 'G' && map[r][c-1] != 'G'){
                    tile = cimg_library::CImg<float>("MarioTiles/BowserTL.bmp");
                } else if(map[r][c] == 'G' && (r-1) >= 0 && (c-1) >= 0 && map[r-1][c] != 'G' && map[r][c-1] != 'G'){
                    tile = cimg_library::CImg<float>("MarioTiles/BowserTL.bmp");
                }*/
            } else if(archetype == "bridge"){
                
            } else if(archetype == "platform"){
                
            }
                
                
            for(int tiley=0; tiley<tile.height(); tiley++){
                for(int tilex=0; tilex<tile.width(); tilex++){
                    ret(tilex+c*16, tiley+r*16, 0, 0)= *tile.data(tilex, tiley, 0, 0);
                    ret(tilex+c*16, tiley+r*16, 0, 1)= *tile.data(tilex, tiley, 0, 1);
                    ret(tilex+c*16, tiley+r*16, 0, 2)= *tile.data(tilex, tiley, 0, 2);
                }
            }
        }
    }
    
    ret.save(imageName.c_str());
    //ret.display();
    
    return ret;
}

cimg_library::CImg<float> ConvertMarioMapToImagePath(int width, int height, string mapName1, string mapName2, string imageName){
    
    //first, read in the map to be translated
    ifstream input(mapName1);
    vector< vector< char> > map;
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i]!= '\0' && line[i]!= '\n' && line[i]!= '\t' &&
               line[i]!= '\r'){
                row.push_back(line[i]);
            }
        }
        map.push_back(row);
        row.clear();
    }
    input.close();
    input.open(mapName2);
    vector< vector< char> > map2;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i]!= '\0' && line[i]!= '\n' && line[i]!= '\t' &&
               line[i]!= '\r'){
                row.push_back(line[i]);
            }
        }
        map2.push_back(row);
        row.clear();
    }
    input.close();
    
    //initialize an image of the proper size
    cimg_library::CImg<float> ret(16*width, 16*height, 1, 3);
    
    //then, for each tile in the map, set theappropriate pixel values in the output image
    for(int r=height-1; r>=0; r--){
        for(int c=0; c<width; c++){
            
            cimg_library::CImg<float> tile;
            
        
            if(map[r][c] == 'x' && map2[r][c] == 'x'){
                tile = cimg_library::CImg<float>("MarioTiles/smallMario_Overlap.bmp");
            } else if(map[r][c] == 'x' && map2[r][c] != 'x'){
                tile = cimg_library::CImg<float>("MarioTiles/smallMario_Provided.bmp");
            } else if(map[r][c] != 'x' && map2[r][c] == 'x'){
                tile = cimg_library::CImg<float>("MarioTiles/smallMario_Annotated.bmp");
            } else if(map[r][c] == '-'){
                tile = cimg_library::CImg<float>("MarioTiles/Empty.bmp");
            } else if(map[r][c] == '#' && r== height-1){
                tile = cimg_library::CImg<float>("MarioTiles/Solid.bmp");
            } else if(map[r][c] == '#' && r<height-1){
                tile = cimg_library::CImg<float>("MarioTiles/Solid2.bmp");
            } else if(map[r][c] == 'B'){
                tile = cimg_library::CImg<float>("MarioTiles/Block.bmp");
            } else if(map[r][c] == '?'){
                tile = cimg_library::CImg<float>("MarioTiles/QuestionBlock.bmp");
            } else if(map[r][c] == '{'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownLeft.bmp");
            } else if(map[r][c] == '}'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeTopDownRight.bmp");
            } else if(map[r][c] == ']'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeTopRight.bmp");
            } else if(map[r][c] == '['){
                tile = cimg_library::CImg<float>("MarioTiles/PipeTopLeft.bmp");
            } else if(map[r][c] == 'p' || map[r][c] == 'd'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeLeft.bmp");
            } else if(map[r][c] == 'P' || map[r][c] == 'D'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeRight.bmp");
            } else if(map[r][c] == 'C'){
                tile = cimg_library::CImg<float>("MarioTiles/CannonTop.bmp");
            } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'C'){
                tile = cimg_library::CImg<float>("MarioTiles/CannonMid.bmp");
            } else if((r-1) >= 0 && map[r][c]=='c' && map[r-1][c] == 'c'){
                tile = cimg_library::CImg<float>("MarioTiles/CannonBot.bmp");
            } else if(map[r][c] == 'g'){
                tile = cimg_library::CImg<float>("MarioTiles/Goomba.bmp");
            } else if(map[r][c] == 'k'){
                tile = cimg_library::CImg<float>("MarioTiles/Koopa.bmp");
            } else if(map[r][c] == 'K'){
                tile = cimg_library::CImg<float>("MarioTiles/ParaKoopa.bmp");
            } else if(map[r][c] == 't'){
                tile = cimg_library::CImg<float>("MarioTiles/Turtle.bmp");
            } else if(map[r][c] == 'l'){
                tile = cimg_library::CImg<float>("MarioTiles/Lakitu.bmp");
            } else if(map[r][c] == 'h'){
                tile = cimg_library::CImg<float>("MarioTiles/HammerBro.bmp");
            } else if(map[r][c] == 'V'){
                tile = cimg_library::CImg<float>("MarioTiles/PiranhaUp.bmp");
            } else if(map[r][c] == 'X'){
                tile = cimg_library::CImg<float>("MarioTiles/PiranhaDown.bmp");
            } else if(map[r][c] == 'o'){
                tile = cimg_library::CImg<float>("MarioTiles/Coin.bmp");
            } else if(map[r][c] == 'O'){
                tile = cimg_library::CImg<float>("MarioTiles/CoinBlock.bmp");
            } else if(map[r][c] == 'M'){
                tile = cimg_library::CImg<float>("MarioTiles/PowerUp.bmp");
            } else if(map[r][c] == '+'){
                tile = cimg_library::CImg<float>("MarioTiles/ExtraLife.bmp");
            } else if(map[r][c] == '*'){
                tile = cimg_library::CImg<float>("MarioTiles/Star.bmp");
            } else if(map[r][c] == '<' || map[r][c] == '>' || map[r][c] == 'v' || map[r][c] == '^'){
                tile = cimg_library::CImg<float>("MarioTiles/Platform.bmp");
            } else if(map[r][c] == 'f'){
                tile = cimg_library::CImg<float>("MarioTiles/Fire.bmp");
            } else if(map[r][c] == 'Y'){
                tile = cimg_library::CImg<float>("MarioTiles/SpringTop.bmp");
            } else if(map[r][c] == 'y'){
                tile = cimg_library::CImg<float>("MarioTiles/SpringBot.bmp");
            } else if(map[r][c] == 'H'){
                tile = cimg_library::CImg<float>("MarioTiles/Vine.bmp");
            } else if(map[r][c] == '|' && (r-1) >= 0 && map[r-1][c] != '|'){
                tile = cimg_library::CImg<float>("MarioTiles/PoleTop.bmp");
            } else if(map[r][c] == '|'){
                tile = cimg_library::CImg<float>("MarioTiles/PoleBot.bmp");
            } else if(map[r][c] == 'z' && (c-1) >= 0 && map[r][c-1] != 'z'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop1.bmp");
            } else if(map[r][c] == 'z'){
                tile = cimg_library::CImg<float>("MarioTiles/PipeSideTop2.bmp");
            } else if(map[r][c] == '=' && (c-1) >= 0 && map[r][c-1] != '='){
                tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot1.bmp");
            } else if(map[r][c] == '='){
                tile = cimg_library::CImg<float>("MarioTiles/PipeSideBot2.bmp");
            } else if(map[r][c] == '!'){
                tile = cimg_library::CImg<float>("MarioTiles/PoisonMushroom.bmp");
            }
            
            
            for(int tiley=0; tiley<tile.height(); tiley++){
                for(int tilex=0; tilex<tile.width(); tilex++){
                    ret(tilex+c*16, tiley+r*16, 0, 0)= *tile.data(tilex, tiley, 0, 0);
                    ret(tilex+c*16, tiley+r*16, 0, 1)= *tile.data(tilex, tiley, 0, 1);
                    ret(tilex+c*16, tiley+r*16, 0, 2)= *tile.data(tilex, tiley, 0, 2);
                }
            }
        }
    }
    
    ret.save(imageName.c_str());
    //ret.display();
    
    return ret;
}

cimg_library::CImg<float> ConvertKIMapToImage(int width, int height, string mapName, string imageName){
    
    //first, read in the map to be translated
    ifstream input(mapName);
    vector< vector< char> > map;
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i]!= '\0' && line[i]!= '\n' && line[i]!= '\t' &&
               line[i]!= '\r'){
                row.push_back(line[i]);
            }
        }
        map.push_back(row);
        row.clear();
    }
    
    //initialize an image of the proper size
    cimg_library::CImg<float> ret(16*width, 16*height, 1, 3);
    
    //then, for each tile in the map, set the appropriate pixel values in the output image
    for(int r=height-1; r>=0; r--){
        for(int c=0; c<width; c++){
            
            cimg_library::CImg<float> tile;
            
            if(map[r][c] == '-'){
                tile = cimg_library::CImg<float>("KITiles/Empty.bmp");
            } else if(map[r][c] == '#'){
                tile = cimg_library::CImg<float>("KITiles/Block.bmp");
            } else if(map[r][c] == 'H'){
                tile = cimg_library::CImg<float>("KITiles/Hazard.bmp");
            } else if(map[r][c] == 'M'){
                tile = cimg_library::CImg<float>("KITiles/Platform.bmp");
            } else if(r<height-1 && map[r][c] == 'D' && (map[r+1][c] == '#' || map[r+1][c] == 'T' || map[r+1][c] == 'M')){
                tile = cimg_library::CImg<float>("KITiles/DoorBottom.bmp");
            } else if(r<height-1 && r>0 && map[r][c]=='D' && map[r+1][c]=='D' && map[r-1][c]=='D'){
                tile = cimg_library::CImg<float>("KITiles/DoorBottom.bmp");
            } else if(r<height-1 && map[r][c]=='D' && map[r+1][c]=='D' && (r==0 || (r>0 && map[r-1][c]!='D'))){
                tile = cimg_library::CImg<float>("KITiles/DoorTop.bmp");
            } else if(r==height-1 && map[r][c] == 'D' && map[r-1][c]!='D'){
                tile = cimg_library::CImg<float>("KITiles/DoorTop.bmp");
            } else if(c>0 && c<width-1 && map[r][c]=='T' && map[r][c-1]!='T' && map[r][c+1]!='T'){
                tile = cimg_library::CImg<float>("KITiles/CloudCenter.bmp");
            } else if(map[r][c]=='T' && (c==0 || (c>0 && (map[r][c-1]=='#' || map[r][c-1]=='H' || map[r][c-1]=='D' || map[r][c-1]=='M')))){
                tile = cimg_library::CImg<float>("KITiles/CloudRight.bmp");
            } else if(map[r][c]=='T' && (c==width-1 || (c<width-1 && (map[r][c+1]=='#' || map[r][c+1]=='H' || map[r][c+1]=='D' || map[r][c+1]=='M')))){
                tile = cimg_library::CImg<float>("KITiles/CloudLeft.bmp");
            } else if(map[r][c]=='T' && c>0 && map[r][c-1]=='T' && (c==width-1 || map[r][c+1]!='T')){
                tile = cimg_library::CImg<float>("KITiles/CloudRight.bmp");
            } else if(map[r][c]=='T' && c<width-1 && map[r][c+1]=='T' && (c==0 || map[r][c-1]!='T')){
                tile = cimg_library::CImg<float>("KITiles/CloudLeft.bmp");
            } else if(c>0 && c<width-1 && map[r][c]=='T' && map[r][c-1]=='T' && map[r][c+1]=='T'){
                tile = cimg_library::CImg<float>("KITiles/CloudCenter.bmp");
            }
            
            for(int tiley=0; tiley<tile.height(); tiley++){
                for(int tilex=0; tilex<tile.width(); tilex++){
                    ret(tilex+c*16, tiley+r*16, 0, 0)= *tile.data(tilex, tiley, 0, 0);
                    ret(tilex+c*16, tiley+r*16, 0, 1)= *tile.data(tilex, tiley, 0, 1);
                    ret(tilex+c*16, tiley+r*16, 0, 2)= *tile.data(tilex, tiley, 0, 2);
                }
            }
        }
    }
    
    ret.save(imageName.c_str());
    //ret.display();
    
    return ret;
}

void Demo(int rowSplits, int lookAhead, int trainingMaps, string mapName, string trainingFolderName, int networkStructure, int width, int height, string outputFolderName, int toGenerate){
    
    //train the model
    Learner TopLevelLearner = Learner(rowSplits, lookAhead, false, false);
    TopLevelLearner.FindAllTileTypes(trainingMaps, trainingFolderName+mapName, false);
    
    for(int i=networkStructure; i>=0; i--)
        TopLevelLearner.SetDepMatrix(i);
    
    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel(trainingFolderName+mapName, trainingMaps, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    
    Generator TopGen = Generator(width, height);
    TopGen.CopyConfig(TopLevelLearner.Config);
    TopGen.SetTileTypes(TopLevelLearner.TileTypes, false);
    TopGen.ResizeVectors(false);
    TopGen.Config.LookAhead=lookAhead;
    
    for (int dep = 0; dep < TopGen.Config.NumberMatrices; dep++)
        TopGen.CopyProbs("HighLevel_Probabilities_Dep"+ to_string(dep) +".txt", false, dep);
    
    #if defined(_WIN32)
        _mkdir(outputFolderName);
    #else
        mkdir(outputFolderName.c_str(), 0777);
    #endif
    
    //define the constraints
    //int maxPipes = 13;
    //int minPipes = 1;
    //int maxLength = 8;
    //int minLength = 4;
    //int maxGaps = 12;
    //int minGaps = 4;
    int maxEnemies = 10000;
    int minEnemies = 0;
    
    
    //attempt to sample maps satisfying the contraints
    
    ConstraintChecker cc= ConstraintChecker();
    
    float totalAttempts=0;
    int incompleteMaps=0;
    for(int i=0; i<toGenerate; i++){
        
        cout<<"Map "<<i<<" generated\n";
        float attemptsPerMap=21;
        bool failed=false;
        
        //violation detection resampling
        TopGen.GenerateTopLevel(outputFolderName+mapName + to_string(i) + ".txt");
        
        bool tooFew =false;
        bool tooMany=false;
        int pos=-1;
        if(MarioCountEnemies(TopGen.Map) < minEnemies){
            tooFew = true;
        } else if(MarioCountEnemies(TopGen.Map) > maxEnemies){
            tooMany = true;
        } else{
            pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        }
        if(pos >= 208 && !tooMany && !tooFew){
            pos = MarioNoIllFormedPipesConstraintPosition(TopGen.Map, 0, 0);
        }
        
        
        
        //int pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
        vector<int> ret1;
        if(tooFew){
            ret1 = cc.SlidingWindowViolationDetectionCounts(TopGen.Map, true, 20, &MarioCountEnemies, false);
        } else if(tooMany){
            ret1 = cc.SlidingWindowViolationDetectionCounts(TopGen.Map, true, 20, &MarioCountEnemies, true);
        } else if(pos > 2 && pos<209){
            ret1.push_back(max(pos-9, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+10, 209));
            ret1.push_back(13);
        }
        
        //fix the level while there are sections to be edited
        while(ret1.size()>0){
            //cout<<pos<<"("<<ret1[0]<<", "<<ret1[1]<<") ("<<ret1[2]<<", "<<ret1[3]<<")"<<endl;
            //check the hard constraints, playability and ill formed structures
            for (int r=0; r<ret1.size(); r+=4){
                float tilesSampled = TopGen.RegenerateTopLevelSectionWithSmoothing(outputFolderName+mapName+to_string(i)+".txt", ret1[r], ret1[r+1], ret1[r+2], ret1[r+3]);
                
                attemptsPerMap+= tilesSampled/(120.0);
                if(attemptsPerMap >=1050){
                    incompleteMaps++;
                    attemptsPerMap = 1050;
                    failed=true;
                    break;
                }
            }
            if(failed)
                break;
            
            tooFew =false;
            tooMany=false;
            pos=-1;
            ret1.clear();
            if(MarioCountEnemies(TopGen.Map) < minEnemies){
                tooFew = true;
            } else if(MarioCountEnemies(TopGen.Map) > maxEnemies){
                tooMany = true;
            } else{
                pos = MarioPlayabilityDistanceConstraint(TopGen.Map, 0, 0);
            }
            if(pos >= 208 && !tooFew && !tooMany){
                pos = MarioNoIllFormedPipesConstraintPosition(TopGen.Map, 0, 0);
            }
            
            
            if(tooFew){
                ret1 = cc.SlidingWindowViolationDetectionCounts(TopGen.Map, true, 20, &MarioCountEnemies, false);
            } else if(tooMany){
                ret1 = cc.SlidingWindowViolationDetectionCounts(TopGen.Map, true, 20, &MarioCountEnemies, true);
            } else if(pos > 2 && pos<209){
                ret1.push_back(max(pos-9, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+10, 209));
                ret1.push_back(13);
            }
            
            
            
        }
        if(!failed)
            totalAttempts+=attemptsPerMap;
        else
            rename((outputFolderName+mapName+to_string(i)+".txt").c_str(), (outputFolderName+"failed_"+mapName+to_string(i)+".txt").c_str());
        
        ConvertMarioMapToImage(210, 14, outputFolderName+mapName+to_string(i)+".txt", outputFolderName+mapName+to_string(i)+".bmp", "standard");
        //cout<<i<<" "<<attemptsPerMap<<endl;
    }
}

pair<long double, int> MapLikelihoodConfig(vector<vector<char> > Map, vector<vector<int> > depMatrix, vector<char> tileTypes, vector<float> configProbs){
    long double likelihood=0;
    int unseenStates=0;
    
    //ofstream p("ConfigurationsInOrder2.txt");
    
    for (int h=(int)Map.size()-1; h>=0; h--){
        for (int w = 0; w < Map[h].size(); w++){
            int ID = 0;
            int id = 0;
            int P = 0;
            for (int i = 0; i<depMatrix.size(); i++){
                for (int j =0; j <depMatrix[i].size(); j++){
                    if (depMatrix[i][j] == 1){
                        
                        if (((h + i - 2) < 0) || ((h + i - 2) >= Map.size()) || ((w + j - 2) < 0)){
                            id=0;
                        }
                        else{
                            for (int m = 0; m < tileTypes.size(); m++){
                                if (tileTypes.at(m) == Map[h + i - 2][w + j - 2]){
                                    id = m;
                                    break;
                                }
                            }
                        }
                        //p<<tileTypes[id];
                        ID += id*pow(tileTypes.size(), P);
                        P++;
                    }
                }
                //p<<endl;
            }
            //p<<ID<<endl;

            
            if(configProbs[ID] == 0){
                unseenStates++;
            }
            //if(t==0)
            //    unseenStates++;
            else{
                likelihood += log10(configProbs[ID]);
                //likelihood +=model.Probabilities[0][ID+model.Config.Rows[0]*currRow][id];
            }
        }
    }
    //p.close();
    //likelihood= (likelihood)/(Map.size()*Map[0].size());
    return pair<long double, int>(likelihood/Map.size()*Map[0].size(), unseenStates);
}

vector<float> ComputeProbabilitiesConfigurations(vector<vector<int> > depMat, vector<char> tileTypes, int complexity, int numTileTypes, int maps, string mapFolder, string mapName){

    
    ofstream p("ConfigCounts.txt");
    
    
    vector<float> probs;
    int numConfigs=1;
    for(int i=0; i<complexity; i++)
        numConfigs*=numTileTypes;
        
    probs.resize(numConfigs);
    fill(probs.begin(), probs.end(), 0);
    float total=0;//numConfigs*5;
    for(int i=1; i<=maps; i++){
        ifstream input(mapFolder+mapName+to_string(i)+".txt");
        vector< vector< char> > map;
        string line;
        while(getline(input, line)){
            vector<char> row;
            for(int i=0; i<line.length(); i++){
                if(line[i]!= '\0' && line[i]!= '\n' && line[i]!= '\t' &&
                   line[i]!= '\r'){
                    row.push_back(line[i]);
                }
            }
            map.push_back(row);
            row.clear();
        }
    
    
        for (int h=(int)map.size()-1; h>=0; h--){
            for (int w = 0; w < map[h].size(); w++){
                cout<<h<<" "<<w<<endl;
                int ID = 0;
                int id = -1;
                int P = 0;
                for (int i = 0; i<depMat.size(); i++){
                    for (int j =0; j <depMat[i].size(); j++){
                        if (depMat[i][j] == 1){
                        
                            if (((h + i - 2) < 0) || ((h + i - 2) >= map.size()) || ((w + j - 2) < 0)){
                                id=0;
                            }
                            else{
                                for (int m = 0; m < tileTypes.size(); m++){
                                    if (tileTypes.at(m) == map[h + i - 2][w + j - 2]){
                                        id = m;
                                        break;
                                    }
                                }
                            }
                            //p<<tileTypes[id];
                            ID += id*pow(tileTypes.size(), P);
                            P++;
                        }
                    }
                    //p<<endl;
                }
                //p<<ID<<endl;
                //cout<<ID<<" "<<probs.size()<<endl;
                probs[ID]+=1;
                total+=1;
            }
        }
    
    }
    
    //cout<<total+numConfigs<<endl;
    //p.close();
    for(int i=0; i<probs.size(); i++){
        //p<<probs[i]<<endl;
        probs[i]/=total;
    }
    p.close();
    return probs;
}

// This function counts the number of tiles in each domain and compares the frequency of those tiles using the earthmover distance
vector<pair<int, float>> CompareRelativeTileFrequency(string sourceDomainFileName, string targetDomainFileName, int numSourceMaps, int numTargetMaps, string outputFileName, int percentageOfMappingsToReturn){
    
    /*------------------------------------------------------------------------------*/
    /*                                                                              */
    /*   Determine the probability distribution of the tiles in the target domain   */
    /*                                                                              */
    /*------------------------------------------------------------------------------*/
    
    //set up the model to count the tiles
    Learner targetTileCounter = Learner(1, 0, false, false);
    targetTileCounter.SetDepMatrix(0);
    targetTileCounter.FindAllTileTypes(numTargetMaps, targetDomainFileName, false);
    
    targetTileCounter.SetRows(false);
    targetTileCounter.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    targetTileCounter.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    targetTileCounter.SetTotalsTopLevel(targetDomainFileName, numTargetMaps, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    vector<float> targetTileDistribution(targetTileCounter.TileTypes.size());

    for(int i=0; i<targetTileDistribution.size(); i++)
        targetTileDistribution[i] = (float)targetTileCounter.Totals[0][0][i]/
                                            (float)targetTileCounter.Totals[0][0][targetTileCounter.Totals[0][0].size()-1];
    
    
    /*------------------------------------------------------------------------------*/
    /*                                                                              */
    /*   Determine the probability distribution of the tiles in the source domain   */
    /*                        before the maps get converted                         */
    /*                                                                              */
    /*------------------------------------------------------------------------------*/
    
    //set up the model to count the tiles
    Learner sourceTileCounter = Learner(1, 0, false, false);
    sourceTileCounter.SetDepMatrix(0);
    sourceTileCounter.FindAllTileTypes(numSourceMaps, sourceDomainFileName, false);
    
    sourceTileCounter.SetRows(false);
    sourceTileCounter.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    sourceTileCounter.InitFiles("HighLevel_Totals_Dep"+ to_string(0)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(0)+".txt", "", false, 0);
    
    //records the encountered totals for each tile type
    sourceTileCounter.SetTotalsTopLevel(sourceDomainFileName, numSourceMaps, "HighLevel_Totals_Dep"+ to_string(0)+".txt", false, 0);
    vector<float> sourceTileDistribution(sourceTileCounter.TileTypes.size());
    
    for(int i=0; i<sourceTileDistribution.size(); i++)
        sourceTileDistribution[i] = (float)sourceTileCounter.Totals[0][0][i]/
                                            (float)sourceTileCounter.Totals[0][0][sourceTileCounter.Totals[0][0].size()-1];
    
    
    /*------------------------------------------------------------------------------*/
    /*                                                                              */
    /*       For each possible mapping, determine the difference between the        */
    /*                  target and converted source distributions                   */
    /*                                                                              */
    /*------------------------------------------------------------------------------*/
    
    vector<pair<int, float>> tileMappingsAndCost;
    
    
    //number of possible tile mappings is equal to: numberOfTargetTiles^(numberOfSourceTiles) (disregarding Sentinel tiles)
    
    //converting from a base 10 number to a base n
    //
    //  example 20 to base 3
    //  x       x/n     x%n
    //  20      6       2
    //  6       2       0
    //  2       0       2
    //          202_3
    
    
    int mappingsToReturn = pow(targetTileCounter.TileTypes.size()-1, sourceTileCounter.TileTypes.size()-1)*(percentageOfMappingsToReturn/100.0);
    //iterate through every possible mapping
    for(long int mapping=0; mapping<pow(targetTileCounter.TileTypes.size()-1, sourceTileCounter.TileTypes.size()-1); mapping++){
        vector<int> currentTileMapping(sourceTileCounter.TileTypes.size(), 0);
        if(mapping%mappingsToReturn == 0)
            cout<<(mapping/mappingsToReturn)*percentageOfMappingsToReturn<<"% of mappings explored..."<<endl;
            
        int quotient=mapping;
        int iteration=1;
        while(quotient!=0){
            
            int remainder = quotient%(targetTileCounter.TileTypes.size()-1);
            quotient =quotient/(targetTileCounter.TileTypes.size()-1);
            currentTileMapping[iteration] = remainder+1;
            iteration++;
        }
    
        //for each mapping, convert the tile distribution to the proper
        vector<float> convertedSourceTileDistribution(targetTileDistribution.size(), 0);
        for(int i=0; i<targetTileCounter.TileTypes.size(); i++){
            
            //for the current mapping, we need to find the probabilities for the tiles in the converted source domain
            //by checking the probabilities of the converted tiles and summing any double mappings
            for(int j=0; j<currentTileMapping.size(); j++){
                if(currentTileMapping[j] == i){
                    convertedSourceTileDistribution[i] += sourceTileDistribution[j];
                }
            }
        }
        
        /*------------------------------------------------------------------------------*/
        /*                                                                              */
        /*     compute the earthmover distance between the target and source tile       */
        /*            distributions, and store the top performing mappings              */
        /*                                                                              */
        /*------------------------------------------------------------------------------*/
        
        //compute the earthmover distance between the target and source tile distributions
        //Double check Earthmover distance definition
        float earthMoverDistance=0;
        for(int i=0; i<targetTileDistribution.size(); i++){
            earthMoverDistance+= abs(targetTileDistribution[i] - convertedSourceTileDistribution[i]);
        }
        
        //track the top N mappings
        if(tileMappingsAndCost.size() == 0 || (earthMoverDistance >= tileMappingsAndCost.back().second && tileMappingsAndCost.size()<mappingsToReturn)){
            tileMappingsAndCost.push_back(pair<int, float>(mapping, earthMoverDistance));
        } else if(earthMoverDistance < tileMappingsAndCost.back().second){
            //insert the item into list in the appropriate spot
            for(int i=0; i<tileMappingsAndCost.size(); i++){
                if(earthMoverDistance < tileMappingsAndCost[i].second){
                    tileMappingsAndCost.insert(tileMappingsAndCost.begin()+i, pair<int, float>(mapping, earthMoverDistance));
                    tileMappingsAndCost.erase(tileMappingsAndCost.begin()+tileMappingsAndCost.size()-1);
                    break;
                }
            }
        }
    }
    
    /*------------------------------------------------------------------------------*/
    /*                                                                              */
    /*               Return and print the top percentage of mappings                */
    /*                                                                              */
    /*------------------------------------------------------------------------------*/
    
    ofstream out(outputFileName);
    for(int i=0; i<mappingsToReturn; i++){
        vector<int> tileMapping(sourceTileCounter.TileTypes.size(), 0);
        int quotient=tileMappingsAndCost[i].first;
        int iteration=1;
        
        while(quotient!=0){
            int remainder = quotient%(targetTileCounter.TileTypes.size()-1);
            quotient =quotient/(targetTileCounter.TileTypes.size()-1);
            tileMapping[iteration] = remainder+1;
            iteration++;
        }
        for(int j=0; j<sourceTileCounter.TileTypes.size(); j++)
            out<<sourceTileCounter.TileTypes[j]<<"\t";
        out<<endl;
        for(int j=0; j<sourceTileCounter.TileTypes.size(); j++)
            out<<targetTileCounter.TileTypes[tileMapping[j]]<<"\t";
        out<<endl;
        
        out<<tileMappingsAndCost[i].second<<endl;
    }
    
    out.close();
    return tileMappingsAndCost;
}

//This function takes a mapping and a set of maps and converts them using the mapping and saves them to the designated files
void ConvertSourceToTarget(vector<char> mapping, string sourceDomainFileName, int numSourceMaps, string convertedFileName){
    
    Learner sourceTileFinder = Learner(1, 0, false, false);
    sourceTileFinder.SetDepMatrix(0);
    sourceTileFinder.FindAllTileTypes(numSourceMaps, sourceDomainFileName, false);
    
    
    for(int i=0; i<=numSourceMaps; i++){
        ifstream in(sourceDomainFileName+to_string(i)+".txt");
        if(!in)
            continue;
        ofstream out(convertedFileName+to_string(i)+".txt");
        string line;
        while(getline(in, line)){
            for(int c=0; c<line.length(); c++){
                for(int p=0; p<sourceTileFinder.TileTypes.size(); p++){
                    if(sourceTileFinder.TileTypes[p] == line[c]){
                        out<<mapping[p];
                        break;
                    }
                }
            }
            out<<endl;
        }
        out.close();
        in.close();
    }
}


// This funtion takes 2 mappings, 1 from A to B and 1 from B to A, and finds the tiles that got mapped to each other
pair<int, int> FindCommonTileMappings(vector<char> mappingAtoB, vector<char> mappingBtoA, vector<char> tilesA, vector<char> tilesB){
    
    int tilesInCommon=0;
    int tileNumberDifference = abs((int)mappingAtoB.size() - (int)mappingBtoA.size());
    
    for(int i=0; i<mappingAtoB.size(); i++){
        char tile_1=mappingAtoB[i];
        for(int j=0; j<tilesB.size(); j++){
            if(tile_1 == tilesB[j] && mappingBtoA[j] == tilesA[i])
                tilesInCommon++;
        }
    }
    
    return pair<int, int>(tilesInCommon, tileNumberDifference);
}


//TODO: Need to account for other tile positions
//      Need to run a full experiment to determine if it works as intended

int MeasurePlagiarism(string generatedMapName, string trainingMapName){
    
    vector<vector< char> > generatedMap;
    vector<vector< char> > trainingMap;

    vector<vector< int> > plagiarizedMap;
    
    //first, read in the generated map
    ifstream map(generatedMapName);
    string line;
    while(getline(map, line)){
        vector<char> row;
        for(int c=0; c<line.size(); c++){
            row.push_back(line[c]);
        }
        generatedMap.push_back(row);
    }

    
    //read in the training map
    ifstream m(trainingMapName);
    while(getline(m, line)){
        vector<char> row;
        for(int c=0; c<line.size(); c++){
            row.push_back(line[c]);
        }
        trainingMap.push_back(row);
    }
    
    // for each tile in the generated map
    // position that tile over every position in the training map
    // compare the overlapping areas and keep track of how much contiguous area is surrounding the current tile
    
    //start with doing it for one tile (the top right tile of the generated map)
    int currentTileX = (int)generatedMap[0].size();
    int currentTileY = 0;
    
    int maxPlaigarizedSection=0;
    pair<int, int> bestPostition(-1, -1);
    
    ofstream out("TestingOverlap.txt");
    //start at the bottom of the map left corner of the training map
    for(int trainingMapPositionY= (int)trainingMap.size()-1; trainingMapPositionY >= currentTileY*-1; trainingMapPositionY--){
        for(int trainingMapPositionX = 0; trainingMapPositionX < (int)trainingMap[0].size() + ((int)trainingMap[0].size() - (currentTileX)); trainingMapPositionX++){
            
            cout<<trainingMapPositionY<<" "<<trainingMapPositionX<<endl;

            int overlapY = min((int)trainingMap.size()-trainingMapPositionY, (int)generatedMap.size());
            int overlapX = min(trainingMapPositionX+1, (int)generatedMap[0].size());
            
            // initialize the overlapping area vector to false each time
            vector< vector< bool> > overlappingArea;
            for(int i=0; i<overlapY; i++){
                vector<bool> tmp (overlapX, false);
                overlappingArea.push_back(tmp);
            }
            
            for(int y = trainingMapPositionY; y < trainingMapPositionY+overlapY; y++){
                for(int x = (trainingMapPositionX-overlapX)+1; x <= trainingMapPositionX; x++){
                    //cout<<"("<<x<<", "<<y<<") ("<<generatedMap[0].size() - overlapX+x<<", "<<y-((int)trainingMap.size()-overlapY)<<"): "<<trainingMap[y][x]<<" "<<generatedMap[y-((int)trainingMap.size()-overlapY)][generatedMap[0].size() - overlapX+x]<<endl;
                    
                    
                    if(/*trainingMap[y][x] !='-' && generatedMap[y-((int)trainingMap.size()-overlapY)][generatedMap[0].size() - overlapX+x] != '-' && */trainingMap[y][x] == generatedMap[y-((int)trainingMap.size()-overlapY)][generatedMap[0].size() - overlapX+x])
                        overlappingArea[y-trainingMapPositionY][x - ((trainingMapPositionX-overlapX)+1)] = true;
                }
            }
            //cout<<endl;
            
            
            //print the overlapping sections
            for(int i=0; i<overlappingArea.size(); i++){
                for(int j=0; j<overlappingArea[0].size(); j++){
                    out<<overlappingArea[i][j]<<" ";
                }
                out<<endl;
            }
            out<<endl;
            
            
            // find the size of the section that is plaigarized starting with the chosen tile, using breadth first search
            int sizeOfPlaigarizedSection=0;
            vector<pair<int, int>> positionsToCheck(1, pair<int, int>(0, overlappingArea[0].size()-1));

            //first make sure that the starting position overlaps
            if(!overlappingArea[0][overlappingArea[0].size()-1])
                positionsToCheck.erase(positionsToCheck.begin());
            else{
                overlappingArea[0][overlappingArea[0].size()-1] = 0;
                sizeOfPlaigarizedSection++;
            }
            
            while(!positionsToCheck.empty()){
                
                //cout<<positionsToCheck.size()<<endl;
                //check the 4 directions for matches
                
                //pop the front
                pair<int, int> pos = positionsToCheck[0];
                overlappingArea[pos.first][pos.second] = 0;
                positionsToCheck.erase(positionsToCheck.begin());
                //cout<<pos.first<<" "<<pos.second<<endl;
                
                // check top
                // if overlapping, push the new position onto the queue
                if(pos.first-1 >=0 && overlappingArea[pos.first-1][pos.second]){
                    //cout<<"top"<<endl;
                    sizeOfPlaigarizedSection++;
                    positionsToCheck.push_back(pair<int, int>(pos.first-1, pos.second));
                }
                
                // check bottom
                // if overlapping, push the new position onto the queue
                if(pos.first+1 < overlappingArea.size() && overlappingArea[pos.first+1][pos.second]){
                    //cout<<"bottom"<<endl;
                    sizeOfPlaigarizedSection++;
                    positionsToCheck.push_back(pair<int, int>(pos.first+1, pos.second));
                }
                
                // check left
                // if overlapping, push the new position onto the queue
                if(pos.second-1 >=0 && overlappingArea[pos.first][pos.second-1]){
                    //cout<<"left: "<<pos.first<<" "<<pos.second-1<<" "<<overlappingArea[pos.first][pos.second-1]<<endl;
                    sizeOfPlaigarizedSection++;
                    positionsToCheck.push_back(pair<int, int>(pos.first, pos.second-1));
                }
                
                // check right
                // if overlapping, push the new position onto the queue
                if(pos.second+1 < overlappingArea[0].size() && overlappingArea[pos.first][pos.second+1]){
                    //cout<<"right"<<endl;
                    sizeOfPlaigarizedSection++;
                    positionsToCheck.push_back(pair<int, int>(pos.first, pos.second+1));
                }
            }
            if(sizeOfPlaigarizedSection > maxPlaigarizedSection){
                maxPlaigarizedSection = sizeOfPlaigarizedSection;
                bestPostition.first = trainingMapPositionY;
                bestPostition.second = trainingMapPositionX;
            }
        }
    }
    out.close();
    
    cout<< maxPlaigarizedSection <<" ("<<bestPostition.first<<", "<<bestPostition.second<<")"<<endl;
    return 0;
}


//Train with Partial Map
Learner TrainWithPartialMap(string folder, string mapName, string allMapsStructure, int amountOfMapToUse, int dependencyMatrix){
 
    Learner TopLevelLearner = Learner(1, 1, false, false);
    TopLevelLearner.FindAllTileTypes(30, allMapsStructure, false);
    
    TopLevelLearner.SetDepMatrix(dependencyMatrix);

    TopLevelLearner.SetRows(false);
    TopLevelLearner.ResizeVectors(false);
    
    //initializes the files necessary for storing the probabilities and totals
    for (int i = 0; i < TopLevelLearner.Config.NumberMatrices; i++)
        TopLevelLearner.InitFiles("HighLevel_Totals_Dep"+ to_string(i)+".txt", "", "HighLevel_Probabilities_Dep"+ to_string(i)+".txt", "", false, i);
    
    //trim the map to the needed size
    ifstream InputMap((folder+mapName+".txt").c_str());
    string line ="";
    vector<vector<char> > Map;
    while (getline(InputMap,line)){
        
        vector<char> Temp;
        if(line.back() == '\n' || line.back() == '\t' || line.back() == '\r' || line.back() == '\0')
            line.pop_back();
        
        Temp.resize(amountOfMapToUse);
        
        for (int i = 0; i < amountOfMapToUse; i++)
            Temp[i]=line[i];
        
        Map.push_back(Temp);
        Temp.clear();
    }
    
    InputMap.close();
    
    ofstream OutputMap(folder+to_string(amountOfMapToUse)+"-columns-"+mapName+"_1.txt");
    for(int i=0; i<Map.size(); i++){
        for(int j=0; j<amountOfMapToUse; j++){
            OutputMap<<Map[i][j];
        }
        if(i<Map.size()-1)
            OutputMap<<endl;
    }
    OutputMap.close();
    
    //records the encountered totals for each tile type
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetTotalsTopLevel(folder+to_string(amountOfMapToUse)+"-columns-"+mapName, 1, "HighLevel_Totals_Dep"+ to_string(dep)+".txt", false, dep);
    
    //sets the probabilities using the totals
    for (int dep = 0; dep < TopLevelLearner.Config.NumberMatrices; dep++)
        TopLevelLearner.SetProbsForOnlyLowLevel("HighLevel_Probabilities_Dep" + to_string(dep) + ".txt", false, dep);
    
    return TopLevelLearner;
}

void convertImagetoMarioMap(int width, int height, string imageName, string mapName, string archetype){
    
    cout<<mapName<<"\n";
    
    //load the image
    cimg_library::CImg<float> inputMap;
    ifstream check(imageName.c_str());
    if(check.good())
        inputMap = cimg_library::CImg<float>(imageName.c_str());
    else
        return;
    
    vector<string> tileNames;
    vector<char> tileTypes;
    
    int numMarios=129;
    int numEnemies=46;
    int numSolids=145;
    
    tileNames.push_back("bigMario_1.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_2.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_3.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_4.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_5.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_6.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_7.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_8.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_9.bmp");      tileTypes.push_back('x');
    tileNames.push_back("bigMario_10.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_11.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_12.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_13.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_14.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_15.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_16.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_17.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_18.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_19.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_20.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_21.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_22.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_23.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_24.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_25.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_26.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_27.bmp");     tileTypes.push_back('x');
    tileNames.push_back("bigMario_28.bmp");     tileTypes.push_back('x');
    
    tileNames.push_back("smallMario_1.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_2.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_3.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_4.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_5.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_6.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_7.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_8.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_9.bmp");    tileTypes.push_back('x');
    tileNames.push_back("smallMario_10.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_11.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_12.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_13.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_14.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_15.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_16.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_17.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_18.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_19.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_20.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_21.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_22.bmp");   tileTypes.push_back('x');
    tileNames.push_back("smallMario_23.bmp");   tileTypes.push_back('x');
    
    tileNames.push_back("superMario_1.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_2.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_3.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_4.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_5.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_6.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_7.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_8.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_9.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_10.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_11.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_12.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_13.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_14.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_15.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_16.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_17.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_18.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_19.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_20.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_21.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_22.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_23.bmp");    tileTypes.push_back('x');
    tileNames.push_back("superMario_24.bmp");    tileTypes.push_back('x');
    
    tileNames.push_back("starMario_1.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_2.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_3.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_4.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_5.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_6.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_7.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_8.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_9.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_10.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_11.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_12.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_13.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_14.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_15.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_16.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_17.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_18.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_19.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_20.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_21.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_22.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_23.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_24.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_25.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_26.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_27.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_28.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_29.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_30.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_31.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_32.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_33.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_34.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_35.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_36.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_37.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_38.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_39.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_40.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_41.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_42.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_43.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_44.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_45.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_46.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_47.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_48.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_49.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_50.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_51.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_52.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_53.bmp");     tileTypes.push_back('x');
    tileNames.push_back("starMario_54.bmp");     tileTypes.push_back('x');
    
    tileNames.push_back("Koopa_1.bmp");         tileTypes.push_back('k');
    tileNames.push_back("Koopa_2.bmp");         tileTypes.push_back('k');
    tileNames.push_back("Koopa_3.bmp");         tileTypes.push_back('k');
    tileNames.push_back("Koopa_4.bmp");         tileTypes.push_back('k');
    tileNames.push_back("Goomba_1.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_2.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_3.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_4.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_5.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_6.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_7.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_8.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_9.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_10.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_11.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_12.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_13.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_14.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_15.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_16.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_17.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_18.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_19.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_20.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_21.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_22.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_23.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_24.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_25.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_26.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_27.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_28.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_29.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_30.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_31.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_32.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_33.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_34.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_35.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_36.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_37.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_38.bmp");        tileTypes.push_back('g');
    tileNames.push_back("Goomba_39.bmp");        tileTypes.push_back('g');
    
    tileNames.push_back("Lakitu_1.bmp");        tileTypes.push_back('l');
    tileNames.push_back("Spiny_1.bmp");        tileTypes.push_back('t');
    tileNames.push_back("Spiny_2.bmp");        tileTypes.push_back('t');
    
    tileNames.push_back("questionBlock_1.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_2.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_3.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_4.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_5.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_6.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_7.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_8.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_9.bmp"); tileTypes.push_back('?');
    tileNames.push_back("questionBlock_10.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_11.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_12.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_13.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_14.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_15.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_16.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_17.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_18.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_19.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_20.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_21.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_22.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_23.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_24.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_25.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_26.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_27.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_28.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_29.bmp");tileTypes.push_back('?');
    tileNames.push_back("questionBlock_30.bmp");tileTypes.push_back('?');
    
    tileNames.push_back("rightTopPipe_1.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightTopPipe_2.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightTopPipe_3.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightTopPipe_4.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightTopPipe_5.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightTopPipe_6.bmp");  tileTypes.push_back('P');
    
    tileNames.push_back("rightBotPipe_1.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightBotPipe_2.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightBotPipe_3.bmp");  tileTypes.push_back('P');
    tileNames.push_back("rightBotPipe_4.bmp");  tileTypes.push_back('P');
    
    tileNames.push_back("leftTopPipe_1.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_2.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_3.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_4.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_5.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_6.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_7.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftTopPipe_8.bmp");   tileTypes.push_back('p');
    
    tileNames.push_back("leftBotPipe_1.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftBotPipe_2.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftBotPipe_3.bmp");   tileTypes.push_back('p');
    tileNames.push_back("leftBotPipe_4.bmp");   tileTypes.push_back('p');
    
    tileNames.push_back("Brick_1.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_2.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_3.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_4.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_5.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_6.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_7.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_8.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_9.bmp");         tileTypes.push_back('B');
    tileNames.push_back("Brick_10.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_11.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_12.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_13.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_14.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_15.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_16.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_17.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_18.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_19.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_20.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_21.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_22.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_23.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_24.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_25.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_26.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_27.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_28.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_29.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_30.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_31.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_32.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_33.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_34.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_35.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_36.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_37.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_38.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_39.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_40.bmp");        tileTypes.push_back('B');
    tileNames.push_back("Brick_41.bmp");        tileTypes.push_back('B');
    
    //tileNames.push_back("flag_1.bmp");          tileTypes.push_back('|');
    //tileNames.push_back("flag_2.bmp");          tileTypes.push_back('|');
    
    tileNames.push_back("Solid_1.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_2.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_3.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_4.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_5.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_6.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_7.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_8.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_9.bmp");         tileTypes.push_back('#');
    tileNames.push_back("Solid_10.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_11.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_12.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_13.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_14.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_15.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_16.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_17.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_18.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_19.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_20.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_21.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_22.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_23.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_24.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_25.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_26.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_27.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_28.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_29.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_30.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_31.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_32.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_33.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_34.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_35.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_36.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_37.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_38.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_39.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_40.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_41.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_42.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_43.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_44.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_45.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_46.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_47.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_48.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_49.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_50.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_51.bmp");        tileTypes.push_back('#');
    tileNames.push_back("Solid_52.bmp");        tileTypes.push_back('#');

    tileNames.push_back("Cloud_1.bmp");         tileTypes.push_back('-');
    tileNames.push_back("Cloud_2.bmp");         tileTypes.push_back('-');
    tileNames.push_back("Cloud_3.bmp");         tileTypes.push_back('-');
    tileNames.push_back("Cloud_4.bmp");         tileTypes.push_back('-');
    
    tileNames.push_back("Bush_1.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Bush_2.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Bush_3.bmp");          tileTypes.push_back('-');
    
    tileNames.push_back("Hill_1.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Hill_2.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Hill_3.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Hill_4.bmp");          tileTypes.push_back('-');
    tileNames.push_back("Hill_5.bmp");          tileTypes.push_back('-');
    
    tileNames.push_back("Empty_1.bmp");          tileTypes.push_back('-');
    
    //find where the first solid tile appears (where the first full column starts)
    int row=208;
    cimg_library::CImg<float> solidTile = cimg_library::CImg<float>( "Mario_Maps/Playthrough/Tiles/Solid_1.bmp");
    solidTile.normalize();
    inputMap.normalize();
    int max=-1;
    float min= 99999999;
    int index=0;
    for(int col=0; col<256; col++){
        int currMatch=0;
        float totalDifference=0;
        for(int y=0; y<16; y++){
            for(int x=0; x<16; x++){
                
                totalDifference += abs(inputMap(col+x, row+y, 0, 0) - solidTile(x, y, 0, 0)) +
                                    abs(inputMap(col+x, row+y, 0, 1) - solidTile(x, y, 0, 1))+
                                    abs(inputMap(col+x, row+y, 0, 2) - solidTile(x, y, 0, 2));
            }
        }
        //cout<<col<<" "<<totalDifference<<endl;
        if(totalDifference < min){
            min = totalDifference;
            index = col;
        }
    }
    
    //account for gaps
    index = index%16;

    vector< vector< char> > outputMap;
    outputMap.resize(height);
    
    //total length is 256 pixels
    //if the first full tile starts after or at 8 pixels in,
    //then mario can fit in the beginning half tile, and we
    //must determne what that first column of half tiles is
    if(index >= 8){
        for(int i=0; i<outputMap.size(); i++)
            outputMap[i].resize(16, '0');
        //outputMap[12].resize(16, ".");
    } else {
        for(int i=0; i<outputMap.size(); i++)
            outputMap[i].resize(15, '0');
        //outputMap[12].resize(15, ".");
    }

    //determine the first half column before moving onto the rest
    if(index >= 8){
        for(int r=32; r<224; r+=16){
            int c=0;
            vector<float> diffs(tileTypes.size()-numMarios-numEnemies, 0);
        
            for(int i=numMarios+numEnemies; i<tileTypes.size(); i++){
                cimg_library::CImg<float> tile = cimg_library::CImg<float>( ("Mario_Maps/Playthrough/Tiles/"+tileNames[i]).c_str() );
                tile.normalize();
                float totalDifferences=0;
                for(int y=0; y<index; y++){
                    for(int x=0; x<index; x++){
                        
                        totalDifferences+= abs(inputMap(x, r+y, 0, 0) - tile(16-index+x, y, 0, 0)) +
                        abs(inputMap(x, r+y, 0, 1) - tile(16-index+x, y, 0, 1)) +
                        abs(inputMap(x, r+y, 0, 2) - tile(16-index+x, y, 0, 2));
                    }
                }
                diffs[i-numMarios-numEnemies] = totalDifferences;
            }
            float min= 40;//diffs[0];
            int match=-1;
            for(int i=1; i<diffs.size(); i++){
                if(diffs[i] < min){
                    match = i+numMarios+numEnemies;
                    min = diffs[i];
                }
            }
            
            //cout<<"Index: "<<index<<" "<<(r/16)-2<<" "<<round((float)c/16)<<" "<<min<<" ";
            if(match >= 0){
                outputMap[(r/16)-2][c/16] = tileTypes[match];
                //cout<<tileNames[match]<<endl;
            }
            else{
                outputMap[(r/16)-2][c/16] = '-';
                //cout<<"No Match"<<endl;
            }
        }
        
        //outputMap[height][0] = to_string(mapLengthThusFar);
    }
    
    //loop through the image and compare against the tile types we have
    for(int r=32; r<224; r+=16){
        for(int c=index; c<256-(16-index); c+=16){
            vector<float> diffs(tileTypes.size()-numMarios-numEnemies, 0);
            
            for(int i=numMarios+numEnemies; i<tileTypes.size(); i++){
                cimg_library::CImg<float> tile = cimg_library::CImg<float>( ("Mario_Maps/Playthrough/Tiles/"+tileNames[i]).c_str() );
                tile.normalize();
                float totalDifferences=0;
                for(int y=0; y<16; y++){
                    for(int x=0; x<16; x++){
                        
                        totalDifferences+= abs(inputMap(c+x, r+y, 0, 0) - tile(x, y, 0, 0)) +
                                            abs(inputMap(c+x, r+y, 0, 1) - tile(x, y, 0, 1)) +
                                            abs(inputMap(c+x, r+y, 0, 2) - tile(x, y, 0, 2));
                    }
                }
                diffs[i-numMarios-numEnemies] = totalDifferences;
            }
            float min= 100;//diffs[0];
            int match=-1;
            for(int i=1; i<diffs.size(); i++){
                if(diffs[i] < min){
                    match = i+numMarios+numEnemies;
                    min = diffs[i];
                }
            }
            
            //cout<<"Index: "<<index<<" "<<(r/16)-2<<" "<<round((float)(c-index)/16)<<" "<<min<<" ";
            if(match >= 0){
                if(index >= 8){
                    outputMap[(r/16)-2][round((float)c/16)] = tileTypes[match];
                } else{
                    outputMap[(r/16)-2][(c-index)/16] = tileTypes[match];
                }
                //cout<<tileNames[match]<<endl;
            }
            else{
                if(index >= 8){
                    outputMap[(r/16)-2][round((float)c/16)] = '-';
                }
                else{
                    outputMap[(r/16)-2][(c-index)/16] = '-';
                }
                //cout<<"No Match"<<endl;
            }
            
            /*if(index >= 8 && outputMap[12][round((float)c/16)] == "."){
                outputMap[height][round((float)c/16)] = to_string(mapLengthThusFar);
                mapLengthThusFar++;
            } else if (index < 8 && outputMap[12][(c-index)/16] == "."){
                outputMap[height][(c-index)/16] = to_string(mapLengthThusFar);
                mapLengthThusFar++;
            }*/
        }
    }
    vector< tuple<int, int, float, char> > possibleMarioPositions (20, tuple<int, int, float, char>(-1, -1, 100, 'x')); //(row, column, difference), cap the size at 20
    //find the position of Mario in the section
    //min=9999999;
    //int marioPositionX=-1;
    //int marioPositionY=-1;
    for(int r=32; r<224; r++){
        for(int c=0; c<256-(16-index); c++){
            for(int i=0; i<numMarios; i++){
                cimg_library::CImg<float> tile = cimg_library::CImg<float>( ("Mario_Maps/Playthrough/Tiles/"+tileNames[i]).c_str() );
                tile.normalize();
                float totalDifferences=0;
                for(int y=0; y<16; y++){
                    for(int x=0; x<16; x++){
                        
                        totalDifferences+= abs(inputMap(c+x, r+y, 0, 0) - tile(x, y, 0, 0)) +
                                            abs(inputMap(c+x, r+y, 0, 1) - tile(x, y, 0, 1)) +
                                            abs(inputMap(c+x, r+y, 0, 2) - tile(x, y, 0, 2));
                    }
                }
                
                
                //if the new position has a lower cost than the last element in the vector,
                //insert the position into the correct position in the vector
                if(totalDifferences < get<2>(possibleMarioPositions.back())){
                    for(int p=0; p<possibleMarioPositions.size(); p++){
                        //cout<<r<<" "<<r/16-2<<" "<<c<<" "<<round(((float)c)/16.0)<<endl;
                        if(totalDifferences < get<2>(possibleMarioPositions[p]) &&
                                outputMap[r/16-2]
                                            [round(((float)c)/16.0)] =='-'){
                            possibleMarioPositions.pop_back();
                            possibleMarioPositions.insert(possibleMarioPositions.begin()+p, tuple<int, int, int, char>(r, c, totalDifferences, tileTypes[i]));
                        }
                    }
                }
             }
        }
    }
    while(get<0>(possibleMarioPositions.back()) == -1)
        possibleMarioPositions.pop_back();
    
    int marioPosition=-1;
    for(int p=0; p<possibleMarioPositions.size(); p++){
        
        bool correctPosition=true;
        for(int bt=numMarios+numEnemies+numSolids; bt<tileNames.size(); bt++){
            cimg_library::CImg<float> tile = cimg_library::CImg<float>( ("Mario_Maps/Playthrough/Tiles/"+tileNames[bt]).c_str() );
            tile.normalize();
            float totalDifferences=0;
            for(int y=0; y<16; y++){
                for(int x=0; x<16; x++){
                    
                    totalDifferences+=
                            abs(inputMap(get<1>(possibleMarioPositions[p])+x, get<0>(possibleMarioPositions[p])+y, 0, 0) - tile(x, y, 0, 0)) +
                            abs(inputMap(get<1>(possibleMarioPositions[p])+x, get<0>(possibleMarioPositions[p])+y, 0, 1) - tile(x, y, 0, 1)) +
                            abs(inputMap(get<1>(possibleMarioPositions[p])+x, get<0>(possibleMarioPositions[p])+y, 0, 2) - tile(x, y, 0, 2));
                }
            }
            
            if(totalDifferences < get<2>(possibleMarioPositions[p])){
                correctPosition=false;
                break;
            }
            
        }
        
        if(correctPosition){
        
            //cout<<round(((float)get<0>(possibleMarioPositions[p])/16.0)-2)<<" "<<round((float)get<1>(possibleMarioPositions[p])/16.0)<<endl;
            outputMap[round(((float)get<0>(possibleMarioPositions[p])/16.0)-2)][round((float)get<1>(possibleMarioPositions[p])/16.0)]
                = get<3>(possibleMarioPositions[p]);
            break;
        }
    }
    
    //find enemy positions
    int enemyPositionX=-1;
    int enemyPositionY=-1;
    for(int r=32; r<224; r++){
        for(int c=0; c<256-(16-index); c++){
            int match=-1;
            min =70;
            for(int i=numMarios; i<numMarios+numEnemies; i++){
                cimg_library::CImg<float> tile = cimg_library::CImg<float>( ("Mario_Maps/Playthrough/Tiles/"+tileNames[i]).c_str() );
                tile.normalize();
                float totalDifferences=0;
                for(int y=0; y<16; y++){
                    for(int x=0; x<16; x++){
                        
                        totalDifferences+= abs(inputMap(c+x, r+y, 0, 0) - tile(x, y, 0, 0)) +
                        abs(inputMap(c+x, r+y, 0, 1) - tile(x, y, 0, 1)) +
                        abs(inputMap(c+x, r+y, 0, 2) - tile(x, y, 0, 2));
                    }
                }
                if(totalDifferences < min){
                    match = i;
                    min = totalDifferences;
                }
            }
            //cout<<r/16-2<<" "<<c/16<<" "<<min<<endl;

            if(match >= 0 && index < 8)
                outputMap[round(((float)r/16.0)-2)][round((float)(c-index)/16.0)] = tileTypes[match];
            else if(match >= 0 && index >= 8)
                outputMap[round(((float)r/16.0)-2)][round((float)c/16.0)] = tileTypes[match];

        }
    }
    
    ofstream output(mapName);
    for(int i=0; i<outputMap.size(); i++){
        for(int j=0; j<outputMap[i].size(); j++){
            //cout<<outputMap[i][j]<<'\t';
            output<<outputMap[i][j];
        }
        if(i<outputMap.size()-1){
            output<<endl;
            //cout<<endl;
        }
        //cout<<endl;
    }
    output.close();
    return;
}


vector< vector< char> > simplifyMapRepresentation(string mapName, bool simplifyEnemies, bool simplifyPipes, bool simplifyBlocks, bool removePath){
    
    vector<char> enemyTypes     = {'e', 'g', 'k', 'K', 'l', 't', 'E'};
    
    vector<char> leftPipeTypes  = {'p', '[', '{', 'd', '<'};
    vector<char> rightPipeTypes = {'P', ']', '}', 'D', '>'};
    vector<char> enemyPipeTypes = {'-', 'V'};
    
    vector<char> blockTypes     = {'B', 'M', 'O', 'S', 'H'};
    vector<char> emptyTypes     = {'-', '*'};
    vector<char> questionTypes  = {'?', 'Q'};
    vector<char> solidTypes     = {'#', 'X'};
    
    
    vector<vector< char> > map;
    
    ifstream input(mapName);
    string line;
    while(getline(input, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i] != '\t' && line[i] != '\0' && line[i] != '\n' && line[i] != '\r')
                row.push_back(line[i]);
        }
        map.push_back(row);
    }
    
    //if(map.size() > 14)
    //    map.resize(14);
    
    
    for(int r=0; r<map.size(); r++){
        for(int c=0; c<map[r].size(); c++){
            if(simplifyEnemies){
                if(find(enemyTypes.begin()+1, enemyTypes.end(), map[r][c]) != enemyTypes.end()){
                    map[r][c] = enemyTypes[0];
                    continue;
                }
            }
            
            if(simplifyPipes){
                if(find(leftPipeTypes.begin()+1, leftPipeTypes.end(), map[r][c]) != leftPipeTypes.end()){
                    map[r][c] = leftPipeTypes[0];
                    continue;
                } else if(find(rightPipeTypes.begin()+1, rightPipeTypes.end(), map[r][c]) != rightPipeTypes.end()){
                    map[r][c] = rightPipeTypes[0];
                    continue;
                } else if(find(enemyPipeTypes.begin()+1, enemyPipeTypes.end(), map[r][c]) != enemyPipeTypes.end()){
                    map[r][c] = enemyPipeTypes[0];
                    continue;
                }
            }
            
            if(simplifyBlocks){
                if(find(blockTypes.begin()+1, blockTypes.end(), map[r][c]) != blockTypes.end()){
                    map[r][c] = blockTypes[0];
                    continue;
                }
                if(find(questionTypes.begin()+1, questionTypes.end(), map[r][c]) != questionTypes.end()){
                    map[r][c] = questionTypes[0];
                    continue;
                }
                if(find(solidTypes.begin()+1, solidTypes.end(), map[r][c]) != solidTypes.end()){
                    map[r][c] = solidTypes[0];
                    continue;
                }
            }
            
            if(removePath){
                if(find(emptyTypes.begin()+1, emptyTypes.end(), map[r][c]) != emptyTypes.end()){
                    map[r][c] = emptyTypes[0];
                    continue;
                }
            }
            
        }
    }
    
    return map;
}


//use the distance metrics developed for clustering
int computeDistanceBetweenSegments(vector<vector< char > > segment_1, vector<vector< char > > segment_2, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), vector<char> tileTypes){
    
    float distance = f(segment_1, segment_2, vector<vector<float>>(), tileTypes, (int)segment_1[0].size(), (int)segment_1.size());
    
    return distance;
}

//take a frame and extract the nXm window around mario
void extractWindowFromFrame(string frameName, string outputWindowName, int windowWidth, int windowHeight){
    
    ifstream inputFrame(frameName);
    vector<string> frame;
    
    string line;
    while(getline(inputFrame, line))
        frame.push_back(line);
    
    
    vector<vector< char > > window;
    window.resize(windowHeight);
    for(int i=0; i<windowHeight; i++)
        window[i].resize(windowWidth, '&');
    
    //find mario's position
    int marioX=-1;
    int marioY=-1;
    for(int i=0; i<frame.size(); i++){
        for(int j=0; j<frame[i].size(); j++){
            if(frame[i][j] == 'x'){
                marioX = j;
                marioY = i;
                i=(int)frame.size();
                break;
            }
        }
    }
    

    
    //populate the window with tiles surrounding mario
    for(int i=marioY-((windowHeight-1)/2); i<=marioY+((windowHeight-1)/2); i++){
        for(int j=marioX-((windowWidth-1)/2); j<=marioX+((windowWidth-1)/2); j++){
            if(i<0 || j<0 || i >= frame.size() || j >= frame[0].size()){
                //if out of range, leave the tile as an "unknown"
                continue;
            }
            //otherwise, populate the window
            window[i-(marioY-((windowHeight-1)/2))][j-(marioX-((windowWidth-1)/2))] = frame[i][j];
        }
    }
    
    //print the window to a file
    fstream outputWindow;
    outputWindow.open(outputWindowName, fstream::in | fstream::app);
    
    for(int i=0; i<windowHeight; i++){
        for(int j =0; j<windowWidth; j++){
            outputWindow<<window[i][j];
        }
        outputWindow<<endl;
    }
}

//Call Summerville's agent to annotate a map with it's path, leave off the '.txt' of the map name
bool MarioPlayabilityPath(string mapName){
    
    FILE *p;
    char buff[5000];
    string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'"+mapName+"\',\'2\')\"";
    //system(systemCall.c_str());
    cout<<"Done getting path"<<endl;
    p = popen(systemCall.c_str(), "r");
    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;
    
    if(buff[0] == '0'){
        pclose(p);
        return false;
    } else{
        pclose(p);
        return true;
    }
}

bool MarioPlayabilityPath(vector<vector<char> > Map){
    
    ofstream map("tmpMap.txt");
    for(int i=0; i<Map.size(); i++){
        for(int j=0; j<Map[i].size(); j++){
            map<<Map[i][j];
        }
        map<<endl;
    }
    map.close();
    
    
    FILE *p;
    char buff[5000];
    
    string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'tmpMap\',\'2\')\"";
    p = popen(systemCall.c_str(), "r");
    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;
    
    if(buff[0] == '0'){
        pclose(p);
        return false;
    } else{
        pclose(p);
        return true;
    }
}

//using the 'Direct' distance metric, find how the two provided sections overlap (structurally) in order to find how Mario is moving
int findOverlap(string mapSection1, string mapSection2){
    
    
    //read in the map sections
    ifstream in1(mapSection1);
    ifstream in2(mapSection2);
    
    string line1, line2;
    vector<vector<char> > map1;
    vector<vector<char> > map2;
    
    while(getline(in1, line1) && getline(in2, line2)){
        
        vector<char> row1;
        vector<char> row2;
        for(int i=0; i<line1.length(); i++){
            if(line1[i] != '\n' && line1[i] != '\t' && line1[i] != '\r' && line1[i] != '\0')
                row1.push_back(line1[i]);
        }
        for(int i=0; i<line2.length(); i++){
            if(line2[i] != '\n' && line2[i] != '\t' && line2[i] != '\r' && line2[i] != '\0')
                row2.push_back(line2[i]);
        }
        map1.push_back(row1);
        map2.push_back(row2);
    }
    in1.close();
    in2.close();
    
    //offset for the overlap
    
    //full overlap
    float minDifference = Direct(map1, map2, min((int)map1[0].size(), (int)map2[0].size()), (int)map1.size());

    int overlapPosition=0;
    
    
    for(int i=1; i<=3; i++){
        float difference=0;
        for(int h=0; h<map1.size(); h++){
            for(int w=0; w<map1[0].size(); w++){
                
                if((w+i) < map1[h].size() && w < map2[h].size() && map1[h][w+i] != map2[h][w] && map1[h][w+i] != 'x' && map2[h][w] != 'x')
                    difference++;
            }
        }
        
        if(difference < minDifference){
            minDifference = difference;
            overlapPosition = i;
        }
    }
    
    for(int i=1; i<=3; i++){
        float difference=0;
        for(int h=0; h<map2.size(); h++){
            for(int w=0; w<map2[0].size(); w++){
                
                if((w+i) < map2[h].size() && w < map1[h].size() && map2[h][w+i] != map1[h][w] && map2[h][w+i] != 'x' && map1[h][w] != 'x')
                    difference++;
            }
        }
        
        if(difference < minDifference){
            minDifference = difference;
            overlapPosition = i*-1;
        }
    }
    
    //find the mario position and movement
    int mario1y=-1;
    int mario1x=-1;
    for(int h=0; h<map1.size(); h++){
        for(int w=0; w<map1[h].size(); w++){
            if(map1[h][w] == 'x'){
                mario1y = h;
                mario1x = w;
            }
        }
    }
    
    int mario2y=-1;
    int mario2x=-1;
    for(int h=0; h<map2.size(); h++){
        for(int w=0; w<map2[h].size(); w++){
            if(map2[h][w] == 'x'){
                mario2y = h;
                mario2x = w;
            }
        }
    }
    
    //cout<<minDifference<<" "<<overlapPosition<<" ("<<mario1y<<", "<<mario1x<<") -> ("<<mario2y<<", "<<mario2x+overlapPosition<<") " ;
    
    int movementY = mario2y - mario1y;
    int movementX = (mario2x+overlapPosition) - mario1x;
    
    //cout<<movementY<<" "<<movementX<<endl;
    
    //Movement categorizations
    //
    //  8 1 2
    //
    //  7 0 3
    //
    //  6 5 4
    
    if(movementX == 0 && movementY < 0){     //move up
        return 1;
    } else if(movementX > 0 && movementY < 0){      //move up-right (diagonal)
        return 2;
    } else if(movementX > 0 && movementY == 0){     //move right
        return 3;
    } else if(movementX > 0 && movementY > 0){      //move down-right (diagonal)
        return 4;
    } else if(movementX == 0 && movementY > 0){     //move down
        return 5;
    } else if(movementX < 0 && movementY > 0){      //move down-left (diagonal)
        return 6;
    } else if(movementX < 0 && movementY == 0){     //move left
        return 7;
    } else if(movementX < 0 && movementY < 0){      //move up-left (diagonal)
        return 8;
    } else {                                        //no movement
        return 0;
    }
}

vector< vector< double> > trainProbabilisticFSM(vector<int> trace){
    
    vector<vector< double> > transitionTable;
    transitionTable.resize(9);
    for(int i=0; i<transitionTable.size(); i++)
        transitionTable[i].resize(9, 1.0/9.0);
    
    vector<int> totals(9, 1);
    
    for(int i=1; i<trace.size(); i++){
        
        totals[trace[i]]++;
        transitionTable[trace[i-1]][trace[i]]++;
    }

    /*for(int i=0; i<transitionTable.size(); i++){
        for(int j=0; j<transitionTable[i].size(); j++){
            transitionTable[i][j] = transitionTable[i][j]/totals[i];
            cout<<transitionTable[i][j]<<"\t";
        }

    }*/
    
    return transitionTable;
}

vector< vector< double> > trainProbabilisticFSM(vector<int> trace, vector<vector<vector<char> > > medoids, int numSections, string sectionName){
    
    
    //transition from (a board state and previous movement) -> (next movement)
    vector< vector< double> >transitionTable;
    transitionTable.resize(9*medoids.size());
    for(int i=0; i<transitionTable.size(); i++)
        transitionTable[i].resize(9, 1.0/9.0);
    
    vector<int> totals(9*medoids.size(), 1);
    
    for(int j=0; j<trace.size(); j++){
        
        //get the movements
        int movement_1;
        if(j==0)
            movement_1 =0;
        else
            movement_1 = trace[j-1];
        int movement_2 = trace[j];
        
        //read in the section================================================================================
        string line;
        ifstream in(sectionName+to_string(j+460)+".txt");
        vector<vector<char> > map;
        while(getline(in, line)){
            vector<char> row;
            for(int i=0; i<line.length(); i++){
                if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                    row.push_back(line[i]);
            }
            
            map.push_back(row);
        }
        in.close();
        
        //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int i=0; i<medoids[0].size(); i++)
            window[i].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=-1;
        int marioY=-1;
        for(int i=0; i<map.size(); i++){
            for(int k=0; k<map[i].size(); k++){
                if(map[i][k] == 'x'){
                    marioX = k;
                    marioY = i;
                    i=(int)map.size();
                    break;
                }
            }
        }
        
        //populate the window with tiles surrounding mario
        for(int i=marioY-(((int)medoids[0].size()-1)/2); i<=marioY+((medoids[0].size()-1)/2); i++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(i<0 || k<0 || i >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                window[i-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[i][k];
            }
        }
        
        
        //determine which medoid the current frame is most similar to=========================================
        int medoid =-1;
        float difference = 100000000;
        for(int i=0; i<medoids.size(); i++){
            float diff = Direct(window, medoids[i], (int)medoids[i][0].size(), (int)medoids[i].size());
            if(diff < difference){
                difference = diff;
                medoid = i;
            }
        }
        
        // increment the totals and transition matrices
        totals[movement_1+medoid*9]++;
        transitionTable[movement_1+medoid*9][movement_2]++;     //move + medoid*(# of possible moves)
    }
    
    /*for(int r=0; r < transitionTable.size(); r++){
        for(int c=0; c<transitionTable[r].size(); c++){
            if(totals[r] == 0)
                totals[r]++;
            transitionTable[r][c] = transitionTable[r][c]/totals[r];
        }
    }*/
    
    return transitionTable;
}


vector< vector< double> > trainProbabilisticFSMOnlyStates(vector<int> trace, vector<vector<vector<char> > > medoids, int numSections, string sectionName, int start){
    //transition from (a board state and previous movement) -> (next movement)
    vector< vector< double> >transitionTable;
    transitionTable.resize(medoids.size());
    for(int i=0; i<transitionTable.size(); i++)
        transitionTable[i].resize(9, 1.0/9.0);
    
    vector<int> totals(medoids.size(), 1);
    
    for(int j=0; j<trace.size(); j++){
        
        //get the movements
        int movement_1;
        if(j==0)
            movement_1 =0;
        else
            movement_1 = trace[j-1];

        
        //read in the section================================================================================
        string line;
        ifstream in(sectionName+to_string(j+start)+".txt");
        vector<vector<char> > map;
        while(getline(in, line)){
            vector<char> row;
            for(int i=0; i<line.length(); i++){
                if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                    row.push_back(line[i]);
            }
            
            map.push_back(row);
        }
        in.close();
        
        //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int i=0; i<medoids[0].size(); i++)
            window[i].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=-1;
        int marioY=-1;
        for(int i=0; i<map.size(); i++){
            for(int k=0; k<map[i].size(); k++){
                if(map[i][k] == 'x'){
                    marioX = k;
                    marioY = i;
                    i=(int)map.size();
                    break;
                }
            }
        }
        
        //populate the window with tiles surrounding mario
        for(int i=marioY-(((int)medoids[0].size()-1)/2); i<=marioY+((medoids[0].size()-1)/2); i++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(i<0 || k<0 || i >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                window[i-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[i][k];
            }
        }
        
        
        //determine which medoid the current frame is most similar to=========================================
        int medoid =-1;
        float difference = 100000000;
        for(int i=0; i<medoids.size(); i++){
            float diff = Direct(window, medoids[i], (int)medoids[i][0].size(), (int)medoids[i].size());
            if(diff < difference){
                difference = diff;
                medoid = i;
            }
        }
        
        // increment the totals and transition matrices
        totals[medoid]++;
        transitionTable[medoid][movement_1]++;     //move + medoid*(# of possible moves)
    }
    
    /*for(int r=0; r < transitionTable.size(); r++){
        for(int c=0; c<transitionTable[r].size(); c++){
            if(totals[r] == 0)
                totals[r]++;
            transitionTable[r][c] = transitionTable[r][c]/totals[r];
        }
    }*/
    
    return transitionTable;
}

long double getPathLikelihood(vector<vector< double> > transitionTable, string pathFile){    //A* agent path likelihood (moves only)
    
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    
    long double log_likelihood=0;
    for(int i=2; i<trace.size(); i++){
        //cout<<log_likelihood<<endl;
    
        int movementY_1 = trace[i-1].first - trace[i-2].first;
        int movementX_1 = trace[i-1].second - trace[i-2].second;
    
        int movementY_2 = trace[i].first - trace[i-1].first;
        int movementX_2 = trace[i].second - trace[i-1].second;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
    
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }
        
        
        int movement_2 = -1;
        if(movementX_2 == 0 && movementY_2 < 0){            //move up
            movement_2 = 1;
        } else if(movementX_2 > 0 && movementY_2 < 0){      //move up-right (diagonal)
            movement_2 = 2;
        } else if(movementX_2 > 0 && movementY_2 == 0){     //move right
            movement_2 = 3;
        } else if(movementX_2 > 0 && movementY_2 > 0){      //move down-right (diagonal)
            movement_2 = 4;
        } else if(movementX_2 == 0 && movementY_2 > 0){     //move down
            movement_2 = 5;
        } else if(movementX_2 < 0 && movementY_2 > 0){      //move down-left (diagonal)
            movement_2 = 6;
        } else if(movementX_2 < 0 && movementY_2 == 0){     //move left
            movement_2 = 7;
        } else if(movementX_2 < 0 && movementY_2 < 0){      //move up-left (diagonal)
            movement_2 = 8;
        } else {                                            //no movement
            movement_2 = 0;
        }
    
        log_likelihood+=log(transitionTable[movement_1][movement_2]);
        //cout<<transitionTable[movement_1][movement_2]<<" "<<log_likelihood<<endl;
    }
    //cout<<log_likelihood<<endl;
    return exp(log_likelihood/trace.size());
}

// A* agent path likelihood with moves and window
long double getPathLikelihood(vector<vector< double> > transitionTable, string pathFile, vector<vector<vector< char> > > medoids, string mapName){
    
    long double log_likelihood =0;
    
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    in.close();
    
    //read in the map
    in.open(mapName);
    vector<vector<char> > map;
    while(getline(in, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                row.push_back(line[i]);
        }
        
        map.push_back(row);
    }
    in.close();
    
    
    for(int i=1; i<trace.size(); i++){
        
        
        //Get the movements======================================================================
        
        int movementY_1;
        int movementX_1;
        if(i==1){
            movementX_1 = 0;
            movementY_1 = 0;
        } else{
            movementY_1 = trace[i-1].first - trace[i-2].first;
            movementX_1 = trace[i-1].second - trace[i-2].second;
        }
            
        
        int movementY_2 = trace[i].first - trace[i-1].first;
        int movementX_2 = trace[i].second - trace[i-1].second;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }
        
        
        int movement_2 = -1;
        if(movementX_2 == 0 && movementY_2 < 0){            //move up
            movement_2 = 1;
        } else if(movementX_2 > 0 && movementY_2 < 0){      //move up-right (diagonal)
            movement_2 = 2;
        } else if(movementX_2 > 0 && movementY_2 == 0){     //move right
            movement_2 = 3;
        } else if(movementX_2 > 0 && movementY_2 > 0){      //move down-right (diagonal)
            movement_2 = 4;
        } else if(movementX_2 == 0 && movementY_2 > 0){     //move down
            movement_2 = 5;
        } else if(movementX_2 < 0 && movementY_2 > 0){      //move down-left (diagonal)
            movement_2 = 6;
        } else if(movementX_2 < 0 && movementY_2 == 0){     //move left
            movement_2 = 7;
        } else if(movementX_2 < 0 && movementY_2 < 0){      //move up-left (diagonal)
            movement_2 = 8;
        } else {                                            //no movement
            movement_2 = 0;
        }
        
       //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int j=0; j<medoids[0].size(); j++)
            window[j].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=trace[i].second;
        int marioY=trace[i].first;
        
        
        //populate the window with tiles surrounding mario
        for(int j=marioY-(((int)medoids[0].size()-1)/2); j<=marioY+((medoids[0].size()-1)/2); j++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(j<0 || k<0 || j >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                if(map[j][k] != 'x')
                    window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[j][k];
                else
                    window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = '-';
            }
        }
        
        
        //determine which medoid the current frame is most similar to
        int medoid =-1;
        float difference = 100000000;
        for(int j=0; j<medoids.size(); j++){
            float diff = Direct(window, medoids[j], (int)medoids[j][0].size(), (int)medoids[j].size());
            if(diff < difference){
                difference = diff;
                medoid = j;
            }
        }
        
        //cout<<movement_1<<" "<<movement_2<<" "<<medoid<<" "<<transitionTable[movement_1+medoid*9][movement_2]<<endl;
        log_likelihood+=log(transitionTable[movement_1+medoid*9][movement_2]);
    }
    
    return exp(log_likelihood/(trace.size()-1));
}

long double getPathLikelihoodOnlyStates(vector<vector< double> > transitionTable, string pathFile, vector<vector<vector< char> > > medoids, string mapName){
    long double log_likelihood =0;
    
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    in.close();
    
    //read in the map
    in.open(mapName);
    vector<vector<char> > map;
    while(getline(in, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                row.push_back(line[i]);
        }
        
        map.push_back(row);
    }
    in.close();
    
    
    for(int i=1; i<trace.size(); i++){
        
        
        //Get the movements======================================================================
        
        int movementY_1;
        int movementX_1;
        if(i==1){
            movementX_1 = 0;
            movementY_1 = 0;
        } else{
            movementY_1 = trace[i].first - trace[i-1].first;
            movementX_1 = trace[i].second - trace[i-1].second;
        }
        
        
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }
        
        
        //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int j=0; j<medoids[0].size(); j++)
            window[j].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=trace[i].second;
        int marioY=trace[i].first;
        
        
        //populate the window with tiles surrounding mario
        for(int j=marioY-(((int)medoids[0].size()-1)/2); j<=marioY+((medoids[0].size()-1)/2); j++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(j<0 || k<0 || j >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                if(map[j][k] != 'x')
                    window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[j][k];
                else
                    window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = '-';
            }
        }
        
        
        //determine which medoid the current frame is most similar to
        int medoid =-1;
        float difference = 100000000;
        for(int j=0; j<medoids.size(); j++){
            float diff = Direct(window, medoids[j], (int)medoids[j][0].size(), (int)medoids[j].size());
            if(diff < difference){
                difference = diff;
                medoid = j;
            }
        }
        
        //cout<<movement_1<<" "<<movement_2<<" "<<medoid<<" "<<transitionTable[movement_1+medoid*9][movement_2]<<endl;
        log_likelihood+=log(transitionTable[medoid][movement_1]);
    }
    
    return exp(log_likelihood/(trace.size()-1));
}

long double getPathLikelihood(vector<vector< double> > transitionTable, vector<int> movements){ //player path likelihood (moves only)
    long double log_likelihood=0;
    for(int i=1; i<movements.size(); i++){
        //cout<<log_likelihood<<endl;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        int movement_1 = movements[i-1];
        int movement_2 = movements[i];
        
        log_likelihood+=log(transitionTable[movement_1][movement_2]);
        //cout<<transitionTable[movement_1][movement_2]<<" "<<log_likelihood<<endl;
    }
    //cout<<log_likelihood<<endl;
    return exp(log_likelihood/movements.size());
}

//player path likelihood with moves and window
long double getPathLikelihood(vector<vector< double> > transitionTable, vector<int> movements, vector<vector<vector< char> > > medoids, string sectionName){
    
    long double log_likelihood =0;
    
    for(int i=0; i<movements.size(); i++){
        
        //get the movement======================================================================================
        int movement_1 = 0;
        if(i!=0)
            movement_1 = movements[i-1];
        int movement_2 = movements[i];
        
        //categorize the section================================================================================
        //read in the section
        string line;
        ifstream in(sectionName+to_string(460+i)+".txt");
        vector<vector<char> > map;
        while(getline(in, line)){
            vector<char> row;
            for(int i=0; i<line.length(); i++){
                if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                    row.push_back(line[i]);
            }
            
            map.push_back(row);
        }
        in.close();
        
        //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int j=0; j<medoids[0].size(); j++)
            window[j].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=-1;
        int marioY=-1;
        for(int j=0; j<map.size(); j++){
            for(int k=0; k<map[j].size(); k++){
                if(map[j][k] == 'x'){
                    marioX = k;
                    marioY = j;
                    j=(int)map.size();
                    break;
                }
            }
        }
        
        //populate the window with tiles surrounding mario
        for(int j=marioY-(((int)medoids[0].size()-1)/2); j<=marioY+((medoids[0].size()-1)/2); j++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(j<0 || k<0 || j >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[j][k];
            }
        }
        
        
        //determine which medoid the current frame is most similar to
        int medoid =-1;
        float difference = 100000000;
        for(int j=0; j<medoids.size(); j++){
            float diff = Direct(window, medoids[j], (int)medoids[j][0].size(), (int)medoids[j].size());
            if(diff < difference){
                difference = diff;
                medoid = j;
            }
        }
        
        //cout<<movement_1<<" "<<movement_2<<" "<<medoid<<" "<<transitionTable[movement_1+medoid*9][movement_2]<<endl;
        log_likelihood+=log(transitionTable[movement_1+medoid*9][movement_2]);
    }
    
    return exp(log_likelihood/movements.size());
}

long double getPathLikelihoodOnlyStates(vector<vector< double> > transitionTable, vector<int> movements, vector<vector<vector< char> > > medoids, string sectionName){
    long double log_likelihood =0;
    
    for(int i=0; i<movements.size(); i++){
        
        //get the movement======================================================================================
        int movement = movements[i];
        
        //categorize the section================================================================================
        //read in the section
        string line;
        ifstream in(sectionName+to_string(460+i)+".txt");
        vector<vector<char> > map;
        while(getline(in, line)){
            vector<char> row;
            for(int i=0; i<line.length(); i++){
                if(line[i] != '\n' && line[i] != '\t' && line[i] != '\r' && line[i] != '\0')
                    row.push_back(line[i]);
            }
            
            map.push_back(row);
        }
        in.close();
        
        //find the mario position and window==================================================================
        vector<vector< char > > window;
        window.resize(medoids[0].size());
        for(int j=0; j<medoids[0].size(); j++)
            window[j].resize(medoids[0][0].size(), '&');
        
        //find mario's position
        int marioX=-1;
        int marioY=-1;
        for(int j=0; j<map.size(); j++){
            for(int k=0; k<map[j].size(); k++){
                if(map[j][k] == 'x'){
                    marioX = k;
                    marioY = j;
                    j=(int)map.size();
                    break;
                }
            }
        }
        
        //populate the window with tiles surrounding mario
        for(int j=marioY-(((int)medoids[0].size()-1)/2); j<=marioY+((medoids[0].size()-1)/2); j++){
            for(int k=marioX-(((int)medoids[0][0].size()-1)/2); k<=marioX+((medoids[0][0].size()-1)/2); k++){
                if(j<0 || k<0 || j >= map.size() || k >= map[0].size()){
                    //if out of range, leave the tile as an "unknown"
                    continue;
                }
                //otherwise, populate the window
                window[j-(marioY-((medoids[0].size()-1)/2))][k-(marioX-((medoids[0][0].size()-1)/2))] = map[j][k];
            }
        }
        
        
        //determine which medoid the current frame is most similar to
        int medoid =-1;
        float difference = 100000000;
        for(int j=0; j<medoids.size(); j++){
            float diff = Direct(window, medoids[j], (int)medoids[j][0].size(), (int)medoids[j].size());
            if(diff < difference){
                difference = diff;
                medoid = j;
            }
        }
        
        //cout<<movement_1<<" "<<movement_2<<" "<<medoid<<" "<<transitionTable[movement_1+medoid*9][movement_2]<<endl;
        log_likelihood+=log(transitionTable[medoid][movement]);
    }
    
    return exp(log_likelihood/movements.size());
    
}

vector<int> countMovements(vector<double> actionDistribution, string pathFile){
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    
    vector<int> agentActionDistribution(actionDistribution.size(), 0);
    
    for(int i=1; i<trace.size(); i++){
        int movementY_1 = trace[i].first - trace[i-1].first;
        int movementX_1 = trace[i].second - trace[i-1].second;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }
        
        if(movement_1 > 0){
            agentActionDistribution[movement_1-1]++;
            agentActionDistribution[agentActionDistribution.size()-1]++;
        }
        else
            cout<<"No Movement"<<endl;
    }
    return agentActionDistribution;
}


long double getDistanceFromDistribution(vector<vector< double> > transitionTabel, vector<double> actionDistribution, string pathFile){
    
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    
    vector<double> agentActionDistribution(actionDistribution.size(), 0);

    for(int i=1; i<trace.size(); i++){
        int movementY_1 = trace[i].first - trace[i-1].first;
        int movementX_1 = trace[i].second - trace[i-1].second;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }

        if(movement_1 > 0){
            agentActionDistribution[movement_1-1]++;
            agentActionDistribution[agentActionDistribution.size()-1]++;
        }
        else
            cout<<"No Movement"<<endl;
    }

    /*for(int i=0; i<agentActionDistribution.size(); i++){
        cout<<agentActionDistribution[i]<<" ";
    }
    cout<<endl;
    */for(int i=0; i<agentActionDistribution.size(); i++){
        agentActionDistribution[i] = agentActionDistribution[i]/agentActionDistribution[agentActionDistribution.size()-1];
    }

    /*for(int i=0; i<agentActionDistribution.size(); i++){
        cout<<agentActionDistribution[i]<<" ";
    }
    cout<<endl;
    */long double dist=0;
    
    //cout<<agentActionDistribution.size()<<endl;
    for(int i=0; i<agentActionDistribution.size()-1; i++){
        //cout<<agentActionDistribution[i]<<" "<<actionDistribution[i]<<" "<<agentActionDistribution[i] - actionDistribution[i]<<" "<<pow((agentActionDistribution[i] - actionDistribution[i]),2)<<endl;
        dist+= pow((agentActionDistribution[i] - actionDistribution[i]),2);
    }
    dist = sqrt(dist);
    
    return dist;
}

long double getDistanceFromDistribution(vector<vector< double> > transitionTabel, vector<double> actionDistribution, vector<double> agentActionDistribution){
    
    long double dist=0;
    
    for(int i=0; i<agentActionDistribution.size()-1; i++)
        dist+= pow((agentActionDistribution[i] - actionDistribution[i]),2);

    dist = sqrt(dist);
    
    return dist;
}

vector<int> getMoveDiffsFromDistribution(vector<vector< double> > transitionTabel, vector<double> actionDistribution, string pathFile){
    
    ifstream in(pathFile);
    
    string line;
    vector<pair<int, int> > trace;
    while(getline(in, line)){
        stringstream s;
        s.str(line);
        string word;
        pair<int, int> pos;
        
        getline(s, word, '\t');
        pos.first = stoi(word);
        getline(s, word, '\t');
        pos.second = stoi(word);
        
        trace.push_back(pos);
    }
    
    vector<int> agentActionDistribution(actionDistribution.size(), 0);
    
    for(int i=1; i<trace.size(); i++){
        int movementY_1 = trace[i].first - trace[i-1].first;
        int movementX_1 = trace[i].second - trace[i-1].second;
        
        //Movement categorizations
        //
        //  8 1 2
        //
        //  7 0 3
        //
        //  6 5 4
        
        // no movement
        int movement_1 = -1;
        if(movementX_1 == 0 && movementY_1 < 0){            //move up
            movement_1 = 1;
        } else if(movementX_1 > 0 && movementY_1 < 0){      //move up-right (diagonal)
            movement_1 = 2;
        } else if(movementX_1 > 0 && movementY_1 == 0){     //move right
            movement_1 = 3;
        } else if(movementX_1 > 0 && movementY_1 > 0){      //move down-right (diagonal)
            movement_1 = 4;
        } else if(movementX_1 == 0 && movementY_1 > 0){     //move down
            movement_1 = 5;
        } else if(movementX_1 < 0 && movementY_1 > 0){      //move down-left (diagonal)
            movement_1 = 6;
        } else if(movementX_1 < 0 && movementY_1 == 0){     //move left
            movement_1 = 7;
        } else if(movementX_1 < 0 && movementY_1 < 0){      //move up-left (diagonal)
            movement_1 = 8;
        } else {                                            //no movement
            movement_1 = 0;
        }
        
        if(movement_1 > 0){
            agentActionDistribution[movement_1-1]++;
            agentActionDistribution[agentActionDistribution.size()-1]++;
        }
        else
            cout<<"No Movement"<<endl;
    }
    

     for(int i=0; i<agentActionDistribution.size(); i++){
         agentActionDistribution[i] = agentActionDistribution[i]/agentActionDistribution[agentActionDistribution.size()-1];
     }
   
    for(int i=0; i<agentActionDistribution.size()-1; i++){
        agentActionDistribution[i] = agentActionDistribution[i] - actionDistribution[i];
    }
    
    return agentActionDistribution;
}

vector<vector< char> > readInPlayerPathLayer(string trainingDataName){
    
    vector<vector<char>> layer;
    
    ifstream in(trainingDataName);
    string line;
    while(getline(in, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i] == '\n' || line[i] == '\t' || line[i] == '\0' || line[i] == '\r')
                continue;
            else if(line[i] != 'x')
                row.push_back('-');
            else
                row.push_back('x');
        }
        layer.push_back(row);
    }
    return layer;
}

vector<vector< char> > readInStructuralLayer(string trainingDataName){
 
    vector<vector<char>> layer;
    
    ifstream in(trainingDataName);
    string line;
    while(getline(in, line)){
        vector<char> row;
        for(int i=0; i<line.length(); i++){
            if(line[i] == '\n' || line[i] == '\t' || line[i] == '\0' || line[i] == '\r')
                continue;
            else
                row.push_back(line[i]);
        }
        layer.push_back(row);
    }
    return layer;
}

vector<pair<int, int> > setNetworkStructure(int ns, bool mainLayer){
    
    vector<pair<int, int> > dependencyNodes;
    
    if(ns >= 0 && !mainLayer){
        dependencyNodes.push_back(pair<int, int>(0, 0));
    }
    if(ns >= 1){
        dependencyNodes.push_back(pair<int, int>(0, -1));
    }
    if(ns >= 2){
        dependencyNodes.push_back(pair<int, int>(+1, 0));
    }
    if(ns >= 3){
        dependencyNodes.push_back(pair<int, int>(+1, -1));
    }
    
    return dependencyNodes;
}

vector<char> FindTileTypes(vector<vector<vector<char> > >& maps){
    
    vector<char> tileTypes;
    
    for(int i=0; i<maps.size(); i++){
        for(int j=0; j<maps[i].size(); j++){
            for(int k=0; k<maps[i][j].size(); k++){
                bool seen = false;
                for (int l = 0; l < tileTypes.size(); l++){
                    if (maps[i][j][k] == tileTypes[l]){
                        seen = true;
                        break;
                    }
                }
                if (!seen)
                    tileTypes.push_back(maps[i][j][k]);
            }
        }
    }
    return tileTypes;
}

int getTileID(char tile, vector<char> tileTypes){
    
    for(int i=0; i<tileTypes.size(); i++){
        if(tileTypes[i] == tile)
            return i;
    }
    return -1;
}


vector<vector<double> > TrainMultiLayerModel(vector<vector<vector< vector< char> > > >& layers, vector< vector< pair<int, int> > >& dependencyNodes,
                                             vector<vector<char > >& layerTileTypes){
    
    vector< vector< double > > trainedProbabilityDistribution;
    int sizeOfTable=1;
    
    for(int i=0; i<layers.size(); i++){
        sizeOfTable *= pow(layerTileTypes[i].size(), dependencyNodes[i].size());
    }
    
    
    
    trainedProbabilityDistribution.resize(sizeOfTable);
    for(int i=0; i<sizeOfTable; i++)
        trainedProbabilityDistribution[i].resize(layerTileTypes[0].size()+1, 0);
    
    for(int map = 0; map<layers[0].size(); map++){
 
        for(int y=(int)layers[0][map].size()-1; y>=0; y--){
            for(int x=0; x<layers[0][map][y].size(); x++){
                
                //cout<<y<<" "<<x<<endl;
                
                int rowIndex=0;
                for(int l=0; l<layers.size(); l++){
                    
                    int exponent = 0;
                    int layerRowIndex=0;
                    for(int d=0; d<dependencyNodes[l].size(); d++){
                        
                        int yIndex=y+dependencyNodes[l][d].first;
                        int xIndex=x+dependencyNodes[l][d].second;
                        char currentTile = 'S';

                        if( yIndex >= 0 && yIndex < layers[l][map].size() && xIndex >= 0 && xIndex < layers[l][map][yIndex].size()){
                            currentTile = layers[l][map][yIndex][xIndex];
                        }
                        
                        layerRowIndex += getTileID(currentTile, layerTileTypes[l])*pow(layerTileTypes[l].size(), exponent);
                        exponent++;
                        //cout<<currentTile<<" ";
                    }
                    for(int k=0; k<l; k++)
                        layerRowIndex *= pow(layerTileTypes[k].size(), dependencyNodes[k].size());
                    //cout<<endl;
                    rowIndex += layerRowIndex;
                }
                
                //cout<<rowIndex<<endl;
                trainedProbabilityDistribution[rowIndex][getTileID(layers[0][map][y][x], layerTileTypes[0])]++;
                trainedProbabilityDistribution[rowIndex][layerTileTypes[0].size()]++;
                
            }
        }
    }
    
    for(int r=0; r<trainedProbabilityDistribution.size(); r++){
        for(int c=0; c<trainedProbabilityDistribution[r].size()-1; c++){
            
            if(trainedProbabilityDistribution[r][layerTileTypes[0].size()] == 0)
                trainedProbabilityDistribution[r][c] = 0;
            else
                trainedProbabilityDistribution[r][c]/=trainedProbabilityDistribution[r][layerTileTypes[0].size()];
        }
    }
    
    
    
    return trainedProbabilityDistribution;
}


int SampleTileWithPlayerPath(vector<vector<char> >& layerTileTypes, vector<vector<vector<pair<int, int> > > >& dependencyNodes,
                             vector<vector<vector<char> > >& layers, vector<vector<double> >& probabilities,
                             int depth, int y, int x, int lookAhead, vector<int>& whichNS){
    
    int id = -1;
    double selector;
    
    random_device rd;
    
    
    mt19937 e2(rd());
    
    uniform_real_distribution<> dist(0, 1);
    
    
    vector<char> PT(layerTileTypes[0].size());	//create a copy of the possible tiles vector to use for changes
    vector<double> Probs;
    for (int i = 0; i<layerTileTypes[0].size(); i++)
        PT[i] = layerTileTypes[0][i];
    
    while (!PT.empty()){		//while there is still a possible tile
        //check if this state has been observed before
        int rowIndex=0;
        int ggg=0;
        for(int l=0; l<layers.size(); l++){
            
            int exponent = 0;
            int layerRowIndex=0;
            
            for(int d=0; d<dependencyNodes[l][whichNS[l]].size(); d++){
                
                int yIndex=y+dependencyNodes[l][whichNS[l]][d].first;
                int xIndex=x+dependencyNodes[l][whichNS[l]][d].second;
                char currentTile = 'S';
                
                //cout<<yIndex<<" "<<xIndex<<": ";
                
                if( yIndex >= 0 && yIndex < layers[l].size() && xIndex >= 0 && xIndex < layers[l][yIndex].size()){
                    currentTile = layers[l][yIndex][xIndex];
                }
                
                layerRowIndex += getTileID(currentTile, layerTileTypes[l])*pow(layerTileTypes[l].size(), exponent);
                exponent++;
                
            }
            //This will need to be generalized if additional layers are to be used
            for(int k=0; k<l; k++){
                layerRowIndex *= pow(layerTileTypes[k].size(), dependencyNodes[k][whichNS[k]].size());
            }
            
            rowIndex += layerRowIndex;
        }
        
        bool observed = true;
        if(probabilities[rowIndex][probabilities[rowIndex].size()-1] == 0){
            observed = false;
            //cout<<"Unobserved"<<endl;
        }
        
        if (!observed && depth != lookAhead){		//if this state hasnt been observed and this is NOT the top level in the lookahead
            return -2;
        }
        else if (!observed && depth == lookAhead){	//if we havent observed this state, and it IS the first step, then break out of
            break;                                          //of the loop, and we will fallback to a simpler network structure
        }
        else if (observed){                                      //if we have observed this state, then generate the next tile
            double Norm = 0;
            Probs.resize(PT.size());
            for (int i = 0; i< PT.size(); i++)	//reset the value of Probs
                Probs[i] = 0;
            
            for (int a = 0; a < PT.size(); a++){
                for (int b = 0; b < layerTileTypes[0].size(); b++){
                    if (PT[a] == layerTileTypes[0][b]){
                        Probs[a] = probabilities[rowIndex][b];
                        Norm += probabilities[rowIndex][b];
                    }
                }
            }
            for (int i = 0; i<PT.size(); i++){						//renormalize the probabilities
                Probs[i] /= Norm;
            }
            
            //PICK THE TILE
            double PR = 0;
            int tracker = 0;
            selector = dist(e2);
            //cout<<selector<<endl;
            for (int a = 0; a<PT.size(); a++){
                PR = PR + Probs[a];
                if (Probs[a]>0)
                tracker = a;
                if (selector <= PR){
                    id = a;
                    break;
                }
            }
            
            if(id==-1){
                cout<<"Broken"<<endl;
            }
            
            if (y < layers[0].size() && x < layers[0][0].size())
                layers[0][y][x] = PT[id];
            
            //call the GenTile function to generate the next tile
            //set to a test variable, so we can check the validity
            //of the next state
            int test = 1;
            
            if (depth > 0){
                if (x < layers[0][0].size()-1){
                    vector<int> NS;
                    for(int i=0; i<layers.size(); i++){
                        if(i == 0)
                            NS.push_back(whichNS[0]);
                        else
                            NS.push_back(0);
                    }
                    test = SampleTileWithPlayerPath(layerTileTypes, dependencyNodes, layers, probabilities, depth-1, y, x+1, lookAhead, whichNS);
                }
            }
            
            //if the next state isnt valid, remove the tile chosen in this state and try a different one
            if (test == -2){
                int j = 0;
                if (id < PT.size()){
                    for (vector<char>::iterator i = PT.begin(); i != PT.end(); i++){
                        if (PT.at(j) == PT.at(id)){
                            PT.erase(i);
                            break;
                        }
                        j++;
                    }
                } else
                PT.clear();
            } else{
                for (int i = 0; i < layerTileTypes[0].size(); i++){
                    if (PT[id] == layerTileTypes[0][i])
                        return i;
                }
            }
        }
    }									//end of loop
    
    if (depth == lookAhead){				//if no valid options on top level, return error code 100
        return -1;
    }
    else{
        return -2;				//if no valid options on sub level, return error.
    }							//forces a different choice on upper levels.
}

vector<vector<char> > SampleMultiLayerModelWithPath(vector<vector<vector<double> > >& probabilities,
                                                    vector<vector<vector<pair<int, int> > > >& networkStructures, vector<vector<char> >& playerPathLayer,
                                                    vector<vector<char> >& layerTileTypes, int lookAhead, string outputMapName){
    
    for(int i=0; i<probabilities[0].size(); i++){
        for(int j=0; j<probabilities[0][i].size(); j++){
            if(probabilities[0][i][j]!=0)
                cout<<i<<" "<<j<<endl;
        }
    }
    
    
    int height = (int)playerPathLayer.size();
    int width = (int)playerPathLayer[0].size();
    
    vector<vector<char> > outputMap;
    outputMap.resize(playerPathLayer.size());
    for(int  i=0; i<outputMap.size(); i++)
        outputMap[i].resize(playerPathLayer[i].size(), '.');
    
    //for each position
    for(int y=height-1; y>=0; y--){
        for(int x=0; x<width; x++){
            
            //sample the current tile and the lookahead positions
            //try to generate a tile with the first config matrix
            
            vector<vector<vector< char> > > layers;
            layers.push_back(outputMap);
            layers.push_back(playerPathLayer);
            
            int id=-1;
            int ns=0;
            
            while(id==-1 && ns < (int)networkStructures[0].size()){
                
                vector<int> NS;
                for(int i=0; i<networkStructures.size(); i++){
                    if(i == 0)
                        NS.push_back(ns);
                    else
                        NS.push_back(0);
                }
                
                id = SampleTileWithPlayerPath(layerTileTypes, networkStructures, layers, probabilities[ns], lookAhead, y, x, lookAhead, NS);
                ns++;
            }
            if(id ==-1){
                cout<<"Error sampling tile, setting to \'.\'\n";
                outputMap[y][x] = 'X';
            } else{
                outputMap[y][x] = layerTileTypes[0][id];
            }
        }
    }
    
    ofstream out(outputMapName);
    for(int y=0; y<outputMap.size(); y++){
        for(int x=0; x<outputMap[y].size(); x++){
            out<<outputMap[y][x];
        }
        out<<"\n";
    }
    out.close();
    
    return outputMap;
}

vector<vector<char> > SampleMultiLayerModel(vector<vector<vector<double> > >& probabilities,
                                            vector<vector<vector<pair<int, int> > > >& networkStructures, vector<vector<vector<char> > >& otherLayers,
                                            vector<vector<char> >& layerTileTypes, int lookAhead, string outputMapName){
    
    int height = (int)otherLayers[0].size();
    int width = 210;//(int)otherLayers[0][0].size();
    
    vector<vector<char> > outputMap;
    outputMap.resize(height);
    for(int  i=0; i<outputMap.size(); i++)
        outputMap[i].resize(width, '.');
    
    //for each position
    for(int y=height-1; y>=0; y--){
        for(int x=0; x<width; x++){
            
            //sample the current tile and the lookahead positions
            //try to generate a tile with the first config matrix
            
            vector<vector<vector< char> > > layers;
            layers.push_back(outputMap);
            for(int i=0; i<otherLayers.size(); i++)
                layers.push_back(otherLayers[i]);
            
            int id=-1;
            int ns=0;
            
            while(id==-1 && ns < (int)networkStructures[0].size()){
                
                vector<int> NS;
                for(int i=0; i<networkStructures.size(); i++){
                    if(i == 0)
                    NS.push_back(ns);
                    else
                    NS.push_back(0);
                }
                
                id = SampleTileWithPlayerPath(layerTileTypes, networkStructures, layers, probabilities[ns], lookAhead, y, x, lookAhead, NS);
                ns++;
            }
            if(id ==-1){
                cout<<"Error sampling tile, setting to \' \'\n";
                outputMap[y][x] = ' ';
            } else{
                outputMap[y][x] = layerTileTypes[0][id];
            }
        }
    }
    
    ofstream out(outputMapName);
    for(int y=0; y<outputMap.size(); y++){
        for(int x=0; x<outputMap[y].size(); x++){
            out<<outputMap[y][x];
        }
        out<<"\n";
    }
    out.close();
    
    return outputMap;
}

void RegenerateTopLevelSectionWithSmoothingAndLayers(vector<vector<char> >& map, vector<vector<vector<double> > >& probabilities,
                                                                      vector<vector<vector<pair<int, int> > > >& networkStructures,
                                                                      vector<vector<vector< char> > >& otherLayers, vector<vector<char> >& layerTileTypes,
                                                                      int lookAhead, int xStart, int yStart, int xEnd, int yEnd){
    
    //initialize the set of possible tiles for this level
    vector<char> PossibleTiles(layerTileTypes[0].size()-1);
    for (int i = 0; i < layerTileTypes[0].size()-1; i++)
        PossibleTiles[i]=layerTileTypes[0][i+1];
    
    //initialize the section of the map to be generated
    
    
    //make a copy fo the map so that the lookahead does not destroy the
    //sections fo the map that have already been sampled
    vector<vector< char> > tmpMap = map;
    
/*    for(int i=0; i<map.size(); i++){
        vector<char> tmpRow;
        for (int j=0; j<map[i].size(); j++) {
            tmpRow.push_back(map[i][j]);
        }
        tmpMap.push_back(tmpRow);
        tmpRow.clear();
    }
*/
    
    for(int h=yStart; h<=yEnd; h++){
        for (int w=xStart; w<xEnd; w++) {
            map[h][w] = '.';
        }
    }
    
    for (int y=yEnd; y>=yStart; y--){
        for (int x = xStart; x <map[y].size(); x++){
            //try to generate a tile with the first config matrix
            
            vector<vector<vector< char> > > layers;
            layers.push_back(tmpMap);
            for(int i=0; i<otherLayers.size(); i++)
                layers.push_back(otherLayers[i]);
                    
            int id=-1;
            int ns=0;
                    
            while(id==-1 && ns < (int)networkStructures[0].size()){
                        
                vector<int> NS;
                for(int i=0; i<networkStructures.size(); i++){
                    if(i == 0)
                        NS.push_back(ns);
                    else
                        NS.push_back(0);
                }
                        
                id = SampleTileWithPlayerPath(layerTileTypes, networkStructures, layers, probabilities[ns], lookAhead, y, x, lookAhead, NS);
                ns++;
            }
            if(id ==-1){
                cout<<"Error resampling tile, setting to \' \'\n";
                tmpMap[y][x] = ' ';
            } else{
                tmpMap[y][x] = layerTileTypes[0][id];
            }
            
            if(tmpMap[y][x] == map[y][x]){  //if the sampled tile matches the tile already in the map, move onto the next row
                break;
            } else   //if they're not the same, keep sampling in that row
                map[y][x] = tmpMap[y][x];
        }
    }
    tmpMap.clear();
}

void LayersDemo(int lookAhead, int trainingMaps, string mapName, string trainingFolderName, string outputFolderName, int toGenerate){
    
    vector<vector<vector<vector< char >>>> layers;
    
    vector<vector<vector< char >>> playerPathLayers;
    vector<vector<vector< char >>> structuralLayers;
    vector<vector<vector< char >>> rowSplitLayers;
    
    for(int i=1; i<=25; i++){
        playerPathLayers.push_back(readInPlayerPathLayer(trainingFolderName+mapName+to_string(i)+"_Annotated_Path.txt"));
        
        structuralLayers.push_back(readInStructuralLayer(trainingFolderName+mapName+to_string(i)+".txt"));
        
        rowSplitLayers.push_back(readInStructuralLayer("RowSplitsLayer_2.txt"));
    }
    
    layers.push_back(structuralLayers);
    layers.push_back(playerPathLayers);
    layers.push_back(rowSplitLayers);
    
    
    vector<vector<pair<int, int>>> networkStructures_3;
    networkStructures_3.push_back(setNetworkStructure(3, true));
    networkStructures_3.push_back(setNetworkStructure(0, false));
    networkStructures_3.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_2;
    networkStructures_2.push_back(setNetworkStructure(2, true));
    networkStructures_2.push_back(setNetworkStructure(0, false));
    networkStructures_2.push_back(setNetworkStructure(0, false));
    
    vector<vector<pair<int, int>>> networkStructures_1;
    networkStructures_1.push_back(setNetworkStructure(1, true));
    networkStructures_1.push_back(setNetworkStructure(0, false));
    networkStructures_1.push_back(setNetworkStructure(0, false));
    
    
    vector<vector< char> > layerTileTypes;
    layerTileTypes.push_back(FindTileTypes(structuralLayers));
    layerTileTypes.push_back(FindTileTypes(playerPathLayers));
    layerTileTypes.push_back(FindTileTypes(rowSplitLayers));
    
    layerTileTypes[0].insert(layerTileTypes[0].begin(), 'S');
    
    for(int i=0; i<layerTileTypes.size(); i++){
        for(int j=0; j<layerTileTypes[i].size(); j++){
            cout<<layerTileTypes[i][j]<<" ";
        }
        cout<<endl;
    }
    
    vector<vector<vector<double> > > probabilities;
    
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_3, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_2, layerTileTypes));
    probabilities.push_back(TrainMultiLayerModel(layers, networkStructures_1, layerTileTypes));
    
    
    vector<vector<vector<pair<int, int> > > > networkStructures;
    vector<vector<pair<int, int> > > ns_1;
    ns_1.push_back(networkStructures_3[0]);
    ns_1.push_back(networkStructures_2[0]);
    ns_1.push_back(networkStructures_1[0]);
    
    vector<vector<pair<int, int> > > ns_2;
    ns_2.push_back(networkStructures_3[1]);
    ns_2.push_back(networkStructures_2[1]);
    ns_2.push_back(networkStructures_1[1]);
    
    vector<vector<pair<int, int> > > ns_3;
    ns_3.push_back(networkStructures_3[1]);
    ns_3.push_back(networkStructures_2[1]);
    ns_3.push_back(networkStructures_1[1]);
    
    networkStructures.push_back(ns_1);
    networkStructures.push_back(ns_2);
    networkStructures.push_back(ns_3);
    
    
    vector<vector<vector<char> > > otherLayers;
    //otherLayers.push_back(playerPathLayers[1]);
    otherLayers.push_back(readInPlayerPathLayer("Annotated_Path_2.txt"));
    //otherLayers.push_back(readInStructuralLayer("RowSplitsLayer_Test.txt"));
    otherLayers.push_back(rowSplitLayers[0]);
    
    #if defined(_WIN32)
        _mkdir(outputFolderName);
    #else
        mkdir(outputFolderName.c_str(), 0777);
    #endif
    
    //attempt to sample maps satisfying the contraints
    ConstraintChecker cc= ConstraintChecker();
    
    for(int i=0; i<toGenerate; i++){
        
        //violation detection resampling
        vector<vector< char> > map = SampleMultiLayerModel(probabilities, networkStructures, otherLayers, layerTileTypes, lookAhead, outputFolderName+mapName+to_string(i)+".txt");
        bool playable=false;
        int pos=-1;
        pos = MarioPlayabilityDistanceConstraint(map, 0, 0);
        if(pos >= map[0].size()-3){
            playable = true;
            pos = MarioNoIllFormedPipesConstraintPosition(map, 0, 0);
        }
        
        vector<int> ret1;
        if(playable && pos >= 0){
            ret1.push_back(max(pos-1, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+2, (int)map[0].size()-1));
            ret1.push_back(13);
        }
        
        if(!playable){
            ret1.push_back(max(pos-4, 0));
            ret1.push_back(0);
            ret1.push_back(min(pos+5, (int)map[0].size()-1));
            ret1.push_back(13);
        }
        
        //fix the level while there are sections to be fixed
        
        while(ret1.size()>0){
            for (int r=0; r<ret1.size(); r+=4){
                RegenerateTopLevelSectionWithSmoothingAndLayers(map, probabilities, networkStructures, otherLayers, layerTileTypes, lookAhead, ret1[r], ret1[r+1], ret1[r+2], ret1[r+3]);
            }
            pos=-1;
            ret1.clear();
            playable = false;
            pos = MarioPlayabilityDistanceConstraint(map, 0, 0);
            if(pos >= map[0].size()-3){
                playable=true;
                pos = MarioNoIllFormedPipesConstraintPosition(map, 0, 0);
            }
            
            if(playable && pos >= 0){
                ret1.push_back(max(pos-1, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+2, (int)map[0].size()-1));
                ret1.push_back(13);
            }
            
            if(!playable){
                ret1.push_back(max(pos-4, 0));
                ret1.push_back(0);
                ret1.push_back(min(pos+5, (int)map[0].size()-1));
                ret1.push_back(13);
            }
        }
        
        cout<<"Converting Map "<<i<<endl;
        
        //print the map
        ofstream out(outputFolderName+mapName+to_string(i)+".txt");
        for(int y=0; y< map.size(); y++){
            for(int x=0; x<map[y].size(); x++){
                out<<map[y][x];
            }
            out<<endl;
        }
        out.close();
        
        out.open(outputFolderName+mapName+to_string(i)+"_Provided_Path.txt");
        for(int y=0; y< map.size(); y++){
            for(int x=0; x<map[y].size(); x++){
                
                if(otherLayers[0][y][x] == 'x' && map[y][x] != 'B' && map[y][x] != '#' && map[y][x] != '?' && map[y][x] != 'p'
                   && map[y][x] != 'P' && map[y][x] != '[' && map[y][x] != ']' && map[y][x] != 'd' && map[y][x] != 'D'
                   && map[y][x] != '{' && map[y][x] != '}' && map[y][x] != 'M' && map[y][x] != '*' && map[y][x] != 'H'
                   && map[y][x] != '0' && map[y][x] != 'y' && map[y][x] != 'Y' && map[y][x] != 'c' && map[y][x] != 'C')
                    out<<"x";
                else
                    out<<map[y][x];
            }
            out<<endl;
        }
        out.close();
        
        MarioPlayabilityPath(outputFolderName+mapName+to_string(i));
        
        ConvertMarioMapToImage((int)map[0].size(), (int)map.size(), outputFolderName+mapName+to_string(i)+".txt", outputFolderName+mapName+to_string(i)+".bmp", "standard");
    }
}

vector<int> ComputeNumberOfUnreachableBlocks(string levelNameBase, int numberOfMaps){
    
    vector<int> counts;
    for(int i=0; i<numberOfMaps; i++){
        cout<<i<<": ";
        string levelName = levelNameBase+to_string(i);
        FILE *p;
        char buff[5000];
        string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'"+levelName+"\',\'3\')\"";
        p = popen(systemCall.c_str(), "r");
        while(fgets(buff, sizeof(buff), p)!=NULL)
            continue;
        int r = stoi(buff);
        cout<<r<<endl;
        counts.push_back(r);
    }
    return counts;
}

vector<int> DistanceBetweenPaths(string providedPathName, string levelNameBase, int numberOfLevels){
    
    vector<int> distances;
    
    ifstream in(providedPathName);
    string line;
    vector<vector<char> > providedPath;
    while(getline(in, line)){
        vector<char> row;
        for(int j=0; j<line.length(); j++){
            if(line[j]!= '\n' && line[j]!= '\t' && line[j]!= '\r' && line[j]!= '\0')
                row.push_back(line[j]);
        }
        providedPath.push_back((row));
    }
    
    for(int i=0; i<numberOfLevels; i++){
        ifstream in(levelNameBase+to_string(i)+"_Annotated_Path.txt");
        string line;
        vector<vector<char> > annotatedPath;
        while(getline(in, line)){
            vector<char> row;
            for(int j=0; j<line.length(); j++){
                if(line[j]!= '\n' && line[j]!= '\t' && line[j]!= '\r' && line[j]!= '\0')
                    row.push_back(line[j]);
            }
            annotatedPath.push_back((row));
        }
        if(annotatedPath.size() == 0)
            continue;
        int distance=0;
        for(int y=0; y<providedPath.size(); y++){
            for(int x=0; x<providedPath[y].size(); x++){
                
                if((providedPath[y][x] == 'x' && annotatedPath[y][x] !='x') ||
                   (providedPath[y][x] != 'x' && annotatedPath[y][x] =='x'))
                    distance++;
            }
        }
        cout<<i<<"\t"<<distance<<endl;
        distances.push_back(distance);
    }
    return distances;
}

pair<bool, bool> isSpringNecessary(string levelName){
    ifstream in(levelName);
    string line;
    vector<vector<char> > level;
    vector<vector<char> > levelWithoutSprings;
    bool containsSpring=false;
    while(getline(in, line)){
        vector<char> row;
        vector<char> row2;
        for(int j=0; j<line.length(); j++){
            if(line[j]!= '\n' && line[j]!= '\t' && line[j]!= '\r' && line[j]!= '\0'){
                row.push_back(line[j]);
                if(line[j] == 'y' || line[j] == 'Y'){
                    row2.push_back('-');
                    containsSpring=true;
                }
                else
                    row2.push_back(line[j]);
            }
        }
        level.push_back(row);
        levelWithoutSprings.push_back(row2);
    }
    
    bool P1 = MarioPlayabilityPath(level);
    bool P2 = MarioPlayabilityPath(levelWithoutSprings);
    
    //cout<<levelName<<" "<<P1<<" "<<P2<<endl;
    
    if(P1 && !P2)
        return pair<bool, bool>(containsSpring, true);
    else
        return pair<bool, bool>(containsSpring, false);
    
}

double ComputeFrechetDistance(vector<pair<float, float> > path_1, vector<pair<float, float> > path_2){
    
    vector<vector<double> > Dmatrix;
    for(int i=0; i<path_1.size(); i++){
        
        vector<double> row(path_2.size(), -1);
        Dmatrix.push_back(row);
    }
    
    //cout<<Dmatrix.size()<<" "<<Dmatrix[0].size()<<endl;
    //cout<<"============\n";
    
    return DynamicFrechetUpdate(path_1, path_2, path_1.size()-1, path_2.size()-1, Dmatrix);
}

double EuclideanDistance(pair<float,float> p1, pair<float,float> p2){
    return sqrt(pow(p1.first - p2.first, 2)+pow(p1.second - p2.second, 2));
}

double DynamicFrechetUpdate(vector<pair<float,float> > path_1,vector<pair<float,float> > path_2,int index_1,int index_2,vector<vector<double> > &Dmatrix){
    
    //cout<<Dmatrix.size()<<" "<<Dmatrix[0].size()<<endl;
    
    if(Dmatrix[index_1][index_2] > -1)
        return Dmatrix[index_1][index_2];
    
    else if(index_1 == 0 && index_2 == 0)
        Dmatrix[index_1][index_2] = EuclideanDistance(path_1[index_1], path_2[index_2]);
    
    else if(index_1 > 0 && index_2 == 0)
        Dmatrix[index_1][index_2] = max(DynamicFrechetUpdate(path_1, path_2, index_1-1, index_2, Dmatrix), EuclideanDistance(path_1[index_1], path_2[index_2]));
    
    else if(index_1 == 0 && index_2 > 0)
        Dmatrix[index_1][index_2] = max(DynamicFrechetUpdate(path_1, path_2, index_1, index_2-1, Dmatrix), EuclideanDistance(path_1[index_1], path_2[index_2]));
    
    else if(index_1 > 0 && index_2 > 0){
        double m = min(min(DynamicFrechetUpdate(path_1, path_2, index_1-1, index_2, Dmatrix),
                           DynamicFrechetUpdate(path_1, path_2, index_1, index_2-1, Dmatrix)),
                           DynamicFrechetUpdate(path_1, path_2, index_1-1, index_2-1, Dmatrix));
        double e = EuclideanDistance(path_1[index_1], path_2[index_2]);
        Dmatrix[index_1][index_2] = max(m, e);
    }
    else
        Dmatrix[index_1][index_2] = 999999999999;
    
    return Dmatrix[index_1][index_2];
}