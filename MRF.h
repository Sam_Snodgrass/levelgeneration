#pragma once
//#include "CommonStructs.h"
#include<vector>
#include<string>
using namespace std;

struct Totals_Pair{
	char type;
	int times_encountered;
};

struct Probabilities_Pair{
	char type;
	double probability;
};

class MRF{

public:
	vector< vector<vector<int> > > Dependency_Matrices;
	vector<int> Dependency_Complexity;
	vector<char> TileTypes;
    
    //needed for the non-parametric sampling
    
    //holds all the maps
    vector<vector<vector<char> > > Maps;
    void LoadMaps(string fileName, int NumberOfMaps);
    vector<float> FindInstances(vector<vector<char> > Prototype, vector<vector<float> > DistanceMatrix);
    vector<vector<char> > InitializeMap(int SizeOfSeed, int mapWidth, int mapHeight);
    vector<vector<char> > GetPrototype(int windowSize, vector<vector<char> > ToGenerate, int rowIndex, int colIndex);
    void SampleMap(int windowWidth, int mapHeight, int mapWidth, int SizeOfSeed, int RandomOffset, vector<vector<float> > DistanceMatrix);
    //----------------------------------------------------

    
    //needed for standard learning and generation
    
	//a 2 dimensional vector of totals for each Dependency Matrix
	vector< vector< vector<Totals_Pair> > > Totals;

	//a 2 dimensional vector of probabilities for each Dependency Matrix
	vector< vector< vector<Probabilities_Pair> > > Probabilities;

	//instantiate a MRF model of order "Dependency_Matrix"
	MRF(int Dependency_Matrix);
    MRF();

	//add a new dependency matrix to the set of dependency matrix
	void SetDependencyMatrix(int Dependency_Matrix);

	//given a file, extract the different tipe types
	void FindTileTypes(string MapName);
	//given a file name base and a number of files, extract the unique tiles
	void FindAllTileTypes(string MapNameBase, int NumberOfMaps);

	//after dependency matrices and tiles are set, initialize the totals and probabilities vectors to the proper sizes
	void InitializeDistributions();

	//given a file, determine the tile configuration at each tile, and increment the appropriate columns
	void SetTotals(string MapName, int Dependency_Matrix);
	void SetAllTotals(string MapNameBase, int Dependency_Matrix, int NumberOfMaps);

    //combines all the fallback matrices of a given complexity into one probability matrix
    vector<vector<Probabilities_Pair>> CombineFallBack(int ComplexityOfInitial, vector<vector< int >> InitialDependencyMatrix, vector<vector<vector< int >>> FallbackMatrices, vector<vector<vector< Probabilities_Pair >>> FallbackProbs);
    
    //smooth the ToSmooth probability table using the Smoother probability table
    vector<vector<Probabilities_Pair>> SmoothMatrix(vector<vector<Totals_Pair>> ToSmooth, vector<vector<Probabilities_Pair>> Smoother);
    //Propogate smoothing to the top
    vector<vector<Probabilities_Pair>> SmoothAll();
    
    //helper function to resize the raw distribution since it does not need to be combined with any others before smoothing
    vector<vector<Probabilities_Pair>> ConvertRawToProperSize(int NumberOfConfigurations);
    
	//convert the combination of tiles into a unique id for that configuration
	int GetID(long long config_id, float NumberOfTypes);
    
    //extract the configuration from the computed ID
    string GetConfigFromID(long long config_id, float NumberOfTypes, int Dependency_Complexity);
    
    //given a string, the main dep matrix, and the fallback dep matrix, find the fallback string config
    string GetFallbackStringFromFullString(string full_config, vector< vector<int> > Full_Dependency, vector< vector<int> >Fallback_Dependency);
    
	//convert the observed totals into probabilities
	void SetProbabilities(int Dependency_Matrix);

	//sample from the probability distribution in order to create a new map
	void GenerateLevel(string MapName, int width, int height, int Threshold, int Dependency_Matrix);
    
    //computes the likelihood of the tiles in the neighborhood aroudn the given position
    float ComputeNeighborhoodLikelihood(int x, int y, vector<vector< char> > Map);
    long double NewComputeNeighborhoodLikelihood(int x, int y, vector< vector<char> > Map);
    
    //compute the likelihood of the entire map
    float MapLikelihood(vector<vector<char> > Map);
private:


};