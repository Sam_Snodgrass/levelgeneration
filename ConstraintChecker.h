//
//  ConstraintChecker.h
//  PCG
//
//  Created by Snodgrass,Sam on 1/6/16.
//  Copyright (c) 2016 Snodgrass,Sam. All rights reserved.
//

#ifndef PCG_ConstraintChecker_h
#define PCG_ConstraintChecker_h

#include <string>
#include <vector>
using namespace std;

class ConstraintChecker{
    
    
public:
    bool CheckConstraintFullMap(string mapName, float min, float max, bool (*f)(string, float, float));
    
    // Mario Constraints (Automatically applicable to full maps)
    // Can be applied to partially generated maps, if map matrix is augmented
    /*bool MarioPlayabilityConstraint(string mapName, float min, float max);
    bool MarioNoIllFormedPipesConstraint(vector<vector< char> > map, float min, float max);
    bool MarioNumberOfPipesConstraint(vector<vector< char> > map, float min, float max);
    bool MarioNumberOfGapsConstraint(vector<vector< char> > map, float min, float max);
    bool MarioNumberOfEnemiesConstraint(vector<vector< char> > Map, float min, float max);
    bool MarioLongestGapConstraint(vector<vector< char> > Map, float min, float max);
    bool MarioLinearityBoundConstraint(vector<vector< char> > Map, float min, float max);
    bool MarioLeniencyBoundConstraint(vector<vector< char> > Map, float min, float max);
    
    //Loderunner Constraints
    bool LoderunnerPlayabilityConstraint(vector<vector< char> > map, float min, float max);
    bool LoderunnerPlayabilityConstraint(string mapName, float min, float max);
    bool LoderunnerNumberOfTreasuresConstraint(vector<vector< char> > map, float min, float max);
    bool LoderunnerNumberOfGuardsConstraint(vector<vector< char> > map, float min, float max);
    bool LoderunnerDensityBoundConstraint(vector<vector< char> > map, float min, float max);
    
    //Kid Icarus Constraints
    bool KidIcarusPlayabilityConstraint(vector<vector< char> > map, float min, float max);
    bool KidIcarusPlayabilityConstraint(string mapName, float min, float max);
    bool KidIcarusNumberOfHazardsConstraint(vector<vector< char> > map, float min, float max);
    bool KidIcarusAveragePlatformSizeConstraint(vector<vector< char> > map, float min, float max);
    bool KidIcarusLongestGapConstraint(vector<vector< char> > map, float min, float max);
    */
    //Constraint Violation Detection
    vector< int> BinarySearchViolationDetection(vector<vector< char> > map, bool verticalSlices, int minMapSectionSize, float min, float max, bool (*f)(vector<vector< char> >, float, float));
    vector<int> SectionBySectionViolationDetection(vector<vector< char> > map, bool verticalSlices, int sectionSize, float min, float max, bool (*f)(vector<vector< char> >, float, float));
    vector<int> SlidingWindowViolationDetection(vector<vector< char> > map, bool verticalSlices, int sectionSize);
    vector<int> SlidingWindowViolationDetectionCounts(vector<vector< char> > map, bool verticalSlices, int sectionSize, float (*f)(vector<vector< char> >), bool high);
    
private:
    
};

// Mario Constraints (Automatically applicable to full maps)
// Can be applied to partially generated maps, if map matrix is augmented
bool MarioPlayabilityConstraint(string mapName, float min, float max);
bool MarioPlayabilityConstraint(vector<vector<char> > mapSection, float min, float max);
int MarioPlayabilityDistanceConstraint(string mapName, float min, float max);
int MarioPlayabilityDistanceConstraint(vector<vector<char> >& map, float min, float max);
bool MarioNoIllFormedPipesConstraint(vector<vector< char> > map, float min, float max);
int MarioNoIllFormedPipesConstraintPosition(vector<vector<char> >& map, float min, float max);
bool MarioNumberOfPipesConstraint(vector<vector< char> > map, float min, float max);
float MarioCountPipes(vector<vector<char> > map);
bool MarioNumberOfGapsConstraint(vector<vector< char> > map, float min, float max);
float MarioCountGaps(vector<vector<char> > map);
bool MarioNumberOfEnemiesConstraint(vector<vector< char> > Map, float min, float max);
float MarioCountEnemies(vector<vector<char> > map);
bool MarioLongestGapConstraint(vector<vector< char> > Map, float min, float max);
float MarioCountGapLength(vector<vector<char> > map);
bool MarioLinearityBoundConstraint(vector<vector< char> > Map, float min, float max);
bool MarioLeniencyBoundConstraint(vector<vector< char> > Map, float min, float max);
 
//Loderunner Constraints
bool LoderunnerPlayabilityConstraint(vector<vector< char> > map, float min, float max);
bool LoderunnerPlayabilityConstraint(string mapName, float min, float max);
bool LoderunnerNumberOfTreasuresConstraint(vector<vector< char> > map, float min, float max);
bool LoderunnerNumberOfGuardsConstraint(vector<vector< char> > map, float min, float max);
float LoderunnerDensityBoundConstraint(vector<vector< char> > map, float min, float max);
 
//Kid Icarus Constraints
bool KidIcarusPlayabilityConstraint(vector<vector< char> > map, float min, float max);
bool KidIcarusPlayabilityConstraint(string mapName, float min, float max);
bool KidIcarusPlayabilityConstraintSection(vector<vector< char> > map, float min, float max);
bool KidIcarusNumberOfHazardsConstraint(vector<vector< char> > map, float min, float max);
float KidIcarusNumberOfHazardsCount(vector<vector< char> > map);
bool KidIcarusAveragePlatformSizeConstraint(vector<vector< char> > map, float min, float max);
float KidIcarusAveragePlatformSizeCount(vector<vector< char> > map);
bool KidIcarusLongestGapConstraint(vector<vector< char> > map, float min, float max);
float KidIcarusLongestGapCount(vector<vector< char> > map);
 


int KidIcarusPlayabilitySection(vector<vector< char> > map);
int KidIcarusPlayability(vector<vector< char > > map);
double ComputeLinearity(vector<vector< char> > map);
double ComputeLeniency(vector<vector< char> > map);

#endif
