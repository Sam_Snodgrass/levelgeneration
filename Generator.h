#pragma once
#include "CommonStructs.h"
#include "ConstraintChecker.h"

class Generator{
    
public:
    int HeightOfLevel;
    int LengthOfLevel;
    int START;
    int BackTracks;
    int badPipes;
    Configuration Config;
    vector< char > TileTypes;
    vector< char > HierTileTypes;
    vector< int> NumberTilesWithEachMatrix;
    vector< vector< vector <float> > > Probabilities;
    vector< vector< vector <float> > > FBProbabilities;
    vector< vector< vector< vector <float> > > > HierProbabilities;
    vector< vector< vector< vector <float> > > > FBHierProbabilities;
    
    vector< vector< char> > Map;
    
    //used for generating the Hier tiles or the standard map if not doing Hier method
    void GenerateTopLevel(string filename);
    void GenerateTopLevelColumns(string filename);
    void GenerateBottomLevel(string filename, string HierMapFile, bool manualConversion);
    void GenerateLevel(string filename, string statsfile);
    string GetConfigFromID(long long config_id, float NumberOfTypes, int Dependency_Complexity);
    
    void CopyProbs(string ProbsFile, bool Hier, int WhichDepMat);
    void CopyConfig(Configuration ToCopy);
    void SetTileTypes(vector<char> Types, bool Hier);
    void SetLevelHeight(string SampleMap);
    Generator(int DesiredLength, int DesiredHeight);
    void ResizeVectors(bool Hier);
    void RegenerateTopLevelSection(string output, int xStart, int yStart, int xEnd, int yEnd);
    int RegenerateTopLevelSectionWithSmoothing(string output, int xStart, int yStart, int xEnd, int yEnd);
    int GenerateTopLevelSectionWithConstraints(string output, int xStart, int yStart, int xEnd, int yEnd);
    int GenerateTopLevelRowWithConstraints(string output, int sectionSize, char domain);
    void GenerateTopLevelWithConstraints(string filename, bool horizontal, int sectionSize, bool sectionBySection);
private:
    
    ConstraintChecker Enforcer;
    //returns the placement of a certain config within the matrix
    int GetID(int type, float NumTypes);
    int GenerateTopLevelTile(vector<char> PossibleTiles, int depth, int h, int w, int offset, vector< vector< int > > Method, int WhichDepMat);
    int RegenerateTopLevelTile(vector<vector<char> > &tmpMap, vector<char> PossibleTiles, int depth, int h, int w, int offset, vector< vector< int > > Method, int WhichDepMat);
    int GenerateBottomLevelTile(vector<char> PossibleTiles, int depth, int h, int w, vector<vector< int> > Method, vector< vector< char> > HierMap, int WhichDepMat, bool manualConversion);
};