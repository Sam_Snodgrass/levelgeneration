#pragma once
#include<vector>
#include<string>

using namespace std;

//holds a map image file
//able to convert the file to a standard tile array or a Hierarchical tile array
//stores the tilized to a file to be read by the rest of the application
class MapReader{
    
private:
    static const int SCREEN_BPP = 32;
    
    //converts the tile map to a Hierarchical model
    bool ConvertTileMapToHier(int width, int height, string InputMapName, string OutputMapName);
    
    //crop the maps in order to gather the correct blocks
    void CropMap(string InputMap, string OutputMap, int width, int height);
    
    //Following functions are used to classify Hier tiles--------------------------------------
    
    //classifies a chunk of the level as a Hier tile
    int ClassifyChunk(vector< vector<char> > Chunk, int width, int height);
    
    //classifies a chunk of ground as either gap or not
    int ClassifyGroundChunk(vector<char> Chunk, int width);
    
    //Loops through the ground chunk given
    //Tracks the biggest gap that appears in this chunk
    //If a gap larger than 1 tile appears, return false
    //only called on bottom groundChunks
    //id = 0
    bool isSolid(vector<char> Chunk, int width);
    
    //Returns true if gap larger than 1 appears
    //returns false otherwise
    //only called on bottom groundChunks
    //id = 1
    bool isGap(vector<char> Chunk, int width);
    
    //if there are more than width nonempty tiles, return false
    //id = 2
    bool isEmpty(vector< vector<char> > Chunk, int height, int width);
    
    //Looks for complete pP pairs.
    //Looks for incomplete pP pairs at the start and end of  chunk.
    //id = 3
    bool isPipe(vector< vector<char> > Chunk, int height, int width);
    
    //checks for ground tiles
    //checks if a chunk of ground tiles is taller than it is wide
    //makes some checks about height difference, which is
    //required so as not to classify slope chunks as pillars
    //id = 4
    bool isPillar(vector< vector<char> > Chunk, int height, int width);
    
    //checks for ground tiles
    //checks if a chunk of ground tiles at least as wide as it is tall
    //makes some checks about height difference, which is
    //required so as not to classify slope chunks as plateaus
    //id = 5
    bool isPlateau(vector< vector<char> > Chunk, int height, int width);
    
    //looks for platforms made of ?, #, and B tiles
    //if length at least 2, return true
    //id = 6
    bool isPlatform(vector< vector<char> > Chunk, int height, int width);
    
    //looks for chunk of groung tiles
    //checks if there is a significant slope
    //returns true if there is, and it is upward
    //id = 7
    bool isUpSlope(vector< vector<char> > Chunk, int height, int width);
    
    //looks for chunk of groung tiles
    //checks if there is a significant slope
    //returns true if there is, and it is downward
    //id = 8
    bool isDnSlope(vector< vector<char> > Chunk, int height, int width);
    
    //prints the tiled map, but with the chunks sectioned off for the Hier model
    void TryMapSplits(int width, int height, string InputMapFile, string Output);
    //end Hier tile functions ------------------------------------------
    
public:
    
    //default constructor/destructor
    MapReader();
    
    //collection of the different types of blocks that are possible
    //vector<Block> Types;
    
    //loops over all maps with a certain name structure and
    //converts them to the proper format using the ConvertMap function
    //int ConvertAllMapsToTiles(string InputMapNameBase, int NumberOfMaps);
    
    //convert the tile maps to a Hierarchical model using the clustered blocks
    void ConvertAllTileMapsToHier(string InputMapNameBase, string InputFolder, string OutputFolder, int NumberOfMaps, int width, int height, vector<vector<vector<char> > > Medoids, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), string Comparison_Function, vector< vector< float> > TileSimilarities, vector<char> types);
    
    //loops over all tile maps and converts them to the Hier model
    int ConvertAllTileMapsToHier(string InputMapNameBase, string OutputMapNameBase, int NumberofMaps, int width, int height);
    
    //takes all the blocks from the tile maps and put into 1 file
    //for clustering
    void GatherAllBlocks(string Folder, string InputMapNameBase, int NumberOfMaps, int width, int height);
};

