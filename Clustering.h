#pragma once
#include<vector>

using namespace std;

class Clustering{
    
public:
    
    Clustering();
    Clustering(int w, int h);
    
    //computes the differences between each block and stores the values in a file
    void ComputeDifferences(string BlockFile, string Output, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), vector< vector< float> > TileSimilarities, vector<char> types);
    void ComputeDifferencesWithWeights(string BlockFile, string Output, float(*f) (vector< vector< char > >, vector< vector< char> >, vector<char>, int, int, vector< vector< float> >), vector<char> types, vector<vector< float> > weights);
    void kMedoids(string DifferenceFileName, int k);
    void PrintMedoids(string BlockFile, string OutputFile);
    void RemoveDuplicates(string Folder, string FileName);
    
    //dimesnions of blocks
    int width;
    int height;
    
    //Medoids and cluster groups
    vector<int> Medoids;
    vector<vector<int> > Clusters;
    
    //the chunks in each cluster group
    vector<vector<vector<vector<char> > > > ClusteredBlocks;
    vector<vector<vector<char> > > MedoidBlocks;
    
};

//compares the blocks tile-by-tile
float Direct(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector< vector< float> > TileSimilarities, vector<char> types, int w, int h);

//compares the blocks tile-by-tile, but use similarity between tiles
float Similarity(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector< vector< float> > TileSimilarities, vector<char> types, int w, int h);

//compares the number of each type of tile in the blocks
float Count(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector< vector< float> > TileSimilarities, vector<char> types, int w, int h);

//compares the shapes in each block by doing an overlay
float Shape(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector< vector< float> > TileSimilarities, vector<char> types, int w, int h);

//compares the Markov distribution of the 2 blocks using the specified markov chain
float Markov(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector< vector< float> > TileSimilarities, vector<char> types, int w, int h);


//With Weights

//compares the blocks tile-by-tile
float Direct(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector<char> types, int w, int h, vector< vector< float> > weights);

//compares the number of each type of tile in the blocks
float Count(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector<char> types, int w, int h, vector< vector< float> > weights);

//compares the shapes in each block by doing an overlay
float Shape(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector<char> types, int w, int h, vector< vector< float> > weights);

//compares the Markov distribution of the 2 blocks using the specified markov chain
float Markov(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, vector<char> types, int w, int h, vector< vector< float> > weights);


//Direct distance between 2 chunks
float Direct(vector< vector< char> > PrototypeBlock, vector< vector< char> > BlockToCompare, int w, int h);




