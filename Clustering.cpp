#include "Clustering.h"
#include <fstream>
#include<string>
#include<sstream>
#include<iostream>
#include<time.h>

Clustering::Clustering(){
    this->width = 6;
    this->height = 6;
}

Clustering::Clustering(int w, int h){
    this->width = w;
    this->height = h;
}

//compare the blocks tile by tile (0 for same, 1 for different), and returns the average distance (divide by size of block)
float Direct(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector< vector< float> > TileSimilarities, vector<char> types, int width, int height){
    
    int DifferentTiles = 0;
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            if (Block_1[i][j] != Block_2[i][j])
            DifferentTiles++;
        }
    }
    
    return (float)DifferentTiles;
}

//compare the blocks tile by tile (0 for same, 1 for different), and returns the average distance (divide by size of block)
float Direct(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector<char> types, int width, int height, vector< vector< float> > weights){
    
    float DifferentTiles = 0;
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            if (Block_1[i][j] != Block_2[i][j])
                DifferentTiles+=1.0*weights[i][j];
        }
    }
    
    return DifferentTiles;
}

float Direct(vector< vector< char> > Block_1, vector< vector< char> > Block_2, int w, int h){
    float DifferentTiles = 0;
    
    for (int i = 0; i < h; i++){
        for (int j = 0; j < w; j++){
            if (Block_1[i][j] != Block_2[i][j] && Block_1[i][j] != 'x' && Block_2[i][j] != 'x')
                DifferentTiles++;
        }
    }
    
    return DifferentTiles;
}


//Related to Direct, but in addition to direct matches, if the tiles do NOT match, a similarity measure is used between the blocks
float Similarity(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector< vector< float> > TileSimilarities, vector< char> TileOrder, int width, int height){
    
    float Difference = 0;
    int index_1 = 0;
    int index_2 = 0;
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            //get the correct index for the tile types seen
            for (int t = 0; t < TileOrder.size(); t++){
                if (TileOrder.at(t) == Block_1.at(i).at(j))
                index_1 = t;
                if (TileOrder.at(t) == Block_2.at(i).at(j))
                index_2 = t;
            }
            //find the difference between the two tiles and add it to the total
            Difference += TileSimilarities.at(index_1).at(index_2);
        }
    }
    return Difference;
}

//Compare the total numbers of each tile type in the 2 blocks
float Count(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector< vector< float> > TileSimilarities, vector<char> types, int width, int height){
    
    float Difference = 0;
    
    //initialize counts vector to proper size and to all 0's
    vector< int> counts_1;
    vector< int> counts_2;
    
    counts_1.resize(types.size());
    counts_2.resize(types.size());
    
    for (int i = 0; i < types.size(); i++){
        counts_1.at(i) = 0;
        counts_2.at(i) = 0;
    }
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            for (int t = 0; t < types.size(); t++){
                if (Block_1.at(i).at(j) == types.at(t))
                counts_1.at(t)++;
                if (Block_2.at(i).at(j) == types.at(t))
                counts_2.at(t)++;
            }
        }
    }
    
    for (int i = 0; i < types.size(); i++){
        Difference += abs(counts_1.at(i) - counts_2.at(i));
    }
    
    return Difference/2.0;
}

//Compare the total numbers of each tile type in the 2 blocks
float Count(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector<char> types, int width, int height, vector<vector< float> > weights){
    
    float Difference = 0;
    
    //initialize counts vector to proper size and to all 0's
    vector< int> counts_1;
    vector< int> counts_2;
    
    counts_1.resize(types.size());
    counts_2.resize(types.size());
    
    for (int i = 0; i < types.size(); i++){
        counts_1.at(i) = 0;
        counts_2.at(i) = 0;
    }
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            for (int t = 0; t < types.size(); t++){
                if (Block_1.at(i).at(j) == types.at(t))
                    counts_1.at(t)++;
                if (Block_2.at(i).at(j) == types.at(t))
                    counts_2.at(t)++;
            }
        }
    }
    
    for (int i = 0; i < types.size(); i++){
        Difference += abs(counts_1.at(i) - counts_2.at(i));
    }
    
    return Difference/2.0;
}

//compares the shapes in each block by doing an overlay
float Shape(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector< vector< float> > TileSimilarities, vector<char> types, int width, int height){
    
    //I should just have an 'effectively empty' tile set
        // includes: empty, coins, enemies, others?
    
    //char EmptyTile = '-';
    
    vector<char> EmptyTiles{'-', 'o', 'k', 'K', 'g', 't', 'V', 'h', 'l', '|', 'X'};
    //vector<char> EnemyTiles{'k', 'K', 'g', 't', 'V', 'h', 'l'};
    //char EnemyTile = 'e';
    //track the position of the best match
    float maxMatch = 0;
    int overlap = 0;
    
    vector< vector< char> >TempBlock;
    
    int CurrentMatch = 0;
    //The immobile block
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            
            //push stuff onto the tempBlock for comparison
            vector<char> tmp;
            int Hstart = height -(i + 1);
            int Wstart = width - (j + 1);
            for (int k = 0; k < height; k++){
                for (int f = 0; f < width; f++){
                    
                    //if in bounds, check consistency
                    if(k >= Hstart && f >= Wstart){
                        bool block_1_empty = false;
                        bool block_2_empty = false;
                    
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_1[k][f]) != EmptyTiles.end())
                            block_1_empty = true;
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_2[k - Hstart][f - Wstart]) != EmptyTiles.end())
                            block_2_empty = true;
                        
                        if(block_1_empty == block_2_empty)
                            CurrentMatch++;
                    
                    
                    }
                    
                    /*                    if (k >= Hstart && f >= Wstart && (
                                (  (Block_1.at(k).at(f) == EmptyTile  || Block_1.at(k).at(f) == EnemyTile) &&
                                   (Block_2.at(k - Hstart).at(f - Wstart) == EmptyTile || Block_2.at(k - Hstart).at(f - Wstart) == EnemyTile)) ||
                                (  (Block_1.at(k).at(f) != EmptyTile && Block_1.at(k).at(f) != EnemyTile &&
                                    Block_2.at(k - Hstart).at(f - Wstart) != EmptyTile && Block_2.at(k - Hstart).at(f - Wstart) != EnemyTile))))
                    CurrentMatch++;*/
                }
            }
            
            if (CurrentMatch > maxMatch){
                maxMatch = CurrentMatch;
                overlap = (height - Hstart)*(width - Wstart);
            }
            CurrentMatch = 0;
        }
    }
    
    
    //flip the mobile and immobile blocks
    CurrentMatch = 0;
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            
            //push stuff onto the tempBlock for comparison
            vector<char> tmp;
            int Hstart = height - (i + 1);
            int Wstart = width - (j + 1);
            for (int k = 0; k < height; k++){
                for (int f = 0; f < width; f++){
                    
                    
                    //if in bounds, check consistency
                    if(k >= Hstart && f >= Wstart){
                        bool block_1_empty = false;
                        bool block_2_empty = false;
                        
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_2[k][f]) != EmptyTiles.end())
                            block_2_empty = true;
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_1[k - Hstart][f - Wstart]) != EmptyTiles.end())
                            block_1_empty = true;
                        
                        if(block_1_empty == block_2_empty)
                            CurrentMatch++;
                        
                        
                    }
                    
                    
                    
                    //if in bounds, check consistency
                    /*f (k >= Hstart && f >= Wstart && (
                                (  (Block_2.at(k).at(f) == EmptyTile  || Block_2.at(k).at(f) == EnemyTile) &&
                                   (Block_1.at(k - Hstart).at(f - Wstart) == EmptyTile || Block_1.at(k - Hstart).at(f - Wstart) == EnemyTile)) ||
                                (  (Block_2.at(k).at(f) != EmptyTile && Block_2.at(k).at(f) != EnemyTile &&
                                    Block_1.at(k - Hstart).at(f - Wstart) != EmptyTile && Block_1.at(k - Hstart).at(f - Wstart) != EnemyTile))))
                    CurrentMatch++;*/
                
                }
            }
            
            if (CurrentMatch > maxMatch){
                maxMatch = CurrentMatch;
                overlap = (height - Hstart)*(width - Wstart);
            }
            CurrentMatch = 0;
        }
    }
    
    return (width*height)*2 - (overlap + maxMatch);
}

//compares the shapes in each block by doing an overlay
float Shape(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector<char> types, int width, int height, vector<vector<float> > weights){
    
    //I should just have an 'effectively empty' tile set
    // includes: empty, coins, enemies, others?
    
    //char EmptyTile = '-';
    
    vector<char> EmptyTiles{'-', 'o', 'k', 'K', 'g', 't', 'V', 'h', 'l', '|', 'X'};
    //vector<char> EnemyTiles{'k', 'K', 'g', 't', 'V', 'h', 'l'};
    //char EnemyTile = 'e';
    //track the position of the best match
    float maxMatch = 0;
    int overlap = 0;
    
    vector< vector< char> >TempBlock;
    
    int CurrentMatch = 0;
    //The immobile block
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            
            //push stuff onto the tempBlock for comparison
            vector<char> tmp;
            int Hstart = height -(i + 1);
            int Wstart = width - (j + 1);
            for (int k = 0; k < height; k++){
                for (int f = 0; f < width; f++){
                    
                    //if in bounds, check consistency
                    if(k >= Hstart && f >= Wstart){
                        bool block_1_empty = false;
                        bool block_2_empty = false;
                        
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_1[k][f]) != EmptyTiles.end())
                            block_1_empty = true;
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_2[k - Hstart][f - Wstart]) != EmptyTiles.end())
                            block_2_empty = true;
                        
                        if(block_1_empty == block_2_empty)
                            CurrentMatch++;
                        
                        
                    }
                    
                    /*                    if (k >= Hstart && f >= Wstart && (
                     (  (Block_1.at(k).at(f) == EmptyTile  || Block_1.at(k).at(f) == EnemyTile) &&
                     (Block_2.at(k - Hstart).at(f - Wstart) == EmptyTile || Block_2.at(k - Hstart).at(f - Wstart) == EnemyTile)) ||
                     (  (Block_1.at(k).at(f) != EmptyTile && Block_1.at(k).at(f) != EnemyTile &&
                     Block_2.at(k - Hstart).at(f - Wstart) != EmptyTile && Block_2.at(k - Hstart).at(f - Wstart) != EnemyTile))))
                     CurrentMatch++;*/
                }
            }
            
            if (CurrentMatch > maxMatch){
                maxMatch = CurrentMatch;
                overlap = (height - Hstart)*(width - Wstart);
            }
            CurrentMatch = 0;
        }
    }
    
    
    //flip the mobile and immobile blocks
    CurrentMatch = 0;
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            
            //push stuff onto the tempBlock for comparison
            vector<char> tmp;
            int Hstart = height - (i + 1);
            int Wstart = width - (j + 1);
            for (int k = 0; k < height; k++){
                for (int f = 0; f < width; f++){
                    
                    
                    //if in bounds, check consistency
                    if(k >= Hstart && f >= Wstart){
                        bool block_1_empty = false;
                        bool block_2_empty = false;
                        
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_2[k][f]) != EmptyTiles.end())
                            block_2_empty = true;
                        if(find(EmptyTiles.begin(), EmptyTiles.end(), Block_1[k - Hstart][f - Wstart]) != EmptyTiles.end())
                            block_1_empty = true;
                        
                        if(block_1_empty == block_2_empty)
                            CurrentMatch++;
                        
                        
                    }
                    
                    
                    
                    //if in bounds, check consistency
                    /*f (k >= Hstart && f >= Wstart && (
                     (  (Block_2.at(k).at(f) == EmptyTile  || Block_2.at(k).at(f) == EnemyTile) &&
                     (Block_1.at(k - Hstart).at(f - Wstart) == EmptyTile || Block_1.at(k - Hstart).at(f - Wstart) == EnemyTile)) ||
                     (  (Block_2.at(k).at(f) != EmptyTile && Block_2.at(k).at(f) != EnemyTile &&
                     Block_1.at(k - Hstart).at(f - Wstart) != EmptyTile && Block_1.at(k - Hstart).at(f - Wstart) != EnemyTile))))
                     CurrentMatch++;*/
                    
                }
            }
            
            if (CurrentMatch > maxMatch){
                maxMatch = CurrentMatch;
                overlap = (height - Hstart)*(width - Wstart);
            }
            CurrentMatch = 0;
        }
    }
    
    return (width*height)*2 - (overlap + maxMatch);
}

//compares the Markov distribution of the 2 blocks using an order 1 markov chain
//be sure to pass the types vector containing 'S'
float Markov(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector< vector< float> > TileSimilarities, vector<char> types, int width, int height){
    
    
    //init the total array for the order 1 Markov chain
    vector< vector< int> > Totals_1;
    vector< vector< int> > Totals_2;
    vector< int > tmp;
    for (int i = 0; i <= types.size(); i++){
        for (int j = 0; j <= types.size(); j++){
            tmp.push_back(0);
        }
        Totals_1.push_back(tmp);
        Totals_2.push_back(tmp);
        tmp.clear();
    }
    
    int index_1 = -1;
    int index_2 = -1;
    int index_3 = -1;
    int index_4 = -1;
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            for (int d = 0; d < types.size(); d++){
                //previous tile Block_1
                if (index_1 == -1){
                    if ((j - 1 >= 0) && Block_1.at(i).at(j - 1) == types.at(d))
                    index_1 = d;
                    else if ((j - 1) < 0)
                    index_1 = 0;
                    //shouldn't happen
                    else{
                        index_1 = -1;
                    }
                }
                if (index_2 == -1){
                    //current tile
                    if (Block_1.at(i).at(j) == types.at(d))
                    index_2 = d;
                    //shouldn't happen
                    else{
                        index_2 = -1;
                    }
                }
                
                //previous tile Block_2
                if (index_3 == -1){
                    if ((j - 1 >= 0) && Block_2.at(i).at(j - 1) == types.at(d))
                    index_3 = d;
                    else if ((j - 1) < 0)
                    index_3 = 0;
                    //shouldn't happen
                    else{
                        index_3 = -1;
                    }
                }
                if (index_4 == -1){
                    //current tile
                    if (Block_2.at(i).at(j) == types.at(d))
                    index_4 = d;
                    //shouldn't happen
                    else{
                        index_4 = -1;
                    }
                }
            }
            
            vector<char> test = types;
            
            vector<vector<char> > B_1 = Block_1;
            vector<vector<char> > B_2 = Block_2;
            
            Totals_1.at(index_1).at(index_2)++;
            Totals_1.at(index_1).at(types.size())++;
            Totals_2.at(index_3).at(index_4)++;
            Totals_2.at(index_3).at(types.size())++;
            index_1 = -1;
            index_2 = -1;
            index_3 = -1;
            index_4 = -1;
        }
    }
    
    vector< vector < float> > Probs_1;
    vector< vector < float> > Probs_2;
    vector<float> tmp_1;
    vector<float> tmp_2;
    for (int i = 0; i < types.size(); i++){
        for (int j = 0; j < types.size(); j++){
            
            float denom = Totals_1.at(i).at(types.size());
            if (denom == 0)
            denom++;
            
            tmp_1.push_back((float)Totals_1.at(i).at(j) / denom);
            
            denom = Totals_2.at(i).at(types.size());
            if (denom == 0)
            denom++;
            
            tmp_2.push_back((float)Totals_2.at(i).at(j) / denom);
        }
        Probs_1.push_back(tmp_1);
        Probs_2.push_back(tmp_2);
        tmp_1.clear();
        tmp_2.clear();
    }
    
    float Difference = 0;
    for (int i = 0; i < types.size(); i++){
        for (int j = 0; j < types.size(); j++){
            
            float diff = Probs_1.at(i).at(j) - Probs_2.at(i).at(j);
            if(diff<0)
            diff*=-1;
            Difference += diff;
        }
    }
    
    return Difference;
}

//compares the Markov distribution of the 2 blocks using an order 1 markov chain
//be sure to pass the types vector containing 'S'
float Markov(vector<vector< char> > Block_1, vector<vector< char> > Block_2, vector<char> types, int width, int height, vector<vector<float> > weights){
    
    
    //init the total array for the order 1 Markov chain
    vector< vector< int> > Totals_1;
    vector< vector< int> > Totals_2;
    vector< int > tmp;
    for (int i = 0; i <= types.size(); i++){
        for (int j = 0; j <= types.size(); j++){
            tmp.push_back(0);
        }
        Totals_1.push_back(tmp);
        Totals_2.push_back(tmp);
        tmp.clear();
    }
    
    int index_1 = -1;
    int index_2 = -1;
    int index_3 = -1;
    int index_4 = -1;
    
    for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
            for (int d = 0; d < types.size(); d++){
                //previous tile Block_1
                if (index_1 == -1){
                    if ((j - 1 >= 0) && Block_1.at(i).at(j - 1) == types.at(d))
                        index_1 = d;
                    else if ((j - 1) < 0)
                        index_1 = 0;
                    //shouldn't happen
                    else{
                        index_1 = -1;
                    }
                }
                if (index_2 == -1){
                    //current tile
                    if (Block_1.at(i).at(j) == types.at(d))
                        index_2 = d;
                    //shouldn't happen
                    else{
                        index_2 = -1;
                    }
                }
                
                //previous tile Block_2
                if (index_3 == -1){
                    if ((j - 1 >= 0) && Block_2.at(i).at(j - 1) == types.at(d))
                        index_3 = d;
                    else if ((j - 1) < 0)
                        index_3 = 0;
                    //shouldn't happen
                    else{
                        index_3 = -1;
                    }
                }
                if (index_4 == -1){
                    //current tile
                    if (Block_2.at(i).at(j) == types.at(d))
                        index_4 = d;
                    //shouldn't happen
                    else{
                        index_4 = -1;
                    }
                }
            }
            
            vector<char> test = types;
            
            vector<vector<char> > B_1 = Block_1;
            vector<vector<char> > B_2 = Block_2;
            
            Totals_1.at(index_1).at(index_2)++;
            Totals_1.at(index_1).at(types.size())++;
            Totals_2.at(index_3).at(index_4)++;
            Totals_2.at(index_3).at(types.size())++;
            index_1 = -1;
            index_2 = -1;
            index_3 = -1;
            index_4 = -1;
        }
    }
    
    vector< vector < float> > Probs_1;
    vector< vector < float> > Probs_2;
    vector<float> tmp_1;
    vector<float> tmp_2;
    for (int i = 0; i < types.size(); i++){
        for (int j = 0; j < types.size(); j++){
            
            float denom = Totals_1.at(i).at(types.size());
            if (denom == 0)
                denom++;
            
            tmp_1.push_back((float)Totals_1.at(i).at(j) / denom);
            
            denom = Totals_2.at(i).at(types.size());
            if (denom == 0)
                denom++;
            
            tmp_2.push_back((float)Totals_2.at(i).at(j) / denom);
        }
        Probs_1.push_back(tmp_1);
        Probs_2.push_back(tmp_2);
        tmp_1.clear();
        tmp_2.clear();
    }
    
    float Difference = 0;
    for (int i = 0; i < types.size(); i++){
        for (int j = 0; j < types.size(); j++){
            
            float diff = Probs_1.at(i).at(j) - Probs_2.at(i).at(j);
            if(diff<0)
                diff*=-1;
            Difference += diff;
        }
    }
    
    return Difference;
}

//computes the differences between each block and stores the values in a file
void Clustering::ComputeDifferences(string BlockFile, string Output, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), vector< vector< float> > TileSimilarities, vector<char> types){
    
    //csv to write the comparisons to
    ofstream Out;
    Out.open(Output.c_str());
    
    //take the first block and compare to all others using specified measure
    vector< vector< char> > Prototype;
    vector< vector< char> > To_Compare;
    
    int CurrentProto = 0;
    string row = "";
    
    //for every block, break when there are no more blocks left
    while (true){
        
        int CurrentCompare = 0;
        //open the file containing all the blocks
        ifstream BFile;
        BFile.open(BlockFile.c_str());
        bool Done = false;
        CurrentCompare = 0;
        //gets the block to which all other blocks will be compared
        for (int p = 0; p <= CurrentProto; p++){
            for (int h = 0; h < this->height; h++){
                if (getline(BFile, row)){
                    vector<char> tmp;
                    for (int w = 0; w < row.size(); w++)
                    tmp.push_back(row.at(w));
                    Prototype.push_back(tmp);
                    tmp.clear();
                }
                else
                Done = true;
            }
            if (p != CurrentProto)
            Prototype.clear();
            else
            BFile.close();
        }
        
        //if we reach the end of the file when looking for the prototype block, then we have compared all blocks
        if (Done)
        break;
        
        
        //get the next block for comparison to the prototype block
        BFile.open(BlockFile.c_str());
        while (!BFile.eof()){
            
            To_Compare.clear();
            for (int h = 0; h < this->height; h++){
                getline(BFile, row);
                vector<char> tmp;
                for (int w = 0; w < row.size(); w++)
                tmp.push_back(row.at(w));
                if (tmp.size() == 0)
                break;
                To_Compare.push_back(tmp);
                tmp.clear();
            }
            CurrentCompare++;
            if (To_Compare.empty())
            break;
            
            //compare and print results to a csv
            Out << f(Prototype, To_Compare, TileSimilarities, types, this->width, this->height) << ",";
            Out.flush();
        }
        BFile.close();
        CurrentProto++;
        Out << endl;
    }
    
    Out.close();
}

//computes the differences between each block and stores the values in a file
void Clustering::ComputeDifferencesWithWeights(string BlockFile, string Output, float(*f) (vector< vector< char > >, vector< vector< char> >, vector<char>, int, int, vector< vector< float> >), vector<char> types, vector<vector< float> > weights){
    
    //csv to write the comparisons to
    ofstream Out;
    Out.open(Output.c_str());
    
    //take the first block and compare to all others using specified measure
    vector< vector< char> > Prototype;
    vector< vector< char> > To_Compare;
    
    int CurrentProto = 0;
    string row = "";
    
    //for every block, break when there are no more blocks left
    while (true){
        
        int CurrentCompare = 0;
        //open the file containing all the blocks
        ifstream BFile;
        BFile.open(BlockFile.c_str());
        bool Done = false;
        CurrentCompare = 0;
        //gets the block to which all other blocks will be compared
        for (int p = 0; p <= CurrentProto; p++){
            for (int h = 0; h < this->height; h++){
                if (getline(BFile, row)){
                    vector<char> tmp;
                    for (int w = 0; w < row.size(); w++)
                        tmp.push_back(row.at(w));
                    Prototype.push_back(tmp);
                    tmp.clear();
                }
                else
                    Done = true;
            }
            if (p != CurrentProto)
                Prototype.clear();
            else
                BFile.close();
        }
        
        //if we reach the end of the file when looking for the prototype block, then we have compared all blocks
        if (Done)
            break;
        
        
        //get the next block for comparison to the prototype block
        BFile.open(BlockFile.c_str());
        while (!BFile.eof()){
            
            To_Compare.clear();
            for (int h = 0; h < this->height; h++){
                getline(BFile, row);
                vector<char> tmp;
                for (int w = 0; w < row.size(); w++)
                    tmp.push_back(row.at(w));
                if (tmp.size() == 0)
                    break;
                To_Compare.push_back(tmp);
                tmp.clear();
            }
            CurrentCompare++;
            if (To_Compare.empty())
                break;
            
            //compare and print results to a csv
            Out << f(Prototype, To_Compare, types, this->width, this->height, weights) << ",";
            Out.flush();
        }
        BFile.close();
        CurrentProto++;
        Out << endl;
    }
    
    Out.close();
}

void Clustering::kMedoids(string DifferenceFileName, int k){
    
    //number of data points
    int n=0;
    
    //when performing a new clustering, clear all the old data
    Medoids.clear();
    Clusters.clear();
    MedoidBlocks.clear();
    ClusteredBlocks.clear();
    
    //To read in the differences, and print the clusters when done
    ifstream Diff(DifferenceFileName.c_str());
    string cell;
    string row;
    stringstream Row;
    
    //read in the differences matrix
    vector< vector <float> > Differences;
    while (getline(Diff, row)){
        vector<float> Temp;
        Row.str(row);
        
        while (getline(Row, cell, ','))
        Temp.push_back(stof(cell.c_str(), NULL));
        
        Differences.push_back(Temp);
        Temp.clear();
        Row.clear();
        n++;
    }
    //choose k distinct medoids at random
    srand((int)time(NULL));
    while(Medoids.size()<k){
        int tmp = rand()%n;
        if(Medoids.empty()){
            Medoids.push_back(tmp);
        } else{
            bool used=false;
            for(int i=0; i<Medoids.size(); i++){
                //ensure we don't use the same (or equivalent) chunk as multiple medoids
                if(tmp == Medoids[i] || Differences[tmp][Medoids[i]] == 0){
                    used = true;
                    break;
                }
            }
            if(!used)
            Medoids.push_back(tmp);
        }
    }
    bool changed =true;
    int iterations = 0;
    while(changed || iterations >= 500){
        iterations++;
        changed = false;
        //assign each chunk to its closest medoid determined by the loaded in distance matrix
        Clusters.clear();
        Clusters.resize(k);
        for(int j=0; j<n; j++){
            int closest=-1;
            int curr_distance=-1;
            for(int i=0; i<Medoids.size(); i++){
                if(curr_distance == -1 || Differences[j][Medoids[i]] < curr_distance){
                    curr_distance = Differences[j][Medoids[i]];
                    closest = i;
                }
            }
            Clusters[closest].push_back(j);
        }
        
        int tmp=0;
        for(int q=0; q<k; q++){
            for(int j=0; j<Clusters[q].size(); j++){
                tmp+= Differences[Medoids[q]][Clusters[q][j]];
            }
        }
        //for each medoid
        for(int i=0; i < k; i++){
            //swap the medoid with each non medoid chunk and check the total cost
            int min_cost=0;
            int tmp_cost;
            int best_med=Medoids[i];
            for(int q=0; q<k; q++){
                for(int j=0; j<Clusters[q].size(); j++){
                    min_cost+= Differences[Medoids[q]][Clusters[q][j]];
                }
            }
            for(int j=0; j<n; j++){
                bool med =false;
                tmp_cost =0;
                for(int g=0; g<Medoids.size(); g++){
                    if(j == Medoids[g]){
                        med = true;
                        break;
                    }
                }
                if(!med){
                    //calculate the total cost of the configuration
                    for(int q=0; q < k; q++){
                        for(int f=0; f<Clusters[q].size(); f++){
                            //if we are on the current medoid, use the temporary medoid
                            if(q == i){
                                //if trying to compare the tmp medoid to itself, compare to the current medoid instead
                                if(Clusters[q][f] != j)
                                tmp_cost += Differences[j][Clusters[q][f]];
                                else
                                tmp_cost += Differences[j][Medoids[i]];
                            }
                            else
                            tmp_cost += Differences[Medoids[q]][Clusters[q][f]];
                        }
                    }
                }
                if(!med && tmp_cost < min_cost){
                    min_cost = tmp_cost;
                    best_med = j;
                }
            }
            if(best_med != Medoids[i]){
                Medoids[i] = best_med;
                changed = true;
            }
        }
        
    }
}

void Clustering::PrintMedoids(string BlockFile, string OutputFile){
    
    ifstream Input(BlockFile.c_str());
    ofstream Output(OutputFile.c_str());
    
    vector< vector< vector< char > > > Blocks;
    string line;
    vector< vector< char > > Block;
    while(getline(Input, line)){
        
        vector<char> Row;
        for(int i=0; i<line.length(); i++)
        Row.push_back(line.at(i));
        
        Block.push_back(Row);
        if(Block.size() == this->height){
            Blocks.push_back(Block);
            Block.clear();
        }
    }
    Input.close();
    
    for(int i=0; i<this->Medoids.size(); i++)
    MedoidBlocks.push_back(Blocks[this->Medoids[i]]);
    
    for(int i=0; i<MedoidBlocks.size(); i++){
        Output<<this->Clusters[i].size()<<endl;
        for(int h=0; h<MedoidBlocks[i].size(); h++){
            for(int w=0; w<MedoidBlocks[i][h].size(); w++)
            Output<<MedoidBlocks[i][h][w];
            Output<<endl;
        }
        Output<<endl;
    }
    Output.close();
}


void Clustering::RemoveDuplicates(string Folder, string FileName){
    
    ifstream Input(Folder+FileName.c_str());
    if(!Input.is_open())
    cout<<"Couldnt open file: "<<FileName<<endl;
    ofstream Output((Folder+"Duplicates_Removed_" + FileName).c_str());
    vector< vector< vector< char> > > Blocks;
    string line;
    bool running =true;
    while(running){
        vector<vector<char> > block;
        for(int i=0; i<height; i++){
            if(getline(Input, line).eof()){
                running = false;
                break;
            }
            vector<char> row;
            for(int j=0; j<width; j++)
            row.push_back(line.at(j));
            block.push_back(row);
        }
        if(!running)
        break;
        bool duplicate =true;
        if(Blocks.empty())
        duplicate =false;
        else{
            for(int i=0; i<Blocks.size(); i++){
                duplicate=true;
                for(int y=0; y<Blocks[i].size(); y++){
                    for(int x =0; x<Blocks[i][y].size(); x++){
                        if(Blocks[i][y][x] != block[y][x]){
                            duplicate =false;
                            break;
                        }
                    }
                    if(!duplicate)
                    break;
                }
                if(duplicate)
                break;
            }
        }
        if(!duplicate)
        Blocks.push_back(block);
    }
    
    Input.close();
    
    for(int i=0; i<Blocks.size();i++){
        for(int j=0; j<Blocks[i].size(); j++){
            for(int k=0; k<Blocks[i][j].size(); k++)
            Output<<Blocks[i][j][k];
            Output<<endl;
        }
    }
}
