//
//  ConstraintChecker.cpp
//  PCG
//
//  Created by Snodgrass,Sam on 1/6/16.
//  Copyright (c) 2016 Snodgrass,Sam. All rights reserved.
//
#include "ConstraintChecker.h"

#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <gsl/gsl_fit.h>
#include<cmath>
#include <unistd.h>
//#include <Python/Python.h>

using namespace std;

//------------------//
//                  //
//      Start       //
// Helper Functions //
//                  //
//------------------//

//helper function for the linearity constraint
double ComputeLinearity(vector<vector< char> > map){
    
    
    vector<int> xVals;
    vector<int> yVals;
    
    for(int w=0; w<map[0].size(); w++){
        for(int h=(int)map.size()-1; h>=0; h--){
            
            //top of platform
            //record the point
            if(map[h][w] == '-' && h<(map.size()-1) && map[h+1][w] != '-' && map[h+1][w] != 'k' && map[h+1][w] != 'K' && map[h+1][w] != 'l'
               && map[h+1][w] != 'g' && map[h+1][w] != 'h' && map[h+1][w] != 'o' && map[h+1][w] != 'X' && map[h+1][w] != 'V' && map[h+1][w] != '|'
               && map[h+1][w] != 't'){
                xVals.push_back(w);
                yVals.push_back((int)map.size()-1-h);
            }
        }
    }
    
    //create arrays of doubles for the linear regression function
    double *xArray = new double[xVals.size()];
    double *yArray = new double[yVals.size()];
    
    for(int i=0; i<xVals.size(); i++){
        xArray[i] = xVals[i];
        yArray[i] = yVals[i];
    }
    
    double c0, c1, cov00, cov01, cov11, sumsq;
    
    //fit a line to the set of points
    gsl_fit_linear(xArray, 1, yArray, 1, xVals.size(), &c0, &c1, &cov00, &cov01, &cov11, &sumsq);
    
    //compute the sum of distances of the points to the best fit line
    //this gives the linearity
    double dist=0;
    for(int i=0; i<xVals.size(); i++)
    dist+=abs(yVals[i] - (c1*xVals[i]+c0));
    
    //scale by length of the level
    return dist/map[0].size();
}

//helper function for the leniency constraint
double ComputeLeniency(vector<vector< char> >map){
    
    int prevHeight=0;
    float Leniency=0;
    
    //count the uncovered gaps
    for(int w=0; w<map[0].size()-1; w++){
        
        if(map[map.size()-1][w] == '-'){
            bool covered=false;
            for(int h=(int)map.size()-2; h>0; h--){
                if(map[h][w] != '-' && map[h-1][w] == '-' && h > prevHeight-2){
                    covered = true;
                    prevHeight=h;
                    break;
                }
            }
            if(!covered)
                Leniency++;
        } else{
            for(int h=0; h<map.size(); h++){
                if(map[h][w] !='-' && h >= prevHeight-3){
                    prevHeight = h;
                    break;
                }
            }
        }
    }
    
    //count the enemies and cannons
    for(int h=0; h<map.size(); h++){
        for(int w=0; w<map[h].size(); w++){
            if(map[h][w] == 'k' || map[h][w] == 'K' || map[h][w] == 'g' || map[h][w] == 'l' || map[h][w] == 'h' || map[h][w] == 't')
                Leniency+=.5;
            if(map[h][w] == 'C')
                Leniency+=.5;
        }
    }
    
    //scale by size of the level
    return Leniency/(/*map.size()**/map[0].size());
}



bool NewLoderunnerPlayabilityConstraint(vector<vector<char> > Map){
    
    vector<vector<bool> > ReachablePositions;
    vector<vector<int> > GoldPositions;
    
    //find the starting positions for the search (gold positions)
    //and set all of the map as unreachable
    for(int y=0; y<Map.size(); y++){
        vector<bool> t;
        for(int x=0; x<Map[y].size(); x++){
            if(Map[y][x] =='G'){
                vector<int> tmp;
                tmp.push_back(x);
                tmp.push_back(y);
                GoldPositions.push_back(tmp);
            }
            t.push_back(false);
        }
        ReachablePositions.push_back(t);
    }
    
    //Now, for each gold position, search outward from that position
    //in order to determine if the other gold positions are reachable
    
    
    
    
    return true;
}


//used in checking the playability of Loderunner maps
struct Node{
    char type;
    bool bottom;
    bool visited;
    int x;
    int y;
    bool up;
    bool down;
    bool left;
    bool right;
    vector<int> Adj;
};

//creates a graph of nodes using the input maps
void CreateGraph(vector<vector< Node> > &Map, vector< vector< char> > inputMap){
    
    for(int i=0; i<inputMap.size(); i++){
        vector<Node> Row;
        for(int j=0; j<inputMap[i].size(); j++){
            Node cell;
            cell.up     = false;
            cell.down   = false;
            cell.left   = false;
            cell.right  = false;
            cell.type = inputMap[i][j];
            cell.visited = false;
            Row.push_back(cell);
        }
        Map.push_back(Row);
    }

    for(int h=0; h<Map.size(); h++){
        for(int w=0; w<Map[h].size(); w++){
            Map[h][w].x = w;
            Map[h][w].y = h;
            if(h == Map.size()-1)
                Map[h][w].bottom = true;
            
            //check the current type and the type of the surrounding tiles
            //if empty (or essentially empty (gold, player, enemy, empty)
            if(Map[h][w].type == '.' || Map[h][w].type == 'M' || Map[h][w].type == 'E' || Map[h][w].type == 'G'){
                //check if standing on something solid or it is the bottom tile (we assume a row of solid tiles under)
                if(Map[h][w].bottom || Map[h+1][w].type == 'b' || Map[h+1][w].type == 'B' || Map[h+1][w].type == '#'){
                    //check if anything blocking to the left
                    if(w > 0 && Map[h][w-1].type != 'b' && Map[h][w-1].type != 'B')
                        Map[h][w].left = true;
                    
                    //check if anything blocking to the right
                    if(w < Map[h].size()-1 && Map[h][w+1].type != 'b' && Map[h][w+1].type != 'B')
                        Map[h][w].right = true;
                    
                    //standing on a diggable tile, and something solid to the left of it
                    if(!Map[h][w].bottom && w > 0 && (Map[h+1][w-1].type == 'b' || Map[h+1][w-1].type == 'B'))
                        Map[h][w].down = true;
                    
                    //standing on a diggable tile, and something solid to the right of it
                    if(!Map[h][w].bottom && w < Map[h].size()-1 && (Map[h+1][w+1].type == 'b' || Map[h+1][w+1].type == 'B'))
                        Map[h][w].down = true;
                }
                //if not standing on something solid, then fall
                else if(!Map[h][w].bottom && Map[h+1][w].type != 'b' && Map[h+1][w].type != 'B' && Map[h+1][w].type != '#')
                    Map[h][w].down = true;
                
            } else if(Map[h][w].type == '#'){   //if on a ladder
                //if nothing solid above, move up
                if(h > 0 && Map[h-1][w].type != 'b' && Map[h-1][w].type != 'B')
                    Map[h][w].up = true;
                
                //if nothing below, move down
                if(!Map[h][w].bottom && Map[h+1][w].type != 'b' && Map[h+1][w].type != 'B')
                    Map[h][w].down = true;
                
                //if nothing left, move left
                if(w > 0 && Map[h][w-1].type != 'b' && Map[h][w-1].type != 'B')
                    Map[h][w].left = true;
                
                //if nothing right, move right
                if(w < Map[h].size()-1 && Map[h][w+1].type != 'b' && Map[h][w+1].type != 'B')
                    Map[h][w].right = true;
            } else if(Map[h][w].type == 'b'){    //in a diggable block
                //standing on a diggable tile, and something solid to the left of it
                if(!Map[h][w].bottom && w > 0 && (Map[h][w-1].type == 'b' || Map[h][w-1].type == 'B'))
                    Map[h][w].down = true;
                
                //standing on a diggable tile, and something solid to the right of it
                if(!Map[h][w].bottom && w < Map[h].size()-1 && (Map[h][w+1].type == 'b' || Map[h][w+1].type == 'B'))
                    Map[h][w].down = true;
            } else if(Map[h][w].type == '-'){   //on a rope
                //if nothing below, move down
                if(!Map[h][w].bottom && Map[h+1][w].type != 'b' && Map[h+1][w].type != 'B')
                    Map[h][w].down = true;
                
                //if nothing left, move left
                if(w > 0 && Map[h][w-1].type != 'b' && Map[h][w-1].type != 'B')
                    Map[h][w].left = true;
                
                //if nothing right, move right
                if(w < Map[h].size()-1 && Map[h][w+1].type != 'b' && Map[h][w+1].type != 'B')
                    Map[h][w].right = true;
            }
        }
    }
}

//checks the reachability between the nodes
void CheckReachability(vector<vector< Node>> Map){
    
    vector< vector< bool> > Reachability;
    
    //Now I need to try to travel from each of the treasures to the others
    //gather the gold bar positions
    vector<vector<int>> GoldPositions;
    for(int i=0; i<Map.size(); i++){
        for(int j=0; j<Map[i].size(); j++){
            if(Map[i][j].type == 'G'){
                vector<int> pos;
                pos.push_back(i);
                pos.push_back(j);
                GoldPositions.push_back(pos);
            }
        }
    }
    
    //for each pair of gold bars, determine if one is reachable from the other
    Reachability.resize(GoldPositions.size());
    for(int i=0; i<Reachability.size(); i++)
        Reachability[i].resize(GoldPositions.size());
    
    for(int start = 0; start < GoldPositions.size(); start++){
        for(int end = 0; end < GoldPositions.size(); end++){
            
            //perform depth first search to check if gold is reachable
            if(start == end){
                //ignore the trivial path to itself
                Reachability[start][end] = true;
                continue;
            }
            vector<Node> stack;
            //push back the starting node, and mark it as visited
            stack.push_back(Map[GoldPositions[start][0]][GoldPositions[start][1]]);
            
            //while that stack is not empty
            //or until a path is found
            bool found = false;
            
            
            
            while(!stack.empty()){
                if(stack.back().x == GoldPositions[end][1] && stack.back().y == GoldPositions[end][0]){
                    found = true;
                    break;
                }
                Node curr;
                Map[stack.back().y][stack.back().x].visited = true;
                curr.x = stack.back().x;
                curr.y = stack.back().y;
                curr.left = stack.back().left;
                curr.right= stack.back().right;
                curr.up = stack.back().up;
                curr.down = stack.back().down;
                curr.type = stack.back().type;
                curr.bottom = stack.back().bottom;
                curr.visited = true;
                
                stack.pop_back();
                
                //push all children fo the current node
                if(curr.left && Map[curr.y][curr.x-1].visited == false){
                    stack.push_back(Map[curr.y][curr.x-1]);
                }
                if(curr.down && Map[curr.y+1][curr.x].visited == false){
                    stack.push_back(Map[curr.y+1][curr.x]);
                }
                if(curr.right && Map[curr.y][curr.x+1].visited == false){
                    stack.push_back(Map[curr.y][curr.x+1]);
                }
                if(curr.up && Map[curr.y-1][curr.x].visited == false){
                    stack.push_back(Map[curr.y-1][curr.x]);
                }
                
            }
            
            Reachability[start][end] = found;
            for (int q=0; q<Map.size(); q++) {
                for(int t=0; t<Map[q].size(); t++){
                    Map[q][t].visited = false;
                }
            }
        }
    }
    ofstream Reach("Reachability.txt");
    for(int i=0; i<Reachability.size(); i++){
        for(int j=0; j<Reachability[i].size(); j++){
            Reach<<Reachability[i][j];
        }
        Reach<<endl;
    }
    Reach<<endl;
}

//looks for a path between the treasure nodes in the map
bool FindPath(string inputfile){
    
    vector<vector<bool> > Reach;
    ifstream input(inputfile.c_str());
    
    string line;
    while(!getline(input, line).eof()){
        vector<bool> tmp;
        for(int i=0; i<line.length(); i++){
            if(line.at(i) == '0')
                tmp.push_back(false);
            
            else if(line.at(i) == '1')
                tmp.push_back(true);
            
        }
        if(tmp.size() != 0)
        Reach.push_back(tmp);
        
    }
    
    vector<Node> Golds;

    //initialize the nodes of the graph
    for(int i=0; i<Reach.size(); i++){
        Node tmp;
        tmp.x = i;
        Golds.push_back(tmp);
    }
    
    //build the edges of the graph
    for(int i=0; i<Golds.size(); i++){
        Golds[i].visited = false;
        for(int j=0; j<Reach[i].size(); j++){
            if(Reach[i][j])
            Golds[i].Adj.push_back(j);
        }
    }
    
    //perform depth-first search to check for playability
    //check from each starting point
    for(int i=0; i<Golds.size(); i++){
        vector<Node> DFSstack;
        DFSstack.push_back(Golds[i]);
        
        bool found = false;
        
        while(!DFSstack.empty()){
            
            bool all_visited=true;
            for(int j=0; j<Golds.size(); j++){
                if(!Golds[j].visited){
                    all_visited = false;
                    break;
                }
            }
            if(all_visited){
                found = true;
                break;
            }
            
            DFSstack.back().visited =true;
            Node curr;
            for(int j=0; j<DFSstack.back().Adj.size(); j++)
                curr.Adj.push_back(DFSstack.back().Adj[j]);
            
            curr.visited = true;
            curr.x = DFSstack.back().x;
            Golds[curr.x].visited = true;
            
            DFSstack.pop_back();
            
            //push all children of the current node
            for(int j=0; j<curr.Adj.size(); j++){
                if(!Golds[curr.Adj[j]].visited)
                    DFSstack.push_back(Golds[curr.Adj[j]]);
            }
        }
        
        DFSstack.clear();
        for(int j=0; j<Golds.size(); j++)
            Golds[j].visited = false;
        
        //if a path is found, return
        if(found)
            return true;
    }
    return false;
}

//checks whether a path exists between the treasures in the map
bool LoderunnerPlayable(vector< vector< char> > map){
    
    vector<vector<Node>> testingMap;
    CreateGraph(testingMap, map);
    CheckReachability(testingMap);
    
    return FindPath("Reachability.txt");
}

//checks whether a full Kid Icarus map is playable
int KidIcarusPlayability(vector<vector< char> > map){
    
    vector< vector< char> > testMap;
    
    for(int i=0; i<map.size(); i++){
        vector<char> tmp;
        for(int j=0; j<map[i].size(); j++){
            tmp.push_back(map[i][j]);
        }
        testMap.push_back(tmp);
        tmp.clear();
    }
    
    //mark the bottom spots as reachable
    for(int i=(int)testMap.size()-1; i> testMap.size()-10; i--){
        for(int j=0; j<testMap[i].size(); j++){
            if((testMap[i][j] == '#' || testMap[i][j] == 'T' ) && testMap[i-1][j] == '-')
                testMap[i][j] = 'R';
        }
    }
    for(int i=0; i<5; i++){
        for(int h=(int)testMap.size()-1; h>8; h--){
            //going to the right
            for(int w=0; w<testMap[h].size(); w++){
                //if the space is reachable, determine what else could be reached from it
                if(testMap[h][w] == 'R'){
                    for(int h1=-3; h1<=3; h1++){
                        if((h-h1) >=testMap.size() || (h-h1)<0)
                        continue;
                        for(int w1= 0; w1<=4; w1++){
                            int tmpW1 = w+w1;
                            if(tmpW1 >= testMap[h-h1].size())
                            tmpW1 =tmpW1 - (int)testMap[h-h1].size();
                            
                            if((testMap[h-h1][tmpW1] == 'T' || testMap[h-h1][tmpW1] == '#' || testMap[h-h1][tmpW1] == 'M' || testMap[h-h1][tmpW1] == 'R') && testMap[h-h1-1][tmpW1] == '-' && (tmpW1-1 < 0 || (testMap[h-h1-1][tmpW1-1] != '#' && testMap[h-h1-1][tmpW1-1] != 'H')))
                            testMap[h-h1][tmpW1] = 'R';
                            
                        }
                    }
                }
            }
            //going to the left
            for(int w=(int)testMap[h].size()-1; w>=0; w--){
                //if the space is reachable, determine what else could be reached from it
                if(testMap[h][w] == 'R'){
                    for(int h1=-3; h1<=3; h1++){
                        if((h-h1) >=testMap.size() || (h-h1) <0)
                        continue;
                        //cout<<h<<" "<<h1<<endl;
                        for(int w1= 0; w1<=4; w1++){
                            int tmpW2 = w-w1;
                            if(tmpW2 < 0)
                            tmpW2 = (int)testMap[h-h1].size()+tmpW2;
                            if((testMap[h-h1][tmpW2] == 'T' || testMap[h-h1][tmpW2] == '#' || testMap[h-h1][tmpW2] == 'M' || testMap[h-h1][tmpW2] == 'R') && testMap[h-h1-1][tmpW2] == '-' && (tmpW2 +1 >= testMap[0].size() || (testMap[h-h1-1][tmpW2+1] != '#' && testMap[h-h1-1][tmpW2+1] != 'H')))
                            testMap[h-h1][tmpW2] = 'R';
                        }
                    }
                }
            }
            //going to the right a second time
            for(int w=0; w<testMap[h].size(); w++){
                //if the space is reachable, determine what else could be reached from it
                if(testMap[h][w] == 'R'){
                    for(int h1=-3; h1<=3; h1++){
                        if((h-h1) >=testMap.size() || (h-h1)<0)
                        continue;
                        for(int w1= 0; w1<=4; w1++){
                            int tmpW1 = w+w1;
                            if(tmpW1 >= testMap[h-h1].size())
                            tmpW1 =tmpW1 - (int)testMap[h-h1].size();
                            
                            if(  (testMap[h-h1][tmpW1] == 'T' || testMap[h-h1][tmpW1] == '#' || testMap[h-h1][tmpW1] == 'M' || testMap[h-h1][tmpW1] == 'R') && testMap[h-h1-1][tmpW1] == '-' && (tmpW1-1 < 0 || (testMap[h-h1-1][tmpW1-1] != '#' && testMap[h-h1-1][tmpW1-1] != 'H')))
                            testMap[h-h1][tmpW1] = 'R';
                            
                        }
                    }
                }
            }
            //going to the left a second time
            for(int w=(int)testMap[h].size()-1; w>=0; w--){
                //if the space is reachable, determine what else could be reached from it
                if(testMap[h][w] == 'R'){
                    for(int h1=-3; h1<=3; h1++){
                        if((h-h1) >=testMap.size() || (h-h1)<0)
                        continue;
                        for(int w1= 0; w1<=4; w1++){
                            int tmpW2 = w-w1;
                            if(tmpW2 < 0)
                            tmpW2 = (int)testMap[h-h1].size()+tmpW2;
                            
                            if((testMap[h-h1][tmpW2] == 'T' || testMap[h-h1][tmpW2] == '#' || testMap[h-h1][tmpW2] == 'M' || testMap[h-h1][tmpW2] == 'R') && testMap[h-h1-1][tmpW2] == '-' && (tmpW2 +1 >= testMap[0].size() || (testMap[h-h1-1][tmpW2+1] != '#' && testMap[h-h1-1][tmpW2+1] != 'H')))
                            testMap[h-h1][tmpW2] = 'R';
                        }
                    }
                }
            }
        }
    }
    
    for(int h=0; h<testMap.size(); h++){
        for(int w=0; w<testMap[h].size(); w++){
            if(testMap[h][w] == 'R')
                return h;
        }
    }
    //should never be reached
    return -1;
}

//checks whether a section of a Kid Icarus map is playable
int KidIcarusPlayabilitySection(vector<vector< char> > map){
    
    vector< vector< char> > testMap;
    
    for(int i=0; i<map.size(); i++){
        vector<char> tmp;
        for(int j=0; j<map[i].size(); j++){
            tmp.push_back(map[i][j]);
        }
        testMap.push_back(tmp);
        tmp.clear();
    }
    
    //mark the bottom spots as reachable
    bool marked =false;
    for (int i=(int)testMap.size()-1; i>(int)testMap.size()-3; i--) {
        if(!marked){
            for(int j=0; j<testMap[testMap.size()-1].size(); j++){
                if((testMap[i][j] == '#' || testMap[i][j] == 'T' || testMap[i][j] == 'M') && testMap[i-1][j] == '-'){
                    testMap[i][j] = 'R';
                    marked = true;
                }
            }
        } else
            break;
    }
    
    int changes = 1;
    //while a new spot is reachable, keep checking the playability
    while(changes>0){
        changes = 0;
        for (int h=(int)testMap.size()-1; h>=0; h--) {
            for (int w=0; w<(int)testMap[h].size(); w++) {
                if(testMap[h][w] == 'R'){
                    
                    //check each possible tile individually,
                    //starting from the top left of the below figure
                    //X is possibly reachable, O is not, P is player
                    //C is the current tile
                    //OOOOO
                    //OXXXO
                    //OXXXX
                    //PXXXX
                    //CXXXX
                    //OXXXX
                    //OXXXX
                    
                    //Notice that the player's position is actually h-1,
                    //h is the tile the player is standing on
                    //Therefore, we must subtract an additional 1 from
                    //the tile position
                    
                    //first row, first X
                    //the width of the map wraps around
                    int tileW=(w+1)%testMap[0].size();
                    //first check the bounds of the map
                    //Then check if the tile we are trying to reach is solid (M, D, #, T)
                    //Then check if the tiles surrounding it are empty (or able to be passed through: -, M, T)
                    if((h-3) >= 0 && (map[h-3][tileW] == 'M' || map[h-3][tileW] == 'D' || map[h-3][tileW] == '#' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')
                            && ((h-5) < 0 || map[h-5][w] == '-' || map[h-5][w] == 'T' || map[h-5][w] == 'M')
                            && ((h-5) < 0 || map[h-5][tileW] == '-' || map[h-5][tileW] == 'T' || map[h-5][tileW] == 'M')){
                        if(testMap[h-3][tileW]!='R'){
                            testMap[h-3][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //first row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((h-3) >= 0 && (map[h-3][tileW] == 'M' || map[h-3][tileW] == 'D' || map[h-3][tileW] == '#' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && (map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')
                            //&& ((h-5) < 0 || map[h-5][w] == '-' || map[h-5][w] == 'T' || map[h-5][w] == 'M')
                            && ((h-5) < 0 || map[h-5][(w+1)%testMap[0].size()] == '-' || map[h-5][(w+1)%testMap[0].size()] == 'T' || map[h-5][(w+1)%testMap[0].size()] == 'M')
                            && ((h-5) < 0 || map[h-5][tileW] == '-' || map[h-5][tileW] == 'T' || map[h-5][tileW] == 'M')){
                        if(testMap[h-3][tileW]!='R'){
                            testMap[h-3][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //first row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((h-3) >= 0 && (map[h-3][tileW] == 'M' || map[h-3][tileW] == 'D' || map[h-3][tileW] == '#' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && (map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && (map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                            //&& ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')
                            //&& ((h-5) < 0 || map[h-5][w] == '-' || map[h-5][w] == 'T' || map[h-5][w] == 'M')
                            && ((h-5) < 0 || map[h-5][(w+1)%testMap[0].size()] == '-' || map[h-5][(w+1)%testMap[0].size()] == 'T' || map[h-5][(w+1)%testMap[0].size()] == 'M')
                            && ((h-5) < 0 || map[h-5][(w+2)%testMap[0].size()] == '-' || map[h-5][(w+2)%testMap[0].size()] == 'T' || map[h-5][(w+2)%testMap[0].size()] == 'M')
                            && ((h-5) < 0 || map[h-5][tileW] == '-' || map[h-5][tileW] == 'T' || map[h-5][tileW] == 'M')){
                        if(testMap[h-3][tileW]!='R'){
                            testMap[h-3][tileW] = 'R';
                            changes++;
                        }
                    }

                    //second row, first X
                    tileW=(w+1)%testMap[0].size();
                    if((h-2) >= 0 && (map[h-2][tileW] == 'M' || map[h-2][tileW] == 'D' || map[h-2][tileW] == '#' || map[h-2][tileW] == 'T'  || map[h-2][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')){
                        if(testMap[h-2][tileW]!='R'){
                            testMap[h-2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((h-2) >= 0 && (map[h-2][tileW] == 'M' || map[h-2][tileW] == 'D' || map[h-2][tileW] == '#' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')){
                        if(testMap[h-2][tileW]!='R'){
                            testMap[h-2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((h-2) >= 0 && (map[h-2][tileW] == 'M' || map[h-2][tileW] == 'D' || map[h-2][tileW] == '#' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][tileW] == '-' || map[h-4][tileW] == 'T' || map[h-4][tileW] == 'M')){
                        if(testMap[h-2][tileW]!='R'){
                            testMap[h-2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, fourth X
                    tileW = (w+4)%testMap[0].size();
                    if((h-2) >= 0 && (map[h-2][tileW] == 'M' || map[h-2][tileW] == 'D' || map[h-2][tileW] == '#' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                            && ((h-3) < 0 || map[h-3][(w+3)%testMap[0].size()] == '-' || map[h-3][(w+3)%testMap[0].size()] == 'M' || map[h-3][(w+3)%testMap[0].size()] == 'T')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'M')
                       
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                            && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')){
                        if(testMap[h-2][tileW]!='R'){
                            testMap[h-2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, first X
                    tileW=(w+1)%testMap[0].size();
                    if((h-1) >= 0 && (map[h-1][tileW] == 'M' || map[h-1][tileW] == 'D' || map[h-1][tileW] == '#' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'M')){
                        if(testMap[h-1][tileW]!='R'){
                            testMap[h-1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((h-1) >= 0 && (map[h-1][tileW] == 'M' || map[h-1][tileW] == 'D' || map[h-1][tileW] == '#' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'M' || map[h-3][w] == 'T')
                       
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'M' || map[h-3][tileW] == 'T')){
                        if(testMap[h-1][tileW]!='R'){
                            testMap[h-1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((h-1) >= 0 && (map[h-1][tileW] == 'M' || map[h-1][tileW] == 'D' || map[h-1][tileW] == '#' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 ||map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 ||map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'M')){
                        if(testMap[h-1][tileW]!='R'){
                            testMap[h-1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, fourth X
                    tileW = (w+4)%testMap[0].size();
                    if((h-1) >= 0 && (map[h-1][tileW] == 'M' || map[h-1][tileW] == 'D' || map[h-1][tileW] == '#' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 ||map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 ||map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                            && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][(w+3)%testMap[0].size()] == '-' || map[h-2][(w+3)%testMap[0].size()] == 'T' || map[h-2][(w+3)%testMap[0].size()] == 'M')
                            && ((h-3) < 0 || map[h-3][(w+3)%testMap[0].size()] == '-' || map[h-3][(w+3)%testMap[0].size()] == 'T' || map[h-3][(w+3)%testMap[0].size()] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'M') ){
                        if(testMap[h-1][tileW]!='R'){
                            testMap[h-1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, first X
                    tileW=(w+1)%testMap[0].size();
                    if((map[h][tileW] == 'M' || map[h][tileW] == 'D' || map[h][tileW] == '#' || map[h][tileW] == 'T' || map[h][tileW] == 'H')
                            && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M')
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')){
                        if(testMap[h][tileW]!='R'){
                            testMap[h][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((map[h][tileW] == 'M' || map[h][tileW] == 'D' || map[h][tileW] == '#' || map[h][tileW] == 'T' || map[h][tileW] == 'H')
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'M' || map[h-3][tileW] == 'T')
                       
                            && ( ( ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'M' || map[h-3][w] == 'T')
                                && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                                && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M') )
                       
                                || ((h-1) < 0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'M' || map[h-1][(w+1)%testMap[0].size()] == 'T') )){
                        if(testMap[h][tileW]!='R'){
                            testMap[h][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((map[h][tileW] == 'M' || map[h][tileW] == 'D' || map[h][tileW] == '#' || map[h][tileW] == 'T' || map[h][tileW] == 'H')
                            && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M')
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                       
                            && (  (((h-1) < 0  || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                && ((h-3) < 0 || map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                                && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                                && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                                && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                                && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M'))
                       
                            ||  ((((h-1) < 0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'M' || map[h-1][(w+1)%testMap[0].size()] == 'T'))
                                && ((h-1) < 0 || map[h-1][(w+2)%testMap[0].size()] == '-' || map[h-1][(w+2)%testMap[0].size()] == 'M' || map[h-1][(w+2)%testMap[0].size()] == 'T')) )){
                        
                        if(testMap[h][tileW]!='R'){
                            testMap[h][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, fourth X
                    tileW = (w+4)%testMap[0].size();
                    if((map[h][tileW] == 'M' || map[h][tileW] == 'D' || map[h][tileW] == '#' || map[h][tileW] == 'T' || map[h][tileW] == 'H')
                            && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M')
                            && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-2][tileW] == 'M')
                            && ((h-3) < 0 || map[h-3][tileW] == '-' || map[h-3][tileW] == 'T' || map[h-3][tileW] == 'M')
                       
                            && ( ( ((h-2) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                && ((h-3) < 0 || map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                                && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                                && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                                && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                                && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                       
                                && ((h-2) < 0 || map[h-2][(w+3)%testMap[0].size()] == '-' || map[h-2][(w+3)%testMap[0].size()] == 'T' || map[h-2][(w+3)%testMap[0].size()] == 'M')
                                && ((h-3) < 0 || map[h-3][(w+3)%testMap[0].size()] == '-' || map[h-3][(w+3)%testMap[0].size()] == 'T' || map[h-3][(w+3)%testMap[0].size()] == 'M'))
                            
                            ||((((h-1) < 0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'M' || map[h-1][(w+1)%testMap[0].size()] == 'T'))
                                && ((h-1) < 0 || map[h-1][(w+2)%testMap[0].size()] == '-' || map[h-1][(w+2)%testMap[0].size()] == 'M' || map[h-1][(w+2)%testMap[0].size()] == 'T')
                                && ((h-1) < 0 || map[h-1][(w+3)%testMap[0].size()] == '-' || map[h-1][(w+3)%testMap[0].size()] == 'M' || map[h-1][(w+3)%testMap[0].size()] == 'T')) )){
                        if(testMap[h][tileW]!='R'){
                            testMap[h][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, first X
                    tileW = (w+1)%testMap[0].size();
                    if(((h+1)<testMap.size() && (map[h+1][tileW] == '#' || map[h+1][tileW] == 'D' || map[h+1][tileW] == 'T' || map[h+1][tileW] == 'M' || map[h+1][tileW] == 'H')
                        
                        && (map[h][tileW] == '-' || map[h][tileW] == 'T' || map[h][tileW] == 'M')
                        && ((h-1)<0 || map[h-1][tileW] == '-'))){
                        
                        if(testMap[h+1][tileW]!='R'){
                            testMap[h+1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((h+1)<testMap.size() && (map[h+1][tileW] == '#' || map[h+1][tileW] == 'D' || map[h+1][tileW] == 'T' || map[h+1][tileW] == 'M' || map[h+1][tileW] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'T' || map[h-1][(w+1)%testMap[0].size()] == 'M')
                       && ((h-1)<0 || map[h-1][tileW] == '-')
                       && (map[h][tileW] == '-')){
                        
                        if(testMap[h+1][tileW]!='R'){
                            testMap[h+1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((h+1)<testMap.size() && (map[h+1][tileW] == '#' || map[h+1][tileW] == 'D' || map[h+1][tileW] == 'T' || map[h+1][tileW] == 'M' || map[h+1][tileW] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'T' || map[h-1][(w+1)%testMap[0].size()] == 'M')
                       && ((h-1)<0 || map[h-1][(w+2)%testMap[0].size()] == '-' || map[h-1][(w+2)%testMap[0].size()] == 'T' || map[h-1][(w+2)%testMap[0].size()] == 'M')
                       && (map[h][(w+2)%testMap[0].size()] == '-' || map[h][(w+2)%testMap[0].size()] == 'T' || map[h][(w+2)%testMap[0].size()] == 'M')
                       && (map[h][tileW] == '-')){
                        
                        if(testMap[h+1][tileW]!='R'){
                            testMap[h+1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, fourth X
                    tileW = (w+4)%testMap[0].size();
                    if((h+1)<testMap.size() && (map[h+1][tileW] == 'M' || map[h+1][tileW] == 'D' || map[h+1][tileW] == '#' || map[h+1][tileW] == 'T' || map[h+1][tileW] == 'H')
                       && (map[h][tileW] == '-' || map[h][tileW] == 'T' || map[h][tileW] == 'M')
                       && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M')
                       
                       
                       && ((h-1) < 0 ||map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-2) < 0 ||map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                       && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                       && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+3)%testMap[0].size()] == '-' || map[h-3][(w+3)%testMap[0].size()] == 'M' || map[h-3][(w+3)%testMap[0].size()] == 'T')
                       && ((h-2) < 0 || map[h-2][(w+3)%testMap[0].size()] == '-' || map[h-2][(w+3)%testMap[0].size()] == 'T' || map[h-2][(w+3)%testMap[0].size()] == 'M')
                       
                       && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-1][tileW] == 'M')
                       && ((h-1) < 0 || map[h-1][tileW] == '-')
                       && (map[h][tileW] == '-')){
                        
                        if(testMap[h+1][tileW]!='R'){
                            testMap[h+1][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, first X
                    tileW = (w+1)%testMap[0].size();
                    if(((h+2)<testMap.size() && (map[h+2][tileW] == '#' || map[h+2][tileW] == 'D' || map[h+2][tileW] == 'T' || map[h+2][tileW] == 'M' || map[h+2][tileW] == 'H')
                        
                        && (map[h+1][tileW] == '-')
                        && (map[h][tileW] == '-')
                        && ((h-1)<0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M'))){
                        
                        if(testMap[h+2][tileW]!='R'){
                            testMap[h+2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, second X
                    tileW = (w+2)%testMap[0].size();
                    if((h+2)<testMap.size() && (map[h+2][tileW] == '#' || map[h+2][tileW] == 'D' || map[h+2][tileW] == 'T' || map[h+2][tileW] == 'M' || map[h+2][tileW] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'T' || map[h-1][(w+1)%testMap[0].size()] == 'M')
                       && (map[h][tileW] == '-' || map[h][tileW] == 'T' || map[h][tileW] == 'M')
                       && (map[h+1][tileW] == '-')){
                        
                        if(testMap[h+2][tileW]!='R'){
                            testMap[h+2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, third X
                    tileW = (w+3)%testMap[0].size();
                    if((h+2)<testMap.size() && (map[h+2][tileW] == '#' || map[h+2][tileW] == 'D' || map[h+2][tileW] == 'T' || map[h+2][tileW] == 'M' || map[h+2][tileW] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][(w+1)%testMap[0].size()] == '-' || map[h-1][(w+1)%testMap[0].size()] == 'T' || map[h-1][(w+1)%testMap[0].size()] == 'M')
                       && ((h-1)<0 || map[h-1][(w+2)%testMap[0].size()] == '-' || map[h-1][(w+2)%testMap[0].size()] == 'T' || map[h-1][(w+2)%testMap[0].size()] == 'M')
                       && (map[h][(w+2)%testMap[0].size()] == '-' || map[h][(w+2)%testMap[0].size()] == 'T' || map[h][(w+2)%testMap[0].size()] == 'M')
                       && (map[h][tileW] == '-' || map[h][tileW] == 'T' || map[h][tileW] == 'M')
                       && (map[h+1][(w+2)%testMap[0].size()] == '-')
                       && (map[h+1][tileW] == '-')){
                        
                        if(testMap[h+2][tileW]!='R'){
                            testMap[h+2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, fourth X
                    tileW = (w+4)%testMap[0].size();
                    if((h+2)<testMap.size() && (map[h+2][tileW] == 'M' || map[h+2][tileW] == 'D' || map[h+2][tileW] == '#' || map[h+2][tileW] == 'T' || map[h+2][tileW] == 'H')
                       && (map[h][tileW] == '-' || map[h][tileW] == 'T' || map[h][tileW] == 'M')
                       && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'T' || map[h-1][tileW] == 'M')
                       
                       
                       && ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+1)%testMap[0].size()] == '-' || map[h-3][(w+1)%testMap[0].size()] == 'M' || map[h-3][(w+1)%testMap[0].size()] == 'T')
                       && ((h-4) < 0 || map[h-4][(w+1)%testMap[0].size()] == '-' || map[h-4][(w+1)%testMap[0].size()] == 'T' || map[h-4][(w+1)%testMap[0].size()] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+2)%testMap[0].size()] == '-' || map[h-3][(w+2)%testMap[0].size()] == 'M' || map[h-3][(w+2)%testMap[0].size()] == 'T')
                       && ((h-4) < 0 || map[h-4][(w+2)%testMap[0].size()] == '-' || map[h-4][(w+2)%testMap[0].size()] == 'T' || map[h-4][(w+2)%testMap[0].size()] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][(w+3)%testMap[0].size()] == '-' || map[h-3][(w+3)%testMap[0].size()] == 'M' || map[h-3][(w+3)%testMap[0].size()] == 'T')
                       && ((h-2) < 0 || map[h-2][(w+3)%testMap[0].size()] == '-' || map[h-2][(w+3)%testMap[0].size()] == 'T' || map[h-2][(w+3)%testMap[0].size()] == 'M')
                       
                       && ((h-2) < 0 || map[h-2][tileW] == '-' || map[h-2][tileW] == 'T' || map[h-1][tileW] == 'M')
                       && ((h-1) < 0 || map[h-1][tileW] == '-' || map[h-1][tileW] == 'M' || map[h-1][tileW] == 'T')
                       && (map[h][tileW] == '-')
                       && (map[h+1][tileW] == '-')){
                        
                        if(testMap[h+2][tileW]!='R'){
                            testMap[h+2][tileW] = 'R';
                            changes++;
                        }
                    }
                    
                    //Now check leftward movement
                    int w1= w-1;
                    if(w1<0)
                        w1 = (int)map[0].size()-1+w1;
                    
                    int w2 = w-2;
                    if(w2<0)
                        w2 = (int)map[0].size()-1+w2;
                    
                    int w3 = w-3;
                    if(w3<0)
                        w3 = (int)map[0].size()-1+w3;
                    
                    int w4 = w-4;
                    if(w4<0)
                        w4 = (int)map[0].size()-1+w4;
                    
                    
                    //first row, first X
                    if((h-3) >= 0 && (map[h-3][w1] == 'M' || map[h-3][w1] == 'D' || map[h-3][w1] == '#' || map[h-3][w1] == 'T' || map[h-3][w1] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-5) < 0 || map[h-5][w] == '-' || map[h-5][w] == 'T' || map[h-5][w] == 'M')
                            && ((h-5) < 0 || map[h-5][w1] == '-' || map[h-5][w1] == 'T' || map[h-5][w1] == 'M')){
                        if(testMap[h-3][w1]!='R'){
                            testMap[h-3][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //first row, second X
                    if((h-3) >= 0 && (map[h-3][w2] == 'M' || map[h-3][w2] == 'D' || map[h-3][w2] == '#' || map[h-3][w2] == 'T' || map[h-3][w2] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && (map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                            && ((h-5) < 0 || map[h-5][w1] == '-' || map[h-5][w1] == 'T' || map[h-5][w1] == 'M')
                            && ((h-5) < 0 || map[h-5][w2] == '-' || map[h-5][w2] == 'T' || map[h-5][w2] == 'M')){
                        if(testMap[h-3][w2]!='R'){
                            testMap[h-3][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //first row, third X
                    if((h-3) >= 0 && (map[h-3][w3] == 'M' || map[h-3][w3] == 'D' || map[h-3][w3] == '#' || map[h-3][w3] == 'T' || map[h-3][w3] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && (map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && (map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && (map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                            && ((h-4) < 0 || map[h-4][w3] == '-' || map[h-4][w3] == 'T' || map[h-4][w3] == 'M')
                            && ((h-5) < 0 || map[h-5][w1] == '-' || map[h-5][w1] == 'T' || map[h-5][w1] == 'M')
                            && ((h-5) < 0 || map[h-5][w1] == '-' || map[h-5][w1] == 'T' || map[h-5][w1] == 'M')
                            && ((h-5) < 0 || map[h-5][w3] == '-' || map[h-5][w3] == 'T' || map[h-5][w3] == 'M')){
                        if(testMap[h-3][w3]!='R'){
                            testMap[h-3][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, first X
                    if((h-2) >= 0 && (map[h-2][w1] == 'M' || map[h-2][w1] == 'D' || map[h-2][w1] == '#' || map[h-2][w1] == 'T' || map[h-2][w1] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')){
                        if(testMap[h-2][w1]!='R'){
                            testMap[h-2][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, second X
                    if((h-2) >= 0 && (map[h-2][w2] == 'M' || map[h-2][w2] == 'D' || map[h-2][w2] == '#' || map[h-2][w2] == 'T' || map[h-2][w2] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w] == '-' || map[h-4][w] == 'T' || map[h-4][w] == 'M')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')){
                        if(testMap[h-2][w2]!='R'){
                            testMap[h-2][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, third X
                    if((h-2) >= 0 && (map[h-2][w3] == 'M' || map[h-2][w3] == 'D' || map[h-2][w3] == '#' || map[h-2][w3] == 'T' || map[h-2][w3] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                            && ((h-4) < 0 || map[h-4][w3] == '-' || map[h-4][w3] == 'T' || map[h-4][w3] == 'M')){
                        if(testMap[h-2][w3]!='R'){
                            testMap[h-2][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //second row, fourth X
                    if((h-2) >= 0 && (map[h-2][w4] == 'M' || map[h-2][w4] == 'D' || map[h-2][w4] == '#' || map[h-2][w4] == 'T' || map[h-2][w4] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && (map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'M' || map[h-3][w3] == 'T')
                            && ((h-3) < 0 || map[h-3][w4] == '-' || map[h-3][w4] == 'T' || map[h-3][w4] == 'M')
                       
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')){
                        if(testMap[h-2][w4]!='R'){
                            testMap[h-2][w4] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, first X
                    if((h-1) >= 0 && (map[h-1][w1] == 'M' || map[h-1][w1] == 'D' || map[h-1][w1] == '#' || map[h-1][w1] == 'T' || map[h-1][w1] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w1] == '-' || map[h-2][w1] == 'T' || map[h-2][w1] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'T' || map[h-3][w1] == 'M')){
                        if(testMap[h-1][w1]!='R'){
                            testMap[h-1][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, second X
                    if((h-1) >= 0 && (map[h-1][w2] == 'M' || map[h-1][w2] == 'D' || map[h-1][w2] == '#' || map[h-1][w2] == 'T' || map[h-1][w2] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'M' || map[h-3][w] == 'T')
                       
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][w2] == '-' || map[h-2][w2] == 'T' || map[h-2][w2] == 'M')
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')){
                        if(testMap[h-1][w2]!='R'){
                            testMap[h-1][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, third X
                    if((h-1) >= 0 && (map[h-1][w3] == 'M' || map[h-1][w3] == 'D' || map[h-1][w3] == '#' || map[h-1][w3] == 'T' || map[h-1][w3] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 ||map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 ||map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                            && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'T' || map[h-3][w3] == 'M')){
                        if(testMap[h-1][w3]!='R'){
                            testMap[h-1][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //third row, fourth X
                    if((h-1) >= 0 && (map[h-1][w4] == 'M' || map[h-1][w4] == 'D' || map[h-1][w4] == '#' || map[h-1][w4] == 'T' || map[h-1][w4] == 'H')
                            && (map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 ||map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                            && ((h-3) < 0 ||map[h-3][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                            && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'T' || map[h-3][w3] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][w4] == '-' || map[h-2][w4] == 'T' || map[h-2][w4] == 'M')
                            && ((h-3) < 0 || map[h-3][w4] == '-' || map[h-3][w4] == 'T' || map[h-3][w4] == 'M') ){
                        if(testMap[h-1][w4]!='R'){
                            testMap[h-1][w4] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, first X
                    if((map[h][w1] == 'M' || map[h][w1] == 'D' || map[h][w1] == '#' || map[h][w1] == 'T' || map[h][w1] == 'H')
                            && ((h-1) < 0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M')
                            && ((h-2) < 0 || map[h-2][w1] == '-' || map[h-2][w1] == 'T' || map[h-2][w1] == 'M')){
                        if(testMap[h][w1]!='R'){
                            testMap[h][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, second X
                    if((map[h][w2] == 'M' || map[h][w2] == 'D' || map[h][w2] == '#' || map[h][w2] == 'T' || map[h][w2] == 'H')
                            && ((h-2) < 0 || map[h-2][w2] == '-' || map[h-2][w2] == 'T' || map[h-2][w2] == 'M')
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                       
                            && ( (   ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                  && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                  && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'M' || map[h-3][w] == 'T')
                                  && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                                  && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M') )
                           
                                || ((h-1) < 0 || map[h-1][w1] == '-' || map[h-1][w1] == 'M' || map[h-1][w1] == 'T') )){
                        if(testMap[h][w2]!='R'){
                            testMap[h][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, third X
                    if((map[h][w3] == 'M' || map[h][w3] == 'D' || map[h][w3] == '#' || map[h][w3] == 'T' || map[h][w3] == 'H')
                            && ((h-1) < 0 || map[h-1][w3] == '-' || map[h-1][w3] == 'T' || map[h-1][w3] == 'M')
                            && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                       
                            && ( (   ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                  && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                  && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                             
                                  && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                                  && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                             
                                  && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                                  && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M'))
                           
                                ||  ((((h-1) < 0 || map[h-1][w1] == '-' || map[h-1][w1] == 'M' || map[h-1][w1] == 'T'))
                                     && ((h-1) < 0 || map[h-1][w2] == '-' || map[h-1][w2] == 'M' || map[h-1][w2] == 'T')) )){
                               
                        if(testMap[h][w3]!='R'){
                            testMap[h][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //fourth row, fourth X
                    if((map[h][w4] == 'M' || map[h][w4] == 'D' || map[h][w4] == '#' || map[h][w4] == 'T' || map[h][w4] == 'H')
                            && ((h-1) < 0 || map[h-1][w3] == '-' || map[h-1][w3] == 'T' || map[h-1][w3] == 'M')
                            && ((h-2) < 0 || map[h-2][w4] == '-' || map[h-2][w4] == 'T' || map[h-2][w4] == 'M')
                            && ((h-3) < 0 || map[h-3][w4] == '-' || map[h-3][w4] == 'T' || map[h-3][w4] == 'M')
                       
                            && ( (   ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                                  && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                                  && ((h-3) < 0 || map[h-3][w] == '-' || map[h-3][w] == 'T' || map[h-3][w] == 'M')
                             
                                  && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                                  && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                             
                                  && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                                  && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                             
                                  && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                                  && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'T' || map[h-3][w3] == 'M'))
                           
                                ||((((h-1) < 0 || map[h-1][w1] == '-' || map[h-1][w1] == 'M' || map[h-1][w1] == 'T'))
                                   && ((h-1) < 0 || map[h-1][w2] == '-' || map[h-1][w2] == 'M' || map[h-1][w2] == 'T')
                                   && ((h-1) < 0 || map[h-1][w3] == '-' || map[h-1][w3] == 'M' || map[h-1][w3] == 'T')) )){
                        if(testMap[h][w4]!='R'){
                            testMap[h][w4] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, first X
                    if(((h+1)<testMap.size() && (map[h+1][w1] == '#' || map[h+1][w1] == 'D' || map[h+1][w1] == 'T' || map[h+1][w1] == 'M' || map[h+1][w1] == 'H')
                        
                            && (map[h][w1] == '-' || map[h][w1] == 'T' || map[h][w1] == 'M')
                            && ((h-1)<0 || map[h-1][w1] == '-'))){
                        
                        if(testMap[h+1][w1]!='R'){
                            testMap[h+1][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, second X
                    if((h+1)<testMap.size() && (map[h+1][w2] == '#' || map[h+1][w2] == 'D' || map[h+1][w2] == 'T' || map[h+1][w2] == 'M' || map[h+1][w2] == 'H')
                       
                            && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-1)<0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M')
                            && ((h-1)<0 || map[h-1][w2] == '-')
                            && (map[h][w2] == '-')){
                        
                        if(testMap[h+1][w2]!='R'){
                            testMap[h+1][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, third X
                    if((h+1)<testMap.size() && (map[h+1][w3] == '#' || map[h+1][w3] == 'D' || map[h+1][w3] == 'T' || map[h+1][w3] == 'M' || map[h+1][w3] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M')
                       && ((h-1)<0 || map[h-1][w2] == '-' || map[h-1][w2] == 'T' || map[h-1][w2] == 'M')
                       && (map[h][w2] == '-' || map[h][w2] == 'T' || map[h][w2] == 'M')
                       && (map[h][w3] == '-')){
                        
                        if(testMap[h+1][w3]!='R'){
                            testMap[h+1][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //fifth row, fourth X
                    if((h+1)<testMap.size() && (map[h+1][w4] == 'M' || map[h+1][w4] == 'D' || map[h+1][w4] == '#' || map[h+1][w4] == 'T' || map[h+1][w4] == 'H')
                            && (map[h][w4] == '-' || map[h][w4] == 'T' || map[h][w4] == 'M')
                            && ((h-1) < 0 || map[h-1][w4] == '-' || map[h-1][w4] == 'T' || map[h-1][w4] == 'M')
                       
                       
                            && ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                            && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                             
                            && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                            && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                             
                            && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                            && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                       
                            && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'M' || map[h-3][w3] == 'T')
                            && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                       
                            && ((h-2) < 0 || map[h-2][w4] == '-' || map[h-2][w4] == 'T' || map[h-1][w4] == 'M')
                            && ((h-1) < 0 || map[h-1][w4] == '-')
                            && (map[h][w4] == '-')){
                        
                        if(testMap[h+1][w4]!='R'){
                            testMap[h+1][w4] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, first X
                    if(((h+2)<testMap.size() && (map[h+2][w1] == '#' || map[h+2][w1] == 'D' || map[h+2][w1] == 'T' || map[h+2][w1] == 'M' || map[h+2][w1] == 'H')
                        
                        && (map[h+1][w1] == '-')
                        && (map[h][w1] == '-')
                        && ((h-1)<0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M'))){
                        
                        if(testMap[h+2][w1]!='R'){
                            testMap[h+2][w1] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, second X
                    if((h+2)<testMap.size() && (map[h+2][w2] == '#' || map[h+2][w2] == 'D' || map[h+2][w2] == 'T' || map[h+2][w2] == 'M' || map[h+2][w2] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M')
                       && (map[h][w2] == '-' || map[h][w2] == 'T' || map[h][w2] == 'M')
                       && (map[h+1][w2] == '-')){
                        
                        if(testMap[h+2][w2]!='R'){
                            testMap[h+2][w2] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, third X
                    if((h+2)<testMap.size() && (map[h+2][w3] == '#' || map[h+2][w3] == 'D' || map[h+2][w3] == 'T' || map[h+2][w3] == 'M' || map[h+2][w3] == 'H')
                       
                       && ((h-1)<0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-1)<0 || map[h-1][w1] == '-' || map[h-1][w1] == 'T' || map[h-1][w1] == 'M')
                       && ((h-1)<0 || map[h-1][w2] == '-' || map[h-1][w2] == 'T' || map[h-1][w2] == 'M')
                       && (map[h][w2] == '-' || map[h][w2] == 'T' || map[h][w2] == 'M')
                       && (map[h][w3] == '-' || map[h][w3] == 'T' || map[h][w3] == 'M')
                       && (map[h+1][w2] == '-')
                       && (map[h+1][w3] == '-')){
                        
                        if(testMap[h+2][w3]!='R'){
                            testMap[h+2][w3] = 'R';
                            changes++;
                        }
                    }
                    
                    //sixth row, fourth X
                    if((h+2)<testMap.size() && (map[h+2][w4] == 'M' || map[h+2][w4] == 'D' || map[h+2][w4] == '#' || map[h+2][w4] == 'T' || map[h+2][w4] == 'H')
                       && (map[h][w4] == '-' || map[h][w4] == 'T' || map[h][w4] == 'M')
                       && ((h-1) < 0 || map[h-1][w4] == '-' || map[h-1][w4] == 'T' || map[h-1][w4] == 'M')
                       
                       
                       && ((h-1) < 0 || map[h-1][w] == '-' || map[h-1][w] == 'T' || map[h-1][w] == 'M')
                       && ((h-2) < 0 || map[h-2][w] == '-' || map[h-2][w] == 'T' || map[h-2][w] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][w1] == '-' || map[h-3][w1] == 'M' || map[h-3][w1] == 'T')
                       && ((h-4) < 0 || map[h-4][w1] == '-' || map[h-4][w1] == 'T' || map[h-4][w1] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][w2] == '-' || map[h-3][w2] == 'M' || map[h-3][w2] == 'T')
                       && ((h-4) < 0 || map[h-4][w2] == '-' || map[h-4][w2] == 'T' || map[h-4][w2] == 'M')
                       
                       && ((h-3) < 0 || map[h-3][w3] == '-' || map[h-3][w3] == 'M' || map[h-3][w3] == 'T')
                       && ((h-2) < 0 || map[h-2][w3] == '-' || map[h-2][w3] == 'T' || map[h-2][w3] == 'M')
                       
                       && ((h-2) < 0 || map[h-2][w4] == '-' || map[h-2][w4] == 'T' || map[h-1][w4] == 'M')
                       && ((h-1) < 0 || map[h-1][w4] == '-' || map[h-1][w4] == 'M' || map[h-1][w4] == 'T')
                       && (map[h][w4] == '-')
                       && (map[h+1][w4] == '-')){
                        
                        if(testMap[h+2][w4]!='R'){
                            testMap[h+2][w4] = 'R';
                            changes++;
                        }
                    }
                    
                    
                    
                    //Check for vertical jumps
                    
                    //platform immediately above
                    if((h-1)>=0  && (map[h-1][w] == 'M' || map[h-1][w] == 'T')
                            && ((h-2)<0 || map[h-2][w] == 'M' || map[h-2][w] == 'T' || map[h-2][w] == '-')){
                        if(testMap[h-1][w]!='R'){
                            testMap[h-1][w] = 'R';
                            changes++;
                        }
                    }
                    
                    //platform 2 above
                    if((h-2)>=0  && (map[h-2][w] == 'M' || map[h-2][w] == 'T')
                            && ((h-1)<0 || map[h-1][w] == 'M' || map[h-1][w] == 'T' || map[h-1][w] == '-')
                            && ((h-3)<0 || map[h-3][w] == 'M' || map[h-3][w] == 'T' || map[h-3][w] == '-')){
                        if(testMap[h-2][w]!='R'){
                            testMap[h-2][w] = 'R';
                            changes++;
                        }
                    }
                    
                    //platform 3 above (jump through the bottom)
                    if((h-3)>=0  && (map[h-3][w] == 'M' || map[h-3][w] == 'T')
                            && ((h-1)<0 || map[h-1][w] == 'M' || map[h-1][w] == 'T' || map[h-1][w] == '-')
                            && ((h-2)<0 || map[h-2][w] == 'M' || map[h-2][w] == 'T' || map[h-2][w] == '-')
                            && ((h-4)<0 || map[h-4][w] == 'M' || map[h-4][w] == 'T' || map[h-4][w] == '-')){
                        if(testMap[h-3][w]!='R'){
                            testMap[h-3][w] = 'R';
                            changes++;
                        }
                    }
                    
                    //platform 3 above (jump around to the left and back)
                    if((h-3)>=0  && (map[h-3][w] == 'M' || map[h-3][w] == 'T' || map[h-3][w] == '#' || map[h-3][w] == 'D' || map[h-3][w] == 'T')
                            && (map[h-1][w] == 'M' || map[h-1][w] == 'T' || map[h-1][w] == '-')
                            && (map[h-2][w] == 'M' || map[h-2][w] == 'T' || map[h-2][w] == '-')
                            && ((h-4)<0 || map[h-4][w] == 'M' || map[h-4][w] == 'T' || map[h-4][w] == '-')
                            && (map[h-2][w1] == 'M' || map[h-2][w1] == 'T' || map[h-2][w1] == '-')
                            && (map[h-3][w1] == 'M' || map[h-3][w1] == 'T' || map[h-3][w1] == '-')
                            && ((h-4)<0 || map[h-4][w1] == 'M' || map[h-4][w1] == 'T' || map[h-4][w1] == '-')){
                        if(testMap[h-3][w]!='R'){
                            testMap[h-3][w] = 'R';
                            changes++;
                        }
                    }
                    
                    //platform 3 above (jump around to the right and back)
                    if((h-3)>=0  && (map[h-3][w] == 'M' || map[h-3][w] == 'T' || map[h-3][w] == '#' || map[h-3][w] == 'D' || map[h-3][w] == 'T')
                            && (map[h-1][w] == 'M' || map[h-1][w] == 'T' || map[h-1][w] == '-')
                            && (map[h-2][w] == 'M' || map[h-2][w] == 'T' || map[h-2][w] == '-')
                            && ((h-4)<0 || map[h-4][w] == 'M' || map[h-4][w] == 'T' || map[h-4][w] == '-')
                            && (map[h-2][(w+1)%testMap[h].size()] == 'M' || map[h-2][(w+1)%testMap[h].size()] == 'T' || map[h-2][(w+1)%testMap[h].size()] == '-')
                            && (map[h-3][(w+1)%testMap[h].size()] == 'M' || map[h-3][(w+1)%testMap[h].size()] == 'T' || map[h-3][(w+1)%testMap[h].size()] == '-')
                            && ((h-4)<0 || map[h-4][(w+1)%testMap[h].size()] == 'M' || map[h-4][(w+1)%testMap[h].size()] == 'T' || map[h-4][(w+1)%testMap[h].size()] == '-')){
                        if(testMap[h-3][w]!='R'){
                            testMap[h-3][w] = 'R';
                            changes++;
                        }
                    }
                }
            }
        }
    }

    /*for(int h=0; h<testMap.size(); h++){
        for(int w=0; w<testMap[h].size(); w++){
            cout<<testMap[h][w];
        }
        cout<<endl;
    }
    cout<<endl;
    */
    for(int h=0; h<testMap.size(); h++){
        for(int w=0; w<testMap[h].size(); w++){
            if(testMap[h][w] == 'R')
                return h;
        }
    }
    //should never be reached
    return -1;
}

//------------------//
//                  //
//       End        //
// Helper Functions //
//                  //
//------------------//

//-------------------//
//                   //
//       Start       //
// Mario Constraints //
//                   //
//-------------------//

//calls Summerville's A* agent to check for playability of a Completed Mario map
bool MarioPlayabilityConstraint(string mapName, float min, float max){
    
    FILE *p;
    char buff[5000];
    
    string systemCall = "python -c \"from test_level import testingPlayability; print testingPlayability(\'SMB_Sam.json\', \'"+mapName+"\',\'1\')\"";
    p = popen(systemCall.c_str(), "r");

    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;
    
    if(buff[0] == '0'){
        pclose(p);
        return false;
    } else{
        pclose(p);
        return true;
    }
}


//calls Summerville's A* agent to check the playability of a section os a map
bool MarioPlayabilityConstraint(vector<vector<char> > mapSection, float min, float max){
    
    ofstream tmp("MarioMapSection.txt");
    for (int i=0; i<mapSection.size(); i++) {
        for (int j=0; j<mapSection[i].size(); j++) {
            tmp<<mapSection[i][j];
        }
        tmp<<endl;
    }
    
    tmp.close();
    
    FILE *p;
    char buff[5000];
    
    string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'MarioMapSection\',\'1\')\"";
    p = popen(systemCall.c_str(), "r");
    
    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;

    if(buff[0] == '0'){
        pclose(p);
        return false;
    } else{
        pclose(p);
        return true;
    }
}

//calls Summerville's A* agent to check the playability of a section os a map
int MarioPlayabilityDistanceConstraint(string mapName, float min, float max){
    
    FILE *p;
    char buff[5000];
    
    string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'"+mapName+"\',\'1\')\"";
    p = popen(systemCall.c_str(), "r");
    
    int s=0;
    while(p==NULL){
        return 0;
    }
        
    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;
    
    string ret = buff;
    int distanceTravelled = atoi(ret.c_str());
    
    pclose(p);
    return distanceTravelled;
}

//calls Summerville's A* agent to check the playability of a section os a map
int MarioPlayabilityDistanceConstraint(vector<vector<char> >& mapSection, float min, float max){
    
    ofstream tmp("MarioMapSection.txt");
    for (int i=0; i<mapSection.size(); i++) {
        for (int j=0; j<mapSection[i].size(); j++) {
            tmp<<mapSection[i][j];
        }
        tmp<<endl;
    }
    
    tmp.close();
    
    FILE *p;
    char buff[5000];
    
    string systemCall = "python -c \"from test_level_springs import testingPlayability; print testingPlayability(\'SMB_Sam_Springs.json\', \'MarioMapSection\',\'1\')\"";
    p = popen(systemCall.c_str(), "r");
    
    
    while(fgets(buff, sizeof(buff), p)!=NULL)
        continue;
    
    string ret = buff;
    int distanceTravelled = atoi(ret.c_str());
    
    pclose(p);
    return distanceTravelled;
}


//checks whether the number of pipes in the given map falls within the provided range (inclusive)
bool MarioNumberOfPipesConstraint(vector<vector< char> > map, float min, float max){
    
    int numberOfPipes=0;
    //left to right
    for(int w=0; w<map[0].size(); w++){
        //top to bottom
        bool pipeFound =false;
        for(int h=0; h<map.size(); h++){
            
            if(map[h][w] == 'p' && (map[h][w+1] == 'P' || w == map[0].size()-1))
                pipeFound = true;
            if(pipeFound && (h==map.size()-1 || (map[h][w] != 'p' && (map[h][w+1] != 'P' || w == map[0].size()-1)))){
                numberOfPipes++;
                pipeFound = false;
            }
            
        }
    }
    
    return (numberOfPipes <= max && numberOfPipes >= min);
}

float MarioCountPipes(vector<vector<char> > map){
    int numberOfPipes=0;
    //left to right
    for(int w=0; w<map[0].size(); w++){
        //top to bottom
        bool pipeFound =false;
        for(int h=0; h<map.size(); h++){
            
            if(map[h][w] == 'p' && (map[h][w+1] == 'P' || w == map[0].size()-1))
            pipeFound = true;
            if(pipeFound && (h==map.size()-1 || (map[h][w] != 'p' && (map[h][w+1] != 'P' || w == map[0].size()-1)))){
                numberOfPipes++;
                pipeFound = false;
            }
            
        }
    }
    return numberOfPipes;
}

//checks whether the number of gaps in the given map falls within the provided range (inclusive)
bool MarioNumberOfGapsConstraint(vector<vector< char> > map, float min, float max){
    
    //only check the bottom row for gaps
    bool gapFound=false;
    int numberOfGaps=0;
    for(int w = 0; w<map[0].size(); w++){
        if(!gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e'))
            gapFound = true;
        if(gapFound &&  ( w == map[0].size()-1 || (map[map.size()-1][w] != '-' && map[map.size()-1][w] != 'e'))){
            gapFound = false;
            numberOfGaps++;
        }
    }
    
    return (numberOfGaps <= max && numberOfGaps >= min);
}

float MarioCountGaps(vector< vector< char> > map){
    //only check the bottom row for gaps
    bool gapFound=false;
    int numberOfGaps=0;
    for(int w = 0; w<map[0].size(); w++){
        if(!gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e'))
        gapFound = true;
        if(gapFound &&  ( w == map[0].size()-1 || (map[map.size()-1][w] != '-' && map[map.size()-1][w] != 'e'))){
            gapFound = false;
            numberOfGaps++;
        }
    }
    return numberOfGaps;
}



//checks whether the number of enemies in the given map falls within the provided range (inclusive)
bool MarioNumberOfEnemiesConstraint(vector<vector< char> > map, float min, float max){
    
    int numberOfEnemies=0;
    for(int h=0; h<map.size(); h++){
        for(int w=0; w<map[h].size(); w++){
            if(map[h][w] == 'e')
                numberOfEnemies++;
            if(map[h][w] == 'c' && ((h+1)>=(int)map.size() || map[h+1][w] != 'c'))
                numberOfEnemies++;
        }
    }

    return (numberOfEnemies <= max && numberOfEnemies >= min);
}

float MarioCountEnemies(vector< vector< char> > map){
    int numberOfEnemies=0;
    for(int h=0; h<map.size(); h++){
        for(int w=0; w<map[h].size(); w++){
            if(map[h][w] == 'e')
            numberOfEnemies++;
            if(map[h][w] == 'c' && ((h+1)>=(int)map.size() || map[h+1][w] != 'c'))
            numberOfEnemies++;
        }
    }
    return numberOfEnemies;
}

//checks whether there are any gaps longer than the prescribed maximum length
bool MarioLongestGapConstraint(vector<vector< char> > map, float min, float max){
    
    //only check the bottom row for gaps
    bool gapFound=false;
    int lengthOfGap=0;
    int longestGap =0;
    for(int w = 0; w<map[0].size(); w++){
        if(!gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e'))
            gapFound = true;
        
        if(gapFound &&  ((map[map.size()-1][w] != '-' && map[map.size()-1][w] != 'e'))){
            gapFound = false;
            lengthOfGap=0;
        } else if(gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e')){
            lengthOfGap++;
            if(longestGap < lengthOfGap)
                longestGap = lengthOfGap;
        }
    }
    return (longestGap>= min && longestGap <= max);
}

float MarioCountGapLength(vector<vector<char> > map){
    //only check the bottom row for gaps
    bool gapFound=false;
    int lengthOfGap=0;
    int longestGap =0;
    for(int w = 0; w<map[0].size(); w++){
        if(!gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e'))
            gapFound = true;
        
        if(gapFound &&  ( (map[map.size()-1][w] != '-' && map[map.size()-1][w] != 'e'))){
            gapFound = false;
            lengthOfGap=0;
        } else if(gapFound && (map[map.size()-1][w] == '-' || map[map.size()-1][w] == 'e')){
            lengthOfGap++;
            if(longestGap < lengthOfGap)
                longestGap = lengthOfGap;
        }
    }

    return longestGap;
}
//checks whether there are any ill-formed pipes in the map
bool MarioNoIllFormedPipesConstraint(vector<vector<char> > map, float min, float max){
    
    bool badPipe=false;
    for(int h=0; h<map.size(); h++){
        for(int w=0; w<map[h].size(); w++){
            
            if( //Left and right pipe types match
                (w+1 < map[h].size() && map[h][w] == 'p' && map[h][w+1] != 'P') ||
                (w+1 < map[h].size() && map[h][w] == 'd' && map[h][w+1] != 'D') ||
                (w+1 < map[h].size() && map[h][w] == '[' && map[h][w+1] != ']') ||
                (w+1 < map[h].size() && map[h][w] == '{' && map[h][w+1] != '}') ||
                (w-1 >= 0 && map[h][w-1] == 'p' && map[h][w] != 'P') ||
                (w-1 >= 0 && map[h][w-1] == 'd' && map[h][w] != 'D') ||
                (w-1 >= 0 && map[h][w-1] == '[' && map[h][w] != ']') ||
                (w-1 >= 0 && map[h][w-1] == '{' && map[h][w] != '}') ||
               
                //pipes are placed vertically correct
                (h+1 < map.size()   && map[h][w] == 'p' && map[h+1][w] != 'p' && map[h+1][w] != '#'
                                    && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
                (h+1 < map.size()   && map[h][w] == 'P' && map[h+1][w] != 'P' && map[h+1][w] != '#'
                                    && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
                (h+1 < map.size()   && map[h][w] == '[' && map[h+1][w] != 'p' && map[h+1][w] != '#'
                                    && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
                (h+1 < map.size()   && map[h][w] == ']' && map[h+1][w] != 'P' && map[h+1][w] != '#'
                                    && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
               
                //make sure nothing is on top of pipe
                (h-1 >= 0           && map[h][w] == '[' && map[h-1][w] != '-' && map[h-1][w] != 'V'
                                    && map[h-1][w] != 'k' && map[h-1][w] != 'K' && map[h-1][w] != 'g'
                                    && map[h-1][w] != 'l' && map[h-1][w] != 'G' && map[h-1][w] != 't'
                                    && map[h-1][w] != 'h' && map[h-1][w] != 'o') ||
                (h-1 >= 0           && map[h][w] == ']' && map[h-1][w] != '-' && map[h-1][w] != 'X'
                                    && map[h-1][w] != 'k' && map[h-1][w] != 'K' && map[h-1][w] != 'g'
                                    && map[h-1][w] != 'l' && map[h-1][w] != 'G' && map[h-1][w] != 't'
                                    && map[h-1][w] != 'h' && map[h-1][w] != 'o') ||
               
                //make sure the pipe has a top
                (h-1 >= 0           && map[h][w] == 'p' && map[h-1][w] != 'p' && map[h-1][w] != '[') ||
                (h-1 >= 0           && map[h][w] == 'P' && map[h-1][w] != 'P' && map[h-1][w] != ']') ||
               
                //downward pipes are placed vertically correct
                (h-1 >= 0   && map[h][w] == 'd' && map[h-1][w] != 'd' && map[h-1][w] != '#'
                            && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
                (h-1 >= 0   && map[h][w] == 'P' && map[h-1][w] != 'P' && map[h-1][w] != '#'
                            && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
                (h-1 >= 0   && map[h][w] == '[' && map[h-1][w] != 'p' && map[h-1][w] != '#'
                            && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
                (h-1 >= 0   && map[h][w] == ']' && map[h-1][w] != 'P' && map[h-1][w] != '#'
                            && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
               
                //make sure nothing is on top of pipe
                (h+1 < map.size()   && map[h][w] == '{' && map[h+1][w] != '-' && map[h+1][w] != 'V'
                                    && map[h+1][w] != 'k' && map[h+1][w] != 'K' && map[h+1][w] != 'g'
                                    && map[h+1][w] != 'l' && map[h+1][w] != 'G' && map[h+1][w] != 't'
                                    && map[h+1][w] != 'h' && map[h+1][w] != 'o') ||
                (h+1 < map.size()   && map[h][w] == '}' && map[h+1][w] != '-' && map[h+1][w] != 'X'
                                    && map[h+1][w] != 'k' && map[h+1][w] != 'K' && map[h+1][w] != 'g'
                                    && map[h+1][w] != 'l' && map[h+1][w] != 'G' && map[h+1][w] != 't'
                                    && map[h+1][w] != 'h' && map[h+1][w] != 'o') ||

               //make sure the pipe has a bottom
               (h+1 < map.size()    && map[h][w] == 'd' && map[h+1][w] != 'd' && map[h+1][w] != '{') ||
               (h+1 < map.size()    && map[h][w] == 'D' && map[h+1][w] != 'D' && map[h+1][w] != '}')){
               
                badPipe=true;
                break;
            }
        }
        if(badPipe)
            break;
    }
    return !badPipe;
}

int MarioNoIllFormedPipesConstraintPosition(vector<vector<char> >& map, float min, float max){
    
    bool badPipe=false;
    int pos=-1;
    for(int h=0; h<map.size(); h++){
        for(int w=0; w<map[h].size(); w++){
            
            if( //Left and right pipe types match
               (w+1 < map[h].size() && map[h][w] == 'p' && map[h][w+1] != 'P') ||
               (w+1 < map[h].size() && map[h][w] == 'd' && map[h][w+1] != 'D') ||
               (w+1 < map[h].size() && map[h][w] == '[' && map[h][w+1] != ']') ||
               (w+1 < map[h].size() && map[h][w] == '{' && map[h][w+1] != '}') ||
               (w-1 >= 0 && map[h][w-1] == 'p' && map[h][w] != 'P') ||
               (w-1 >= 0 && map[h][w-1] == 'd' && map[h][w] != 'D') ||
               (w-1 >= 0 && map[h][w-1] == '[' && map[h][w] != ']') ||
               (w-1 >= 0 && map[h][w-1] == '{' && map[h][w] != '}') ||
               
               //pipes are placed vertically correct
               (h+1 < map.size()   && map[h][w] == 'p' && map[h+1][w] != 'p' && map[h+1][w] != '#'
                && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
               (h+1 < map.size()   && map[h][w] == 'P' && map[h+1][w] != 'P' && map[h+1][w] != '#'
                && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
               (h+1 < map.size()   && map[h][w] == '[' && map[h+1][w] != 'p' && map[h+1][w] != '#'
                && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
               (h+1 < map.size()   && map[h][w] == ']' && map[h+1][w] != 'P' && map[h+1][w] != '#'
                && map[h+1][w] != '?' && map[h+1][w] != 'B') ||
               
               //make sure nothing is on top of pipe
               (h-1 >= 0           && map[h][w] == '[' && map[h-1][w] != '-' && map[h-1][w] != 'V'
                && map[h-1][w] != 'k' && map[h-1][w] != 'K' && map[h-1][w] != 'g'
                && map[h-1][w] != 'l' && map[h-1][w] != 'G' && map[h-1][w] != 't'
                && map[h-1][w] != 'h' && map[h-1][w] != 'o') ||
               (h-1 >= 0           && map[h][w] == ']' && map[h-1][w] != '-'
                && map[h-1][w] != 'k' && map[h-1][w] != 'K' && map[h-1][w] != 'g'
                && map[h-1][w] != 'l' && map[h-1][w] != 'G' && map[h-1][w] != 't'
                && map[h-1][w] != 'h' && map[h-1][w] != 'o') ||
               
               //make sure the pipe has a top
               (h-1 >= 0           && map[h][w] == 'p' && map[h-1][w] != 'p' && map[h-1][w] != '[') ||
               (h-1 >= 0           && map[h][w] == 'P' && map[h-1][w] != 'P' && map[h-1][w] != ']') ||
               
               //downward pipes are placed vertically correct
               (h-1 >= 0   && map[h][w] == 'd' && map[h-1][w] != 'd' && map[h-1][w] != '#'
                && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
               (h-1 >= 0   && map[h][w] == 'D' && map[h-1][w] != 'D' && map[h-1][w] != '#'
                && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
               (h-1 >= 0   && map[h][w] == '{' && map[h-1][w] != 'd' && map[h-1][w] != '#'
                && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
               (h-1 >= 0   && map[h][w] == '}' && map[h-1][w] != 'D' && map[h-1][w] != '#'
                && map[h-1][w] != '?' && map[h-1][w] != 'B' && map[h-1][w] != '-') ||
               
               //make sure nothing is on top of pipe
               (h+1 < map.size()   && map[h][w] == '{' && map[h+1][w] != '-' && map[h+1][w] != 'X'
                && map[h+1][w] != 'k' && map[h+1][w] != 'K' && map[h+1][w] != 'g'
                && map[h+1][w] != 'l' && map[h+1][w] != 'G' && map[h+1][w] != 't'
                && map[h+1][w] != 'h' && map[h+1][w] != 'o') ||
               (h+1 < map.size()   && map[h][w] == '}' && map[h+1][w] != '-'
                && map[h+1][w] != 'k' && map[h+1][w] != 'K' && map[h+1][w] != 'g'
                && map[h+1][w] != 'l' && map[h+1][w] != 'G' && map[h+1][w] != 't'
                && map[h+1][w] != 'h' && map[h+1][w] != 'o') ||
               
               //make sure the pipe has a bottom
               (h+1 < map.size()    && map[h][w] == 'd' && map[h+1][w] != 'd' && map[h+1][w] != '{') ||
               (h+1 < map.size()    && map[h][w] == 'D' && map[h+1][w] != 'D' && map[h+1][w] != '}') ||
               
               //make sure that there aren't any floating piranha plants
               (h+1 < map.size()    && map[h][w] == 'V' && map[h+1][w] != '[') ||
               (h-1 >= 0            && map[h][w] == 'X' && map[h-1][w] != '{') ||
               
               //also just check for springs while we're at it
               (h-1 >= 0            && map[h][w] == 'y' && map[h-1][w] != 'Y')  ||
               (h+1 < map.size()    && map[h][w] == 'Y' && map[h+1][w] != 'y')  ||
               
               //and cannon pieces
               (h-1 >= 0            && map[h][w] == 'c' && map[h-1][w] != 'c' && map[h-1][w] != 'C')  ||
               (h+1 < map.size()    && map[h][w] == 'c' && map[h+1][w] != 'c' && map[h-1][w] != 'C'
                                    && map[h+1][w] != '#' && map[h+1][w] != '?' && map[h+1][w] != 'B')){
                
                badPipe=true;
                pos = w;
                break;
            }
        }
        if(badPipe)
            break;
    }
    return pos;
}

//checks whether the linearity of the map falls within a specified bound
bool MarioLinearityBoundConstraint(vector<vector< char> > map, float min, float max){

    double linearity = ComputeLinearity(map);
    return (linearity <= max && linearity >= min);
}

//checks whether the leniency of the map falls within a specified bound
bool MarioLeniencyBoundConstraint(vector<vector< char> > map, float min, float max){
    double leniency = ComputeLeniency(map);
    return (leniency <= max && leniency >= min);
}

//-------------------//
//                   //
//        End        //
// Mario Constraints //
//                   //
//-------------------//


//------------------------//
//                        //
//         Start          //
// Loderunner Constraints //
//                        //
//------------------------//

//checks whether the given map is playable
bool LoderunnerPlayabilityConstraint(vector<vector< char> > map, float min, float max){
    return LoderunnerPlayable(map);
}

bool LoderunnerPlayabilityConstraint(string mapName, float min, float max){
    
    ifstream input(mapName.c_str());
    
    //read in the regular map
    string line;
    vector<vector<char> > map;
    while (getline(input, line)){
        
        vector<char> tmp;
        for(int z=0; z<line.length(); z++){
            if(line.at(z) == '\n' || line.at(z) == '\r' || line.at(z) == '\t' || line.at(z) == '\0')
                line.erase(line.begin()+z);
        }
        for (int i = 0; i < line.length(); i++)
            tmp.push_back(line.at(i));
        
        map.push_back(tmp);
        tmp.clear();
    }
    
    return LoderunnerPlayable(map);
}

//checks whether the given map has a number of treasures within the given bounds
bool LoderunnerNumberOfTreasuresConstraint(vector<vector< char> > map, float min, float max){
    
    int treasures=0;
    
    for(int i=0; i<map.size(); i++)
        for(int j=0; j<map[i].size(); j++)
            if(map[i][j] =='G')
                treasures++;
    
    return (treasures<=max && treasures>=min);
}

//checks whether the given map has a number of enemies within the given bounds
bool LoderunnerNumberOfGuardsConstraint(vector<vector< char> > map, float min, float max){
    
    int enemies=0;
    
    for(int i=0; i<map.size(); i++)
        for(int j=0; j<map[i].size(); j++)
            if(map[i][j] =='E')
                enemies++;
    
    return (enemies<=max && enemies>=min);
}

//checks whether the density of the map falls within the given bounds
float LoderunnerDensityBoundConstraint(vector<vector< char> > map, float min, float max){
    
    //bricks (breakable and unbreakable) count as 1
    //ropes, ladders, and enemies count as .5
    //empty space, treasures, and spawn points count at 0
    
    float total=0;
    for(int i=0; i<map.size(); i++){
        for(int j=0; j<map[i].size(); j++){
            if(map[i][j] =='b' || map[i][j] =='B')
                total+=1;
            else if(map[i][j] == '-' || map[i][j] =='#' || map[i][j] =='E')
                total+=.5;
        }
    }
    
    total/=(map.size()*map[0].size());
    
    return total;//<=max && total>=min);
}

//------------------------//
//                        //
//          End           //
// Loderunner Constraints //
//                        //
//------------------------//



//------------------------//
//                        //
//         Start          //
// Kid Icarus Constraints //
//                        //
//------------------------//

//checks whether the given map is playable
bool KidIcarusPlayabilityConstraint(vector<vector< char> > map, float min, float max){
    
     if(KidIcarusPlayability(map) <=10)
        return true;
    else
        return false;
}

bool KidIcarusPlayabilityConstraint(string mapName, float min, float max){
    
    ifstream input(mapName.c_str());
    
    //read in the regular map
    string line;
    vector<vector<char> > map;
    while (getline(input, line)){
        
        vector<char> tmp;
        for(int z=0; z<line.length(); z++){
            if(line.at(z) == '\n' || line.at(z) == '\r' || line.at(z) == '\t' || line.at(z) == '\0')
            line.erase(line.begin()+z);
        }
        for (int i = 0; i < line.length(); i++)
            tmp.push_back(line.at(i));
        
        map.push_back(tmp);
        tmp.clear();
    }
    
    return KidIcarusPlayabilityConstraint(map, 0, 0);
}

bool KidIcarusPlayabilityConstraintSection(vector<vector< char> > map, float min, float max){
    
    if(KidIcarusPlayabilitySection(map) < 8)
        return true;
    else
        return false;
}


//checks whether the number of hazards in the maps falls within the given bounds
bool KidIcarusNumberOfHazardsConstraint(vector<vector< char> > map, float min, float max){
    
    int hazards=0;
    
    for(int i=0; i<map.size(); i++)
        for(int j=0; j<map[i].size(); j++)
            if(map[i][j] == 'H')
                hazards++;
    
    return (hazards<=max && hazards >=min);
}

float KidIcarusNumberOfHazardsCount(vector<vector< char> > map){
    
    int hazards=0;
    
    for(int i=0; i<map.size(); i++)
        for(int j=0; j<map[i].size(); j++)
            if(map[i][j] == 'H')
    hazards++;
    
    return hazards;
}


//checks whether the average platform length int the map falls within the given bounds
bool KidIcarusAveragePlatformSizeConstraint(vector<vector< char> > map, float min, float max){
    
    float totalSize=0;
    float numberOfPlatforms=0;
    for (int h=1; h<map.size(); h++) {
        for (int w=0; w<map[h].size(); w++) {
            bool platformFound = false;
            while (w<map[h].size() && map[h-1][w] == '-' && (map[h][w] == 'T' || map[h][w] == 'M' || map[h][w] == '#' || map[h][w] == 'D') ) {
                totalSize+=1;
                w++;
                platformFound = true;
            }
            if(platformFound)
                numberOfPlatforms+=1;
        }
    }

    float avgPlatformSize = totalSize/numberOfPlatforms;
    return (avgPlatformSize<=max && avgPlatformSize>=min);
}

float KidIcarusAveragePlatformSizeCount(vector<vector< char> > map){
    
    float totalSize=0;
    float numberOfPlatforms=0;
    for (int h=1; h<map.size(); h++) {
        for (int w=0; w<map[h].size(); w++) {
            bool platformFound = false;
            while (w<map[h].size() && map[h-1][w] == '-' && (map[h][w] == 'T' || map[h][w] == 'M' || map[h][w] == '#' || map[h][w] == 'D') ) {
                totalSize+=1;
                w++;
                platformFound = true;
            }
            if(platformFound)
            numberOfPlatforms+=1;
        }
    }
    
    float avgPlatformSize = totalSize/numberOfPlatforms;
    return avgPlatformSize;
}

bool KidIcarusLongestGapConstraint(vector<vector< char> > map, float min, float max){
    
    int longestGap=0;
    for (int h=1; h<map.size(); h++) {
        for (int w=0; w<map[h].size(); w++) {
            bool platformFound = false;
            while (w<map[h].size() && map[h-1][w] == '-' && (map[h][w] == 'T' || map[h][w] == 'M' || map[h][w] == '#' || map[h][w] == 'D') ) {
                w++;
                platformFound = true;
            }
            if(platformFound){
                int currGap=0;
                while (w<map[h].size() && map[h][w] == '-') {
                    w++;
                    currGap++;
                }
                if(w<map[h].size()-1 && map[h-1][w] == '-' && longestGap<currGap)
                    longestGap=currGap;
            }
        }
    }
    
    return (longestGap<=max && longestGap>=min);
}

float KidIcarusLongestGapCount(vector<vector< char> > map){
    
    int longestGap=0;
    for (int h=1; h<map.size(); h++) {
        for (int w=0; w<map[h].size(); w++) {
            bool platformFound = false;
            while (w<map[h].size() && map[h-1][w] == '-' && (map[h][w] == 'T' || map[h][w] == 'M' || map[h][w] == '#' || map[h][w] == 'D') ) {
                w++;
                platformFound = true;
            }
            if(platformFound){
                int currGap=0;
                while (w<map[h].size() && map[h][w] == '-') {
                    w++;
                    currGap++;
                }
                if(w<map[h].size()-1 && map[h-1][w] == '-' && longestGap<currGap)
                longestGap=currGap;
            }
        }
    }
    
    return longestGap;
}


//------------------------//
//                        //
//          End           //
// Kid Icarus Constraints //
//                        //
//------------------------//


//------------------------//
//                        //
//          Start         //
//   Violation Detection  //
//                        //
//------------------------//

//performs a binary search on the map, applying the given constraint function on each subdivided map section
//and returns the bottom left (x, y) position and the top right (x, y) position as [x, y, x, y]
//returns [-1] if constrain satisfied throughout
vector<int> ConstraintChecker::BinarySearchViolationDetection(vector<vector< char> > map, bool verticalSlices, int minMapSectionSize, float min, float max, bool (*f)(vector<vector< char> >, float, float)){
    
    //if constraint is satisfied in the whole map, return special vector
    if(f(map, min, max)){
        vector<int> passed{-1};
        return passed;
    }

    int sectionSize;
    if(verticalSlices){
        vector<vector< char> > left;
        vector<vector< char> > right;
        for (int h=0; h<map.size(); h++) {
            vector<char> tmpL;
            vector<char> tmpR;
            for (int w=0; w<map[h].size(); w++) {
                if(w<map[h].size()/2)
                    tmpL.push_back(map[h][w]);
                else
                    tmpR.push_back(map[h][w]);
            }
            left.push_back(tmpL);
            right.push_back(tmpR);
            tmpL.clear();
            tmpR.clear();
        }
        vector<int> leftSat = BinarySearchViolationDetection(left, verticalSlices, minMapSectionSize, min, max, f);
        vector<int> rightSat= BinarySearchViolationDetection(right, verticalSlices, minMapSectionSize, min, max, f);
        if(leftSat[0] == -1 && rightSat[0] == -1){
            vector<int> ret{};
            
        }
        
        
    }
    else{
        sectionSize = (int)map.size()/2;
    }
    vector<int> passed{-1};
    return passed;
}

vector<int> ConstraintChecker::SectionBySectionViolationDetection(vector<vector< char> > map, bool verticalSlices, int sectionSize, float min, float max, bool (*f)(vector<vector< char> >, float, float)){
    
    vector<int> sectionsFailed;
    if(verticalSlices){
        for (int i=0; i<map[0].size(); i+=sectionSize) {
            vector<vector<char> > mapSection;
            for(int h=0; h<map.size(); h++){
                
                vector<char> row;
                for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                    row.push_back(map[h][w]);
                
                mapSection.push_back(row);
                row.clear();
            }
            //if the section fails the constraint check, add that sections coordinates to the return vector
            if(!f(mapSection, min, max)){
                sectionsFailed.push_back(i);
                sectionsFailed.push_back(0);
                if(i+sectionSize < map[0].size())
                    sectionsFailed.push_back(i+sectionSize);
                else
                    sectionsFailed.push_back((int)map[0].size()-1);
                sectionsFailed.push_back((int)map.size()-1);
            }
        }
    } else{
        for (int i=(int)map.size()-1; i>=0; i-=sectionSize) {
            vector<vector<char> > mapSection;
            for(int h=i-sectionSize+1; h<map.size() && h<=i; h++){
                
                vector<char> row;
                for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                
                mapSection.push_back(row);
                row.clear();
            }
            //if the section fails the constraint check, add that sections coordinates to the return vector
            if(!f(mapSection, min, max)){
                sectionsFailed.push_back(0);
                sectionsFailed.push_back(i);
                sectionsFailed.push_back((int)map[0].size()-1);
                if(i-sectionSize+1 >=0)
                    sectionsFailed.push_back(i-sectionSize+1);
                else
                    sectionsFailed.push_back(0);
            }
        }
    }
    return sectionsFailed;
}

vector<int> ConstraintChecker::SlidingWindowViolationDetectionCounts(vector<vector< char> > map, bool verticalSlices, int sectionSize, float (*f)(vector<vector< char> >), bool high){
    
    vector< int> sectionsFailed;
    if(high){
        if(verticalSlices){
            int max=0;
            //first find the max value
            for (int i=0; i<=map[0].size()-sectionSize; i++) {
                vector<vector<char> > mapSection;
                for(int h=0; h<map.size(); h++){
                    
                    vector<char> row;
                    for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                        row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                        row.clear();
                }
                
                int tmp = f(mapSection);
                if(tmp > max)
                    max = tmp;
            }
            //then find the sections with that value and store them
            for (int i=0; i<=map[0].size()-sectionSize; i++) {
                vector<vector<char> > mapSection;
                for(int h=0; h<map.size(); h++){
                    
                    vector<char> row;
                    for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                
                
                int tmp = f(mapSection);
                if(tmp == max){
                    sectionsFailed.clear();
                    sectionsFailed.push_back(i);
                    sectionsFailed.push_back(0);
                    if((i-1+sectionSize) < map[0].size())
                        sectionsFailed.push_back(i-1+sectionSize);
                    else
                        sectionsFailed.push_back((int)map[0].size()-1);
                    sectionsFailed.push_back((int)map.size()-1);
                }
            }

        } else{
            int max=0;
            for (int i=(int)map.size()-1; i>=sectionSize; i--) {
                vector<vector<char> > mapSection;
                for(int h=i-sectionSize+1; h<map.size() && h<=i; h++){
                    
                    vector<char> row;
                    for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                
                //if a violation has not been detected and
                //and the previous section passed, add that sections coordinates to the return vector
                int tmp = f(mapSection);
                if(tmp > max)
                    max = tmp;
            }
            
            for (int i=(int)map.size()-1; i>=sectionSize; i--) {
                vector<vector<char> > mapSection;
                for(int h=i-sectionSize+1; h<map.size() && h<=i; h++){
                    
                    vector<char> row;
                    for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                
                int tmp = f(mapSection);
                if(tmp == max){
                    sectionsFailed.clear();
                    sectionsFailed.push_back(0);
                    sectionsFailed.push_back(i);
                    sectionsFailed.push_back((int)map[0].size()-1);
                    if((i+1-sectionSize)>=0)
                        sectionsFailed.push_back(i+1-sectionSize);
                    else
                        sectionsFailed.push_back(0);
                }
            }
        }
    } else{
        if(verticalSlices){
            int min=99999;
            for (int i=0; i<=map[0].size()-sectionSize; i++) {
                vector<vector<char> > mapSection;
                for(int h=0; h<map.size(); h++){
                
                    vector<char> row;
                    for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                        row.push_back(map[h][w]);
                
                    mapSection.push_back(row);
                    row.clear();
                }
                int tmp = f(mapSection);
                if(tmp < min)
                    min = tmp;
                    
            }
            
            for (int i=0; i<=map[0].size()-sectionSize; i++) {
                vector<vector<char> > mapSection;
                for(int h=0; h<map.size(); h++){
                    
                    vector<char> row;
                    for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                int tmp = f(mapSection);
                if(tmp == min){
                    sectionsFailed.clear();
                    sectionsFailed.push_back(i);
                    sectionsFailed.push_back(0);
                    if((i-1+sectionSize) < map[0].size())
                        sectionsFailed.push_back(i-1+sectionSize);
                    else
                        sectionsFailed.push_back((int)map[0].size()-1);
                    sectionsFailed.push_back((int)map.size()-1);
                }
            }
        } else{
            int min=99999;
            for (int i=(int)map.size()-1; i>=sectionSize; i--) {
                vector<vector<char> > mapSection;
                for(int h=i-sectionSize+1; h<map.size() && h<=i; h++){
                    
                    vector<char> row;
                    for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                
                int tmp = f(mapSection);
                if(tmp < min)
                    min = tmp;
                    
            }
        
            for (int i=(int)map.size()-1; i>=sectionSize; i--) {
                vector<vector<char> > mapSection;
                for(int h=i-sectionSize+1; h<map.size() && h<=i; h++){
                    
                    vector<char> row;
                    for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                    
                    mapSection.push_back(row);
                    row.clear();
                }
                
                int tmp = f(mapSection);
                if(tmp == min){
                    sectionsFailed.clear();
                    sectionsFailed.push_back(0);
                    sectionsFailed.push_back(i);
                    sectionsFailed.push_back((int)map[0].size()-1);
                    if((i+1-sectionSize)>=0)
                        sectionsFailed.push_back(i+1-sectionSize);
                    else
                        sectionsFailed.push_back(0);
                }
            }
        
        }
    }
    return sectionsFailed;
}

vector<int> ConstraintChecker::SlidingWindowViolationDetection(vector<vector< char> > map, bool verticalSlices, int sectionSize){
    
    vector<int> sectionsFailed;
    if(verticalSlices){
        bool violation =false;
        for (int i=0; i<=map[0].size()-sectionSize; i++) {
            vector<vector<char> > mapSection;
            for(int h=0; h<map.size(); h++){
                
                vector<char> row;
                for (int w=i; w<i+sectionSize && w<map[0].size(); w++)
                    row.push_back(map[h][w]);
                
                mapSection.push_back(row);
                    row.clear();
            }
            //cout<<i<<endl;
            //if a violation has not been detected and
            //if the section fails the constraint check, add that sections coordinates to the return vector
            if(!violation && (!MarioNoIllFormedPipesConstraint(mapSection, 0, 0) || !MarioPlayabilityConstraint(mapSection, 0, 0)) ){
                //if the next violation starts within the previous violation's section, combine them
                if(sectionsFailed.size() > 0 && i <= sectionsFailed[sectionsFailed.size()-2]){
                    sectionsFailed.pop_back();
                    sectionsFailed.pop_back();
                }
                else{
                    sectionsFailed.push_back(i);
                    sectionsFailed.push_back(0);
                }
                violation=true;
            } else if(violation && (!MarioNoIllFormedPipesConstraint(mapSection, 0, 0) || !MarioPlayabilityConstraint(mapSection, 0, 0)) ){
                //otherwise, if a violation has been detected in the previous section,
                //but not in this section, then mark everything up until now, as needing
                //to be regenerated
                if((i-1+sectionSize)<map[0].size())
                    sectionsFailed.push_back(i-1+sectionSize);
                else
                    sectionsFailed.push_back((int)map[0].size()-1);
                sectionsFailed.push_back((int)map.size()-1);
                violation=false;
            }
        }
        
        if(sectionsFailed.size()%4 != 0){
            sectionsFailed.push_back((int)map[0].size()-1);
            sectionsFailed.push_back((int)map.size()-1);
        }
        
    } else{
        bool violation=false;
        for (int i=(int)map.size()-1; i>=sectionSize; i--) {
            vector<vector<char> > mapSection;
            for(int h=i-sectionSize; h<map.size() && h<=i; h++){
                
                vector<char> row;
                for (int w=0; w<map[i].size(); w++)
                    row.push_back(map[h][w]);
                
                mapSection.push_back(row);
                    row.clear();
            }
            
            //if a violation has not been detected and
            //and the previous section passed, add that sections coordinates to the return vector
            if(!violation && KidIcarusPlayabilitySection(mapSection)>=1 ){
                if(sectionsFailed.size() > 0 && i >= sectionsFailed[sectionsFailed.size()-1]){
                    sectionsFailed.pop_back();
                    sectionsFailed.pop_back();
                } else{
                    sectionsFailed.push_back(0);
                    sectionsFailed.push_back(i);
                }
                violation=true;
            } else if(violation && KidIcarusPlayabilitySection(mapSection)<1){
                //otherwise, if a violation has been detected in the previous section,
                //but not in this section, then mark everything up until now, as needing
                //to be regenerated
                sectionsFailed.push_back((int)map[0].size()-1);
                if((i-sectionSize) >=0)
                    sectionsFailed.push_back(i-sectionSize);
                else
                    sectionsFailed.push_back(0);
                violation=false;
            }

        }
        
        if(sectionsFailed.size()%4 != 0){
            sectionsFailed.push_back((int)map[0].size()-1);
            sectionsFailed.push_back(0);
        }
    }
    return sectionsFailed;
}
